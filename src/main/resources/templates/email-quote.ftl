Nombres y Apellidos: ${NAME} <br/>
Teléfono: ${PHONE} <br/>
Correo: ${EMAIL} <br/><br/>

<b>Datos del Evento:</b><br/><br/>

Hotel: ${HOTEL}<br/><br/>

Cantidad de Asistentes: ${QUANTITY} <br/>
Fecha de Inicio: ${BEGIN_DATE} <br/>
Fecha de Fin: ${END_DATE} <br/>
Hora de Inicio: ${BEGIN_HOUR} <br/>
Hora de Fin: ${END_HOUR} <br/>
Tipo de Armado: ${ARMED_TYPES} <br/>
Opciones de Alimentación: ${FOOD_OPTIONS} <br/>
Equipos Audiovisuales: ${AUDIOVISUAL_EQUIPMENTS} <br/><br/>

Comentarios Adicionales:<br/>

${COMMENTS}

