Hola ${FULL_NAME},<br/>
<br/>
Para recuperar su contraseña haga click en el enlace.<br/>
<br/>
${VERIFICATION_URL}<br/>
<br/>
Si no solicitó recuperar su contraseña, ignore este correo electrónico.<br/>
<br/>
Team Casa Andina,