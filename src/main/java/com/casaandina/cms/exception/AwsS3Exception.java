package com.casaandina.cms.exception;

public class AwsS3Exception extends Exception {

	private static final long serialVersionUID = -4915408437222085008L;
	
	private final String code;
	private final String description;

	public AwsS3Exception(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

}
