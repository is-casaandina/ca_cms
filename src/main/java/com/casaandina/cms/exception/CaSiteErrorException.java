package com.casaandina.cms.exception;

public class CaSiteErrorException extends Exception {

    private static final long serialVersionUID = 3791180234904248148L;
    private final int status;
    private final String language;

    public CaSiteErrorException(String message, String language, int status) {
        super(message);
        this.status = status;
        this.language = language;
    }

    public int getStatus() {
        return status;
    }
    public String getLanguage() { return language;}
}
