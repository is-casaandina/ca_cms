package com.casaandina.cms.exception;

public class CaRequiredException extends Exception {

	private static final long serialVersionUID = -8265717287249763767L;

	public CaRequiredException(final String message) {
		super(message);
	}
}
