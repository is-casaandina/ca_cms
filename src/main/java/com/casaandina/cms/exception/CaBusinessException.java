package com.casaandina.cms.exception;

public class CaBusinessException extends Exception {

	private static final long serialVersionUID = 3791180234904248148L;
	private final int status;

	public CaBusinessException(String message, int status) {
		super(message);
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
