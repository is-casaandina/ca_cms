package com.casaandina.cms.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casaandina.cms.exception.ResourceNotFoundException;
import com.casaandina.cms.model.User;
import com.casaandina.cms.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	private UserRepository userRepository;
	
	@Autowired
	public CustomUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) {
		
		Optional<User> user = userRepository.findByUsername(username);
		
		if(!user.isPresent()) {
			throw new UsernameNotFoundException("el usuario " + username + " no esta registrado en el sistema");
		}
		
		return UserPrincipal.create(user.get());
	}
	
	 @Transactional
    public UserDetails loadUserById(Integer id) {
        User user = userRepository.findById(id).orElseThrow(
            () -> new ResourceNotFoundException("User", "id", id)
        );

        return UserPrincipal.create(user);
    }
	
	

}
