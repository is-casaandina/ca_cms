package com.casaandina.cms.security;

import com.casaandina.cms.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.casaandina.cms.util.ConstantsUtil.EMPTY;
import static com.casaandina.cms.util.ConstantsUtil.SPACE;

public class UserPrincipal implements UserDetails{

	private static final long serialVersionUID = -8021568978492424450L;
	
	private Integer id;

    private String username;
    
    private String fullname;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;
    
    public UserPrincipal(Integer id, 
			    		 String username,
			    		 String fullname,
			    		 String password,
			    		 Collection<? extends GrantedAuthority> authorities) {
    	this.id = id;
    	this.username = username;
    	this.fullname = fullname;
    	this.password = password;
    	this.authorities = authorities;
    }
    
    public static UserPrincipal create(User user) {
    	
    	List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
    	grantedAuthorities.add(new SimpleGrantedAuthority(user.getRoleCode()));
    	
    	return new UserPrincipal(
                user.getId(),
                user.getUsername(),
				(user.getName() != null ? user.getName() + SPACE : EMPTY)  +
						 (user.getLastname() != null ? user.getLastname() + SPACE : EMPTY),
                user.getPassword(),
                grantedAuthorities
        );
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	
	public Integer getId() {
		return id;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}
	
	public String getFullname() {
		return fullname;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
