package com.casaandina.cms.component;

import com.casaandina.cms.dto.GeneralParameters;
import com.casaandina.cms.dto.RevinateOpinionIndicator;
import com.casaandina.cms.dto.RevinateRatingIndicator;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.repository.*;
import com.casaandina.cms.serviceclient.RevinateServiceClient;
import com.casaandina.cms.serviceclient.RoibackServiceClient;
import com.casaandina.cms.serviceclient.response.Content;
import com.casaandina.cms.serviceclient.response.RevinateOpinionsResponse;
import com.casaandina.cms.serviceclient.response.RevinateRatingsResponse;
import com.casaandina.cms.serviceclient.response.ValuesByReviewSite;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.LanguageType;
import com.casaandina.cms.util.ObjectMapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.casaandina.cms.util.CaUtil.validateIsNotNullAndNotEmpty;

@Component
public class ScheduledTasks {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTasks.class);
	
	private final RevinateServiceClient revinateServiceClient;
	
	private final ReviewSiteRepository reviewSiteRepository;
	
	private final HotelRepository hotelRepository;
	
	private final OpinionRepository opinionRepository;
	
	private final RatingRepository ratingRepository;
	
	private final ParameterRepository parameterRepository;

	private final RoibackServiceClient roibackServiceClient;
	
	@Autowired
	public ScheduledTasks(RevinateServiceClient revinateServiceClient, ReviewSiteRepository reviewSiteRepository,
						  OpinionRepository opinionRepository, HotelRepository hotelRepository, ParameterRepository parameterRepository,
						  RatingRepository ratingRepository, RoibackServiceClient roibackServiceClient) {
		this.revinateServiceClient = revinateServiceClient;
		this.reviewSiteRepository = reviewSiteRepository;
		this.opinionRepository = opinionRepository;
		this.ratingRepository = ratingRepository;
		this.hotelRepository = hotelRepository;
		this.parameterRepository = parameterRepository;
		this.roibackServiceClient = roibackServiceClient;
	}
	
	@Scheduled(cron = "0 0 1 * * *")
	@Transactional
	public void registerOpinionsByHotel() throws CaRequiredException {
		
		LOGGER.info(ConstantsUtil.REGISTERING_OPINIONS_BY_HOTEL);
		GeneralParameters generalParameters = this.parameterRepository.customFindByCodeAndLanguageCode(
				ConstantsUtil.REVINATE_OPINIONS, LanguageType.SPANISH.getCode());
		
		if (Objects.nonNull(generalParameters) && !generalParameters.getIndicator().isEmpty()) {
			RevinateOpinionIndicator revinateOpinionIndicator = ObjectMapperUtils.map(generalParameters.getIndicator().get(0), RevinateOpinionIndicator.class);
			for (Hotel hotel : this.hotelRepository.findAll()) {
				StringBuilder url = new StringBuilder(revinateOpinionIndicator.getUrl());
				url.append(hotel.getCode()).append(buildOpinionsEndpointByHotel(revinateOpinionIndicator));
				saveOpinionsByHotel(hotel.getCode(), url.toString());
			}
		}
	}
	
	@Scheduled(cron = "0 10 1 * * *")
	@Transactional
	public void registerRatingsByHotel() throws CaRequiredException {
		
		LOGGER.info(ConstantsUtil.REGISTERING_RATINGS_BY_HOTEL);
		GeneralParameters generalParameters = this.parameterRepository.customFindByCodeAndLanguageCode(
				ConstantsUtil.REVINATE_RATINGS, LanguageType.SPANISH.getCode());
		
		if (Objects.nonNull(generalParameters) && !generalParameters.getIndicator().isEmpty()) {
			RevinateRatingIndicator revinateRatingsParameters = ObjectMapperUtils.map(generalParameters.getIndicator().get(0), RevinateRatingIndicator.class);
			for (Hotel hotel : this.hotelRepository.findAll()) {
				saveRatingsByHotel(hotel.getCode(), hotel.getCity(), revinateRatingsParameters);
			}
		}
		
	}
	
	private void saveOpinionsByHotel(final String hotelCode, final String url) throws CaRequiredException {
		
		LOGGER.info(hotelCode);
		
		List<Opinion> oldOpiniones = this.opinionRepository.findAllByHotelCode(hotelCode);
		List<Opinion> newOpinions = new ArrayList<>();
		
		RevinateOpinionsResponse revinateOpinionsResponse = this.revinateServiceClient.consultOpinionsRevinate(url);
		
		Map<String, String> map = new HashMap<>();
		this.reviewSiteRepository.findAll().stream().forEach(x -> map.put(x.getSlug(), x.getId()));
		
		int count = 0;
		if(Objects.nonNull(revinateOpinionsResponse) &&  Objects.nonNull(revinateOpinionsResponse.getContent())
			&& !revinateOpinionsResponse.getContent().isEmpty() ){
			for (Content content : revinateOpinionsResponse.getContent()) {

				if (Objects.nonNull(content.getBody()) && content.getBody().length() > 99 && content.getRating() >= 4.0  && map.containsKey(content.getReviewSite().getSlug())) {
					newOpinions.add(Opinion.builder()
							.title(content.getTitle())
							.body(content.getBody())
							.author(content.getAuthor())
							.dateReview(content.getDateReview())
							.rating(content.getRating())
							.hotelCode(hotelCode)
							.reviewSite(ReviewSite.builder().id(map.get(content.getReviewSite().getSlug())).build())
							.build());

					if (++count > 19) break;
				}
			}

			this.opinionRepository.saveAll(newOpinions);
			if (!oldOpiniones.isEmpty()) {
				this.opinionRepository.deleteAll(oldOpiniones);
			}

			LOGGER.info(ConstantsUtil.SUCCESSFUL_REGISTER);
		}
	}
	
	private void saveRatingsByHotel(final String hotelCode, final String destination,
			final RevinateRatingIndicator revinateRatingsParameter) throws CaRequiredException {
		
		LOGGER.info(hotelCode);
		StringBuilder url = new StringBuilder(revinateRatingsParameter.getUrl());
		url.append(hotelCode).append(revinateRatingsParameter.getEndpoint());
		
		List<Rating> oldRatings = this.ratingRepository.findAllByHotelCode(hotelCode);
		List<Rating> newRatings = new ArrayList<>();
		
		RevinateRatingsResponse revinateRatingsResponse = this.revinateServiceClient.consultRatingsRevinate(url.toString());
		
		String rankingMessage = getRankingMessage(revinateRatingsResponse.getAggregateValues().getTripadvisorMarketRanking(),
				destination, revinateRatingsParameter);
				
		Map<Integer, String> map = new HashMap<>();
		this.reviewSiteRepository.findAll().stream().forEach(x -> map.put(x.getReviewSiteId(), x.getName()));
		
		for (ValuesByReviewSite valuesByReviewSite : revinateRatingsResponse.getValuesByReviewSite()) {
			
			if (map.containsKey(valuesByReviewSite.getReviewSiteId())) {
				newRatings.add(Rating.builder()
						.reviewSiteName(map.get(valuesByReviewSite.getReviewSiteId()))
						.avarageRating(valuesByReviewSite.getValues().getAverageRating())
						.hotelCode(hotelCode)
						.rankingMessage(rankingMessage)
						.build());
			}
		}
		
		this.ratingRepository.saveAll(newRatings);
		if(!oldRatings.isEmpty()) {
			this.ratingRepository.deleteAll(oldRatings);
		}
		
		LOGGER.info(ConstantsUtil.SUCCESSFUL_REGISTER);
	}
	
	private String getRankingMessage(final Integer ranking, final String destination, final RevinateRatingIndicator revinateRatingsParameter) {
		StringBuilder defaultMessage = new StringBuilder(revinateRatingsParameter.getDefaultMessage()).append(destination);
		StringBuilder customMessage = new StringBuilder(ConstantsUtil.POSITION).append(ranking)
				.append(revinateRatingsParameter.getCustomMessage()).append(destination);
		
		return ranking > revinateRatingsParameter.getRanking() ? defaultMessage.toString() : customMessage.toString();
	}
	
	private String buildOpinionsEndpointByHotel(final RevinateOpinionIndicator revinateOpinionParameter) {
		StringBuilder endpoint = new StringBuilder(revinateOpinionParameter.getEndpoint());
		endpoint.append(ConstantsUtil.REVINATE_LANGUAGE_PARAMETER).append(revinateOpinionParameter.getLanguageCode())
		.append(ConstantsUtil.REVINATE_SIZE_PARAMETER).append(revinateOpinionParameter.getSize())
		.append(ConstantsUtil.REVINATE_RATING_PARAMETER).append(revinateOpinionParameter.getMinRating())
			.append(ConstantsUtil.COMMA).append(revinateOpinionParameter.getMaxRating())
		.append(ConstantsUtil.REVINATE_SORT_PARAMETER).append(revinateOpinionParameter.getSort())
			.append(ConstantsUtil.COMMA).append(revinateOpinionParameter.getOrder());
		
		return endpoint.toString();
	}

	@Scheduled(cron = "0 30 1 * * *")
	@Transactional
	public void registerRoibackLowPriceByHotels()  {
		List<PageHeader> pageHeaders = this.roibackServiceClient.getPageHeaderRoiback();
		if(Objects.nonNull(pageHeaders) && !pageHeaders.isEmpty()){

			LOGGER.info(ConstantsUtil.RoibackWebService.REGISTERING_ROIBACK_PRICES_BY_HOTEL);

			List<LanguageType> languages = LanguageType.getList();

			LOGGER.info("Logging Revinate\n");

			languages.forEach( languageType ->
				pageHeaders.forEach(x -> {
					try{
						if(validateIsNotNullAndNotEmpty(x.getRoiback())){
							LOGGER.info(String.format("Roiback: %s \tIdioma: %s", x.getRoiback(), languageType.getCode()));
							this.roibackServiceClient.getRoibackElement(x.getRoiback(), languageType.getCode());
						}
					} catch(CaRequiredException ca){
						LOGGER.error(ca.getMessage());
					}
				})
			);
		}
	}

}
