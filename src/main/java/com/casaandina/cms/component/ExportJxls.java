package com.casaandina.cms.component;

import lombok.extern.log4j.Log4j2;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component
@Log4j2
public class ExportJxls {

    public ResponseEntity buildExcelJxls(Map<String, Object> objs, String nameReport){
        try{
            Map beans = new HashMap();
            Iterator it = objs.entrySet().iterator();
            Map.Entry ent = null;
            while (it.hasNext()) {
                ent = (Map.Entry) it.next();
                beans.put(ent.getKey(), ent.getValue());
            }
            ByteArrayOutputStream binarios = new ByteArrayOutputStream();

            InputStream in = this.getClass().getResourceAsStream("/jxls/" + nameReport + ".xls");

            XLSTransformer transformer = new XLSTransformer();
            Workbook workbook = transformer.transformXLS(in, beans);

            HSSFFormulaEvaluator.evaluateAllFormulaCells((HSSFWorkbook) workbook);
            workbook.write(binarios);
            byte[] data = binarios.toByteArray();

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-disposition", "attachment; filename=" + nameReport + ".xls");

            return ResponseEntity.
                    ok().headers(headers).
                    contentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")).body(data);
        } catch(Exception ex){
            log.error("ocurrio un error al generar excel" + ex.getMessage());
            return null;
        }
    }

    public byte[] getBytesExcelJxls(Map<String, Object> objs, String nameReport){
        try{
            Map beans = new HashMap();
            Iterator it = objs.entrySet().iterator();
            Map.Entry ent = null;
            while (it.hasNext()) {
                ent = (Map.Entry) it.next();
                beans.put(ent.getKey(), ent.getValue());
            }
            ByteArrayOutputStream binarios = new ByteArrayOutputStream();

            InputStream in = this.getClass().getResourceAsStream("/jxls/" + nameReport + ".xls");

            XLSTransformer transformer = new XLSTransformer();
            Workbook workbook = transformer.transformXLS(in, beans);

            HSSFFormulaEvaluator.evaluateAllFormulaCells((HSSFWorkbook) workbook);
            workbook.write(binarios);
            return binarios.toByteArray();
        } catch(Exception ex){
            log.error("ocurrio un error al generar excel" + ex.getMessage());
            return null;
        }
    }
}
