package com.casaandina.cms.component;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.util.ConstantsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.cbor.MappingJackson2CborHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

@Component
public class RestTemplateCustom {
	
	private final RestTemplate restTemplate;
	
	@Autowired
	public RestTemplateCustom(RestTemplate restTemplate) {
		restTemplate.getMessageConverters().add(new MappingJackson2CborHttpMessageConverter());
		this.restTemplate = restTemplate;
	}
	
	public <T> T getResponse(final String url, HttpMethod httpMethod, HttpEntity<?> httpEntity,
			Class<T> classResponse) throws CaRequiredException {
		
		ResponseEntity<T> responseEntity = this.restTemplate.exchange(url, httpMethod, httpEntity, classResponse);
		if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
			return responseEntity.getBody();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_SERVICE);
		}
		
	}

	public <T> T exchangeResponseUtf8(final String url, HttpMethod httpMethod, HttpEntity<?> httpEntity,
							 Class<T> classResponse) throws CaRequiredException {

		this.restTemplate.getMessageConverters()
				.add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		ResponseEntity<T> responseEntity = this.restTemplate.exchange(url, httpMethod, httpEntity, classResponse);
		if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
			return responseEntity.getBody();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_SERVICE);
		}

	}

}
