package com.casaandina.cms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;

@Configuration
@PropertySource("classpath:mail.properties")
public class MailConfig {

	@Bean
    public SimpleMailMessage templateSimpleMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setText("Esta es la plantilla de email de prueba para su email:\n%s\n");
        return message;
    }
}
