package com.casaandina.cms.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.casaandina.cms.dto.InputStreamParameter;
import com.casaandina.cms.exception.AwsS3Exception;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.logging.Logger;

@Service
public class AmazonClient {

	private AmazonS3 s3client;

	@Value("${aws.endpointUrl}")
	private String endpointUrl;

	@Value("${aws.bucketName}")
	private String bucketName;

	@Value("${aws.accessKey}")
	private String accessKey;

	@Value("${aws.secretKey}")
	private String secretKey;

	private static Logger logger = Logger.getLogger(AmazonClient.class.getName());
	
	private final MessageProperties messageProperties;
	
	@Autowired
	public AmazonClient(MessageProperties messageProperties) {
		this.messageProperties = messageProperties;
	}

	@PostConstruct
	private void init() {
		BasicAWSCredentials creds = new BasicAWSCredentials(this.accessKey, this.secretKey);
		this.s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).withCredentials(new AWSStaticCredentialsProvider(creds))
				.build();
	}

	public File convertMultiPartToFile(MultipartFile file) {
		File converterFile = new File(file.getOriginalFilename());
		try (FileOutputStream fos = new FileOutputStream(converterFile);) {
			fos.write(file.getBytes());
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
		return converterFile;
	}

	public String generateFileName(MultipartFile multiPart) {
		String name = multiPart.getOriginalFilename();
		name = CaUtil.replaceEspecialCharacters(name);
		return name.replace(" ", "_");
	}

	public void validateFile(String directory, String fileName) throws AwsS3Exception {
		if (fileExist(directory, fileName))
			throw new AwsS3Exception("001010",messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_FILE_EXIST));
	}

	public void uploadFileToS3Bucket(String directory, String fileName, File file) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, directory + ConstantsUtil.SEPARATOR + fileName, file)
				.withCannedAcl(CannedAccessControlList.PublicRead);

		s3client.putObject(putObjectRequest);
	}

	public void uploadFileToS3BucketWithType(String directory, String fileName, File file, String type) {
		try{

			ObjectMetadata objectMetadata = new ObjectMetadata();
			objectMetadata.setContentLength(file.length());
			objectMetadata.setContentType(type);
			objectMetadata.setCacheControl("public, max-age=31536000");

			PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, directory + ConstantsUtil.SEPARATOR + fileName, new FileInputStream(file), objectMetadata)
					.withCannedAcl(CannedAccessControlList.PublicRead);

			s3client.putObject(putObjectRequest);
		} catch(Exception e) {
			logger.info("An occurred error: " + e.getMessage());
		}
	}

	public void uploadInputStream( String directory, InputStreamParameter inputStreamParameter) {
		ObjectMetadata objectMetadata = new ObjectMetadata();
		objectMetadata.setContentLength(inputStreamParameter.getLength());
		objectMetadata.setContentType(inputStreamParameter.getContentType());
		objectMetadata.setCacheControl("public, max-age=31536000");

		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, directory + ConstantsUtil.SEPARATOR + inputStreamParameter.getFileName(), inputStreamParameter.getFis(), objectMetadata)
				.withCannedAcl(CannedAccessControlList.PublicRead);

		s3client.putObject(putObjectRequest);
	}


	public String deleteFileFromS3Bucket(String directory, String filename) {
		s3client.deleteObject(new DeleteObjectRequest(bucketName, directory + ConstantsUtil.SEPARATOR + filename));
		return "deleted";
	}

	public Boolean fileExist(String directory, String filename) {

		BasicAWSCredentials creds = new BasicAWSCredentials(this.accessKey, this.secretKey);

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds))
				.build();
		S3Object s3Object = null;
		try {
			s3Object = s3Client.getObject(bucketName, directory + ConstantsUtil.SEPARATOR + filename);
		} catch (AmazonS3Exception ex) {
			if (ex.getStatusCode() == 404)
				return Boolean.FALSE;
		}
		return s3Object != null ? Boolean.TRUE : Boolean.FALSE;
	}
	
	public InputStream getFile(String directory, String filename){
        BasicAWSCredentials creds = new BasicAWSCredentials(this.accessKey, this.secretKey);
        AmazonS3Client s3Client = (AmazonS3Client) AmazonS3ClientBuilder.standard()
        		.withCredentials(new AWSStaticCredentialsProvider(creds))
        		.withRegion(Regions.US_EAST_1)
        		.build();
        S3Object s3Object =  s3Client.getObject(bucketName, directory + ConstantsUtil.SEPARATOR + filename);
        return s3Object.getObjectContent();
    }
	
	public String getFileUrl(final String filePath) {
		BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
		AmazonS3Client amazonS3Client = (AmazonS3Client) AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
				.withRegion(Regions.EU_WEST_1)
				.build();
		
		return amazonS3Client.getResourceUrl(bucketName, filePath);
	}
}
