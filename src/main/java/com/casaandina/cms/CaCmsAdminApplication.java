package com.casaandina.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.annotation.PostConstruct;
import java.util.Locale;
import java.util.TimeZone;
import org.springframework.core.SpringVersion;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

@SpringBootApplication
@EnableMongoAuditing
@EnableScheduling
@EnableCaching
public class CaCmsAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaCmsAdminApplication.class, args);
                
                System.out.println("version: " + SpringVersion.getVersion());
                 System.out.println("Hola Wlson");

	}
	
	@Bean
	public LocaleResolver localeResolver() {
		AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
		localeResolver.setDefaultLocale(Locale.US);
		return localeResolver;
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@PostConstruct
	public void setDefaultTimeZone(){
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
	}
	
}
