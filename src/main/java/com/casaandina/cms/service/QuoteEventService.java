package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.QuoteEvent;

public interface QuoteEventService {
	
	/**
	 * Saves a QuoteEvent and a subscriber
	 * entity instance completely.
	 *
	 * @param entity must not be {@literal null}.
	 * @return the saved entity will never be {@literal null}.
	 */
	QuoteEvent saveQuoteEvent(QuoteEvent quoteEvent) throws CaBusinessException;

	void sendMail(QuoteEvent quoteEvent);

}
