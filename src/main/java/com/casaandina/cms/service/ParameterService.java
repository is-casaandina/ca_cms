package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Parameter;

import java.util.List;

public interface ParameterService {
	
	Parameter createParameter(Parameter parameter) throws CaRequiredException;
	
	Parameter getParameter(String parameterId) throws CaRequiredException;
	
	Parameter updateParameter(Parameter parameter) throws CaRequiredException;
	
	void deleteParameter(String parameterId) throws CaRequiredException;
	
	List<Parameter> getAllTheParameters() throws CaRequiredException;

//	Parameter getStatusPages() throws CaRequiredException;
	
	Parameter getStatesAll(String code) throws CaRequiredException;

	Parameter getStateByCodeAndLanguage(String code, String languageCode) throws CaRequiredException;

}
