package com.casaandina.cms.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Role;
import com.casaandina.cms.util.pagination.Page;

public interface RoleService {
	Optional<Role> findById(String roleId);
	Page<Role> findAllRolesPaginated(Pageable pageable) throws CaBusinessException;
	List<Role> findAll() throws CaBusinessException;
	List<Role> findRolesPermissionDetail() throws CaBusinessException;
	Page<Role> findRolesPermissionDetailPaginated(Pageable pageable) throws CaBusinessException;
	void saveRole(Role role) throws CaRequiredException, CaBusinessException;
	void updateRole(Role role) throws CaRequiredException, CaBusinessException;
	void deleteRole(String roleCode) throws CaRequiredException, CaBusinessException;
	
}
