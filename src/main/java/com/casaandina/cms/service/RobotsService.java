package com.casaandina.cms.service;

import com.casaandina.cms.model.Robots;

import java.util.List;

public interface RobotsService {
    List<Robots> getAll();
    Robots addValueAgent(Robots robots);
    void deleteValueAgents(String _id);
}
