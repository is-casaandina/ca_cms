package com.casaandina.cms.service;

import com.casaandina.cms.dto.LivingRoomDestination;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.rest.response.RangeTheaterResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface SiteService {

    RoibackLowPrice getPriceByRoibackAndCurrency(String roiback, String currencyIso, String language) throws CaRequiredException;

    RangeTheaterResponse getRangeTheater() throws CaRequiredException;

    Page<LivingRoom> getAllTheLivingRooms(final List<String> destinationsId,
                                          final List<String> hotelsId, final List<Integer> capacityRange, Pageable pageable) throws CaRequiredException;

    Parameter getStateByCodeAndLanguage(String code, String languageCode) throws CaRequiredException;

    Currency findByCodeIsoAndStatus(String currencyIso);

    List<LivingRoom> customFindLivingRoomByDestinationIdAndHotelId(String destinationId, String hotelId, Sort sort);

    List<LivingRoom> customFindLivingRoomByDestinationIdAndHotelIdAndCategory(String destinationId, String hotelId, List<String> categories);

    List<Descriptor> customFindDescriptorByIds(List<String> ids);

    List<Assistance> getAssistanceByIdsAndTypeAndLanguage(List<String> ids, String language);

    List<Country> customFindAllByStatus();

    Configurations getConfigurations();

    String getUrlRoibackHotel(String language) throws CaRequiredException;

    String getRangeDateRoiback() throws CaRequiredException;

    List<DestinationMap> getDestinationMapByIds(List<String> items);

    List<PromotionType> getPromotionTypeByLanguage(String language);

    Configurations getLanguagesForSite();

    List<Currency> getCurrenciesForSite();

    FormTemplate getFormTemplateByCodeAndLanguage(String code, String language);

    Domain getDomainByPath(String path);

    Domain customFindByPath(String path);

    Cluster getClusterByClusterId(String clusterId);

    Country getCountryById(String countryId);

    String getUrlRoibackHotel(String language, String roiback) throws CaRequiredException;

    List<LivingRoomDestination> customFindAllDestinationWithLivingRoom();
    List<PageHeader> findPageHeaderHotelsByDestinationsIds(List<String> destinationIds);

    List<PageHeader> getDestinationByPromotions(List<String> items);
    List<PageHeader> getHotelByDestinationsForPromotions(List<String> items);

}
