package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Component;

import java.util.List;

public interface ComponentService {
	
	List<Component> findComponentShortByStatus() throws CaBusinessException;
	
	Component findByIdAndStatus(String id) throws CaRequiredException;
	
	Component findByCodeAndStatus(String code) throws CaRequiredException;

	List<Component> findComponentByTypeAndStatus(Integer type);

}
