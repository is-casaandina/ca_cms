package com.casaandina.cms.service;

import java.util.List;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Currency;

public interface CurrencyService {
	
	Currency saveCurrency(Currency currency) throws CaBusinessException, CaRequiredException;
	
	List<Currency> findByStatus();
	
	List<Currency> updateListCurrency(List<Currency> listCurrency) throws CaBusinessException;
	
	Double currencyConverter(String codeIsoOrigin, String codeIsoTarget, Double amount) throws CaBusinessException;

}
