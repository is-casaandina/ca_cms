package com.casaandina.cms.service;

import java.util.List;

import com.casaandina.cms.model.Opinion;

public interface OpinionService {
	
	List<Opinion> getAllOpinionsByHotelCode(String hotelCode);

}
