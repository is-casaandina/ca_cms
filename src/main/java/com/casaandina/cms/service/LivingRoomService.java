package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.LivingRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LivingRoomService {
	
	LivingRoom registerLivingRoom(LivingRoom livingRoom) throws CaBusinessException;
	
	LivingRoom getLivingRoom(String livingRoomId) throws CaRequiredException;
	
	LivingRoom updateLivingRoom(LivingRoom livingRoom) throws CaBusinessException;
	
	void deleteLivingRoom(String livingRoomId) throws CaRequiredException;
	
	Page<LivingRoom> getAllTheLivingRooms(List<String> destinationId, List<String> hotelId,
			List<Integer> capacityRange, Pageable pageable) throws CaRequiredException;

	Page<LivingRoom> findAllLivingRoomsPaginated(Pageable pageable, String hotelId);

	Page<LivingRoom> findLivingRoomsByHotelAndCategoryPaginated(Pageable pageable, String hotelId, String category);
}
