package com.casaandina.cms.service;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;

import java.io.IOException;

public interface GeoLocationService {

    CountryResponse getIpLocation(String ip) throws IOException, GeoIp2Exception;
}
