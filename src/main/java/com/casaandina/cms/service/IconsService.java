package com.casaandina.cms.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Icons;

public interface IconsService {
	
	void uploadIconsFilesFromZip(MultipartFile fileZip) throws CaBusinessException;
	
	List<Icons> findAllIcons();

}
