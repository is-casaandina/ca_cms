package com.casaandina.cms.service;

import com.casaandina.cms.dto.SubscriberTray;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Parameter;
import com.casaandina.cms.model.Subscriber;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SubscriberService {
	
	List<Parameter> findMotiveByStatusAndLanguage(String language) throws CaBusinessException;
	
	List<Parameter> findGenderByStatusAndLanguage(String language) throws CaBusinessException;
	
	Subscriber saveSubscriberContact(Subscriber subscriberContactRequest) throws CaBusinessException;
	
	Subscriber subscribePerson(Subscriber subscriber) throws CaBusinessException;
	
	Subscriber subscribeLegalPerson(Subscriber subscriber) throws CaBusinessException;
	
	List<Subscriber> getActiveSubscriptionsList() throws CaBusinessException;

	List<Subscriber> findContactByStatus();

	org.springframework.data.domain.Page<Subscriber> findSubscriberByFilters(SubscriberTray subscriberTray, Pageable pageable);

	ResponseEntity downloadExcelByFilters(SubscriberTray subscriberTray);

	byte[] bytesExcelByFilters(SubscriberTray subscriberTray);

	void deleteSubscriber(String id)throws CaBusinessException;

	void sendMailPerson(Subscriber subscriber, String code);

}
