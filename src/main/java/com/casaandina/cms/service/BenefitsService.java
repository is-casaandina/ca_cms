package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Benefits;

import java.util.Optional;

public interface BenefitsService {
    Optional<Benefits> get();
    Benefits save(Benefits benefits) throws CaBusinessException, CaRequiredException;
}
