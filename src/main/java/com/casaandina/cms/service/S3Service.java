package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.rest.request.PageTrayRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.ImageS3Response;
import com.casaandina.cms.rest.response.RangeDateResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface S3Service {

	@SuppressWarnings("rawtypes")
	GenericResponse uploadFavicon(MultipartFile fileIcon, Integer userId) throws CaBusinessException;

	String getFavicon();

	@SuppressWarnings("rawtypes")
	GenericResponse uploadImagen(MultipartFile fileImage, PropertyImage properties) throws CaBusinessException;

	ImageS3 updateImage(ImageS3 imageS3) throws CaBusinessException;

	List<ImageS3Response> findAllImagesS3();

	RangeDateResponse getRangeDatesImages();

	Page<ImageS3> getImagesS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException;

	@SuppressWarnings("rawtypes")
	GenericResponse validateExistImage(@PathVariable String filename) throws CaBusinessException;

	@SuppressWarnings("rawtypes")
	GenericResponse deleteImages(String imageId) throws CaBusinessException;

	@SuppressWarnings("rawtypes")
	GenericResponse uploadDocument(MultipartFile file, String type) throws IOException, CaRequiredException, CaBusinessException;

	@SuppressWarnings("rawtypes")
	GenericResponse uploadFilejs(MultipartFile file, String type, String name) throws IOException, CaRequiredException, CaBusinessException;

	List<Document> getAllTheDocument();

	List<FileJS> getAllTheFilejs();

	ResponseEntity<InputStreamResource> downloadDocument(String documentId) throws CaBusinessException;

	ResponseEntity<InputStreamResource> downloadFilejs(String filejsId) throws CaBusinessException;

	@SuppressWarnings("rawtypes")
	GenericResponse uploadVideo(MultipartFile fileVideo, PropertyImage properties) throws CaBusinessException;

	RangeDateResponse getRangeDatesVideos();

	Page<VideoS3> getVideosS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException;

	@SuppressWarnings("rawtypes")
	GenericResponse validateExistVideo(@PathVariable String filename) throws CaBusinessException;

	@SuppressWarnings("rawtypes")
	GenericResponse deleteVideo(String videoId) throws CaBusinessException;

	RangeDateResponse getRangeDatesDocuments();

	RangeDateResponse getRangeDatesFilejs();

	Page<Document> getDocumentsS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException;

	Page<FileJS> getFilejsS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException;

	GenericResponse deleteDocument(String documentId) throws CaBusinessException;

	GenericResponse deleteFilejs(String filejsId) throws CaBusinessException;

	IconS3 uploadIcon(MultipartFile fileImage, PropertyImage properties) throws CaBusinessException;

	RangeDateResponse getRangeDatesIcons();

	Page<IconS3> getIconsS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException;

	GenericResponse validateExistIcon(String filename) throws CaBusinessException;

	GenericResponse deleteIcon(String iconId) throws CaBusinessException;

	void resizeAllImages(PageTrayRequest pageTrayRequest);

	void resizeAllImagesExtraSmall(PageTrayRequest pageTrayRequest);

}
