package com.casaandina.cms.service;

import java.util.List;

import com.casaandina.cms.dto.OptionTo;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Option;
import com.casaandina.cms.rest.response.OptionResponse;

public interface OptionService {

	Option findById(Integer id);
	
	Option saveOption(OptionTo optionTo) throws CaRequiredException, CaBusinessException;
	
	Option moveOption(OptionTo optionTo) throws CaRequiredException, CaBusinessException;
	
	List<OptionResponse> getOptions() throws CaRequiredException, CaBusinessException;
	
	OptionResponse renameOption(OptionResponse optionResponse) throws CaRequiredException, CaBusinessException;
	
	void deleteOption(Integer id, String username) throws CaBusinessException;
	
	Option duplicateOption(OptionResponse optionResponse) throws CaBusinessException;
	
}
