package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Assistance;
import com.casaandina.cms.rest.request.AssistanceSiteRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AssistanceService {

    Assistance createAssistance(Assistance assistance) throws CaBusinessException;

    Assistance updateAssistance(String assistanceId, Assistance assistance) throws CaBusinessException;

    void deleteAssistance(String id) throws CaBusinessException;

    Page<Assistance> getPageAssistanceByType(Integer type, String title, Pageable pageable);

    List<Assistance> getListAssistanceByType(Integer type);

    List<Assistance> getListAssistanceByIdsAndTypeAndLanguage(AssistanceSiteRequest assistanceSiteRequest);
}
