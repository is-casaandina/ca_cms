package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.PageError;
import com.casaandina.cms.model.PageHeader;

import java.util.List;
import java.util.Optional;

public interface PageErrorService {

    Optional<PageError> getPageErrorByCodeAndState(Integer errorCode);

    Optional<Page> customFindPagePathByIdAndLanguage(String id, String language);

    List<PageError> findAllByState();

    PageError insertPageError(PageError pageError) throws CaBusinessException;

    PageError updatePageError(PageError pageError) throws CaBusinessException;

    void deletePageError(String id) throws CaBusinessException;

    List<PageHeader> findAllPageError();
}
