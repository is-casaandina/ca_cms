package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.LanguagePageNotification;
import com.casaandina.cms.model.PageNotification;
import com.casaandina.cms.util.pagination.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PageNotificationService {
    void insertPageNotification(PageNotification pageNotification) throws CaBusinessException, CaRequiredException;
    void updatePageNotification(Integer pageNotificationId, PageNotification pageNotification) throws CaBusinessException, CaRequiredException;
    void deletePageNotification(Integer id) throws CaBusinessException, CaRequiredException;
    PageNotification getByPageNotificationId(Integer id);
    List<LanguagePageNotification> listByPageId(String languageCode, String pageId);
    Page<PageNotification> listPaginated(Boolean status, Pageable pageable);
}
