package com.casaandina.cms.service;

import java.util.List;

import com.casaandina.cms.model.Permission;

public interface PermissionService {
	
	List<Permission> findAllActive();

}
