package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.TemplateType;

import java.util.List;

public interface TemplateTypeService {

    TemplateType saveTemplateType(TemplateType templateType) throws CaBusinessException;

    List<TemplateType> getTemplateTypeAll();

    void deleteTemplateType(String templateTypeId) throws CaRequiredException;
}
