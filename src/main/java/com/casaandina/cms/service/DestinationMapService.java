package com.casaandina.cms.service;

import com.casaandina.cms.model.DestinationMap;

import java.util.List;

public interface DestinationMapService {

    List<DestinationMap> findByCountryIdAndState(String countryId);
}
