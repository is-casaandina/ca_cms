package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.User;

public interface EmailService {

	Boolean sendVerificationTokenMail(User user, String token) throws CaBusinessException ;

	Boolean sendMail(String toEmail, String subject, String body) throws CaBusinessException;
}
