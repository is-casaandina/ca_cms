package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Template;

import java.util.List;

public interface TemplateService {

	List<Template> listViewTemplates(List<String> categories);
	
	Template getTemplateForEdit(String code) throws CaBusinessException;
	
	Template saveTemplate(Template template) throws CaRequiredException;

}
