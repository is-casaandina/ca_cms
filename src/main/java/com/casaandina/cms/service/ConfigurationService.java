package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Configurations;
import com.casaandina.cms.model.Parameter;

import java.util.List;

public interface ConfigurationService {

	Configurations get();

	Configurations save(Configurations configurations) throws CaBusinessException, CaRequiredException;

	List<Parameter> saveServiceRoiback(List<Parameter> parametersList) throws CaBusinessException, CaRequiredException;
	
	Parameter saveSocialNetworks(Parameter parameter) throws CaBusinessException, CaRequiredException;
	
	List<Parameter> getPriceRoiback() throws CaRequiredException;
	
	Parameter getSocialNetworking() throws CaRequiredException;
	
	List<Parameter> getServiceRoiback();
	
	List<Parameter> savePriceRoiback(List<Parameter> parametersList) throws CaBusinessException, CaRequiredException;

	void siteMapGenerate(Boolean acceptExternalPages);

	void uploadTxtToS3(String robotsTxt);

}
