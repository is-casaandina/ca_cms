package com.casaandina.cms.service;

import com.casaandina.cms.dto.FeaturedPromotion;
import com.casaandina.cms.dto.HostIpCountry;
import com.casaandina.cms.dto.LivingDestinationHotel;
import com.casaandina.cms.dto.PromotionUrlReservation;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.exception.CaSiteErrorException;
import com.casaandina.cms.model.*;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface PageService {
	
	DraftPage savePage(Page page) throws CaBusinessException;
	
	Page publishPage(String id) throws CaBusinessException;
	
	DraftPage getPageForEdit(String id, String username) throws CaBusinessException;
	
	List<PageHeader> getListPageHeaderForTray();
	
	List<Page> findPageByLanguageAndPath(String language, String value, Integer limit);

	List<Page> findDestinationByCountryIdAndLanguageCustomized(String countryId);

	List<Page> findHotelsByDestinationCustomized(String destination);

	List<Page> findHotelsByDescriptorCustomized(String descriptor);

	List<Page> findPromotionsByDescriptorAndCategory(String descriptorData, String categoryData);

	void deletePage(String id) throws CaBusinessException;

	Page getPageForView(String id, Integer state) throws CaRequiredException;

	Page findByIdAndState(String id);

	DraftPage unPublishPage(String id) throws CaBusinessException;

	List<Page> findRestaurantsByDescriptorCustomized(String descriptor);

	List<Page> findHotelsByLanguageAndName(String language, String value, Integer limit);

	List<LanguagePage> customFindByIdAndLanguageCode(String id, String language);

	Page findByPathAndState(String path, String id, String language);

	List<PageHeader> findPageHotelsByNameHotelAndNameDestinations(String name);

	List<PageHeader> findPageHotelsByNameHotelAndNameDestinationsToPage(String name);

	List<PageHeader> findPageAutocompleteByNameAndCategory(String name, String templateType);

	List<PageHeader> findPageAutocompleteByNameAndCategoryToPage(String name, String templateType);

	List<PageHeader> getPageHotelsByName(String name);

	Page viewPageExternal(String id, String language) throws CaBusinessException;

	List<Page> findPageByLanguageAndPathAndExternalNot(String language, String value, Integer limit);

	List<Page> findPageByLanguageAndPathAndExternal(String language, String value, Integer limit);

	List<PageHeader> findDestinationsByStateNotPublish();

	List<Page> findHotelsByDestinationAndLanguage(String destinationId, String language);

	Optional<PageHeader> findPageHeaderByPageId(String pageId);

	List<PageHeader> getHotelsByArraysIds(List<String> items);

	List<Page> findHotelsRestaurantsByDescriptorInCustomized(List<String> descriptors, String language);

	List<Page> getHotelsByDestinationIdForSite(String destinationId, String language);

	HotelDestination getHotelsByDestinationIdAndLanguageForSite(String destinationId, String language);

	PageHeader updatePageHeaderInEdition(PageHeader pageHeader) throws CaBusinessException;

	List<PageHeader> findPageHeaderHotelsByDestinationsIds(List<String> destinationIds);

	List<Page> getHotelsWithLivingToEvents(String destinationId, String language);

	List<Page> findHotelsByDestinationAndDescriptorCustomized(String destination, String descriptor);

	List<Page> findRestaurantsByDestinationAndDescriptorCustomized(String destination, String descriptor);

	List<Page> findHotelsByIdsAndLanguageCustomized(List<String> ids, String language);

	List<Page> findHotelsByIdsAndLanguageCustomized(LivingDestinationHotel livingDestinationHotel);

	List<PageHeader> getTitle2PageFromContact(List<String> items);

	List<PageHeader> findDestinationsByStateNotPublishByItems(List<String> items);

	List<Page> customFindPageByIdsAndLanguageAndStateNot(List<String> ids, String language);

	List<Page> getPromotionsDetail(String language);

	Page getPromotionByIdAndLanguage(String id, String language) throws CaBusinessException;

	List<Page> customFindHotelsByDestinationIdAndDescriptorIdAndLanguage(String destinationId,
																		 String descriptorId, String language);

	List<Page> customFindDestinationByStateAndLanguage(String language);

	List<Page> customFindDescriptorByHotelsAndDestinationIds(List<String> items);

	List<Page> getContactByPages(List<String> id, String language) throws CaRequiredException;

	List<Page> getHotelsByDestinationAndDescriptorAndStateNot(String destinationId, String descriptorId);

	List<Page> getPromotionBell(List<String> promotions, String language);

	List<PageHeader> getDestinationsAll();

	List<Page> findHotelsByListDestinationsAndLanguageCustomized(List<String> ids, String language);

	List<Page> getPromotionsByPromotionType(String promotionType, String language);

	com.casaandina.cms.util.pagination.Page<PagePromotionView> getFeaturedPromotionsByFilters(FeaturedPromotion featuredPromotion, HostIpCountry hostIpCountry, Pageable pageable);

	List<PromotionUrlReservation> getUrlReservationByPromotionAndHotelsAndRestaurants(String promotions, String currency, String language);

	List<LanguagePage> findPageByIdAndLanguage(String id, String language);

	List<Page> getPageWithDescriptor(List<String> items, String language);

	List<Page> findPromotionsByStateAndLanguageCustomized(String language);

	DraftPage getDraftPageByIdAndLanguage(String id, String language) throws CaSiteErrorException;

	List<PageHeader> findAllPageHome();

	List<PageHeader> findAllPageByCategoryId(String categoryId);

	List<PageHeader> findAllPagePromotionAll();

	Optional<Page> customFindPathsBySEO(String id);

	Optional<Page> customFindPagePathByIdAndLanguage(String id, String language);

	List<PageHeader> findPageAutocompleteByNameAndCategoryAndDestinationId(String name, String templateType, String destinationId);

	List<Page> findPromotionsByStateAndLanguageCustomizedFilter(String language);

    List<Page> getHotelsByIdAndDescriptorAndStateNot(String hotelId);

    List<Page> findAllHotels(String language);

	PageHeader liberatePage(String id) throws CaBusinessException;
}
