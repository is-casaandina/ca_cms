package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Domain;

import java.util.List;

public interface DomainService {

    List<Domain> getAllDomainsByState();

    Domain insertDomain(Domain domain) throws CaBusinessException;

    Domain updateDomain(Domain domain) throws CaBusinessException;

    void deleteDomain(String id) throws CaBusinessException;
}
