package com.casaandina.cms.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import com.casaandina.cms.dto.UserTo;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.User;
import com.casaandina.cms.util.pagination.Page;

public interface UserService {

	Optional<User> findBydId(Integer userId);
	List<User> findAll() throws CaRequiredException;
	User saveUser(User user) throws CaBusinessException, CaRequiredException;
	User updateUser(User user) throws CaBusinessException, CaRequiredException;
	User updateProfile(User user) throws CaBusinessException, CaRequiredException;
	User updateStatus(Integer id) throws CaRequiredException;
	Page<User> listUserPaginated(String[] rolesCode, Pageable pageable);
	String recoveryPassword(String email) throws CaBusinessException, CaRequiredException;
	String updatePasswordWithToken(UserTo userTo) throws CaBusinessException;
	String resetPassword(UserTo user) throws CaBusinessException;
	String passwordChange(Integer userId, String currentPassword, String password, String confirmPassword) throws CaBusinessException;
	
}