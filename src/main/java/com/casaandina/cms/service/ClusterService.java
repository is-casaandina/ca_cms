package com.casaandina.cms.service;

import java.util.List;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Cluster;

public interface ClusterService {
	
	Cluster saveCluster(Cluster cluster) throws CaBusinessException;
	
	Cluster getClusterById(String clusterId) throws CaRequiredException;
	
	Cluster updateCluster(String clusterId, Cluster cluster)
			throws CaBusinessException, CaRequiredException;
	
	void deleteCluster(String clusterId) throws CaRequiredException;
	
	List<Cluster> getAllClusters() throws CaBusinessException;

	List<Cluster> getAllClusterCustomized()throws CaBusinessException;

}
