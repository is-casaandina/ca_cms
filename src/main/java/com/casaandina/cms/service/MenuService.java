package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Menu;
import com.casaandina.cms.model.PageHeader;

import java.util.List;
import java.util.Optional;

public interface MenuService {
	
	Menu createMenu(Menu menu) throws CaRequiredException;
	
	Menu readMenu(String menuId) throws CaRequiredException;
	
	Menu updateMenu(Menu menu) throws CaRequiredException;
	
	void deleteMenu(String menuId) throws CaRequiredException;
	
	List<Menu> getAllMenusByLanguageCode(String languageCode) throws CaRequiredException;

	List<PageHeader> getPageHeaderWithCategory(List<String> ids);

	Optional<PageHeader> getPageHeaderWithCategory(String id);

}
