package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.PromotionType;

import java.util.List;

public interface PromotionTypeService {

    List<PromotionType> findByStateAll();

    List<PromotionType> getPromotionTypeByLanguage(String language);

    PromotionType savePromotion(PromotionType promotionType) throws CaBusinessException;
}
