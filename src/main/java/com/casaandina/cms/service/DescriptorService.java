package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Descriptor;

import java.util.List;

public interface DescriptorService {
	
	Descriptor createTag(Descriptor tag) throws CaRequiredException;
	
	Descriptor readTag(String tagId) throws CaRequiredException;
	
	Descriptor updateTag(Descriptor tag) throws CaRequiredException;
	
	void deleteTag(String tagId);
	
	List<Descriptor> getAllTheTags();

	List<Descriptor> getAllByCategory(String category);

	List<Descriptor> customFindAllDescriptors();

}
