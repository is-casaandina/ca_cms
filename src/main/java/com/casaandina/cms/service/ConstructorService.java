package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.ConstructorTemplate;

import java.util.List;

public interface ConstructorService {
    
        List<ConstructorTemplate> listViewConstructorTemplates(List<String> categories);
	
	ConstructorTemplate getConstructorTemplateForEdit(String code) throws CaBusinessException;
	
	ConstructorTemplate saveConstructorTemplate(ConstructorTemplate constructorTemplate) throws CaRequiredException;
}
