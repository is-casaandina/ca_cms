package com.casaandina.cms.service;

public interface SequenceService {
	
	Integer getNextSequence(String sequenceName);

}
