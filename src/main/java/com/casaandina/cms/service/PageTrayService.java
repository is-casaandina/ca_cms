package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.model.PageJoinView;
import com.casaandina.cms.rest.request.PageTrayRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface PageTrayService {

    Page<PageHeader> listPageTrayByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException;

    Page<PageHeader> listHotelsTrayByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException;

    List<PageHeader> findDestinationsByState();

    List<PageJoinView> findPageByCategoriesAndCountries(List<String> categories, List<String> countries, Sort sort);

    List<PageJoinView> findPageByCategories(List<String> categories, Sort sort);
}
