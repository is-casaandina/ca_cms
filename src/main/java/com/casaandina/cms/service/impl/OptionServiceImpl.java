package com.casaandina.cms.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.casaandina.cms.dto.OptionTo;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Option;
import com.casaandina.cms.repository.OptionRepository;
import com.casaandina.cms.rest.response.OptionResponse;
import com.casaandina.cms.service.OptionService;
import com.casaandina.cms.service.SequenceService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.StatusPageType;
import com.casaandina.cms.util.StatusType;

@Service
public class OptionServiceImpl implements OptionService {

	private final OptionRepository optionRepository;

	private final SequenceService sequenceService;

	private final MessageProperties messageProperties;

	private static final String COPY_URL = "_copy";
	private static final String COPY_NAME = " Copy";
	
	@Autowired
	public OptionServiceImpl(OptionRepository optionRepository, SequenceService sequenceService,
			MessageProperties messageProperties) {
		this.optionRepository = optionRepository;
		this.sequenceService = sequenceService;
		this.messageProperties = messageProperties;
	}

	@Override
	public Option findById(Integer id) {
		Optional<Option> op = optionRepository.findById(id);
		return op.isPresent() ? op.get() : null;
	}

	@Override
	public Option saveOption(OptionTo optionTo) throws CaRequiredException, CaBusinessException {

		validateInsertOption(optionTo);
		Integer id = sequenceService.getNextSequence(ConstantsUtil.OPTION_SEQUENCE);
		optionTo.setId(id);

		Option optionSave = getOptionFromTo(optionTo);

		return optionRepository.insert(optionSave);

	}

	public Option getOptionFromTo(OptionTo optionTo) {
		Option option = new Option();

		option.setId(optionTo.getId());
		option.setName(optionTo.getName());
		option.setDescription(optionTo.getDescription());
		option.setLevel(optionTo.getLevel() == null ? ConstantsUtil.ONE : optionTo.getLevel());
		option.setIdOptionParent(optionTo.getIdOptionParent());
		option.setIdPage(optionTo.getIdPage());
		option.setStatus(StatusType.ACTIVE.getCode());
		option.setEditionStatus(StatusPageType.IN_EDITION.getCode());
		option.setLastUpdate(CaUtil.currentDateString());
		option.setUsername(optionTo.getUsername());
		option.setOwner(optionTo.getUsername());
		option.setOrder(optionTo.getOrder());
		option.setLanguages(optionTo.getLanguages());

		return option;
	}

	private void validateInsertOption(OptionTo optionTo) throws CaRequiredException, CaBusinessException {
		if (StringUtils.isEmpty(optionTo.getName()) || optionTo.getName().trim().isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_REQUIRED));

		Optional<Option> optionName = optionRepository.findByNameAndLevelAndStatus(optionTo.getName(),
				optionTo.getLevel(), StatusType.ACTIVE.getCode());
		if (optionName.isPresent())
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_REQUIRED),HttpStatus.INTERNAL_SERVER_ERROR.value());

		if (optionTo.getIdOptionParent() != null) {
			Optional<Option> option = optionRepository.findById(optionTo.getIdOptionParent());
			if (!option.isPresent())
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_CODE_PARENT_INVALID), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	@Override
	public Option moveOption(OptionTo optionTo) throws CaRequiredException, CaBusinessException {
		validateMoveOption(optionTo);

		Optional<Option> optional = optionRepository.findById(optionTo.getId());
		if (optional.isPresent()) {
			Option option = optional.get();
			option.setIdOptionParent(optionTo.getIdOptionParent());
			option.setUsername(optionTo.getUsername());

			return optionRepository.save(option);
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_NOT_EXIST), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	private void validateMoveOption(OptionTo optionTo) throws CaRequiredException {
		if (optionTo.getIdOptionParent() == null) {
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_CODE_PARENT_REQUIRED));
		}

		Optional<Option> optional = optionRepository.findById(optionTo.getIdOptionParent());
		if (!optional.isPresent())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_CODE_PARENT_NOT_EXIST));
	}

	@Override
	public List<OptionResponse> getOptions() throws CaRequiredException, CaBusinessException {
		List<Option> options = optionRepository.findByStatusAndIdOptionParentIsNull(StatusType.ACTIVE.getCode());

		if (CaUtil.validateListIsNotNullOrEmpty(options)) {
			List<OptionResponse> lstOptionResponse = new ArrayList<>();
			options.stream().forEach(x -> {
				OptionResponse optionResponse = getOptionResponseFromOption(x);
				optionResponse.setListOptionResponse(generateSubOption(optionResponse));
				lstOptionResponse.add(optionResponse);
			});
			return lstOptionResponse;
		}

		return Collections.emptyList();
	}

	public List<OptionResponse> generateSubOption(OptionResponse obj) {

		List<Option> optionList = optionRepository.findByIdOptionParentAndStatus(obj.getId(),
				StatusType.ACTIVE.getCode());

		List<OptionResponse> listOptionResponse = new ArrayList<>();

		if (CaUtil.validateListIsNotNullOrEmpty(optionList)) {
			optionList.stream().forEach(x -> {
				OptionResponse optionResponse = getOptionResponseFromOption(x);
				optionResponse.setListOptionResponse(generateSubOption(optionResponse));
				listOptionResponse.add(optionResponse);
			});
		}

		return listOptionResponse;
	}

	public OptionResponse getOptionResponseFromOption(Option obj) {

		OptionResponse optionResponse = new OptionResponse();
		optionResponse.setId(obj.getId());
		optionResponse.setName(obj.getName());
		optionResponse.setDescription(obj.getDescription());
		optionResponse.setLevel(obj.getLevel());
		optionResponse.setIdOptionParent(obj.getIdOptionParent());
		optionResponse.setIdPage(obj.getIdPage());
		optionResponse.setStatus(obj.getStatus());
		optionResponse.setLastUpdate(obj.getLastUpdate());
		optionResponse.setUsername(obj.getUsername());
		optionResponse.setOwner(obj.getOwner());
		optionResponse.setOrder(obj.getOrder());

		optionResponse.setLanguages(obj.getLanguages());

		return optionResponse;

	}

	@Override
	public OptionResponse renameOption(OptionResponse optionResponse) throws CaRequiredException, CaBusinessException {
		validateRename(optionResponse);

		Optional<Option> optional = optionRepository.findById(optionResponse.getId());
		if (optional.isPresent()) {
			Option option = optional.get();
			option.setName(optionResponse.getName());
			option.setUsername(optionResponse.getUsername());

			optionRepository.save(option);
			return optionResponse;
		}

		return null;
	}

	private void validateRename(OptionResponse optionResponse) throws CaRequiredException, CaBusinessException {
		if (StringUtils.isEmpty(optionResponse.getName()) || optionResponse.getName().trim().isEmpty()) {
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_REQUIRED));
		}

		List<Option> listOptionResponse = optionRepository.findByStatusAndNameAndIdNot(StatusType.ACTIVE.getCode(),
				optionResponse.getName(), optionResponse.getId());
		if (CaUtil.validateListIsNotNullOrEmpty(listOptionResponse)) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_NAME_EXIST), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	@Override
	public void deleteOption(Integer id, String username) throws CaBusinessException {
		validateDelete(id);
		Optional<Option> optional = optionRepository.findByIdAndStatus(id, StatusType.ACTIVE.getCode());
		if (optional.isPresent()) {
			Option option = optional.get();
			option.setStatus(StatusType.INACTIVE.getCode());
			option.setUsername(username);
			optionRepository.save(option);
		}
	}

	public void validateDelete(Integer id) throws CaBusinessException {
		Optional<Option> optional = optionRepository.findByIdAndStatus(id, StatusType.ACTIVE.getCode());
		if (!optional.isPresent())
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_CODE_PARENT_INVALID), HttpStatus.INTERNAL_SERVER_ERROR.value());

		List<Option> listOption = optionRepository.findByIdOptionParentAndStatus(id, StatusType.ACTIVE.getCode());
		if (CaUtil.validateListIsNotNullOrEmpty(listOption))
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_OPTION_DELETE_HAVE_DEPENDENCY), HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

	@Override
	public Option duplicateOption(OptionResponse optionResponse) throws CaBusinessException {

		Integer order = getMaxOrder(optionResponse) + 1;
		
		Option option = new Option();
		option.setId(sequenceService.getNextSequence(ConstantsUtil.OPTION_SEQUENCE));
		option.setName(optionResponse.getName() + COPY_NAME);
		option.setDescription(optionResponse.getDescription());
		option.setLevel(optionResponse.getLevel());
		option.setIdPage(optionResponse.getIdPage());
		option.setIdOptionParent(optionResponse.getIdOptionParent());
		option.setStatus(optionResponse.getStatus());
		option.setEditionStatus(StatusPageType.IN_EDITION.getCode());
		option.setLastUpdate(CaUtil.currentDateString());
		option.setUsername(optionResponse.getUsername());
		option.setOwner(optionResponse.getOwner());
		option.setOrder(order);

		if (CaUtil.validateListIsNotNullOrEmpty(optionResponse.getLanguages())) {
			optionResponse.getLanguages().forEach(x -> {
				x.setName(x.getName() + COPY_NAME);
				x.setPath(x.getPath() + COPY_URL);
			});
			option.setLanguages(optionResponse.getLanguages());
		}
		
		return optionRepository.insert(option);
	}

	private Integer getMaxOrder(OptionResponse optionResponse) {
		Integer maxOrder = 0;
		List<Option> listOption = optionRepository.findByStatusAndLevelOrderByOrderDesc(StatusType.ACTIVE.getCode(),
				optionResponse.getLevel());

		Optional<Option> optional = listOption.stream().findFirst();
		if (optional.isPresent()) {
			maxOrder = optional.get().getOrder();
		}

		return maxOrder;
	}
}
