package com.casaandina.cms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casaandina.cms.model.Permission;
import com.casaandina.cms.repository.PermissionRepository;
import com.casaandina.cms.service.PermissionService;
import com.casaandina.cms.util.StatusType;

@Service
public class PermissionServiceImpl implements PermissionService {
	
	private final PermissionRepository permissionRepository;
	
	@Autowired
	public PermissionServiceImpl(PermissionRepository permissionRepository) {
		this.permissionRepository = permissionRepository;
	}

	@Override
	public List<Permission> findAllActive() {
		return permissionRepository.findByStatus(StatusType.ACTIVE.getCode());
	}
}