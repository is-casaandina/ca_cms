package com.casaandina.cms.service.impl;

import com.casaandina.cms.dto.LivingRoomDestination;
import com.casaandina.cms.dto.ParameterUrlDto;
import com.casaandina.cms.dto.RoibackHotelDaysDto;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Currency;
import com.casaandina.cms.model.*;
import com.casaandina.cms.repository.*;
import com.casaandina.cms.rest.response.RangeTheaterResponse;
import com.casaandina.cms.service.SiteService;
import com.casaandina.cms.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SiteServiceImpl implements SiteService {

    private final RoibackLowPriceRepository roibackLowPriceRepository;
    private final ParameterRepository parameterRepository;
    private final LivingRoomRepository livingRoomRepository;
    private final CurrencyRepository currencyRepository;
    private final DescriptorRepository descriptorRepository;
    private final AssistanceRepository assistanceRepository;
    private final CountryRepository countryRepository;
    private final ConfigurationsRepository configurationsRepository;
    private final DestinationMapRepository destinationMapRepository;
    private final PromotionTypeRepository promotionTypeRepository;
    private final FormTemplateRepository formTemplateRepository;
    private final DomainRepository domainRepository;
    private final ClusterRepository  clusterRepository;
    private final LivingRoomDestinationViewRepository livingRoomDestinationViewRepository;
    private final PageHeaderRepository pageHeaderRepository;
    private final PagePromotionViewRepository pagePromotionViewRepository;

    @Override
    public RoibackLowPrice getPriceByRoibackAndCurrency(String roiback, String currencyIso, String language) throws CaRequiredException{
        Optional<RoibackLowPrice> roibackLowPriceOptional = roibackLowPriceRepository.findTopByRoibackAndLanguageOrderByCreatedDateDesc(roiback, language);

        List<Price> prices = new ArrayList<>();
        if(roibackLowPriceOptional.isPresent()){

            RoibackLowPrice roibackLowPrice = roibackLowPriceOptional.get();

            Price price = roibackLowPrice.getPrices().stream().filter(x -> currencyIso.equals(x.getCurrency())).findFirst().orElse(null);
            prices.add(price);
            return RoibackLowPrice.builder()
                    .id(roibackLowPrice.getId())
                    .roiback(roibackLowPrice.getRoiback())
                    .prices(prices)
                    .urlReservation(getUrlRoibackHotel(language, roiback))
                    .build();
        }
        return RoibackLowPrice.builder().urlReservation(getUrlRoibackHotel(language, roiback)).build();
    }

    @Override
    public String getUrlRoibackHotel(String language, String roiback) throws CaRequiredException {
        String path;
        Parameter parameterUrlRoibackHotel = getStateByCodeAndLanguage(ParameterType.ROIBACK_HOTEL.getCode(), language);

        String urlBaseRoibackHotel = parameterUrlRoibackHotel!= null
                ? ObjectMapperUtils.map(parameterUrlRoibackHotel.getIndicator(), ParameterUrlDto.class).getUrl()
                : ConstantsUtil.EMPTY;

        Parameter parameterRoibackHotelDays = getStateByCodeAndLanguage(ParameterType.ROIBACK_HOTEL_DAYS.getCode(),
                LanguageType.SPANISH.getCode());

        RoibackHotelDaysDto roibackHotelDaysDto = parameterRoibackHotelDays != null ? ObjectMapperUtils.map(
                parameterRoibackHotelDays.getIndicator(), RoibackHotelDaysDto.class) : null;

        LocalDate beginDate = LocalDate.now();
        LocalDate endDate = LocalDate.now().plusDays(ConstantsUtil.ONE);

        if(Objects.nonNull(roibackHotelDaysDto)){
            beginDate = beginDate.plusDays(roibackHotelDaysDto.getFrom());
            endDate = ObjectMapperUtils.map(beginDate, LocalDate.class);
            endDate = endDate.plusDays(roibackHotelDaysDto.getUntil());
        }

        path = urlBaseRoibackHotel + roiback + ConstantsUtil.SEPARATOR + beginDate + ConstantsUtil.SEPARATOR + endDate;
        return path;
    }

    @Override
    public RangeTheaterResponse getRangeTheater() throws CaRequiredException {
        Optional<LivingRoom> livingRoomOptionalMin = this.livingRoomRepository.findTopByTeatherNotNullOrderByTeatherAsc();
        Optional<LivingRoom> livingRoomOptionalMax = this.livingRoomRepository.findTopByTeatherNotNullOrderByTeatherDesc();

        RangeTheaterResponse rangeTheaterResponse = getRangeTheaterResponse(ParameterType.LIVING_ROOM.getCode(), LanguageType.SPANISH.getCode());
        rangeTheaterResponse = rangeTheaterResponse == null ? RangeTheaterResponse.builder().build() : rangeTheaterResponse;
        if(livingRoomOptionalMin.isPresent()){
            rangeTheaterResponse.setTheaterMin(livingRoomOptionalMin.get().getTeather());
        }

        if(livingRoomOptionalMax.isPresent()){
            rangeTheaterResponse.setTheaterMax(livingRoomOptionalMax.get().getTeather());
        }
        return rangeTheaterResponse;
    }

    private RangeTheaterResponse getRangeTheaterResponse(final String code, final String languageCode) throws CaRequiredException {
        Optional<Parameter> optional = this.parameterRepository.findByCodeAndLanguageCode(code, languageCode);
        if (optional.isPresent()) {
            return new ObjectMapper().convertValue(optional.get().getIndicator(), RangeTheaterResponse.class);
        } else {
            throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
        }
    }

    @Override
    public Page<LivingRoom> getAllTheLivingRooms(final List<String> destinationsId,
                                                 final List<String> hotelsId, final List<Integer> capacityRange, Pageable pageable) throws CaRequiredException {

        if (CaUtil.validateListIsNotNullOrEmpty(destinationsId)) {
            return getAllByDestinationNonNull(destinationsId, hotelsId, capacityRange, pageable);
        } else {
            return getAllByDestinationNull(hotelsId, capacityRange, pageable);
        }
    }

    private Page<LivingRoom> getAllByDestinationNonNull(final List<String> destinationsId,
                                                        final List<String> hotelsId, final List<Integer> capacityRange, Pageable pageable) {

        if (CaUtil.validateListIsNotNullOrEmpty(hotelsId) && !hotelsId.isEmpty()) {
            if (validateCapacityRange(capacityRange)) {
                return this.livingRoomRepository.findAllByDestinationIdInAndHotelIdInAndTeatherBetween(
                        destinationsId, hotelsId, capacityRange.get(ConstantsUtil.ZERO),
                        (capacityRange.get(ConstantsUtil.ONE) + ConstantsUtil.ONE), pageable);
            } else {
                return this.livingRoomRepository.findAllByDestinationIdInAndHotelIdIn(destinationsId, hotelsId, pageable);
            }
        } else {
            if (validateCapacityRange(capacityRange)) {
                return this.livingRoomRepository.findAllByDestinationIdInAndTeatherBetween(
                        destinationsId, capacityRange.get(ConstantsUtil.ZERO), (capacityRange.get(ConstantsUtil.ONE) + ConstantsUtil.ONE), pageable);
            } else {
                return this.livingRoomRepository.findAllByDestinationIdIn(destinationsId, pageable);
            }
        }
    }

    private Page<LivingRoom> getAllByDestinationNull(final List<String> hotelsId, final List<Integer> capacityRange, Pageable pageable){
        if (CaUtil.validateListIsNotNullOrEmpty(hotelsId)) {
            if (validateCapacityRange(capacityRange)) {
                return this.livingRoomRepository.findAllByHotelIdInAndTeatherBetween(
                        hotelsId, capacityRange.get(ConstantsUtil.ZERO), (capacityRange.get(ConstantsUtil.ONE) + ConstantsUtil.ONE), pageable);
            } else {
                return this.livingRoomRepository.findAllByHotelIdIn(hotelsId, pageable);
            }
        } else {
            if (validateCapacityRange(capacityRange)) {
                return this.livingRoomRepository.findAllByTeatherBetween(capacityRange.get(ConstantsUtil.ZERO),
                        (capacityRange.get(ConstantsUtil.ONE) + ConstantsUtil.ONE), pageable);
            } else {
                return this.livingRoomRepository.findAll(pageable);
            }
        }
    }

    private boolean validateCapacityRange(final List<Integer> capacityRange) {
        return Objects.nonNull(capacityRange) && capacityRange.size() == ConstantsUtil.TWO;
    }

    @Override
    public Parameter getStateByCodeAndLanguage(String code, String languageCode) throws CaRequiredException {

        Optional<Parameter> parameterOptional = this.parameterRepository.findByCodeAndLanguageCode(code, languageCode);

        if (!parameterOptional.isPresent()) {
            throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
        } else {
            return parameterOptional.get();
        }
    }

    @Override
    public Currency findByCodeIsoAndStatus(String currencyIso){
        Optional<Currency> currencyOptional = this.currencyRepository.findByCodeIsoAndStatus(currencyIso, StatusType.ACTIVE.getCode());
        if(currencyOptional.isPresent()){
            return currencyOptional.get();
        } else {
            return null;
        }
    }

    @Override
    public List<LivingRoom> customFindLivingRoomByDestinationIdAndHotelId(String destinationId, String hotelId, Sort sort){
        return this.livingRoomRepository.customFindLivingRoomByDestinationIdAndHotelId(destinationId, hotelId, sort);
    }

    @Override
    public List<LivingRoom> customFindLivingRoomByDestinationIdAndHotelIdAndCategory(String destinationId, String hotelId, List<String> categories){
        Sort sort = new Sort(Sort.Direction.ASC, "order");
        return this.livingRoomRepository.customFindLivingRoomByDestinationIdAndHotelIdAndCategory(destinationId, hotelId, categories, sort);
    }

    @Override
    public List<Descriptor> customFindDescriptorByIds(List<String> ids){
        return this.descriptorRepository.customFindDescriptorByIds(ids);
    }

    @Override
    public List<Assistance> getAssistanceByIdsAndTypeAndLanguage(List<String> ids, String language) {
        List<Assistance> assistances = this.assistanceRepository.customFindByIdsAndTypeAndLanguage(ids,StatusType.ACTIVE.getCode(), language);
        assistances.forEach(x ->
            x.getAssistanceTitles().forEach(y ->{
                String title = y.getTitle();
                if(LanguageType.SPANISH.getCode().equals(language)){
                    x.setSpanishTitle(title);
                } else if(LanguageType.ENGLISH.getCode().equals(language)){
                    x.setEnglishTitle(title);
                } else if(LanguageType.PORTUGUESE.getCode().equals(language)){
                    x.setPortugueseTitle(title);
                }
            })
        );
        return assistances;
    }

    @Override
    public List<Country> customFindAllByStatus() {
        return this.countryRepository.customFindAllByStatus(Boolean.TRUE);
    }

    @Override
    public Configurations getConfigurations() {
        return this.configurationsRepository.findAll().stream().findFirst().get();
    }

    @Override
    public String getUrlRoibackHotel(String language) throws CaRequiredException {
        Parameter parameterUrlRoibackHotel = getStateByCodeAndLanguage(ParameterType.ROIBACK_HOTEL.getCode(), language);

        return Objects.nonNull(parameterUrlRoibackHotel)
                ? ObjectMapperUtils.map(parameterUrlRoibackHotel.getIndicator(), ParameterUrlDto.class).getUrl()
                : ConstantsUtil.EMPTY;
    }

    @Override
    public String getRangeDateRoiback() throws CaRequiredException {
        Parameter parameterRoibackHotelDays = getStateByCodeAndLanguage(ParameterType.ROIBACK_HOTEL_DAYS.getCode(),
                LanguageType.SPANISH.getCode());

        RoibackHotelDaysDto roibackHotelDaysDto = parameterRoibackHotelDays != null ? ObjectMapperUtils.map(
                parameterRoibackHotelDays.getIndicator(), RoibackHotelDaysDto.class) : null;

        LocalDate beginDate = LocalDate.now();
        LocalDate endDate = LocalDate.now().plusDays(ConstantsUtil.ONE);

        if(Objects.nonNull(roibackHotelDaysDto)){
            beginDate = beginDate.plusDays(roibackHotelDaysDto.getFrom());
            endDate = ObjectMapperUtils.map(beginDate, LocalDate.class);
            endDate = endDate.plusDays(roibackHotelDaysDto.getUntil());
        }

        return beginDate + ConstantsUtil.SEPARATOR + endDate;
    }

    @Override
    public List<DestinationMap> getDestinationMapByIds(List<String> items){
        return this.destinationMapRepository.customFindByIdsAndState(items, StatusType.ACTIVE.getCode());
    }

    @Override
    public List<PromotionType> getPromotionTypeByLanguage(String language) {
        return this.promotionTypeRepository.customFindByStateAndLanguage(StatusType.ACTIVE.getCode(), language);
    }

    @Override
    public Configurations getLanguagesForSite(){
        List<Configurations> configurations = configurationsRepository.customFindAllLanguages();
        if( Objects.nonNull(configurations) && !configurations.isEmpty()){
            Configurations configuration = configurations.stream().findFirst().orElse(null);
            return configuration;
        }
        return null;
    }

    @Override
    public List<Currency> getCurrenciesForSite(){
        return this.currencyRepository.findByStatusCustomized(StatusType.ACTIVE.getCode());
    }

    @Override
    public FormTemplate getFormTemplateByCodeAndLanguage(String code, String language) {
        Optional<FormTemplate> formTemplateOptional = this.formTemplateRepository.customFindByCodeAndLanguage(code, language);
        if(formTemplateOptional.isPresent()){
            return formTemplateOptional.get();
        }
        return null;
    }

    @Override
    public Domain getDomainByPath(String path) {
        Optional<Domain> domainOptional = this.domainRepository.findByPathAndState(path, StatusType.ACTIVE.getCode());
        if(domainOptional.isPresent()){
            return domainOptional.get();
        }
        return null;
    }

    @Override
    public Domain customFindByPath(String path) {
        Optional<Domain> domainOptional = this.domainRepository.customFindByPath(path, StatusType.ACTIVE.getCode());
        if(domainOptional.isPresent()){
            return domainOptional.get();
        }
        return null;
    }

    @Override
    public Cluster getClusterByClusterId(String clusterId) {
        Optional<Cluster> clusterOptional = this.clusterRepository.findByIdAndStatus(clusterId, Boolean.TRUE);
        if(clusterOptional.isPresent()){
            return clusterOptional.get();
        }
        return null;
    }

    @Override
    public Country getCountryById(String countryId) {
        Optional<Country> countryOptional = this.countryRepository.findByIdAndStatusCustomized(countryId, Boolean.TRUE);
        if(countryOptional.isPresent()){
            return countryOptional.get();
        }
        return null;
    }

    @Override
    public List<LivingRoomDestination> customFindAllDestinationWithLivingRoom(){
        List<LivingRoomDestinationView> livingRoomDestinationViews = this.livingRoomDestinationViewRepository.customFindAllDestinationWithLivingRoomAndDestinationNot();
        if(Objects.nonNull(livingRoomDestinationViews) && !livingRoomDestinationViews.isEmpty()){
            Set<LivingRoomDestination> livingRoomDestinations = new HashSet<>();
            livingRoomDestinationViews.forEach(living ->
                livingRoomDestinations.add(
                        LivingRoomDestination.builder()
                                .destinationId(living.getDestinationId())
                                .destination(living.getDestination())
                                .build()
                )
            );

            return livingRoomDestinations.stream().collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public List<PageHeader> findPageHeaderHotelsByDestinationsIds(List<String> destinationIds){
        List<LivingRoomDestinationView> livingRoomDestinationViews;
        if(CaUtil.validateListIsNotNullOrEmpty(destinationIds)){
            livingRoomDestinationViews = this.livingRoomDestinationViewRepository.customFindAllLivingHotelsByDestinations(destinationIds);
        } else {
            livingRoomDestinationViews = this.livingRoomDestinationViewRepository.customFindAllLivingHotels();
        }

        if(Objects.nonNull(livingRoomDestinationViews) && !livingRoomDestinationViews.isEmpty())
            return this.pageHeaderRepository.customFindByIdAndStateAndPublish(
                    livingRoomDestinationViews.stream()
                            .map(LivingRoomDestinationView::getHotelId)
                            .collect(Collectors.toList()), StatusPageType.DELETED.getCode(), Boolean.TRUE);
        return Collections.emptyList();
    }

    @Override
    public List<PageHeader> getDestinationByPromotions(List<String> items){
        List<PagePromotionView> pagePromotionViews;
        LocalDateTime today = LocalDateTime.now();
        if(Objects.nonNull(items) && !items.isEmpty())
            pagePromotionViews = this.pagePromotionViewRepository.getDestinationByPromotions(today, items);
        else
            pagePromotionViews = this.pagePromotionViewRepository.getDestinationsAll(today);

        if(Objects.nonNull(pagePromotionViews) && !pagePromotionViews.isEmpty()){
            List<String> destinations = pagePromotionViews.stream().map(this::buildDestination).collect(Collectors.toList());
            return this.pageHeaderRepository.customFindByIdAndStateAndPublish(
                    destinations, StatusPageType.DELETED.getCode(), Boolean.TRUE);
        } else
            return Collections.emptyList();
    }

    private String buildDestination(PagePromotionView pagePromotionViews ){
        if(Objects.nonNull(pagePromotionViews.getItem()))
            return pagePromotionViews.getItem().getDestination();
        return null;
    }

    @Override
    public List<PageHeader> getHotelByDestinationsForPromotions(List<String> items){
        List<PagePromotionView> pagePromotionViews;
        LocalDateTime today = LocalDateTime.now();
        if(Objects.nonNull(items) && !items.isEmpty())
            pagePromotionViews = this.pagePromotionViewRepository.getHotelsByDestinations(today, items);
        else
            pagePromotionViews = this.pagePromotionViewRepository.getHotelsAll(today);

        if(Objects.nonNull(pagePromotionViews) && !pagePromotionViews.isEmpty()){
            List<String> hotels = pagePromotionViews.stream().map(this::buildHotel).collect(Collectors.toList());
            return this.pageHeaderRepository.customFindByIdAndStateAndPublish(
                    hotels, StatusPageType.DELETED.getCode(), Boolean.TRUE);
        } else
            return Collections.emptyList();
    }

    private String buildHotel(PagePromotionView pagePromotionViews ){
        if(Objects.nonNull(pagePromotionViews.getItem()))
            return pagePromotionViews.getItem().getHotel();
        return null;
    }

}
