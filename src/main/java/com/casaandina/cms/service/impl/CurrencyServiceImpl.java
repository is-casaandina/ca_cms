package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Currency;
import com.casaandina.cms.repository.CurrencyRepository;
import com.casaandina.cms.service.CurrencyService;
import com.casaandina.cms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	private final CurrencyRepository currencyRepository;
	private final MessageProperties messageProperties;
	
	private static final Integer INTERNAL_ERROR_SERVER = HttpStatus.INTERNAL_SERVER_ERROR.value();

	@Autowired
	public CurrencyServiceImpl(CurrencyRepository currencyRepository, MessageProperties messageProperties) {
		this.currencyRepository = currencyRepository;
		this.messageProperties = messageProperties;
	}

	@Override
	public Currency saveCurrency(Currency currency) throws CaBusinessException, CaRequiredException {
		validateCurrency(currency);
		
		if(currency.getIsPrincipal() != null && StatusType.ACTIVE.getCode().equals(currency.getIsPrincipal())) {
			List<Currency> lstCurrency = currencyRepository.findByStatus(StatusType.ACTIVE.getCode());
			lstCurrency.stream().forEach(x-> x.setIsPrincipal(StatusType.INACTIVE.getCode()));
			currencyRepository.saveAll(lstCurrency);
		}

		return currencyRepository.save(getCurrencyFromCurrencyRequest(currency));
	}

	private Currency getCurrencyFromCurrencyRequest(Currency currency) {
		LocalDate now = LocalDate.now();
		return Currency.builder()
					   .codeIso(currency.getCodeIso())
					   .name(currency.getName())
					   .symbol(currency.getSymbol())
					   .value(currency.getValue())
					   .exchangeRateDate(now)
					   .status(StatusType.ACTIVE.getCode())
					   .createdDate(now)
					   .updatedDate(now)
					   .isPrincipal(currency.getIsPrincipal())
					   .build();
	}

	private void validateCurrency(Currency currency) throws CaBusinessException, CaRequiredException {

		if (StringUtils.isEmpty(currency.getName()) && currency.getName().trim().isEmpty()) {
			throw new CaRequiredException(
					messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_NAME_REQUIRED));
		}

		if (StringUtils.isEmpty(currency.getSymbol()) && currency.getSymbol().trim().isEmpty()) {
			throw new CaRequiredException(
					messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_SYMBOL_REQUIRED));
		}

		if (currency.getValue() == null) {
			throw new CaRequiredException(
					messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_VALUE_REQUIRED));
		}
		
		if (StringUtils.isEmpty(currency.getCodeIso()) &&currency.getCodeIso().trim().isEmpty()) {
			throw new CaRequiredException(
					messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_CODEISO_REQUIRED));
		}
		
		Optional<Currency> currencyOptional = currencyRepository.findByNameAndStatus(currency.getName(),
				StatusType.ACTIVE.getCode());
		if (currencyOptional.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_NAME_EXIST), INTERNAL_ERROR_SERVER);
		}
	}

	@Override
	public List<Currency> findByStatus() {
		return currencyRepository.findByStatus(StatusType.ACTIVE.getCode());
	}

	public List<Currency> updateListCurrency(List<Currency> listCurrency) throws CaBusinessException {
		
		List<Currency> currenciesUpdate = new ArrayList<>();

		if (CaUtil.validateListIsNotNullOrEmpty(listCurrency)) {
			listCurrency.stream().forEach(x -> {
				Optional<Currency> currencyOptional = currencyRepository.findByCodeIsoAndStatus(x.getCodeIso(), StatusType.ACTIVE.getCode());
				if(currencyOptional.isPresent()) {
					
					Currency currencyUpdate = currencyOptional.get();
					currencyUpdate.setStatus(StatusType.INACTIVE.getCode());
					currencyUpdate.setUpdatedDate(LocalDate.now());
					currenciesUpdate.add(currencyUpdate);
					
					Currency currencyNew = getCurrencyFromCurrencyRequest(x);
					
					currenciesUpdate.add(currencyNew);
				}
			});
			currencyRepository.saveAll(currenciesUpdate);
			return currencyRepository.findByStatus(StatusType.ACTIVE.getCode());
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_LIST_REQUIRED), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}		
	}
	
	@Override
	public Double currencyConverter(String codeIsoOrigin, String codeIsoTarget, Double amount) throws CaBusinessException {
		
		if((StringUtils.isEmpty(codeIsoOrigin) && codeIsoOrigin.trim().isEmpty()) || (StringUtils.isEmpty(codeIsoTarget) && codeIsoTarget.trim().isEmpty()))
		{
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_NAME_REQUIRED), INTERNAL_ERROR_SERVER);
		}
		
		if(amount == null) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_VALUE_EXCHANGE), INTERNAL_ERROR_SERVER);
		}
		
		if(codeIsoOrigin.compareTo(codeIsoTarget) == 0) {
			return amount;
		}
		
		Integer status = StatusType.ACTIVE.getCode();
		Optional<Currency> currencyPrincipal = currencyRepository.findByIsPrincipalAndStatus(status, status);
		
		if(!currencyPrincipal.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_NOT_EXIST_PRINCIPAL), INTERNAL_ERROR_SERVER);
		}
		
		Optional<Currency> originCurrency = currencyRepository.findByCodeIsoAndStatusCustomized(codeIsoOrigin, status);
		Optional<Currency> targetCurrency = currencyRepository.findByCodeIsoAndStatusCustomized(codeIsoTarget, status);
		
		if(originCurrency.isPresent() && targetCurrency.isPresent()) {
			
			Double result = CaUtil.roundDouble(amount/originCurrency.get().getValue(), ConstantsUtil.ROUND_TWO_DECIMALS, Boolean.TRUE);
			
			if(!codeIsoTarget.equals(currencyPrincipal.get().getCodeIso())) {
				result = result * targetCurrency.get().getValue();
			}
			
			return CaUtil.roundDouble(result, ConstantsUtil.TWO, Boolean.TRUE);
			
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_EXCHANGE_NOT_ASSIGNED), INTERNAL_ERROR_SERVER);
		}
	}
}
