package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Component;
import com.casaandina.cms.repository.ComponentRepository;
import com.casaandina.cms.service.ComponentService;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.StatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ComponentServiceImpl implements ComponentService {
	
	private final ComponentRepository componentRepository;
	private final MessageProperties messageProperties;
	
	@Autowired
	public ComponentServiceImpl(ComponentRepository componentRepository, MessageProperties messageProperties) {
		this.componentRepository = componentRepository;
		this.messageProperties = messageProperties;
	}
	
	public List<Component> findAllComponents(){
		return componentRepository.findAll();
	}

	@Override
	public List<Component> findComponentShortByStatus() throws CaBusinessException {
		return componentRepository.findComponentShortByStatus(StatusType.ACTIVE.getCode());
	}

	@Override
	public Component findByIdAndStatus(String id) throws CaRequiredException {
		Optional<Component> component = componentRepository.findByIdAndStatus(id, StatusType.ACTIVE.getCode());
		if(component.isPresent())
			return component.get();
		else
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_COMPONENT_CODE_NOT_EXIST));
	}

	@Override
	public Component findByCodeAndStatus(String code) throws CaRequiredException {
		Optional<Component> component = componentRepository.findByCodeAndStatus(code, StatusType.ACTIVE.getCode());
		if(component.isPresent())
			return component.get();
		else
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_COMPONENT_CODE_NOT_EXIST));
	}

	@Override
	public List<Component> findComponentByTypeAndStatus(Integer type) {
		return componentRepository.findByTypeAndStatus(type, StatusType.ACTIVE.getCode());
	}
}
