package com.casaandina.cms.service.impl;

import com.casaandina.cms.dto.RatingIndicators;
import com.casaandina.cms.dto.RatingParameters;
import com.casaandina.cms.repository.ParameterRepository;
import com.casaandina.cms.repository.RatingRepository;
import com.casaandina.cms.rest.response.RatingResponse;
import com.casaandina.cms.rest.response.RatingsByReviewSite;
import com.casaandina.cms.service.RatingService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.Math;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.ParameterType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

	private final RatingRepository ratingRepository;
	
	private final ParameterRepository parameterRepository;
	
	@Autowired
	public RatingServiceImpl(RatingRepository ratingRepository, ParameterRepository parameterRepository) {
		this.ratingRepository = ratingRepository;
		this.parameterRepository = parameterRepository;
	}
	
	@Override
	public RatingResponse getAllRatingByHotelCode(final String hotelCode) {
		List<RatingsByReviewSite> ratingsByReviewSites = ObjectMapperUtils.mapAll(
				this.ratingRepository.findAllByHotelCode(hotelCode), RatingsByReviewSite.class);
		
		int count = 0;
		double sum = 0.0;
		String rankingMessage = "";
		for (RatingsByReviewSite ratingsByReviewSite : ratingsByReviewSites) {
			if (ConstantsUtil.TRIP_ADVISOR.equalsIgnoreCase(ratingsByReviewSite.getReviewSiteName())) {
				rankingMessage = ratingsByReviewSite.getRankingMessage();
			}
			ratingsByReviewSite.setAvarageRating( Math.setDecimals(ratingsByReviewSite.getAvarageRating(), 1) );
			sum += ratingsByReviewSite.getAvarageRating();
			count++;
		}
		
		double avarageRating = Math.setDecimals(sum/(count > 0 ? count : 1), 1);
		RatingResponse ratingResponse = new RatingResponse();
		ratingResponse.setAvarageRating(avarageRating);
		ratingResponse.setRatingsByReviewSites(ratingsByReviewSites);
		ratingResponse.setRankingMessage(rankingMessage);
		
		RatingParameters ratingParameters = this.parameterRepository.customFindAllByCode(ParameterType.RATINGS.getCode());
		
		for (RatingIndicators ratingIndicators : ratingParameters.getIndicator()) {
			if (avarageRating > ratingIndicators.getMinRating() && avarageRating <= ratingIndicators.getMaxRating()) {
				ratingResponse.setRatingType(ratingIndicators.getRatingType());break;
			}
		}
		
		return ratingResponse;
	}

}
