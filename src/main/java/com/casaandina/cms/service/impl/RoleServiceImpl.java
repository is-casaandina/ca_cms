package com.casaandina.cms.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Role;
import com.casaandina.cms.model.User;
import com.casaandina.cms.repository.PermissionRepository;
import com.casaandina.cms.repository.RoleRepository;
import com.casaandina.cms.repository.UserRepository;
import com.casaandina.cms.service.RoleService;
import com.casaandina.cms.service.SequenceService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.StatusType;
import com.casaandina.cms.util.pagination.Page;
import com.casaandina.cms.util.pagination.PageUtil;

@Service
public class RoleServiceImpl implements RoleService {
	
	private final RoleRepository roleRepository;
	private final PermissionRepository permissionRepository;
	private final UserRepository userRepository;
	private final MessageProperties messageProperties;
	private final SequenceService sequenceService;
	
	@Autowired
	public RoleServiceImpl(RoleRepository roleRepository, PermissionRepository permissionRepository, UserRepository userRepository, MessageProperties messageProperties, SequenceService sequenceService) {
		this.roleRepository = roleRepository;
		this.permissionRepository = permissionRepository;
		this.userRepository = userRepository;
		this.messageProperties = messageProperties;
		this.sequenceService = sequenceService;
	}

	@Override
	public Optional<Role> findById(String roleId) {
		return roleRepository.findById(roleId);
	}
	
	@Override
	public Page<Role> findAllRolesPaginated(Pageable pageable) throws CaBusinessException {
		return PageUtil.pageToResponse(roleRepository.findByStatus(StatusType.ACTIVE.getCode(), pageable));
	}

	@Override
	public List<Role> findAll() throws CaBusinessException {
		return roleRepository.findByStatusOrderByRoleCode(StatusType.ACTIVE.getCode());
	}

	@Override
	public List<Role> findRolesPermissionDetail() throws CaBusinessException  {
		return roleRepository.findByStatus(StatusType.ACTIVE.getCode());
	}

	@Override
	public Page<Role> findRolesPermissionDetailPaginated(Pageable pageable) throws CaBusinessException {
		org.springframework.data.domain.Page<Role> rolePage = roleRepository.findByStatus(StatusType.ACTIVE.getCode(), pageable);
		return PageUtil.pageToResponse(rolePage);
	}

	@Override
	public void saveRole(Role role) throws CaRequiredException, CaBusinessException {
		validatePermission(role);
		validateNameRol(role);

		Integer sequence =  sequenceService.getNextSequence(ConstantsUtil.ROLE_SEQUENCE);
		String roleCode = CaUtil.generateCode(ConstantsUtil.PRE_ROLE, sequence);

		LocalDateTime currentDate = LocalDateTime.now();

		role.setRoleCode(roleCode);
		role.setStatus(StatusType.ACTIVE.getCode());
		role.setCreatedDate(currentDate);
		role.setUpdatedDate(currentDate);

		try {
			roleRepository.insert(role);
		} catch (Exception e) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.SAVE_ROLE_EXCEPTION_ERROR) + ": " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	@Override
	public void updateRole(Role role) throws CaRequiredException, CaBusinessException {
		validateUpdateRole(role);
		Optional<Role> roleExist = roleRepository.findByRoleCodeAndStatus(role.getRoleCode(), StatusType.ACTIVE.getCode());
		if(!roleExist.isPresent())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_CODE_INVALID));
		validatePermission(role);
		Role roleUpd = roleExist.get();
		roleUpd.setUpdatedDate(LocalDateTime.now());
		roleUpd.setPermissionCodeList(role.getPermissionCodeList());
		roleUpd.setRoleName(role.getRoleName());
        roleRepository.save(roleUpd);
	}

	@Override
	public void deleteRole(String roleCode) throws CaRequiredException, CaBusinessException {
			Optional<Role> role = roleRepository.findById(roleCode);
			if (!role.isPresent())
	            throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.DELETE_ROLE_NAME_NOT_EXIST));
			
			List<User> users = userRepository.findByRoleCodeAndStatus(roleCode, StatusType.ACTIVE.getCode());
			
			if(CaUtil.validateListIsNotNullOrEmpty(users))
				throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.DELETE_ROLE_ASSIGN_USER_VALID));
			
			role.get().setStatus(StatusType.INACTIVE.getCode());
			
	        roleRepository.save(role.get());
	}

	private void validatePermission(Role role) throws CaRequiredException {
		if (role.getRoleName().isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_NAME_REQUIRED));
		if (role.getPermissionCodeList().isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_NOT_SELECTED));
		for (String var : role.getPermissionCodeList()) {
			if (!permissionRepository.findById(var).isPresent())
				throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_PERMISSION_CODE_INVALID));
		}
	}

	private void validateNameRol(Role role) throws CaRequiredException {
		if (StringUtils.isEmpty(role.getRoleName()) || role.getRoleName().trim().isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_NAME_INCORRECT));
		List<Role> roleList = roleRepository.findByStatusAndRoleNameIgnoreCase(StatusType.ACTIVE.getCode(), role.getRoleName());
		if (!roleList.isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_NAME_EXIST));
	}

	private void validateUpdateRole(Role role) throws CaRequiredException {
		if (role.getRoleName().isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_NAME_REQUIRED));
		if (role.getPermissionCodeList().isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_NOT_SELECTED));
		if (role.getRoleCode().isEmpty())
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_CODE_INVALID));

		Optional<Role> roleInBd = roleRepository.findByRoleCode(role.getRoleCode());
		List<Role> roleList = roleRepository.findByStatusAndRoleNameIgnoreCase(StatusType.ACTIVE.getCode(), role.getRoleName());
		if(roleInBd.isPresent() && (!roleInBd.get().getRoleName().equalsIgnoreCase(role.getRoleName()) && !roleList.isEmpty())) {
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.UPDATE_ROLE_NAME_EXIST));
		}
	}
}
