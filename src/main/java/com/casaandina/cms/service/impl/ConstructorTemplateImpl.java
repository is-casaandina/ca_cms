package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.ConstructorTemplate;
import com.casaandina.cms.repository.ConstructorRepository;
import com.casaandina.cms.service.ConstructorService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.TemplateType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class ConstructorTemplateImpl implements ConstructorService {
    private static final Integer INTERNAL_SERVER_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();
	
	private final ConstructorRepository constructortemplateRepository;
	
	private final MessageProperties messageProperties;
        
    @Autowired
	public ConstructorTemplateImpl(ConstructorRepository constructortemplateRepository, MessageProperties messageProperties) {
		this.constructortemplateRepository = constructortemplateRepository;
		this.messageProperties = messageProperties;
    }
        
    @Override
    public List<ConstructorTemplate> listViewConstructorTemplates(List<String> categories) {
            if(Objects.nonNull(categories) && !categories.isEmpty()){
			return this.constructortemplateRepository.findTemplatesByCategoriesCustomized(categories);
		} else
			return this.constructortemplateRepository.findTemplatesCustomized(TemplateType.PAGE_ERROR.getCode());
    }

    @Override
    public ConstructorTemplate getConstructorTemplateForEdit(String code) throws CaBusinessException {
        Optional<ConstructorTemplate> optionalTemplate = constructortemplateRepository.findByCode(code);
		
		if(!optionalTemplate.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_CODE_NOT_EXIST), INTERNAL_SERVER_ERROR);
		}
		return optionalTemplate.get();
    }

    @Override
    public ConstructorTemplate saveConstructorTemplate(ConstructorTemplate constructorTemplate) throws CaRequiredException {
        validate(constructorTemplate);
		UUID uuid = UUID.randomUUID();
		ConstructorTemplate constructortemplateSave = new ConstructorTemplate();
		BeanUtils.copyProperties(constructorTemplate, constructortemplateSave);
		constructortemplateSave.setId(uuid.toString());

		return constructortemplateRepository.save(constructortemplateSave);
    }
    
    private void validate(ConstructorTemplate constructorTemplate) throws  CaRequiredException{
		if(!CaUtil.validateIsNotNullAndNotEmpty(constructorTemplate.getCode())){
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_CODE_REQUIRED));
		}

		Optional<ConstructorTemplate> ConstructortemplateOptional = constructortemplateRepository.findByCode(constructorTemplate.getCode());
		if(ConstructortemplateOptional.isPresent()){
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_CODE_EXIST));
		}
	}
    
}
