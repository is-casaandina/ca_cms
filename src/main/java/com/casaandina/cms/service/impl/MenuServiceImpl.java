package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Menu;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.repository.MenuRepository;
import com.casaandina.cms.repository.PageHeaderRepository;
import com.casaandina.cms.service.MenuService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.StatusPageType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MenuServiceImpl implements MenuService {
	
	private final MenuRepository menuRepository;
	private final PageHeaderRepository pageHeaderRepository;
	
	@Autowired
	public MenuServiceImpl(MenuRepository headerRepository, PageHeaderRepository pageHeaderRepository) {
		this.menuRepository = headerRepository;
		this.pageHeaderRepository = pageHeaderRepository;
	}

	@Override
	public Menu createMenu(final Menu menu) throws CaRequiredException {
		return this.menuRepository.insert(menu);
	}

	@Override
	public Menu readMenu(final String menuId) throws CaRequiredException {
		Optional<Menu> optional = this.menuRepository.findById(menuId);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public Menu updateMenu(final Menu menu) throws CaRequiredException {
		Optional<Menu> optional = this.menuRepository.findById(menu.getId());
		if (optional.isPresent()) {
			Menu menu2 = optional.get();
			BeanUtils.copyProperties(menu, menu2, ConstantsUtil.ID, ConstantsUtil.CREATED_DATE);
			return this.menuRepository.save(menu2);
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public void deleteMenu(final String menuId) throws CaRequiredException {
		Optional<Menu> optional = this.menuRepository.findById(menuId);
		if (optional.isPresent()) {
			this.menuRepository.deleteByParentId(menuId);
			this.menuRepository.deleteById(menuId);
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public List<Menu> getAllMenusByLanguageCode(final String languageCode) {
		return this.menuRepository.findByLanguageCode(languageCode);
	}

	@Override
	public List<PageHeader> getPageHeaderWithCategory(List<String> ids) {
		return this.pageHeaderRepository.customFindByIdAndStateAndPublish(ids, StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

	@Override
	public Optional<PageHeader> getPageHeaderWithCategory(String id) {
		return this.pageHeaderRepository.customFindByIdAndStateAndPublish(id, StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

}
