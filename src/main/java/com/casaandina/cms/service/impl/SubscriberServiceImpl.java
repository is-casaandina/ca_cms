package com.casaandina.cms.service.impl;

import com.casaandina.cms.component.ExportJxls;
import com.casaandina.cms.dto.ParameterCodeValue;
import com.casaandina.cms.dto.QuoteEventXls;
import com.casaandina.cms.dto.SubscriberTray;
import com.casaandina.cms.dto.SubscriberXls;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.repository.*;
import com.casaandina.cms.service.EmailService;
import com.casaandina.cms.service.SubscriberService;
import com.casaandina.cms.util.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SubscriberServiceImpl implements SubscriberService {
	
	private final SubscriberRepository subscriberRepository;
	
	private final ParameterRepository parameterRepository;
	
	private final CountryRepository countryRepository;
	
	private final PageRepository pageRepository;
	
	private final MessageProperties messageProperties;

	private final ExportJxls exportJxls;

	private final QuoteEventRepository quoteEventRepository;

	private final EmailService emailService;
	
	private static final Integer INTERNAL_SERVER_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();

	private static Logger logger = LoggerFactory.getLogger(SubscriberServiceImpl.class);

	@Override
	public List<Parameter> findMotiveByStatusAndLanguage(String language) throws CaBusinessException {
		if(StringUtils.isEmpty(language) && language.trim().isEmpty()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_LANGUAGE), INTERNAL_SERVER_ERROR);
		}
		return parameterRepository.findByCodeAndLanguageCodeList(ParameterType.MOTIVE.getCode(), language);
	}

	@Override
	public List<Parameter> findGenderByStatusAndLanguage(String language) throws CaBusinessException {
		if(StringUtils.isEmpty(language) && language.trim().isEmpty()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_LANGUAGE), INTERNAL_SERVER_ERROR);
		}
		return parameterRepository.findByCodeAndLanguageCodeList(ParameterType.GENDER.getCode(), language);
	}
	
	@Override
	public Subscriber saveSubscriberContact(Subscriber subscriber) throws CaBusinessException {
		
		validateContact(subscriber);
		validContactRepository(subscriber);
		if(Objects.isNull(subscriber.getCountry().getId()))
			subscriber.setCountry(null);
		
		subscriber.setStatus(StatusType.ACTIVE.getCode());
		subscriber.setTypeSuscriber(SubscriberType.CONTACT.getCode());
		subscriber.setRegisterDate(LocalDateTime.now());
		
		return subscriberRepository.save(subscriber);	
	}
	
	private void validateContact(Subscriber subscriber) throws CaBusinessException {
		if(StringUtils.isEmpty(subscriber.getName())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_NAME_REQUIRED), INTERNAL_SERVER_ERROR);
		}
		
		if(StringUtils.isEmpty(subscriber.getEmail())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_EMAIL_REQUIRED), INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateEmail(subscriber.getEmail())){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_EMAIL_VALIDATE), INTERNAL_SERVER_ERROR);
		}
		
		if(subscriber.getMotive() == null) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_MOTIVE_REQUIRED), INTERNAL_SERVER_ERROR);
		}
		
		if(subscriber.getDestination() == null || subscriber.getDestination().getId() == null) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_DESTINATION_REQUIRED), INTERNAL_SERVER_ERROR);
		}

	}	
	
	private void validContactRepository(Subscriber subscriber) throws CaBusinessException  {

		if(Objects.nonNull(subscriber.getCountry()) && Objects.nonNull(subscriber.getCountry().getId())) {
			Optional<Country> countryOptional = countryRepository.findById(subscriber.getCountry().getId());
			if (!countryOptional.isPresent()) {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_COUNTRY_REQUIRED_NOT_EXIST), INTERNAL_SERVER_ERROR);
			}
		}

		Optional<Page> page = pageRepository.findByIdCustomized(subscriber.getDestination().getId());
		if(!page.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_DESTINATION_NOT_EXIST), INTERNAL_SERVER_ERROR);
		}
	}
	
	private void validateFieldsForNewsletter(final Subscriber subscriber) throws CaBusinessException {
		if (Objects.isNull(subscriber)) {
			throw new NullPointerException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_NEWSLETTER));
		} else {
			if (StringUtils.isEmpty(subscriber.getName())) {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_NAME_REQUIRED), INTERNAL_SERVER_ERROR);
			}
			if (!CaUtil.validateEmail(subscriber.getEmail())) {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_EMAIL_VALIDATE), INTERNAL_SERVER_ERROR);
			}
			if (StringUtils.isEmpty(subscriber.getCountry().getId())) {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_COUNTRY_REQUIRED), INTERNAL_SERVER_ERROR);
			}
			if (StringUtils.isEmpty(subscriber.getGender())) {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_GENDER_REQUIRED), INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	private void validateMoreFieldsForNewsletter(final Subscriber subscriber) throws CaBusinessException {
		validateFieldsForNewsletter(subscriber);
		if (StringUtils.isEmpty(subscriber.getRuc())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_RUC_REQUIRED), INTERNAL_SERVER_ERROR);
		}
		if (StringUtils.isEmpty(subscriber.getSegment())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_SEGMENT_REQUIRED), INTERNAL_SERVER_ERROR);
		}
	}
	
	private Subscriber subscribeToNewsletter(final Subscriber subscriber) {

		subscriber.setRegisterDate(LocalDateTime.now());
		subscriber.setStatus(StatusType.ACTIVE.getCode());
		subscriber.setTypeSuscriber(SubscriberType.NEWSLETTER.getCode());
		
		return this.subscriberRepository.insert(subscriber);
	}

	@Override
	public Subscriber subscribePerson(final Subscriber subscriber) throws CaBusinessException {
		validateFieldsForNewsletter(subscriber);
		return  subscribeToNewsletter(subscriber);
	}

	@Override
	public Subscriber subscribeLegalPerson(final Subscriber subscriber) throws CaBusinessException {
		validateMoreFieldsForNewsletter(subscriber);
		return subscribeToNewsletter(subscriber);
	}

	@Override
	public List<Subscriber> getActiveSubscriptionsList() throws CaBusinessException {
		return this.subscriberRepository.findAllByStatus(StatusType.ACTIVE.getCode());
	}
	
	public List<Subscriber> findContactByStatus() {
		return subscriberRepository.findByTypeSuscriberAndStatus(SubscriberType.CONTACT.getCode(), StatusType.ACTIVE.getCode());
	}

	@Override
	public org.springframework.data.domain.Page<Subscriber> findSubscriberByFilters(SubscriberTray subscriberTray, Pageable pageable) {

		Date startDate = CaUtil.convertStringtoDateFormatGuion(subscriberTray.getStartDate());
		Date endDate = CaUtil.convertStringtoDateFormatGuion(subscriberTray.getEndDate());
		List<String> typeSubscribers = subscriberTray.getTypeSubscriber();

		org.springframework.data.domain.Page<Subscriber> subscriberPage = null;

		if(!(Objects.nonNull(typeSubscribers) && !typeSubscribers.isEmpty())){
			List<SubscriberType> subscriberTypes = SubscriberType.getList();
			typeSubscribers = subscriberTypes.stream().map(SubscriberType::getCode).collect(Collectors.toList());
		}

		Pageable pageableSort = getPageableSortByLastUpdate(pageable);

		if(startDate != null && endDate != null){
			endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);
			subscriberPage = this.subscriberRepository.customFindByTypeSubscriberAndStatusAndRegisterDateBetween(
					typeSubscribers,
					StatusType.ACTIVE.getCode(),
					startDate,
					endDate,
					pageableSort
			);
		} else {
			subscriberPage = this.subscriberRepository.customFindByTypeSubscriberAndStatus(
					typeSubscribers,
					StatusType.ACTIVE.getCode(),
					pageableSort
			);
		}

		return subscriberPage;
	}

	public Pageable getPageableSortByLastUpdate(Pageable pageable){
		Sort sort = new Sort(Sort.Direction.DESC, "registerDate");
		return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
	}

	@Override
	public ResponseEntity downloadExcelByFilters(SubscriberTray subscriberTray) {
		return this.getExcelFromSubscribers(this.getSubscriber(subscriberTray));
	}


	@Override
	public byte[] bytesExcelByFilters(SubscriberTray subscriberTray) {
		return this.getBytesExcelFromSubscribers(this.getSubscriber(subscriberTray));
	}

	private List<Subscriber> getSubscriber(SubscriberTray subscriberTray) {
		Date startDate = CaUtil.convertStringtoDateFormatGuion(subscriberTray.getStartDate());
		Date endDate = CaUtil.convertStringtoDateFormatGuion(subscriberTray.getEndDate());
		List<String> typeSubscribers = subscriberTray.getTypeSubscriber();

		List<Subscriber> subscribers = null;

		if(!(Objects.nonNull(typeSubscribers) && !typeSubscribers.isEmpty())){
			List<SubscriberType> subscriberTypes = SubscriberType.getList();
			typeSubscribers = subscriberTypes.stream().map(SubscriberType::getCode).collect(Collectors.toList());
		}

		Sort sort = new Sort(Sort.Direction.DESC, "registerDate");

		if(startDate != null && endDate != null) {
			endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);
			subscribers = this.subscriberRepository.findByTypeSuscriberInAndStatusAndRegisterDateBetweenOrderByRegisterDateDesc(
					typeSubscribers,
					StatusType.ACTIVE.getCode(),
					startDate,
					endDate,
					sort
			);
		} else {
			subscribers = this.subscriberRepository.findByTypeSuscriberInAndStatusOrderByRegisterDateDesc(
					typeSubscribers,
					StatusType.ACTIVE.getCode(),
					sort
			);
		}

		return subscribers;
	}

	private Map getMapExcelFromSubscribers(List<Subscriber> subscribers) {
		Map<Integer, String> mapMotives = getMotivesForContact();

		List<SubscriberXls> contact = new ArrayList<>();
		List<SubscriberXls> person = new ArrayList<>();
		List<SubscriberXls> personLegal = new ArrayList<>();
		List<QuoteEventXls> quoteEventXls = new ArrayList<>();

		List<Subscriber> subscribersQuote = subscribers.stream()
				.filter(x -> SubscriberType.QUOTE.getCode().equals(x.getTypeSuscriber()))
				.collect(Collectors.toList());

		List<Subscriber> subscribersList = subscribers.stream()
				.filter(x -> !SubscriberType.QUOTE.getCode().equals(x.getTypeSuscriber()))
				.collect(Collectors.toList());

		if(Objects.nonNull(subscribersList) && !subscribersList.isEmpty()) {

			Optional<Parameter> parameter = this.parameterRepository.findByCodeAndLanguageCode(ParameterType.SEGMENT.getCode(),
					LanguageType.SPANISH.getCode());

			List<ParameterCodeValue> parameterCodeValue = ObjectMapperUtils.mapAll((List) parameter.get().getIndicator(), ParameterCodeValue.class);
			Map<String, ParameterCodeValue> parameterCodeValueMap = parameterCodeValue.stream().collect(Collectors.toMap(ParameterCodeValue::getCode, Function.identity()));

			subscribersList.forEach(x -> {
				if (SubscriberType.CONTACT.getCode().equals(x.getTypeSuscriber())) {
					contact.add(getContactFromSubscriber(x, mapMotives));
				} else if (SubscriberType.NEWSLETTER.getCode().equals(x.getTypeSuscriber())) {
					if (Objects.nonNull(x.getRuc())) {
						personLegal.add(getPersonLegalFromSubscriber(x, parameterCodeValueMap));
					} else {
						person.add(getPersonFromSubscriber(x));
					}
				}
			});
		}

		if(Objects.nonNull(subscribersQuote) && !subscribersQuote.isEmpty()){
			quoteEventXls.addAll(getQuotesFromSubscriber(subscribersQuote));
		}

		Map<String, Object> mapBeans = new HashMap<>();
		mapBeans.put("dateGeneration", CaUtil.convertLocalDateTimeToString(LocalDateTime.now()));
		mapBeans.put("contact", contact);
		mapBeans.put("person", person);
		mapBeans.put("legalPerson", personLegal);
		mapBeans.put("quoteEvent", quoteEventXls);

		return mapBeans;
	}

	private byte[] getBytesExcelFromSubscribers(List<Subscriber> subscribers){
		if(Objects.nonNull(subscribers) && !subscribers.isEmpty()){
			return exportJxls.getBytesExcelJxls(this.getMapExcelFromSubscribers(subscribers), "subscribers");
		}
		return null;
	}

	private ResponseEntity getExcelFromSubscribers(List<Subscriber> subscribers){
		if(Objects.nonNull(subscribers) && !subscribers.isEmpty()){
			return exportJxls.buildExcelJxls(this.getMapExcelFromSubscribers(subscribers), "subscribers");
		}
		return ResponseEntity
				.status(HttpStatus.FORBIDDEN)
				.body("Error Message");
	}

	private Map<Integer, String> getMotivesForContact(){
		Optional<Parameter> parameterOptional = this.parameterRepository.findByCodeAndLanguageCode(ParameterType.CONTACT_REASON.getCode(),
				LanguageType.SPANISH.getCode());
		Map<Integer, String> mapMotives = new HashMap<>();
		if(parameterOptional.isPresent()){
			List<ParameterCodeValue> parameterCodeValue = ObjectMapperUtils.mapAll((List) parameterOptional.get().getIndicator(), ParameterCodeValue.class);
			if(Objects.nonNull(parameterCodeValue) && !parameterCodeValue.isEmpty()){
				parameterCodeValue.forEach(x ->
					mapMotives.put(Integer.parseInt(x.getCode()), x.getValue())
				);
				return mapMotives;
			}
		}
		return mapMotives;
	}

	private SubscriberXls getContactFromSubscriber(Subscriber subscriber, Map<Integer, String> motives){

		String empty = ConstantsUtil.EMPTY;

		return SubscriberXls.builder()
				.name(Objects.nonNull(subscriber.getName()) ? subscriber.getName() : empty)
				.email(Objects.nonNull(subscriber.getEmail()) ? subscriber.getEmail() : empty)
				.phone(Objects.nonNull(subscriber.getPhone()) ? subscriber.getPhone() : empty)
				.country(Objects.nonNull(subscriber.getCountry()) ? subscriber.getCountry().getName() : empty)
				.destination(getNamePage(subscriber.getDestination()))
				.gender(getGenderString(subscriber.getGender()))
				.motive(getMotiveString(subscriber.getMotive(), motives))
				.address(Objects.nonNull(subscriber.getAddress()) ? subscriber.getAddress() : empty)
				.message(Objects.nonNull(subscriber.getMessage()) ? subscriber.getMessage() : empty)
				.registerDate(CaUtil.convertLocalDateTimeToString(subscriber.getRegisterDate()))
				.build();
	}

	private SubscriberXls getPersonFromSubscriber(Subscriber subscriber){

		String empty = ConstantsUtil.EMPTY;

		return SubscriberXls.builder()
				.name(Objects.nonNull(subscriber.getName()) ? subscriber.getName() : empty)
				.email(Objects.nonNull(subscriber.getEmail()) ? subscriber.getEmail() : empty)
				.phone(Objects.nonNull(subscriber.getPhone()) ? subscriber.getPhone() : empty)
				.country(Objects.nonNull(subscriber.getCountry()) ? subscriber.getCountry().getName() : empty)
				.city(Objects.nonNull(subscriber.getCity()) ? subscriber.getCity() : empty)
				.registerDate(CaUtil.convertLocalDateTimeToString(subscriber.getRegisterDate()))
				.build();
	}

	private SubscriberXls getPersonLegalFromSubscriber(Subscriber subscriber, Map<String, ParameterCodeValue> parameterCodeValueMap){

		String empty = ConstantsUtil.EMPTY;
		String segment;
		if(Objects.nonNull(subscriber.getSegment())){
			ParameterCodeValue parameterCodeValue = parameterCodeValueMap.get(subscriber.getSegment().toString());
			segment = Objects.nonNull(parameterCodeValue) ? parameterCodeValue.getValue() : empty;
		} else {
			segment = empty;
		}

		return SubscriberXls.builder()
				.name(Objects.nonNull(subscriber.getName()) ? subscriber.getName() : empty)
				.surname(Objects.nonNull(subscriber.getSurname()) ? subscriber.getSurname() : empty)
				.businessName(Objects.nonNull(subscriber.getBusinessName()) ? subscriber.getBusinessName() : empty)
				.ruc(Objects.nonNull(subscriber.getRuc()) ? subscriber.getRuc() : empty)
				.email(Objects.nonNull(subscriber.getEmail()) ? subscriber.getEmail() : empty)
				.phone(Objects.nonNull(subscriber.getPhone()) ? subscriber.getPhone() : empty)
				.country(Objects.nonNull(subscriber.getCountry()) ? subscriber.getCountry().getName() : empty)
				.city(Objects.nonNull(subscriber.getCity()) ? subscriber.getCity() : empty)
				.gender(getGenderString(subscriber.getGender()))
				.birthdate(subscriber.getBirthdate())
				.registerDate(CaUtil.convertLocalDateTimeToString(subscriber.getRegisterDate()))
				.segment(segment)
				.build();
	}

	private List<QuoteEventXls> getQuotesFromSubscriber(List<Subscriber> subscribers){
		List<QuoteEventXls> quoteEventXls = new ArrayList<>();
		if(Objects.nonNull(subscribers) && !subscribers.isEmpty()){
			List<QuoteEvent> quoteEvents = this.quoteEventRepository.findBySubscriberIn(subscribers);
			if(Objects.nonNull(quoteEvents) && !quoteEvents.isEmpty()){
				quoteEvents.forEach(x ->
					quoteEventXls.add(getQuoteEventXlsFromQuoteEvent(x))
				);
			}
		}
		return quoteEventXls;
	}

	private QuoteEventXls getQuoteEventXlsFromQuoteEvent(QuoteEvent quoteEvent){
		if(Objects.nonNull(quoteEvent)){
			String empty = ConstantsUtil.EMPTY;

			String armedTypes = quoteEvent.getArmedTypes()!= null
					? buildString(quoteEvent.getArmedTypes())
					: ConstantsUtil.EMPTY;

			String foodOptions = quoteEvent.getFoodOptions()!= null
					? buildString(quoteEvent.getFoodOptions())
					: ConstantsUtil.EMPTY;

			String audioVisualEquipments = quoteEvent.getAudiovisualEquipments()!= null
					? buildString(quoteEvent.getAudiovisualEquipments())
					: ConstantsUtil.EMPTY;

			return QuoteEventXls.builder()
					.name((Objects.nonNull(quoteEvent.getSubscriber()) && Objects.nonNull(quoteEvent.getSubscriber().getName()))
							? quoteEvent.getSubscriber().getName() : empty)
					.email((Objects.nonNull(quoteEvent.getSubscriber()) && Objects.nonNull(quoteEvent.getSubscriber().getEmail()))
							? quoteEvent.getSubscriber().getEmail() : empty)
					.phone((Objects.nonNull(quoteEvent.getSubscriber()) && Objects.nonNull(quoteEvent.getSubscriber().getPhone()))
							? quoteEvent.getSubscriber().getPhone() : empty)
					.page(getNamePage(quoteEvent.getPage()))
					.quantityAssistants(quoteEvent.getQuantityAssistants())
					.beginDate(quoteEvent.getBeginDate())
					.endDate(quoteEvent.getEndDate())
					.beginHour(quoteEvent.getBeginHour())
					.endHour(quoteEvent.getEndHour())
					.armedTypes(armedTypes)
					.foodOptions(foodOptions)
					.audiovisualEquipments(audioVisualEquipments)
					.additionalComments(quoteEvent.getAdditionalComments())
					.registerDate(CaUtil.convertLocalDateTimeToString(quoteEvent.getCreatedDate()))
					.build();
		}
		return null;
	}

	public String buildString(List<String> listString){
		StringBuilder build = new StringBuilder();
		int i = 0;
		for(String obj : listString){
			if(i == (listString.size() - ConstantsUtil.ONE))
				build.append(obj);
			else
				build.append(obj + ", ");
			i++;
		}
		return build.toString();
	}

	private String getGenderString(Integer gender){
		String genderString = ConstantsUtil.EMPTY;
		if(Objects.nonNull(gender)){
			GenderType genderType = GenderType.get(gender);
			if(Objects.nonNull(genderType)){
				genderString = genderType.getValue();
			}
		}
		return genderString;
	}

	private String getMotiveString(Integer motive, Map<Integer, String> mapMotives){
		String motiveString = ConstantsUtil.EMPTY;
		if(Objects.nonNull(motive) && !mapMotives.isEmpty()){
			motiveString = mapMotives.get(motive);
		}

		return motiveString;
	}

	private String getNamePage(Page page){
		String namePage = ConstantsUtil.EMPTY;
		if(Objects.nonNull(page) && Objects.nonNull(page.getLanguages()) &&
				!page.getLanguages().isEmpty()){
			LanguagePage languagePage = page.getLanguages()
					.stream()
					.filter(x-> LanguageType.SPANISH.getCode().equals(x.getLanguage()))
					.findFirst().orElse(null);
			if(Objects.nonNull(languagePage)){
				namePage = languagePage.getTitle();
			}
		}
		return namePage;
	}

	@Override
	public void deleteSubscriber(String id)throws CaBusinessException {
		Optional<Subscriber> optionalSubscriber = this.subscriberRepository.findByIdAndStatus(id, StatusType.ACTIVE.getCode());
		if(!optionalSubscriber.isPresent()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_DELETE_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		Subscriber subscriber = optionalSubscriber.get();

		if(SubscriberType.QUOTE.getCode().equals(subscriber.getTypeSuscriber())){
			Optional<QuoteEvent> quoteEventOptional = this.quoteEventRepository.findBySubscriber(subscriber);
			if(quoteEventOptional.isPresent()){
				this.quoteEventRepository.delete(quoteEventOptional.get());
			}
		}

		subscriber.setStatus(StatusType.INACTIVE.getCode());
		this.subscriberRepository.save(subscriber);
	}

	@Async
	@Override
	public void sendMailPerson(Subscriber subscriber, String code){
		try{
			if(Objects.nonNull(subscriber) && Objects.nonNull(subscriber.getEmail())){
				Parameter parameter = this.parameterRepository.customFindRoibackWebServiceByCode(code);
				if(Objects.nonNull(parameter)){
					emailService.sendMail(subscriber.getEmail(), ConstantsUtil.WELCOME_TO_CASA_ANDINA, (String) parameter.getIndicator());
				}
			}
		} catch(Exception ex) {
			logger.error(String.format("There was an error sending mail %s",ex.getMessage()));
		}
	}

}
