package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Benefits;
import com.casaandina.cms.repository.BenefitsRepository;
import com.casaandina.cms.service.BenefitsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BenefitsServiceImpl implements BenefitsService {

    private final BenefitsRepository benefitsRepository;

    public BenefitsServiceImpl(BenefitsRepository benefitsRepository) {
        this.benefitsRepository = benefitsRepository;
    }

    @Override
    public Optional<Benefits> get() {
        return this.benefitsRepository.findAll().stream().findFirst();
    }

    @Override
    public Benefits save(Benefits benefits) throws CaBusinessException, CaRequiredException {
        return this.benefitsRepository.save(benefits);
    }
}
