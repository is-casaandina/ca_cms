package com.casaandina.cms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import com.casaandina.cms.model.Sequence;
import com.casaandina.cms.service.SequenceService;

import org.springframework.data.mongodb.core.query.Update;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class SequenceServiceImpl implements SequenceService {
	
	private final MongoOperations mongoOperations;
	
	@Autowired
	public SequenceServiceImpl(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	@Override
	public Integer getNextSequence(String sequenceName) {
		Sequence sequence = mongoOperations.findAndModify(
                query(where("_id").is(sequenceName)),
                new Update().inc("currentValue", 1),
                options().returnNew(true).upsert(true),
                Sequence.class);
		return sequence.getCurrentValue();
	}

}
