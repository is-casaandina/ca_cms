package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Descriptor;
import com.casaandina.cms.repository.DescriptorRepository;
import com.casaandina.cms.service.DescriptorService;
import com.casaandina.cms.util.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class DescriptorServiceImpl implements DescriptorService {
	
	private final DescriptorRepository descriptorRepository;
	
	private final MessageProperties messageProperties;
	
	@Autowired
	public DescriptorServiceImpl(DescriptorRepository tagRepository, MessageProperties messageProperties) {
		this.descriptorRepository = tagRepository;
		this.messageProperties = messageProperties;
	}

	@Override
	public Descriptor createTag(final Descriptor tag) throws CaRequiredException {
		Optional<Descriptor> optional = this.descriptorRepository.findByTitle(tag.getTitle());
		if (optional.isPresent()) {
			throw new CaRequiredException(this.messageProperties.getMessage(PropertiesConstants.REGISTERED_TITLE_MESSAGE));
		} else {
			return this.descriptorRepository.insert(tag);
		}
	}

	@Override
	public Descriptor readTag(final String tagId) throws CaRequiredException {
		Optional<Descriptor> optional = this.descriptorRepository.findById(tagId);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public Descriptor updateTag(Descriptor newTag) throws CaRequiredException {
		Optional<Descriptor> optional = this.descriptorRepository.findById(newTag.getId());
		if (optional.isPresent()) {
			Descriptor tag = optional.get();
			BeanUtils.copyProperties(newTag, tag, ConstantsUtil.ID, ConstantsUtil.CREATED_DATE);
			return this.descriptorRepository.save(tag);
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public void deleteTag(String tagId) {
		this.descriptorRepository.deleteById(tagId);
	}

	@Override
	public List<Descriptor> getAllTheTags() {
		return this.descriptorRepository.findAll();
	}

	@Override
	public List<Descriptor> getAllByCategory(String category) {
		if(CaUtil.validateIsNotNullAndNotEmpty(category)){
			if(TemplateType.BELL.getCode().equals(category)){
				List<Descriptor> listDescriptor = new ArrayList<>();
				listDescriptor.add(Descriptor.builder().id(ConstantsUtil.DESCRIPTOR_ALL).title(ConstantsUtil.DESCRIPTOR_ALL).build());
				return listDescriptor;
			} else {
				return this.descriptorRepository.findByCategory(category);
			}
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	public List<Descriptor> customFindAllDescriptors() {
		return this.descriptorRepository.customFindAllDescriptors();
	}
}
