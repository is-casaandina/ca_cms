package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Country;
import com.casaandina.cms.repository.CountryRepository;
import com.casaandina.cms.service.CountryService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryServiceImpl implements CountryService {
	
	private final CountryRepository countryRepository;
	
	private final MessageProperties messageProperties;

	@Autowired
	public CountryServiceImpl(CountryRepository countryRepository, MessageProperties messageProperties) {
		this.countryRepository = countryRepository;
		this.messageProperties = messageProperties;
	}
	
	@Override
	public Country saveCountry(final Country country) throws CaBusinessException, CaRequiredException {
		
		Optional<Country> optional = this.countryRepository.findByCode(country.getCode());
		if (optional.isPresent()) {
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.REGISTERED_COUNTRY_CODE));
		} else {
			return this.countryRepository.insert(country);
		}

	}
	
	@Override
	public Country getCountry(final String countryId) throws CaRequiredException {
		Optional<Country> optional = this.countryRepository.findById(countryId);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}
	
	@Override
	public Country updateCountry(final String countryId, final Country country)
			throws CaBusinessException, CaRequiredException {
		
		Optional<Country> optional = this.countryRepository.findById(countryId);
		
		if (optional.isPresent()) {
			Country country2 = optional.get();
			BeanUtils.copyProperties(country, country2, ConstantsUtil.ID);
			return this.countryRepository.save(country2);
		} else {
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.UNREGISTERED_COUNTRY_CODE));
		}
		
	}
	
	@Override
	public void deleteCountry(final String countryId) throws CaRequiredException {
		Optional<Country> optional = this.countryRepository.findById(countryId);
		if (optional.isPresent()) {
			this.countryRepository.delete(optional.get());
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}
	
	@Override
	public List<Country> getCountriesList() throws CaBusinessException {
		return this.countryRepository.findAll();
	}
	
	@Override
	public Country getCountryByCode(final String countryCode) throws CaRequiredException {
		Optional<Country> optional = this.countryRepository.findByCode(countryCode);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
		}
	}

	@Override
	public List<Country> customFindAllByStatus() {
		return countryRepository.customFindAllByStatus(Boolean.TRUE, Boolean.TRUE);
	}

}
