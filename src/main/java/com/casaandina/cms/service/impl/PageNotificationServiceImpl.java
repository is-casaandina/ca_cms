package com.casaandina.cms.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.LanguagePageNotification;
import com.casaandina.cms.model.PageNotification;
import com.casaandina.cms.repository.PageNotificationRepository;
import com.casaandina.cms.service.PageNotificationService;
import com.casaandina.cms.service.SequenceService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.pagination.Page;
import com.casaandina.cms.util.pagination.PageUtil;

@Service
public class PageNotificationServiceImpl implements PageNotificationService {

    private final MessageProperties messageProperties;
    private final PageNotificationRepository pageNotificationRepository;
    private final SequenceService sequenceService;

    @Autowired
    public PageNotificationServiceImpl(PageNotificationRepository pageNotificationRepository,
                                       SequenceService sequenceService,
                                       MessageProperties messageProperties) {
        this.pageNotificationRepository = pageNotificationRepository;
        this.sequenceService = sequenceService;
        this.messageProperties = messageProperties;
    }

    @Override
    public void insertPageNotification(PageNotification pageNotification) throws CaBusinessException, CaRequiredException {

        Integer sequence = sequenceService.getNextSequence(ConstantsUtil.PAGE_NOTIFICATION_SEQUENCE);
        pageNotification.setId(sequence);
        pageNotification.setStatus(ConstantsUtil.ACTIVE);

        pageNotificationRepository.insert(pageNotification);
    }

    @Override
    public void updatePageNotification(Integer pageNotificationId, PageNotification pageNotification) throws CaBusinessException, CaRequiredException {

        Optional<PageNotification> pageNotificationExist = pageNotificationRepository.findByIdAndStatus(pageNotificationId, ConstantsUtil.ACTIVE);

        if (pageNotificationExist.isPresent()) {
            PageNotification pageNotificationUpdate = pageNotificationExist.get();

            pageNotificationUpdate.setPageId(pageNotification.getPageId());
            pageNotificationUpdate.setName(pageNotification.getName());
            pageNotificationUpdate.setBeginDate(pageNotification.getBeginDate());
            pageNotificationUpdate.setEndDate(pageNotification.getEndDate());
            pageNotificationUpdate.setLanguages(pageNotification.getLanguages());

            pageNotificationRepository.save(pageNotificationUpdate);
        } else
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_PAGE_NOTIFICATION_NOT_REGISTERED), HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @Override
    public void deletePageNotification(Integer id) throws CaBusinessException, CaRequiredException {
        Optional<PageNotification> pageNotificationExist = pageNotificationRepository.findByIdAndStatus(id, ConstantsUtil.ACTIVE);

        if (pageNotificationExist.isPresent()) {
            PageNotification pageNotificationUpdate = pageNotificationExist.get();
            pageNotificationUpdate.setStatus(ConstantsUtil.INACTIVE);

            pageNotificationRepository.save(pageNotificationUpdate);
        } else
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_PAGE_NOTIFICATION_NOT_REGISTERED), HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @Override
    public PageNotification getByPageNotificationId(Integer id) {
        PageNotification pageNotification = null;
        Optional<PageNotification> pageNotificationOptional = pageNotificationRepository.findById(id);

        if (pageNotificationOptional.isPresent()) {
            pageNotification = new PageNotification();
            BeanUtils.copyProperties(pageNotificationOptional.get(), pageNotification);
        }
        return pageNotification;
    }

    @Override
    public List<LanguagePageNotification> listByPageId(String languageCode, String pageId) {
        List<LanguagePageNotification> list = pageNotificationRepository.findByPageIdAndLanguageCode(pageId, languageCode);
        return list;
    }

    @Override
    public Page<PageNotification> listPaginated(Boolean status, Pageable pageable) {
        org.springframework.data.domain.Page<PageNotification> pageNotification;
        pageNotification = pageNotificationRepository.findByStatus(status, pageable);
        return PageUtil.pageToResponse(pageNotification);
    }
}
