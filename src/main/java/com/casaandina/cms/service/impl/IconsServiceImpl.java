package com.casaandina.cms.service.impl;

import com.casaandina.cms.aws.AmazonClient;
import com.casaandina.cms.dto.IconsSelectionDto;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Configurations;
import com.casaandina.cms.model.Icons;
import com.casaandina.cms.repository.ConfigurationsRepository;
import com.casaandina.cms.repository.IconsRepository;
import com.casaandina.cms.service.IconsService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
public class IconsServiceImpl implements IconsService {
	
	 private static final Logger logger = LoggerFactory.getLogger(IconsServiceImpl.class);

	private final AmazonClient amazonClient;
	private final IconsRepository iconsRepository;
	private final MessageProperties messageProperties;
	private final ConfigurationsRepository configurationsRepository;
	
	@Value("${aws.endpointUrl}")
	private String endpointUrl;

	@Value("${aws.bucketName}")
	private String bucketName;

	private static final String DOBLE_SLASH = "\\";
	private static final String SLASH = "/";
	private static final String FONTS_STRING = "fonts";
	private static final String DIRECTORY_ICONS = "icons";
	private static final String OTHER = "Other";
	private static final String SELECTION_JSON = "selection.json";
	private static final String STYLE = "style.css";

	@Autowired
	public IconsServiceImpl(AmazonClient amazonClient, IconsRepository iconsRepository, MessageProperties messageProperties, ConfigurationsRepository configurationsRepository) {
		this.amazonClient = amazonClient;
		this.iconsRepository = iconsRepository;
		this.messageProperties = messageProperties;
		this.configurationsRepository = configurationsRepository;
	}

	public void uploadIconsFilesFromZip(MultipartFile fileZip) throws CaBusinessException {
		try {
			
			unZipAndCreateFiles(fileZip);
			
			List<String> arraysIconFiles = Arrays.asList(ConstantsUtil.ARRAYS_ICONS_FILES);
			arraysIconFiles.forEach( x -> {
				try {
					if(!x.equals(FONTS_STRING + SLASH))
						uploadToS3(x);
				} catch (CaBusinessException e) {
					logger.error(e.getMessage());
				}
			});
			
			readJsonAndWriteInMongoDb();
			
			deleteFilesUnZip();
			
		} catch (CaBusinessException ca) {
			throw new CaBusinessException(ca.getMessage(), ConstantsUtil.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_SVG_UPLOAD_ERROR) + ": " + e.getMessage(), ConstantsUtil.INTERNAL_SERVER_ERROR);
		} 
	}
	
	private void uploadToS3(String fileName) throws CaBusinessException {
		try {
			File file = new File(fileName);
			if(ConstantsUtil.STYLE_CSS.equals(fileName)){
				updateConfigurationFontIcons(fileName);
			}
			amazonClient.uploadFileToS3Bucket(DIRECTORY_ICONS, fileName, file);		
			
		} catch (Exception e) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_SVG_UPLOAD_ERROR) + ConstantsUtil.SPACE + e.getMessage(), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}	
	}

	private void updateConfigurationFontIcons(String fileName){
		List<Configurations> configurations =  this.configurationsRepository.findAll();
		if(Objects.nonNull(configurations) && !configurations.isEmpty()){
			Configurations configuration = configurations.stream().findFirst().orElse(null);
			if(Objects.nonNull(configuration)){
				String url = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR +
						DIRECTORY_ICONS + ConstantsUtil.SEPARATOR + fileName;
				configuration.setFontIcons(url);
				this.configurationsRepository.save(configuration);
			}
		}
	}

	private void unZipAndCreateFiles(MultipartFile fileZip) throws CaBusinessException {
		try {

			ZipInputStream zis = new ZipInputStream(fileZip.getInputStream());
			ZipEntry zipEntry = zis.getNextEntry();

			List<String> arraysIconFiles = Arrays.asList(ConstantsUtil.ARRAYS_ICONS_FILES);

			while (zipEntry != null) {
				String fileName = zipEntry.getName();
				if (arraysIconFiles.contains(fileName)) {
					String localPath = System.getProperty(ConstantsUtil.USER_DIR);
					String opFilePath;
					if(SLASH.equals(localPath)){
						opFilePath = fileName;
					} else {
						opFilePath = localPath.replace(DOBLE_SLASH, SLASH) + SLASH + fileName;
					}
					
					if (fileName.equals(FONTS_STRING + SLASH)) {
						if(SLASH.equals(localPath)){
							File file = new File(FONTS_STRING);
							file.mkdir();
						} else {
							File file = new File(localPath + DOBLE_SLASH + FONTS_STRING);
							file.mkdir();
						}
					} else {
						FileOutputStream fos = new FileOutputStream(opFilePath);
						
						byte[] tmp = new byte[ConstantsUtil.BUFFER_LENGTH];

						int size = 0;
						while ((size = zis.read(tmp)) != -1) {
							fos.write(tmp, 0, size);
						}
						fos.close();
						fos.flush();
					}
				}				

				zipEntry = zis.getNextEntry();
			}

			zis.close();

		} catch (Exception e) {
			throw new CaBusinessException(
					messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_SVG_UPLOAD_ERROR) + ConstantsUtil.SPACE + e.getMessage(),
					ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
	}

	private void deleteFilesUnZip(){
		List<String> arraysIconFiles = Arrays.asList(ConstantsUtil.ARRAYS_ICONS_FILES);
		arraysIconFiles.forEach( x -> {
			try {
				if(x.equals(FONTS_STRING + SLASH)) {
					FileUtils.deleteDirectory(new File(FONTS_STRING));
				} else if(x.equals(SELECTION_JSON) || x.equals(STYLE)) {
					Files.deleteIfExists(Paths.get(x));
				}				
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		});		
	}
	
	private void readJsonAndWriteInMongoDb() throws CaBusinessException {
		JSONParser jsonParser = new JSONParser();
		
		try(FileReader reader = new FileReader(SELECTION_JSON))
		{
			Object obj = jsonParser.parse(reader);
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			
			IconsSelectionDto iconsSelection  = new IconsSelectionDto();
			
			iconsSelection = objectMapper.readValue(obj.toString(), IconsSelectionDto.class);
			
			iconsRepository.deleteAllByCustom(Boolean.TRUE);
			
			String prefix = iconsSelection.getPreferences().getFontPref().getPrefix();
			
			List<Icons> listIcons = new ArrayList<>();
			List<String> filters = new ArrayList<>();
			filters.add(OTHER);
			
			iconsSelection.getIcons().forEach( x -> {
				String className = prefix + x.getProperties().getName();
				listIcons.add(Icons.builder()
					.id(className)
					.className(className)
					.custom(Boolean.TRUE)
					.filters(filters)
					.build());
			});
			
			iconsRepository.saveAll(listIcons);
			
		} catch (IOException|ParseException e) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_SVG_UPLOAD_SELECTION_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
	}
	
	@Cacheable("icons")
	@Override
	public List<Icons> findAllIcons() {
		return iconsRepository.findAllCustomized();
	}
}
