package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Parameter;
import com.casaandina.cms.repository.ParameterRepository;
import com.casaandina.cms.service.ParameterService;
import com.casaandina.cms.util.ConstantsUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParameterServiceImpl implements ParameterService {
	
	private final ParameterRepository parameterRepository;
	
	@Autowired
	public ParameterServiceImpl(ParameterRepository parameterRepository) {
		this.parameterRepository = parameterRepository;
	}

	@Override
	public Parameter createParameter(final Parameter parameter) {
		return this.parameterRepository.insert(parameter);
	}

	@Override
	public Parameter getParameter(final String parameterId) throws CaRequiredException {
		Optional<Parameter> optional = this.parameterRepository.findById(parameterId);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public Parameter updateParameter(final Parameter parameter) throws CaRequiredException {
		Optional<Parameter> optional = this.parameterRepository.findById(parameter.getId());
		if (optional.isPresent()) {
			Parameter parameter2 = optional.get();
			BeanUtils.copyProperties(parameter, parameter2, ConstantsUtil.ID);
			return this.parameterRepository.save(parameter2);
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public void deleteParameter(final String parameterId) throws CaRequiredException {
		Optional<Parameter> optional = this.parameterRepository.findById(parameterId);
		if (optional.isPresent()) {
			this.parameterRepository.deleteById(parameterId);
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}
	
	@Override
	public List<Parameter> getAllTheParameters() throws CaRequiredException {
		return this.parameterRepository.findAll();
	}

	@Override
	public Parameter getStatesAll(final String code) throws CaRequiredException {
		
		List<Parameter> parameters = this.parameterRepository.findByCode(code);
		
		if (parameters.isEmpty()) {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
		} else {
			return parameters.get(0);
		}
		
	}

	@Override
	public Parameter getStateByCodeAndLanguage(String code, String languageCode) throws CaRequiredException {

		Optional<Parameter> parameterOptional = this.parameterRepository.findByCodeAndLanguageCode(code, languageCode);

		if (!parameterOptional.isPresent()) {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
		} else {
			return parameterOptional.get();
		}

	}

}
