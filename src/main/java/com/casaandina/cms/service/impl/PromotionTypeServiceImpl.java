package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.PromotionType;
import com.casaandina.cms.repository.PromotionTypeRepository;
import com.casaandina.cms.service.PromotionTypeService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.StatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class PromotionTypeServiceImpl implements PromotionTypeService {

    private final PromotionTypeRepository promotionTypeRepository;
    private final MessageProperties messageProperties;

    private static final Integer NOT_ACCEPTABLE = HttpStatus.NOT_ACCEPTABLE.value();

    @Autowired
    public PromotionTypeServiceImpl(PromotionTypeRepository promotionTypeRepository, MessageProperties messageProperties) {
        this.promotionTypeRepository = promotionTypeRepository;
        this.messageProperties = messageProperties;
    }

    @Override
    public List<PromotionType> findByStateAll() {
        return this.promotionTypeRepository.findByState(StatusType.ACTIVE.getCode());
    }

    @Override
    public List<PromotionType> getPromotionTypeByLanguage(String language) {
        return promotionTypeRepository.customFindByStateAndLanguage(StatusType.ACTIVE.getCode(), language);
    }

    @Override
    public PromotionType savePromotion(PromotionType promotionType) throws CaBusinessException {

        if(CaUtil.validateListIsNotNullOrEmpty(promotionType.getLanguages())){
            return promotionTypeRepository.save(PromotionType.builder()
                    .id(UUID.randomUUID().toString())
                    .languages(promotionType.getLanguages())
                    .createdDate(LocalDateTime.now())
                    .state(StatusType.ACTIVE.getCode())
                    .build());
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PROMOTION_TYPE_DESCRIPTION_REQUIRED),NOT_ACCEPTABLE);
        }
    }
}
