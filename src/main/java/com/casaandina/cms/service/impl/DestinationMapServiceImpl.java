package com.casaandina.cms.service.impl;

import com.casaandina.cms.model.DestinationMap;
import com.casaandina.cms.repository.DestinationMapRepository;
import com.casaandina.cms.service.DestinationMapService;
import com.casaandina.cms.util.StatusType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DestinationMapServiceImpl implements DestinationMapService {

    private final DestinationMapRepository destinationMapRepository;

    @Override
    public List<DestinationMap> findByCountryIdAndState(String countryId) {
        return this.destinationMapRepository.findByCountryIdAndState(countryId, StatusType.ACTIVE.getCode());
    }
}
