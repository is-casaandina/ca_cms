package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Domain;
import com.casaandina.cms.repository.DomainRepository;
import com.casaandina.cms.rest.UserAuthentication;
import com.casaandina.cms.service.DomainService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.StatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class DomainServiceImpl implements DomainService {

    private final DomainRepository domainRepository;
    private final UserAuthentication userAuthentication;
    private final MessageProperties messageProperties;

    private static final Integer NOT_ACCEPTABLE = HttpStatus.NOT_ACCEPTABLE.value();

    @Autowired
    public DomainServiceImpl(DomainRepository domainRepository, UserAuthentication userAuthentication, MessageProperties messageProperties) {
        this.domainRepository = domainRepository;
        this.userAuthentication = userAuthentication;
        this.messageProperties = messageProperties;
    }

    @Override
    public List<Domain> getAllDomainsByState(){
        return this.domainRepository.findByState(StatusType.ACTIVE.getCode());
    }

    @Override
    public Domain insertDomain(Domain domain) throws CaBusinessException {
        validateDomain(domain);

        String userName = userAuthentication.getUserPrincipal().getUsername();
        LocalDateTime now = LocalDateTime.now();

        return this.domainRepository.save(Domain.builder()
                .path(domain.getPath())
                .state(StatusType.ACTIVE.getCode())
                .createdDate(now)
                .modifiedDate(now)
                .owner(userName)
                .user(userName)
                .tax(domain.getTax())
                .promotionAllId(domain.getPromotionAllId())
                .language(domain.getLanguage())
                .currency(domain.getCurrency())
                .hideLanguage(domain.getHideLanguage())
                .build());
    }

    @Override
    public Domain updateDomain(Domain domain) throws CaBusinessException {
        validateDomain(domain);

        String userName = userAuthentication.getUserPrincipal().getUsername();
        LocalDateTime now = LocalDateTime.now();

        Optional<Domain> domainOptional = this.domainRepository.findByIdAndState(domain.getId(), StatusType.ACTIVE.getCode());

        if(!domainOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_DOMAIN_UPDATE_PATH_NOT_EXIST), NOT_ACCEPTABLE);
        }

        Domain domainSave = domainOptional.get();
        domainSave.setPath(domain.getPath());
        domainSave.setTax(domain.getTax());
        domainSave.setPromotionAllId(domain.getPromotionAllId());
        domainSave.setModifiedDate(now);
        domainSave.setUser(userName);
        domainSave.setLanguage(domain.getLanguage());
        domainSave.setCurrency(domain.getCurrency());
        domainSave.setHideLanguage(domain.getHideLanguage());

        return this.domainRepository.save(domainSave);
    }

    private void validateDomain(Domain domain) throws CaBusinessException {
        if(!(Objects.nonNull(domain) && CaUtil.validateIsNotNullAndNotEmpty(domain.getPath()))){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_DOMAIN_PATH_REQUIRED), NOT_ACCEPTABLE);
        }

        Optional<Domain> domainOptional;

        if(Objects.nonNull(domain.getId())){
            domainOptional = this.domainRepository.customFindByPathAndStateAndIdNot(domain.getPath(),
                    StatusType.ACTIVE.getCode(), domain.getId());
        } else {
            domainOptional = this.domainRepository.findByPathAndState(domain.getPath(), StatusType.ACTIVE.getCode());
        }

        if(domainOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_DOMAIN_PATH_EXIST), NOT_ACCEPTABLE);
        }
    }

    @Override
    public void deleteDomain(String id) throws CaBusinessException {
        Optional<Domain> domainOptional = this.domainRepository.findByIdAndState(id, StatusType.ACTIVE.getCode());

        if(!domainOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_DOMAIN_DELETE_PATH_NOT_EXIST), NOT_ACCEPTABLE);
        }

        Domain domain = domainOptional.get();
        this.domainRepository.deleteById(domain.getId());
    }

}
