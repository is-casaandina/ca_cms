package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.repository.*;
import com.casaandina.cms.rest.UserAuthentication;
import com.casaandina.cms.rest.request.PageTrayRequest;
import com.casaandina.cms.service.PageTrayService;
import com.casaandina.cms.util.TemplateType;
import com.casaandina.cms.util.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PageTrayServiceImpl implements PageTrayService {

    private final PageHeaderRepository pageHeaderRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PageJoinViewRepository pageJoinViewRepository;
    private final UserAuthentication userAuthentication;
    private final MessageProperties messageProperties;
    private final PromotionTypeRepository promotionTypeRepository;

    @Override
    public Page<PageHeader> listPageTrayByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException {

        validateUser();

        Page<PageHeader> pageHeaders = null;
        String menuCode = pageTrayRequest.getMenuCode();

        if( PermissionType.PAGE.getCode().equals(menuCode)
            || PermissionType.RESTAURANTS.getCode().equals(menuCode)
            || PermissionType.EVENTS.getCode().equals(menuCode)
            || PermissionType.ERROR.getCode().equals(menuCode)){
            pageHeaders = getPageTrayByFilters(pageTrayRequest, pageable);
        } else if(PermissionType.PROMOTIONS.getCode().equals(menuCode)){
            pageHeaders = getPromotionsByFilters(pageTrayRequest, pageable);
        } else if(PermissionType.DESTINATIONS.getCode().equals(menuCode)){
            pageHeaders = getDestinationsByFilters(pageTrayRequest, pageable);
        } else if(PermissionType.HOTELS.getCode().equals(menuCode)){
            pageHeaders = getHotelsByFilters(pageTrayRequest, pageable);
        }
        return pageHeaders;
    }

    private Page<PageHeader> getHotelsByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException {
        Integer[] states = pageTrayRequest.getStates();
        Date startDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getStartDate());
        Date endDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getEndDate());
        List<String> destinations = pageTrayRequest.getDestinations();
        List<String> categories = pageTrayRequest.getCategories();
        Boolean[] inEdition = getInEdition();
        Boolean[] isPublished = getIsPublished();

        if(states == null || !CaUtil.validateIsNotNullAndPositive(states.length)){
            states = getAllStatusPage();
        }

        Map<Integer, Object> mapStatesEdition = getStatesAndEdition(states, inEdition, isPublished);
        states = (Integer[]) mapStatesEdition.get(ConstantsUtil.ONE);
        inEdition = (Boolean[]) mapStatesEdition.get(ConstantsUtil.TWO);
        isPublished = (Boolean[]) mapStatesEdition.get(ConstantsUtil.THREE);

        Page<PageHeader> pageHeaders = null;

        if(startDate != null && endDate != null) {
            endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);
            if(CaUtil.validateListIsNotNullOrEmpty(destinations)) {
                pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInAndDestinationIdInOrderByLastUpdateDesc(
                        states,
                        pageTrayRequest.getName(),
                        categories,
                        startDate,
                        endDate,
                        inEdition,
                        isPublished,
                        destinations,
                        pageable);
            } else {
                pageHeaders = getStatesNamesCategoriesDatesInEditionPublishedOrder(states, pageTrayRequest.getName(), categories,
                        startDate, endDate, inEdition, isPublished, pageable);
            }

        } else {
            Pageable pageableSort = getPageableSortByLastUpdate(pageable);
            if(CaUtil.validateListIsNotNullOrEmpty(destinations)){
                pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishInAndAndDestinationIdIn(
                        states,
                        pageTrayRequest.getName(),
                        categories,
                        inEdition,
                        isPublished,
                        destinations,
                        pageableSort);
            } else {
                pageHeaders = getByStatesNamesCategoriesInEditionPublished(states, pageTrayRequest.getName(), categories,
                        inEdition, isPublished, pageableSort);
            }
        }

        return pageHeaders;
    }

    private Page<PageHeader> getDestinationsByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException{
        Integer[] states = pageTrayRequest.getStates();
        Date startDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getStartDate());
        Date endDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getEndDate());
        List<String> countries = pageTrayRequest.getCountries();
        List<String> categories = pageTrayRequest.getCategories();
        Boolean[] inEdition = getInEdition();
        Boolean[] isPublished = getIsPublished();

        if(states == null || !CaUtil.validateIsNotNullAndPositive(states.length)){
            states = getAllStatusPage();
        }

        Map<Integer, Object> mapStatesEdition = getStatesAndEdition(states, inEdition, isPublished);
        states = (Integer[]) mapStatesEdition.get(ConstantsUtil.ONE);
        inEdition = (Boolean[]) mapStatesEdition.get(ConstantsUtil.TWO);
        isPublished = (Boolean[]) mapStatesEdition.get(ConstantsUtil.THREE);

        Page<PageHeader> pageHeaders = null;

        if(startDate != null && endDate != null){
            endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);
            if(CaUtil.validateListIsNotNullOrEmpty(countries)) {
                pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInAndCountryIdInOrderByLastUpdateDesc(
                        states,
                        pageTrayRequest.getName(),
                        categories,
                        startDate,
                        endDate,
                        inEdition,
                        isPublished,
                        countries,
                        pageable);
            } else {
                pageHeaders = getStatesNamesCategoriesDatesInEditionPublishedOrder(states, pageTrayRequest.getName(), categories,
                        startDate, endDate, inEdition, isPublished, pageable);
            }
        } else {
            Pageable pageableSort = getPageableSortByLastUpdate(pageable);
            if(CaUtil.validateListIsNotNullOrEmpty(countries)) {
                pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishInAndCountryIdIn(
                        states,
                        pageTrayRequest.getName(),
                        categories,
                        inEdition,
                        isPublished,
                        countries,
                        pageableSort);
            } else {
                pageHeaders = getByStatesNamesCategoriesInEditionPublished(states, pageTrayRequest.getName(), categories,
                        inEdition, isPublished, pageableSort);
            }
        }

        return pageHeaders;
    }

    private Page<PageHeader> getByStatesNamesCategoriesInEditionPublished(Integer[] states, String name, List<String> categories,
                                                                          Boolean[] inEdition, Boolean[] isPublished, Pageable pageable){
        return pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishIn(
                states,
                name,
                categories,
                inEdition,
                isPublished,
                pageable);
    }

    private Page<PageHeader> getStatesNamesCategoriesDatesInEditionPublishedOrder(Integer[] states, String name, List<String> categories,
                                 Date startDate, Date endDate,Boolean[] inEdition,
                                 Boolean[] isPublished, Pageable pageable){
        return pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInOrderByLastUpdateDesc(
                states,
                name,
                categories,
                startDate,
                endDate,
                inEdition,
                isPublished,
                pageable);
    }

    private Page<PageHeader> getPromotionsByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException{
        Integer[] states = pageTrayRequest.getStates();
        Date startDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getStartDate());
        Date endDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getEndDate());
        List<String> genericTypeList = pageTrayRequest.getGenericType();
        List<String> categories = pageTrayRequest.getCategories();
        Boolean[] inEdition = getInEdition();
        Boolean[] isPublished = getIsPublished();

        if(states == null || !CaUtil.validateIsNotNullAndPositive(states.length)){
            states = getAllStatusPage();
        }

        Map<Integer, Object> mapStatesEdition = getStatesAndEdition(states, inEdition, isPublished);
        states = (Integer[]) mapStatesEdition.get(ConstantsUtil.ONE);
        inEdition = (Boolean[]) mapStatesEdition.get(ConstantsUtil.TWO);
        isPublished = (Boolean[]) mapStatesEdition.get(ConstantsUtil.THREE);

        Page<PageHeader> pageHeaders = null;

        if(startDate != null && endDate != null){
            endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);
            if(CaUtil.validateListIsNotNullOrEmpty(genericTypeList)) {
                pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInAndGenericTypeInOrderByLastUpdateDesc(
                        states,
                        pageTrayRequest.getName(),
                        categories,
                        startDate,
                        endDate,
                        inEdition,
                        isPublished,
                        genericTypeList,
                        pageable);
            } else {
                pageHeaders = getStatesNamesCategoriesDatesInEditionPublishedOrder(states, pageTrayRequest.getName(), categories,
                        startDate, endDate, inEdition, isPublished, pageable);
            }
        } else {
            Pageable pageableSort = getPageableSortByLastUpdate(pageable);
            if(CaUtil.validateListIsNotNullOrEmpty(genericTypeList)) {
                pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishInAndGenericTypeIn(
                        states,
                        pageTrayRequest.getName(),
                        categories,
                        inEdition,
                        isPublished,
                        genericTypeList,
                        pageableSort);
            } else {
                pageHeaders = getByStatesNamesCategoriesInEditionPublished(states, pageTrayRequest.getName(), categories,
                        inEdition, isPublished, pageableSort);
            }
        }

        return pageHeaders;
    }

    private Page<PageHeader> getPageTrayByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException{
        Integer[] states = pageTrayRequest.getStates();
        List<String> userList = pageTrayRequest.getUserList();
        List<String> categories = pageTrayRequest.getCategories();
        Date startDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getStartDate());
        Date endDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getEndDate());
        Boolean[] inEdition = getInEdition();
        Boolean[] isPublished = getIsPublished();

        if(states == null || !CaUtil.validateIsNotNullAndPositive(states.length)){
            states = getAllStatusPage();
        }

        if(!CaUtil.validateListIsNotNullOrEmpty(userList)){
            userList = getUsernameByRolePermission();
        }

        if(!CaUtil.validateListIsNotNullOrEmpty(categories)){
            categories = TemplateType.getListCode();
        }

        Map<Integer, Object> mapStatesEdition = getStatesAndEdition(states, inEdition, isPublished);
        states = (Integer[]) mapStatesEdition.get(ConstantsUtil.ONE);
        inEdition = (Boolean[]) mapStatesEdition.get(ConstantsUtil.TWO);
        isPublished = (Boolean[]) mapStatesEdition.get(ConstantsUtil.THREE);

        Page<PageHeader> pageHeaders = null;

        if(startDate != null && endDate != null){
            endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);
            pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndUsernameInAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInOrderByLastUpdateDesc(
                            states,
                            pageTrayRequest.getName(),
                            userList,
                            categories,
                            startDate,
                            endDate,
                            inEdition,
                            isPublished,
                            pageable);
        } else {
            Pageable pageableSort = getPageableSortByLastUpdate(pageable);
            pageHeaders = pageHeaderRepository.findByStateInAndNameContainingIgnoreCaseAndUsernameInAndCategoryIdInAndInEditionInAndPublishIn(
                    states,
                    pageTrayRequest.getName(),
                    userList,
                    categories,
                    pageableSort,
                    inEdition,
                    isPublished);
        }

        return pageHeaders;
    }

    private Integer[] getAllStatusPage(){
        List<Integer> listStatus = StatusPageType.getListCode();
        Integer[] status = new Integer[listStatus.size()];
        status = listStatus.toArray(status);
        return status;
    }

    private void validateUser() throws CaRequiredException {
        String username = userAuthentication.getUserPrincipal().getUsername();
        Optional<User> user =  userRepository.findByUsernameAndStatus(username, StatusType.ACTIVE.getCode());
        if(!user.isPresent())
            throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_INVALID));
    }

    private List<String> getUsernameByRolePermission(){
        List<Role> listRole = roleRepository.findByStatusAndPermissionCodeListIn(StatusType.ACTIVE.getCode(), Collections.singletonList("P003"));
        List<User> listUser = userRepository.findByRoleCodeInAndStatus(listRole.stream().map(this::buildRoleCode).collect(Collectors.toList()), StatusType.ACTIVE.getCode());
        return listUser.stream().map(this::buildUsername).collect(Collectors.toList());
    }

    private String buildUsername(User user){
        return user.getUsername();
    }

    private String buildRoleCode(Role role){
        return role.getRoleCode();
    }

    public Page<PageHeader> listHotelsTrayByFilters(PageTrayRequest pageTrayRequest, Pageable pageable) throws CaRequiredException {
        validateUser();

        Integer[] states = pageTrayRequest.getStates();
        List<String> categories = pageTrayRequest.getCategories();
        List<String> destinations = pageTrayRequest.getDestinations();

        if(states == null || !CaUtil.validateIsNotNullAndPositive(states.length)){
            states = getAllStatusPage();
        }

        if(!CaUtil.validateListIsNotNullOrEmpty(categories)){
            categories.add(TemplateType.HOTELS_DETAIL.getCode());
            categories.add(TemplateType.HOTELS_DESCRIPTOR.getCode());
        }

        if(!CaUtil.validateListIsNotNullOrEmpty(destinations)){
            List<PageHeader> listDestinations = findDestinationsByState();
            destinations = buildDestinationsToListString(listDestinations);
        }

        Pageable pageableSort = getPageableSortByLastUpdate(pageable);
        return pageHeaderRepository.findByStateInAndCategoryIdInAndDestinationIdIn(states, categories, destinations, pageableSort);
    }

    @Override
    public List<PageHeader> findDestinationsByState(){
        return  pageHeaderRepository.customFindDestinationsByStateNotPublish(TemplateType.DESTINATIONS_DETAIL.getCode(), StatusPageType.DELETED.getCode(), Boolean.TRUE);
    }

    public List<String> buildDestinationsToListString(List<PageHeader> listDestinations){
        return listDestinations.stream().map(this::buildDestinationsToString).collect(Collectors.toList());
    }

    private String buildDestinationsToString(PageHeader pageHeader){
        return pageHeader.getId();
    }

    public List<PageJoinView> findPageByCategoriesAndCountries(List<String> categories, List<String> countries, Sort sort) {
        return  this.pageJoinViewRepository.customFindByCategoriesAndCountries(categories, countries, sort);
    }

    public List<PageJoinView> findPageByCategories(List<String> categories, Sort sort) {
        return  this.pageJoinViewRepository.customFindByCategories(categories, sort);
    }

    private Boolean[] getIsPublished(){
        return new Boolean[]{Boolean.TRUE, Boolean.FALSE, null};
    }

    private Boolean[] getInEdition(){
        return new Boolean[]{Boolean.TRUE, Boolean.FALSE, null};
    }

    private Map<Integer, Object> getStatesAndEdition(Integer[] states, Boolean[] inEdition, Boolean[] isPublished){
        Map<Integer, Object> mapStatesEdition = new HashMap<>();

        Integer includeEdition = Arrays.stream(states).filter(x -> StatusPageType.IN_EDITION.getCode().equals(x)).collect(Collectors.toList()).size();
        Integer includePublished = Arrays.stream(states).filter(x -> StatusPageType.PUBLISHED.getCode().equals(x)).collect(Collectors.toList()).size();
        Integer includeDraft = Arrays.stream(states).filter(x -> StatusPageType.DRAFT.getCode().equals(x)).collect(Collectors.toList()).size();

        if(states.length == ConstantsUtil.ONE &&  ConstantsUtil.ONE.equals(includeEdition)){
            states = getAllStatusPage();
            inEdition = new Boolean[]{Boolean.TRUE};
        }

        if(states.length == ConstantsUtil.ONE &&  ConstantsUtil.ONE.equals(includePublished)){
            states = new Integer[]{StatusPageType.PUBLISHED.getCode()};
            inEdition = new Boolean[]{Boolean.FALSE};
        }

        if(states.length == ConstantsUtil.ONE &&  ConstantsUtil.ONE.equals(includeDraft)){
            states = new Integer[]{StatusPageType.IN_EDITION.getCode()};
            inEdition = new Boolean[]{Boolean.FALSE};
            isPublished = new Boolean[]{Boolean.FALSE};
        }

        mapStatesEdition.put(ConstantsUtil.ONE, states);
        mapStatesEdition.put(ConstantsUtil.TWO, inEdition);
        mapStatesEdition.put(ConstantsUtil.THREE, isPublished);

        return mapStatesEdition;
    }

    private List<String> getAllPromotionType(){
        List<PromotionType> promotionTypes = this.promotionTypeRepository.customFindByState(StatusType.ACTIVE.getCode());
        if(Objects.nonNull(promotionTypes) && !promotionTypes.isEmpty()){
            return promotionTypes.stream().map(PromotionType::getId).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public Pageable getPageableSortByLastUpdate(Pageable pageable){
        Sort sort = new Sort(Sort.Direction.DESC, "lastUpdate");
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    }
}
