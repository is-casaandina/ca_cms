package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Template;
import com.casaandina.cms.repository.TemplateRepository;
import com.casaandina.cms.service.TemplateService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.TemplateType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class TemplateServiceImpl implements TemplateService {
	
	private static final Integer INTERNAL_SERVER_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();
	
	private final TemplateRepository templateRepository;
	
	private final MessageProperties messageProperties;
	
	@Autowired
	public TemplateServiceImpl(TemplateRepository templateRepository, MessageProperties messageProperties) {
		this.templateRepository = templateRepository;
		this.messageProperties = messageProperties;
	}
	
	@Override
	public List<Template> listViewTemplates(List<String> categories) {
		if(Objects.nonNull(categories) && !categories.isEmpty()){
			return this.templateRepository.findTemplatesByCategoriesCustomized(categories);
		} else
			return this.templateRepository.findTemplatesCustomized(TemplateType.PAGE_ERROR.getCode());
	}

	@Override
	public Template getTemplateForEdit(String code) throws CaBusinessException {
		Optional<Template> optionalTemplate = templateRepository.findByCode(code);
		
		if(!optionalTemplate.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_CODE_NOT_EXIST), INTERNAL_SERVER_ERROR);
		}
		return optionalTemplate.get();
	}

	@Override
	public Template saveTemplate(Template template) throws CaRequiredException {

		validate(template);
		UUID uuid = UUID.randomUUID();
		Template templateSave = new Template();
		BeanUtils.copyProperties(template, templateSave);
		templateSave.setId(uuid.toString());

		return templateRepository.save(templateSave);
	}

	private void validate(Template template) throws  CaRequiredException{
		if(!CaUtil.validateIsNotNullAndNotEmpty(template.getCode())){
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_CODE_REQUIRED));
		}

		Optional<Template> templateOptional = templateRepository.findByCode(template.getCode());
		if(templateOptional.isPresent()){
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_CODE_EXIST));
		}
	}

}
