package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.repository.*;
import com.casaandina.cms.service.EmailService;
import com.casaandina.cms.service.QuoteEventService;
import com.casaandina.cms.util.*;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class QuoteEventServiceImpl implements QuoteEventService {
	
	private final QuoteEventRepository quoteEventRepository;
	
	private final SubscriberRepository subscriberRepository;
	
	private final PageRepository pageRepository;
	
	private final CountryRepository countryRepository;
	
	private final MessageProperties messageProperties;

	private final EmailService emailService;

	private final ParameterRepository parameterRepository;

	private static Logger logger = LoggerFactory.getLogger(QuoteEventServiceImpl.class);

	@Override
	public QuoteEvent saveQuoteEvent(QuoteEvent quoteEvent) throws CaBusinessException {
		
		validateQuoteEvent(quoteEvent);
		validateContact(quoteEvent.getSubscriber());
		
		Subscriber subscriber = saveSubscriber(quoteEvent.getSubscriber());
		
		Optional<Page> pageOptional = pageRepository.findByIdCustomized(quoteEvent.getPage().getId());
		if(!pageOptional.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_SUBSCRIBER_PAGE_NOT_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		quoteEvent.setSubscriber(subscriber);
		
		return quoteEventRepository.save(quoteEvent);
	}
	
	private void validateQuoteEvent(QuoteEvent quoteEvent) throws CaBusinessException  {
		
		if(!CaUtil.validateIsNotNullAndNotEmpty(quoteEvent.getPage().getId())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_HOTEL_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateIsNotNullAndPositive(quoteEvent.getQuantityAssistants())){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_QUANTITY_ASSISTANTS_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateIsNotNullAndNotEmpty(quoteEvent.getBeginDate())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_BEGIN_DATE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateIsNotNullAndNotEmpty(quoteEvent.getEndDate())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_END_DATE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}

		if(quoteEvent.getBeginDate().isBefore(LocalDate.now())){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_BEGIN_DATE_MINOR_CURRENT_DATE), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(quoteEvent.getEndDate().isBefore(quoteEvent.getBeginDate())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_END_DATE_MINOR_BEGIN_DATE), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateIsNotNullAndNotEmpty(quoteEvent.getBeginHour())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_BEGIN_HOUR_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateIsNotNullAndNotEmpty(quoteEvent.getEndHour())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_QUOTE_END_HOUR_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	private Subscriber saveSubscriber(Subscriber subscriber) {
		
		subscriber.setStatus(StatusType.ACTIVE.getCode());
		subscriber.setRegisterDate(LocalDateTime.now());
		subscriber.setTypeSuscriber(SubscriberType.QUOTE.getCode());
		
		Optional<Country> optional = countryRepository.findByCodeAndStatusCustomized(ConstantsUtil.CODE_ISO_PERU, Boolean.TRUE);
		
		if(optional.isPresent()) {
			subscriber.setCountry(optional.get());
		}
		
		return subscriberRepository.save(subscriber);
	}
	
	private void validateContact(Subscriber subscriber) throws CaBusinessException {
		if(!CaUtil.validateIsNotNullAndNotEmpty(subscriber.getName())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_NAME_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateIsNotNullAndNotEmpty(subscriber.getEmail())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_EMAIL_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateEmail(subscriber.getEmail())){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_EMAIL_VALIDATE), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
		
		if(!CaUtil.validateIsNotNullAndNotEmpty(subscriber.getPhone())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_PHONE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}
	}

	@Async
	@Override
	public void sendMail(QuoteEvent quoteEvent){
		try {
			QuoteEvent quoteEventTemp = ObjectMapperUtils.map(quoteEvent, QuoteEvent.class);

			Optional<Page> pageOptional = pageRepository.customFindByIdAndLanguageNameElementMatch(quoteEvent.getPage().getId()
														, LanguageType.SPANISH.getCode());
			if(pageOptional.isPresent()){
				quoteEventTemp.setPage(pageOptional.get());
			}

			LanguagePage languagePage = quoteEventTemp.getPage().getLanguages().stream()
					.filter(x-> LanguageType.SPANISH.getCode().equals(x.getLanguage()))
					.findFirst().orElse(null);

			Contact contact = quoteEventTemp.getPage().getContact();
			LanguageContact languageContact = contact.getLanguages().stream()
												.filter(x->LanguageType.SPANISH.getCode().equals(x.getLanguage()))
												.findFirst().orElse(null);

			LanguageDetailContact languageDetailContact = (Objects.nonNull(languageContact) && CaUtil.validateListIsNotNullOrEmpty(languageContact.getDetails()))
						? languageContact.getDetails().stream()
							.filter(x-> ContactType.MAIL_BOOKING.getCode().equals(x.getType()))
							.findFirst().orElse(null)
						: null;

			if(Objects.nonNull(languageDetailContact)){
				emailService.sendMail(Objects.nonNull(languageDetailContact) ? languageDetailContact.getDescription() : null, "Cotización de Evento", getBodyWithParametersFromTemplate(quoteEventTemp, languagePage));
			} else {
				logger.error(String.format("El Hotel %s no tiene configurado un correo",
						(Objects.nonNull(languagePage) ? languagePage.getTitle() : ConstantsUtil.EMPTY)));
			}
		} catch(Exception ex){
			logger.error(ex.getMessage());
		}
	}

	private String getBodyWithParametersFromTemplate(QuoteEvent quoteEvent, LanguagePage languagePage) {

		String body = "";
		try {

			Parameter parameter =  this.parameterRepository.customFindRoibackWebServiceByCode(ParameterType.QUOTE_MAIL.getCode());
			if(parameter != null){
				StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
				String nameTemplate = messageProperties.getMessage(PropertiesConstants.TEMPLATE_QUOTE_EVENT_EMAIL);
				String freeMakerTemplate = ObjectMapperUtils.map(parameter.getIndicator(), String.class);
				stringTemplateLoader.putTemplate(nameTemplate, freeMakerTemplate);

				Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
				configuration.setTemplateLoader(stringTemplateLoader);
				Template t = configuration.getTemplate(nameTemplate);

				String armedTypes = (Objects.nonNull(quoteEvent.getArmedTypes()) && !quoteEvent.getArmedTypes().isEmpty())
									? buildString(quoteEvent.getArmedTypes())
									: ConstantsUtil.EMPTY;

				String foodOptions = (Objects.nonNull(quoteEvent.getFoodOptions()) && !quoteEvent.getFoodOptions().isEmpty())
						? buildString(quoteEvent.getFoodOptions())
						: ConstantsUtil.EMPTY;

				String audioVisualEquipments = (Objects.nonNull(quoteEvent.getAudiovisualEquipments()) && !quoteEvent.getAudiovisualEquipments().isEmpty())
						? buildString(quoteEvent.getAudiovisualEquipments())
						: ConstantsUtil.EMPTY;

				Map<String, String> mapParameters = new HashMap<>();
				mapParameters.put("NAME", quoteEvent.getSubscriber().getName());
				mapParameters.put("PHONE", quoteEvent.getSubscriber().getPhone());
				mapParameters.put("EMAIL", quoteEvent.getSubscriber().getEmail());
				mapParameters.put("HOTEL", languagePage != null ? languagePage.getTitle() : ConstantsUtil.EMPTY);
				mapParameters.put("QUANTITY", quoteEvent.getQuantityAssistants() != null ? quoteEvent.getQuantityAssistants().toString(): ConstantsUtil.EMPTY);
				mapParameters.put("BEGIN_DATE", quoteEvent.getBeginDate() != null ? quoteEvent.getBeginDate().toString() : ConstantsUtil.EMPTY);
				mapParameters.put("END_DATE", quoteEvent.getEndDate() != null ? quoteEvent.getEndDate().toString() : ConstantsUtil.EMPTY);
				mapParameters.put("BEGIN_HOUR", quoteEvent.getBeginHour());
				mapParameters.put("END_HOUR", quoteEvent.getEndHour());
				mapParameters.put("ARMED_TYPES", armedTypes);
				mapParameters.put("FOOD_OPTIONS", foodOptions);
				mapParameters.put("AUDIOVISUAL_EQUIPMENTS", audioVisualEquipments);
				mapParameters.put("COMMENTS", quoteEvent.getAdditionalComments());

				body = FreeMarkerTemplateUtils.processTemplateIntoString(t, mapParameters);

			}

		} catch (Exception ex) {
			logger.error(String.format(messageProperties.getMessage(PropertiesConstants.MESSAGE_EMAIL_ERROR_SEND), ex.getMessage()));
		}

		return body;
	}

	public String buildString(List<String> listString){
		StringBuilder build = new StringBuilder();
		int i = 0;
		for(String obj : listString){
			if(i == (listString.size() - ConstantsUtil.ONE))
				build.append(obj);
			else
				build.append(obj + ", ");
			i++;
		}
		return build.toString();
	}
	
}
