package com.casaandina.cms.service.impl;

import com.casaandina.cms.aws.AmazonClient;
import com.casaandina.cms.dto.InputStreamParameter;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.PageError;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.repository.PageErrorRepository;
import com.casaandina.cms.repository.PageHeaderRepository;
import com.casaandina.cms.repository.PageRepository;
import com.casaandina.cms.rest.UserAuthentication;
import com.casaandina.cms.service.PageErrorService;
import com.casaandina.cms.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Log4j2
public class PageErrorServiceImpl implements PageErrorService {

    private final PageErrorRepository pageErrorRepository;
    private final PageRepository pageRepository;
    private final PageHeaderRepository pageHeaderRepository;
    private final UserAuthentication userAuthentication;
    private final MessageProperties messageProperties;
    private final AmazonClient amazonClient;

    @Value("${aws.endpointUrl}")
    private String endpointUrl;

    @Value("${aws.bucketName}")
    private String bucketName;

    private static final String DIRECTORY_PAGE_ERROR = "system/pages-error";

    private static final Integer NOT_ACCEPTABLE = HttpStatus.NOT_ACCEPTABLE.value();

    @Override
    public Optional<PageError> getPageErrorByCodeAndState(Integer errorCode) {
        return this.pageErrorRepository.findByErrorCodeAndState(errorCode, StatusType.ACTIVE.getCode());
    }

    @Override
    public Optional<Page> customFindPagePathByIdAndLanguage(String id, String language) {
        return this.pageRepository.customFindPagePathByIdAndLanguage(id, language);
    }

    @Override
    public List<PageError> findAllByState(){
        return this.pageErrorRepository.findByState(StatusType.ACTIVE.getCode());
    }

    @Override
    public PageError insertPageError(PageError pageError) throws CaBusinessException {
        validatePageError(pageError);

        LocalDateTime now = LocalDateTime.now();
        String userName = userAuthentication.getUserPrincipal().getUsername();

        Optional<Page> pageOptional = this.pageRepository.findById(pageError.getPageId());
        if(!pageOptional.isPresent()){
            throw  new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_ASSOCIATE_SUCCESSFUL), NOT_ACCEPTABLE);
        }

        uploadToS3(pageOptional.get(), pageError.getErrorCode());

        return this.pageErrorRepository.save(PageError.builder()
                .pageId(pageError.getPageId())
                .errorCode(pageError.getErrorCode())
                .state(StatusType.ACTIVE.getCode())
                .createdDate(now)
                .modifiedDate(now)
                .owner(userName)
                .user(userName)
                .build()
        );
    }

    @Override
    public PageError updatePageError(PageError pageError) throws CaBusinessException {
        validatePageError(pageError);

        Optional<PageError> pageErrorOptional = this.pageErrorRepository.findByIdAndState(pageError.getId(), StatusType.ACTIVE.getCode());
        if(!pageErrorOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_UPDATE_NOT_EXIST), NOT_ACCEPTABLE);
        }

        Optional<Page> pageOptional = this.pageRepository.findById(pageError.getPageId());
        if(!pageOptional.isPresent()){
            throw  new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_ASSOCIATE_SUCCESSFUL), NOT_ACCEPTABLE);
        }

        uploadToS3(pageOptional.get(), pageError.getErrorCode());

        String userName = userAuthentication.getUserPrincipal().getUsername();

        PageError pageErrorSave = pageErrorOptional.get();
        pageErrorSave.setPageId(pageError.getPageId());
        pageErrorSave.setErrorCode(pageError.getErrorCode());
        pageErrorSave.setModifiedDate(LocalDateTime.now());
        pageErrorSave.setUser(userName);

        return this.pageErrorRepository.save(pageErrorSave);

    }

    @Override
    public void deletePageError(String id) throws CaBusinessException {
        Optional<PageError> pageErrorOptional = this.pageErrorRepository.findByIdAndState(id, StatusType.ACTIVE.getCode());

        if(!pageErrorOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_DELETE_NOT_EXIST), NOT_ACCEPTABLE);
        }

        String userName = userAuthentication.getUserPrincipal().getUsername();

        PageError pageError = pageErrorOptional.get();
        pageError.setState(StatusType.INACTIVE.getCode());
        pageError.setUser(userName);

        this.pageErrorRepository.save(pageError);

    }

    private void validatePageError(PageError pageError) throws CaBusinessException{

        if(Objects.nonNull(pageError)){
            if(!CaUtil.validateIsNotNullAndNotEmpty(pageError.getPageId())){
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_PAGE_ID_REQUIRED), NOT_ACCEPTABLE);
            }

            if(!(Objects.nonNull(pageError.getErrorCode()) && pageError.getErrorCode() > ConstantsUtil.ZERO)){
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_ERROR_CODE_REQUIRED), NOT_ACCEPTABLE);
            }

            Optional<PageError> pageErrorOptional;

            if(Objects.nonNull(pageError.getId())){
                pageErrorOptional = this.pageErrorRepository.customFindByStateAndErrorCodeAndIdNot(StatusType.ACTIVE.getCode(),
                        pageError.getErrorCode(), pageError.getId());
            } else {
                pageErrorOptional = this.pageErrorRepository.findByStateAndErrorCode(StatusType.ACTIVE.getCode(),
                        pageError.getErrorCode());
            }

            if(pageErrorOptional.isPresent()){
                throw new CaBusinessException(String.format(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_ERROR_CODE_EXIST), pageError.getErrorCode()), NOT_ACCEPTABLE);
            }
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_REQUIRED), NOT_ACCEPTABLE);
        }
    }

    private void uploadToS3(Page page, Integer errorCode){
        try {
            ObjectMapper objectMapper = new ObjectMapper();

            String jsonStr = objectMapper.writeValueAsString(page);

            byte[] byteString = jsonStr.getBytes(StandardCharsets.UTF_8);
            InputStream source = new ByteArrayInputStream(byteString);

            StringBuilder fileName = new StringBuilder(errorCode.toString())
                    .append(ConstantsUtil.PageErrorJson.EXTENSION_JSON);

            InputStreamParameter inputStreamParameter = InputStreamParameter.builder()
                    .contentType(ConstantsUtil.PageErrorJson.CONTENT_TYPE)
                    .length(byteString.length)
                    .fileName(fileName.toString())
                    .fis(source)
                    .build();

            amazonClient.uploadInputStream(DIRECTORY_PAGE_ERROR, inputStreamParameter);

        } catch (Exception e){
            log.error(String.format("There was an error uploading page error json %s", e.getMessage()));
        }
    }

    @Override
    public List<PageHeader> findAllPageError(){
        return this.pageHeaderRepository.customFindByCategoryIdAndStateAndPublish(TemplateType.PAGE_ERROR.getCode(),
                StatusPageType.DELETED.getCode(), Boolean.TRUE);
    }
}
