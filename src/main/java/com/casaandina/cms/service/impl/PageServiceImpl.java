package com.casaandina.cms.service.impl;

import com.casaandina.cms.dto.*;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.exception.CaSiteErrorException;
import com.casaandina.cms.model.Currency;
import com.casaandina.cms.model.*;
import com.casaandina.cms.repository.*;
import com.casaandina.cms.rest.UserAuthentication;
import com.casaandina.cms.security.UserPrincipal;
import com.casaandina.cms.service.PageService;
import com.casaandina.cms.util.TemplateType;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.PageUtil;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class PageServiceImpl implements PageService {
	
	private static final Integer INTERNAL_SERVER_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();
	
	private final PageHeaderRepository pageHeaderRepository;
	
	private final PageRepository pageRepository;
	
	private final DraftPageRepository draftPageRepository;

	private final ConfigurationsRepository configurationsRepository;
	
	private final MessageProperties messageProperties;

	private final UserAuthentication userAuthentication;

	private final DescriptorRepository descriptorRepository;

	private final CurrencyRepository currencyRepository;

	private final PagePromotionViewRepository pagePromotionViewRepository;

	private final DomainRepository domainRepository;

	private final CountryRepository countryRepository;

	@Resource
	private MongoTemplate mongoTemplate;

	@Override
	public DraftPage savePage(Page page) throws CaBusinessException {
		
		DraftPage draftPage;
		UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

		if(page.getId() == null) {
			PageHeader pageHeader = savePageHeader(page, userPrincipal);
			page.setId(pageHeader.getId());
			draftPage = draftPageRepository.insert(getDraftPageFromPage(page, userPrincipal.getUsername()));
		} else {
			validateUpdate(page);
			updatePageHeader(page, userPrincipal, StatusPageType.IN_EDITION.getCode());
			if(TemplateType.LINK.getCode().equals(page.getCategoryId())){
				draftPage = createDraftPageForExternal(page, userPrincipal.getUsername());
			} else {
				draftPage = updateDraftPage(page, userPrincipal.getUsername());
			}
		}
		draftPage.setCategoryName(CaUtil.getTemplateTypeName(draftPage.getCategoryId()));
		return draftPage;
	}
	
	private void validateUpdate(Page page) throws CaBusinessException {

		String pageName = "";
		if(CaUtil.validateListIsNotNullOrEmpty(page.getLanguages())){
			Optional<LanguagePage> languagePage = page.getLanguages().stream().filter(x -> LanguageType.SPANISH.getCode().equals(x.getLanguage())).findFirst();
			if(languagePage.isPresent())
				pageName = languagePage.get().getTitle();
		}

		if(!CaUtil.validateIsNotNullAndNotEmpty(pageName)) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NAME_REQUIRED), HttpStatus.NOT_ACCEPTABLE.value());
		}
		
		Optional<PageHeader> pageHeaderOptional = pageHeaderRepository.findByNameAndIdNotCustomized(pageName, page.getId());
		if(pageHeaderOptional.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NAME_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		if(CaUtil.validateListIsNotNullOrEmpty(page.getLanguages())){
			validatePagePath(page.getLanguages(), page.getCategoryId());
			for(LanguagePage language : page.getLanguages()){
				Optional<Page> pageOptional = pageRepository.findByIdAndLanguagePathCustomized(page.getId(), language.getPath());
				if(pageOptional.isPresent())
					throw new CaBusinessException(String.format(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_PATH_EXIST), language.getPath()), HttpStatus.NOT_ACCEPTABLE.value());
			}
		}

		UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

		Optional<PageHeader> pageHeaderOptionalEdition = pageHeaderRepository.findById(page.getId());
		if(pageHeaderOptionalEdition.isPresent()){
			if(!userPrincipal.getUsername().equals(pageHeaderOptionalEdition.get().getUserEdition()))
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_SET_FREE_VALIDATE), INTERNAL_SERVER_ERROR);
		}

	}

	private void validatePagePath(List<LanguagePage> languagePages, String categoryId)throws CaBusinessException {
		for(LanguagePage languagePage : languagePages){
			if(TemplateType.LINK.getCode().equals(categoryId)){
				if(!CaUtil.validateIsNotNullAndNotEmpty(languagePage.getPath())){
					throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_LANGUAGES_PAGEPATH_REQUIRED), HttpStatus.NOT_ACCEPTABLE.value());
				}
			} else {
				if(!CaUtil.validateIsNotNullAndNotEmpty(languagePage.getPagePath())){
					throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_LANGUAGES_PAGEPATH_REQUIRED), HttpStatus.NOT_ACCEPTABLE.value());
				}
			}
		}
	}
	
	private DraftPage getDraftPageFromPage(Page page, String username) {
		LocalDateTime now = LocalDateTime.now();
		DraftPage draftPage = ObjectMapperUtils.map(page, DraftPage.class);
		
		draftPage.setCreatedDate(now);
		draftPage.setModifiedDate(now);
		draftPage.setOwner(username);
		draftPage.setUser(username);
		draftPage.setState(StatusPageType.IN_EDITION.getCode());
		draftPage.setExternal(TemplateType.LINK.getCode().equals(page.getCategoryId()) ? Boolean.TRUE : Boolean.FALSE );
		
		return draftPage;
	}

	@Override
	public Page publishPage(String id) throws CaBusinessException {

		UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

		Optional<Page> pageOptional = pageRepository.findById(id);

		Page pagePublish = null;

		Optional<DraftPage> draftPageOptional = draftPageRepository.findByIdAndStateNot(id, StatusPageType.DELETED.getCode());
		DraftPage draftPage = draftPageOptional.orElse(null);

		if(pageOptional.isPresent()) {
			if(StatusPageType.DELETED.getCode().equals(pageOptional.get().getState())){
				validateCountLanguages(pageOptional.get());
				draftPage = ObjectMapperUtils.map(pageOptional.get(), DraftPage.class);
				pagePublish = publishPageUnPublish(pageOptional.get(), userPrincipal.getUsername());
			} else {
				if(draftPage != null){
					validateCountLanguages(ObjectMapperUtils.map(draftPage, Page.class));
					draftPage.setUser(userPrincipal.getUsername());
					pagePublish = copyDraftPageInPage(draftPage, pageOptional.get());
				}
			}
		} else {
			if(draftPage != null){
				validateCountLanguages(ObjectMapperUtils.map(draftPage, Page.class));
				pagePublish = pageRepository.save(createPageFromDraftPage(draftPage, userPrincipal.getUsername()));
			}
		}
		updatePageHeader(ObjectMapperUtils.map(draftPage, Page.class), userPrincipal, StatusPageType.PUBLISHED.getCode());

		deleteDraftPage(id);

		if(pagePublish != null)
			pagePublish.setCategoryName(CaUtil.getTemplateTypeName(pagePublish.getCategoryId()));

		return pagePublish;
	}

	private void validateCountLanguages(Page page) throws CaBusinessException{
		if(CaUtil.validateListIsNotNullOrEmpty(page.getLanguages())){
			if(page.getLanguages().size() < ConstantsUtil.Page.COUNT_LANGUAGES)
				throw new CaBusinessException(String.format(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_COUNT_LANGUAGES_REQUIRED), ConstantsUtil.Page.COUNT_LANGUAGES), HttpStatus.NOT_ACCEPTABLE.value());
		} else {
			throw new CaBusinessException(String.format(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_COUNT_LANGUAGES_REQUIRED), ConstantsUtil.Page.COUNT_LANGUAGES), HttpStatus.NOT_ACCEPTABLE.value());
		}
	}
	
	private void deleteDraftPage(String id) {
		draftPageRepository.deleteById(id);
	}
	
	private Page createPageFromDraftPage(DraftPage page, String username) {
		
		LocalDateTime now = LocalDateTime.now();
		
		return Page.builder()
				   .id(page.getId())
				   .name(page.getName())
				   .createdDate(now)
				   .modifiedDate(now)
				   .owner(username)
				   .user(username)
				   .countryId(page.getCountryId())
				   .descriptorId(page.getDescriptorId())
				   .categoryId(page.getCategoryId())
				   .destinationId(page.getDestinationId())
				   .roiback(page.getRoiback())
				   .revinate(page.getRevinate())
				   .pageType(page.getPageType())
				   .type(page.getType())
				   .languages(page.getLanguages())
				   .state(StatusPageType.PUBLISHED.getCode())
				   .external(TemplateType.LINK.getCode().equals(page.getCategoryId()) ? Boolean.TRUE : Boolean.FALSE)
				   .shortName(page.getShortName())
				   .contact(page.getContact())
				   .beginDate(page.getBeginDate())
				   .firstExpiration(page.getFirstExpiration())
				   .secondExpiration(page.getSecondExpiration())
				   .countdown(page.getCountdown())
				   .genericType(page.getGenericType())
				   .domainId(page.getDomainId())
				   .clusterId(page.getClusterId())
				   .destinationMapId(page.getDestinationMapId())
				   .defaultHome(page.getDefaultHome())
				   .build();
	}

	@Override
	public DraftPage getPageForEdit(String id, String username) throws CaBusinessException {
		
		Optional<PageHeader> pageOptional = pageHeaderRepository.findById(id);
		
		if(!pageOptional.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		if(StatusPageType.DELETED.getCode().equals(pageOptional.get().getState())){
			return getFromPageUnPublish(id);
		} else {
			validateEditionByUser(pageOptional.get(), username);

			Optional<PageHeader> pageHeaderOptional = pageHeaderRepository.findById(id);

			if(pageHeaderOptional.isPresent()) {
				PageHeader pageHeader = pageHeaderOptional.get();
				pageHeader.setState(StatusPageType.IN_EDITION.getCode());
				pageHeader.setUserEdition(username);
				pageHeaderRepository.save(pageHeader);
			}

			Optional<DraftPage> draftPage = draftPageRepository.findById(id);

			if(draftPage.isPresent()) {
				return draftPage.get();
			} else {
				DraftPage draftPageSave = saveDraftPageFromPage(id, username);
				if(draftPageSave != null)
					return draftPageSave;
				else
					throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), INTERNAL_SERVER_ERROR);
			}
		}
	}

	private DraftPage getFromPageUnPublish(String id) throws CaBusinessException{
		Optional<Page> pageOpt = pageRepository.findById(id);
		if(pageOpt.isPresent()){
			return ObjectMapperUtils.map(pageOpt.get(), DraftPage.class);
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), INTERNAL_SERVER_ERROR);
		}
	}
	
	private void validateEditionByUser(PageHeader pageHeader, String username) throws CaBusinessException {
		pageHeader.setInEdition(pageHeader.getInEdition() != null ? pageHeader.getInEdition() : Boolean.FALSE);
		if(pageHeader.getInEdition() && !username.equals(pageHeader.getUserEdition())) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_USER_EDIT_EXIST) +  pageHeader.getUserEdition(), HttpStatus.NOT_ACCEPTABLE.value());
		}
	}
	
	private DraftPage saveDraftPageFromPage(String id, String username) {
		LocalDateTime now = LocalDateTime.now();
		DraftPage draftPage = new DraftPage();
		Optional<Page> pageOptional = pageRepository.findById(id);
		
		if(pageOptional.isPresent()) {
			BeanUtils.copyProperties(pageOptional.get(), draftPage);
			draftPage.setUser(username);
			draftPage.setCreatedDate(now);
			draftPage.setModifiedDate(now);
			draftPage.setOwner(username);
			draftPage.setState(StatusPageType.IN_EDITION.getCode());
			return draftPageRepository.insert(draftPage);
		}
		
		return null;
	}
	
	private PageHeader savePageHeader(Page page, UserPrincipal userPrincipal){

		String pageName = ConstantsUtil.EMPTY;

		if(TemplateType.LINK.getCode().equals(page.getCategoryId())){
			LanguagePage languagePage = page.getLanguages().stream()
					.filter(x -> LanguageType.SPANISH.getCode().equals(x.getLanguage())).collect(Collectors.toList())
					.stream().findFirst().orElse(null);
			if(languagePage != null && CaUtil.validateIsNotNullAndNotEmpty(languagePage.getTitle()))
				pageName = languagePage.getTitle();
		}

		UUID uuid = UUID.randomUUID();

		LocalDateTime now = LocalDateTime.now();

		PageHeader pageHeader = new PageHeader();
		pageHeader.setId(uuid.toString());
		pageHeader.setLastUpdate(now);
		pageHeader.setCreatedDate(now);
		pageHeader.setUsername(userPrincipal.getUsername());
		pageHeader.setUserEdition(userPrincipal.getUsername());
		pageHeader.setFullname(userPrincipal.getFullname());
		pageHeader.setPageType(page.getPageType());
		pageHeader.setType(page.getType());
		pageHeader.setCategoryId(page.getCategoryId());
		pageHeader.setDestinationId(page.getDestinationId());
		pageHeader.setRoiback(page.getRoiback());
		pageHeader.setRevinate(page.getRevinate());
		pageHeader.setPublish(Boolean.FALSE);
		pageHeader.setInEdition(page.getInEdition());
		pageHeader.setCountryId(page.getCountryId());
		pageHeader.setGenericType(page.getGenericType());
		
		if(CaUtil.validateListIsNotNullOrEmpty(page.getLanguages())) {			
			List<String> languages = new ArrayList<>();
			
			page.getLanguages().forEach(x -> languages.add(x.getLanguage()));
			
			pageHeader.setLanguages(languages);
		}
		pageName = pageName == null ? ConstantsUtil.EMPTY : pageName;

		pageHeader.setName(ConstantsUtil.EMPTY.equals(pageName) ? ConstantsUtil.Page.DEFAULT_NAME : pageName);
		pageHeader.setState(StatusPageType.IN_EDITION.getCode());
		
		return pageHeaderRepository.save(pageHeader);
	}
	
	private PageHeader updatePageHeader(Page page, UserPrincipal userPrincipal, Integer status) throws CaBusinessException {
		Optional<PageHeader> pageHeaderOptional = pageHeaderRepository.findById(page.getId());
		
		if(!pageHeaderOptional.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST_UPDATE), INTERNAL_SERVER_ERROR);
		}
		
		PageHeader pageHeader = pageHeaderOptional.get();
		pageHeader.setUsername(userPrincipal.getUsername());
		pageHeader.setUserEdition(userPrincipal.getUsername());
		pageHeader.setFullname(userPrincipal.getFullname());
		pageHeader.setState(status);
		pageHeader.setPageType(page.getPageType());
		pageHeader.setCategoryId(page.getCategoryId());
		pageHeader.setPublish(StatusPageType.PUBLISHED.getCode().equals(status) ? Boolean.TRUE : Boolean.FALSE);
		pageHeader.setDestinationId(page.getDestinationId());
		pageHeader.setRoiback(page.getRoiback());
		pageHeader.setRevinate(page.getRevinate());
		pageHeader.setCountryId(page.getCountryId());
		pageHeader.setGenericType(page.getGenericType());
		
		if(CaUtil.validateListIsNotNullOrEmpty(page.getLanguages())) {
			
			List<String> languages = new ArrayList<>();
			
			page.getLanguages().forEach(x -> languages.add(x.getLanguage()));
			
			pageHeader.setLanguages(languages);
			pageHeader.setParentPath(page.getLanguages().get(0).getParentPath());
			Optional<LanguagePage> languagePage = page.getLanguages().stream().filter(x -> LanguageType.SPANISH.getCode().equals(x.getLanguage())).findFirst();
			if(languagePage.isPresent())
				pageHeader.setName(languagePage.get().getTitle());
		}
		
		pageHeader.setLastUpdate(LocalDateTime.now());
		
		return pageHeaderRepository.save(pageHeader);	
	}
	
	private DraftPage updateDraftPage(Page page, String username) throws CaBusinessException {
		
		Optional<DraftPage> draftPageOptional = draftPageRepository.findById(page.getId());
		
		if(!draftPageOptional.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST_UPDATE), INTERNAL_SERVER_ERROR);
		}
		
		DraftPage draftPage = draftPageOptional.get();
		draftPage.setUser(username);
		draftPage.setLanguages(page.getLanguages());
		draftPage.setModifiedDate(LocalDateTime.now());
		draftPage.setType(page.getType());
		draftPage.setPageType(page.getPageType());
		draftPage.setCategoryId(page.getCategoryId());
		draftPage.setCountryId(page.getCountryId());
		draftPage.setDescriptorId(page.getDescriptorId());
		draftPage.setDestinationId(page.getDestinationId());
		draftPage.setRoiback(page.getRoiback());
		draftPage.setRevinate(page.getRevinate());
		draftPage.setExternal(TemplateType.LINK.getCode().equals(page.getCategoryId()) ? Boolean.TRUE : Boolean.FALSE );
		draftPage.setShortName(page.getShortName());
		draftPage.setContact(page.getContact());
		draftPage.setBeginDate(page.getBeginDate());
		draftPage.setFirstExpiration(page.getFirstExpiration());
		draftPage.setSecondExpiration(page.getSecondExpiration());
		draftPage.setCountdown(page.getCountdown());
		draftPage.setGenericType(page.getGenericType());
		draftPage.setDomainId(page.getDomainId());
		draftPage.setClusterId(page.getClusterId());
		draftPage.setDestinationMapId(page.getDestinationMapId());
		draftPage.setDefaultHome(page.getDefaultHome());
		
		return draftPageRepository.save(draftPage);
	}

	private DraftPage createDraftPageForExternal(Page page, String username) {
		LocalDateTime now = LocalDateTime.now();
		DraftPage draftPage = ObjectMapperUtils.map(page, DraftPage.class);

		draftPage.setUser(username);
		draftPage.setModifiedDate(now);

		return draftPageRepository.save(draftPage);
	}

	private Page publishPageUnPublish(Page pageNew, String username) {
		LocalDateTime now = LocalDateTime.now();

		pageNew.setModifiedDate(now);
		pageNew.setPublishDate(now);
		pageNew.setUser(username);
		pageNew.setState(StatusPageType.PUBLISHED.getCode());
		pageNew.setPublish(Boolean.TRUE);
		pageNew.setPublishDate(now);

		return pageRepository.save(pageNew);
	}
	
	private Page copyDraftPageInPage(DraftPage page, Page pageNew) {
		LocalDateTime now = LocalDateTime.now();
		
		pageNew.setModifiedDate(now);
		pageNew.setPublishDate(now);
		pageNew.setUser(page.getUser());
		pageNew.setLanguages(page.getLanguages());
		pageNew.setPageType(page.getPageType());
		pageNew.setType(page.getType());
		pageNew.setName(page.getName());
		pageNew.setPublish(Boolean.TRUE);
		pageNew.setCategoryId(page.getCategoryId());
		pageNew.setCountryId(page.getCountryId());
		pageNew.setDestinationId(page.getDestinationId());
		pageNew.setDescriptorId(page.getDescriptorId());
		pageNew.setRoiback(page.getRoiback());
		pageNew.setRevinate(page.getRevinate());
		pageNew.setExternal(TemplateType.LINK.getCode().equals(page.getCategoryId()) ? Boolean.TRUE : Boolean.FALSE);
		pageNew.setShortName(page.getShortName());
		pageNew.setContact(page.getContact());
		pageNew.setBeginDate(page.getBeginDate());
		pageNew.setFirstExpiration(page.getFirstExpiration());
		pageNew.setSecondExpiration(page.getSecondExpiration());
		pageNew.setCountdown(page.getCountdown());
		pageNew.setGenericType(page.getGenericType());
		pageNew.setDomainId(page.getDomainId());
		pageNew.setClusterId(page.getClusterId());
		pageNew.setDestinationMapId(page.getDestinationMapId());
		pageNew.setDefaultHome(page.getDefaultHome());
		pageNew.setState(StatusPageType.PUBLISHED.getCode());
		
		return pageRepository.save(pageNew);
	}
	
	@Override
	public List<PageHeader> getListPageHeaderForTray() {
		return pageHeaderRepository.findByActiveCustomized(StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> findPageByLanguageAndPath(String language, String value, Integer limit) {
		return pageRepository.findPageByLanguageAndPath(language, value,StatusPageType.DELETED.getCode(), PageUtil.getPageable(1,limit));
	}

	@Override
	public List<Page> findDestinationByCountryIdAndLanguageCustomized(String countryId) {
		return pageRepository.findByCountryIdAndLanguageCustomized(countryId, TemplateType.DESTINATIONS_DETAIL.getCode(), StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> findHotelsByDestinationCustomized(String destination) {
		return pageRepository.findHotelsByDestinationCustomized(TemplateType.HOTELS_DETAIL.getCode(), destination, StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> findHotelsByDescriptorCustomized(String descriptor) {
		return pageRepository.findHotelsByDescriptorCustomized(descriptor, TemplateType.HOTELS_DETAIL.getCode(), StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> findPromotionsByDescriptorAndCategory(String descriptorData, String categoryData) {
		if(ConstantsUtil.DESCRIPTOR_ALL.equalsIgnoreCase(descriptorData)){
			return pageRepository.findPromotionsByCategoryCustomized(TemplateType.PROMOTIONS.getCode(), StatusPageType.DELETED.getCode());
		} else {
			return pageRepository.findPromotionsByDescriptorAndCategoryCustomized(TemplateType.PROMOTIONS.getCode(), descriptorData, categoryData, StatusPageType.DELETED.getCode());
		}
	}

	@Override
	public void deletePage(String id) throws CaBusinessException {
		Optional<PageHeader> pageHeaderOptional = pageHeaderRepository.findById(id);

		if(!pageHeaderOptional.isPresent()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
		}

		pageHeaderRepository.delete(pageHeaderOptional.get());

		Optional<DraftPage> draftPageOptional = draftPageRepository.findById(id);
		if(draftPageOptional.isPresent()){
			draftPageRepository.delete(draftPageOptional.get());
		}

		Optional<Page> pageOptional = pageRepository.findById(id);
		if(pageOptional.isPresent()){
			pageRepository.delete(pageOptional.get());
		}
	}

	@Override
	public Page getPageForView(String id, Integer state) throws CaRequiredException {
		Page page = null;

		String message = messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST);

		if(StatusPageType.IN_EDITION.getCode().equals(state)){

			Optional<DraftPage> draftPageOptional = draftPageRepository.findByIdAndState(id, state);
			if (!draftPageOptional.isPresent())
				throw new CaRequiredException(message);
			page = ObjectMapperUtils.map(draftPageOptional.get(), Page.class);

		} else if (StatusPageType.PUBLISHED.getCode().equals(state)) {

			Optional<Page> pageOptional = pageRepository.findByIdAndState(id, state);
			if (!pageOptional.isPresent())
				throw new CaRequiredException(message);
			page = pageOptional.get();

		}
		return page;
	}

	public Page findByIdAndState(String id){
		return pageRepository.findByIdAndStateNot(id, StatusPageType.DELETED.getCode()).orElse(null);
	}

	@Override
	public DraftPage unPublishPage(String id) throws CaBusinessException {

		UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

		Optional<Page> pageOptional = pageRepository.findById(id);

		if(!pageOptional.isPresent()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		Optional<PageHeader> pageHeaderOptional = pageHeaderRepository.findById(id);
		if(!pageHeaderOptional.isPresent()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		PageHeader pageHeader = pageHeaderOptional.get();

		if(pageHeader.getInEdition()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_USER_EDIT_EXIST) +  pageHeader.getUsername(), HttpStatus.NOT_ACCEPTABLE.value());
		}

		DraftPage draftPage = ObjectMapperUtils.map(pageOptional.get(), DraftPage.class);
		draftPage.setState(StatusPageType.IN_EDITION.getCode());
		draftPage.setUser(userPrincipal.getUsername());
		draftPage.setModifiedDate(LocalDateTime.now());
		draftPage.setPublish(Boolean.FALSE);

		pageRepository.delete(pageOptional.get());
		draftPageRepository.save(draftPage);

		pageHeader.setState(StatusPageType.IN_EDITION.getCode());
		pageHeader.setUsername(userPrincipal.getUsername());
		pageHeader.setPublish(Boolean.FALSE);
		pageHeader.setLastUpdate(LocalDateTime.now());

		pageHeaderRepository.save(pageHeader);

		return draftPage;
	}

	public void validateExistPathInList(List<LanguagePage> languagePages) throws CaBusinessException{
		if(CaUtil.validateIsNotNullAndNotEmpty(languagePages)){
			int size = languagePages.size();
			if(size > ConstantsUtil.ONE){
				List<String> listPath = languagePages.stream().map(this::buildPath).collect(Collectors.toList());
				List<String> listNotDuplicated = listPath.stream().distinct().collect(Collectors.toList());
				if(listPath.size() > listNotDuplicated.size()){
					throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_PATH_REPEAT), HttpStatus.NOT_ACCEPTABLE.value());
				}
			}
		}
	}

	private String buildPath(LanguagePage languagePage){
		return languagePage.getPath();
	}

	@Override
	public List<Page> findRestaurantsByDescriptorCustomized(String descriptor) {
		return pageRepository.findRestaurantsByDescriptorCustomized(descriptor, TemplateType.RESTAURANTS.getCode(), StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> findHotelsByLanguageAndName(String language, String value, Integer limit) {

		List<Page> listPage = pageRepository.findHotelsByLanguageAndName(language, value, StatusPageType.DELETED.getCode(), TemplateType.HOTELS_DETAIL.getCode(), PageUtil.getPageable(1,limit));
		List<Page> listPageResult = new ArrayList<>();

		listPage.forEach(x ->
				x.getLanguages().forEach(k -> {
					if(k.getLanguage().equals(language)) {

						List<LanguagePage> languages = new ArrayList<>();
						languages.add(k);

						listPageResult.add(Page.builder()
								.id(x.getId())
								.external(x.getExternal())
								.languages(languages)
								.build()
						);
					}
				})
		);

		return listPageResult;
	}

	@Override
	public List<LanguagePage> customFindByIdAndLanguageCode(String id, String language) {
		return pageRepository.customFindByIdAndLanguageCode(id, language, TemplateType.HOTELS_DETAIL.getCode(), StatusPageType.DELETED.getCode()).getLanguages();
	}

	@Override
	public List<LanguagePage> findPageByIdAndLanguage(String id, String language) {
		return pageRepository.customPageFindByIdAndLanguageCode(id, language, StatusPageType.DELETED.getCode()).getLanguages();
	}

	@Override
	public Page findByPathAndState(String path, String id, String language){
		if(CaUtil.validateIsNotNullAndNotEmpty(path) && !ConstantsUtil.SEPARATOR.equals(path)){
			return pageRepository.customFindByPathAndStateNot(path, StatusPageType.DELETED.getCode()).orElse(null);
		} else if(CaUtil.validateIsNotNullAndNotEmpty(id)) {
			return pageRepository.findByIdAndStateNotAndLanguage(id, StatusPageType.DELETED.getCode(), language).orElse(null);
		} else {
			Configurations configurations = configurationsRepository.customFindAllHomeId().stream().findFirst().orElse(null);
			if(configurations != null && CaUtil.validateIsNotNullAndNotEmpty(configurations.getHomeId()))
				return pageRepository.findByIdAndStateNotAndLanguage(configurations.getHomeId(), StatusPageType.DELETED.getCode(), language).orElse(null);
			else
				return null;
		}
	}

	private Pageable getPageableAutocomplete(){
		Sort sort = new Sort(Sort.Direction.DESC, "lastUpdate");
		return PageRequest.of(ConstantsUtil.ZERO, ConstantsUtil.LIMIT, sort);
	}

	private Pageable getPageableAutocompleteByName(){
		Sort sort = new Sort(Sort.Direction.ASC, "name");
		return PageRequest.of(ConstantsUtil.ZERO, ConstantsUtil.LIMIT, sort);
	}

	@Override
	public List<PageHeader> findPageHotelsByNameHotelAndNameDestinations(String name){
		List<String> categories = new ArrayList<>();
		categories.add(TemplateType.HOTELS_DETAIL.getCode());
		categories.add(TemplateType.DESTINATIONS_DETAIL.getCode());

		org.springframework.data.domain.Page<PageHeader> destinationsHotelHeader = pageHeaderRepository
				.customFindDestinationHotelByNameLikeAndStateNotAndPublish(categories,name, StatusPageType.DELETED.getCode(), Boolean.TRUE, ConstantsUtil.ZERO.toString(), getPageableAutocompleteByName());

		return getPageHotelsFromListPageHeader(destinationsHotelHeader.getContent());
	}

	@Override
	public List<PageHeader> findPageHotelsByNameHotelAndNameDestinationsToPage(String name){
		List<String> categories = new ArrayList<>();
		categories.add(TemplateType.HOTELS_DETAIL.getCode());
		categories.add(TemplateType.DESTINATIONS_DETAIL.getCode());

		org.springframework.data.domain.Page<PageHeader> destinationsHotelHeader = pageHeaderRepository
				.customFindDestinationHotelByNameLikeAndStateNotAndPublish(categories,name, StatusPageType.DELETED.getCode(), Boolean.TRUE, getPageableAutocomplete());

		return getPageHotelsFromListPageHeader(destinationsHotelHeader.getContent());
	}

	@Override
	public List<PageHeader> findPageAutocompleteByNameAndCategory(String name, String templateType){
		List<String> categories = new ArrayList<>();
		categories.add(templateType);

		org.springframework.data.domain.Page<PageHeader> destinationsHotelHeader = pageHeaderRepository
				.customFindDestinationHotelByNameLikeAndStateNotAndPublish(categories,name, StatusPageType.DELETED.getCode(), Boolean.TRUE, ConstantsUtil.ZERO.toString(), getPageableAutocompleteByName());

		return getPageHotelsFromListPageHeader(destinationsHotelHeader.getContent());
	}

	@Override
	public List<PageHeader> findPageAutocompleteByNameAndCategoryAndDestinationId(String name, String templateType, String destinationId){
		List<String> categories = new ArrayList<>();
		categories.add(templateType);

		org.springframework.data.domain.Page<PageHeader> destinationsHotelHeader = pageHeaderRepository
				.customFindDestinationHotelByNameLikeAndStateNotAndPublishAndDestinationId(categories,name, StatusPageType.DELETED.getCode(), Boolean.TRUE, ConstantsUtil.ZERO.toString(), destinationId, getPageableAutocompleteByName());

		return getPageHotelsFromListPageHeader(destinationsHotelHeader.getContent());
	}

	@Override
	public List<PageHeader> findPageAutocompleteByNameAndCategoryToPage(String name, String templateType){
		List<String> categories = new ArrayList<>();
		categories.add(templateType);

		org.springframework.data.domain.Page<PageHeader> destinationsHotelHeader = pageHeaderRepository
				.customFindDestinationHotelByNameLikeAndStateNotAndPublish(categories,name, StatusPageType.DELETED.getCode(), Boolean.TRUE, getPageableAutocomplete());

		return getPageHotelsFromListPageHeader(destinationsHotelHeader.getContent());
	}

	@Override
	public List<PageHeader> getPageHotelsByName(String name){
		List<PageHeader> listHotels = pageHeaderRepository.customFindHotelByNameLikeAndStateNotAndPublish(TemplateType.HOTELS_DETAIL.getCode(), name, StatusPageType.DELETED.getCode(), Boolean.TRUE);
		return getPageHotelsFromListPageHeader(listHotels);
	}

	private List<PageHeader> getPageHotelsFromListPageHeader(List<PageHeader> listHotels){

		Set<String> setDestinations = listHotels.stream().filter(x -> x.getDestinationId() != null)
				.map(x -> x.getDestinationId())
				.collect(Collectors.toSet());

		if(Objects.nonNull(setDestinations) && !setDestinations.isEmpty()){
			List<PageHeader> destinationsHeader = this.pageHeaderRepository.customFindByIdsShort(setDestinations.stream().collect(Collectors.toList()));
			Map<String, String> mapDestinationsName = destinationsHeader.stream().collect(Collectors.toMap(PageHeader::getId, PageHeader::getName));

			listHotels.forEach(page ->
				page.setDestinationName(Objects.nonNull(page.getDestinationId())
										? mapDestinationsName.get(page.getDestinationId())
										: ConstantsUtil.EMPTY)
			);
		}

		return listHotels.stream().sorted(Comparator.comparing(PageHeader::getLastUpdate)).collect(Collectors.toList());
	}

	@Override
	public Page viewPageExternal(String id, String language) throws CaBusinessException {

		Optional<Page> optionalPage = pageRepository.customFindByIdAndLanguageAndStateNot(id, language, StatusPageType.DELETED.getCode());

		if (!optionalPage.isPresent()){
			throw  new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), HttpStatus.NOT_FOUND.value());
		}

		return optionalPage.get();
	}

	@Override
	public List<Page> findPageByLanguageAndPathAndExternalNot(String language, String value, Integer limit) {
		return pageRepository.findPageByLanguageAndPathAndExternalNot(language, value,StatusPageType.DELETED.getCode(), Boolean.FALSE, PageUtil.getPageable(1,limit));
	}

	@Override
	public List<Page> findPageByLanguageAndPathAndExternal(String language, String value, Integer limit) {
		Sort sort = new Sort(Sort.Direction.ASC, "languages.path");
		return pageRepository.findPageByLanguageAndPathAndExternal(language, value,StatusPageType.DELETED.getCode(), PageUtil.getPageableSort(1,limit, sort));
	}

	@Override
	public List<PageHeader> findDestinationsByStateNotPublish(){
		return pageHeaderRepository.customFindDestinationsByStateNotPublish(TemplateType.DESTINATIONS_DETAIL.getCode(),
				StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

	@Override
	public List<Page> findHotelsByDestinationAndLanguage(String destinationId, String language){
		return pageRepository.findHotelsByDestinationAndLanguageCustomizedAndRoibackNot(TemplateType.HOTELS_DETAIL.getCode(), destinationId, StatusPageType.DELETED.getCode(), language, ConstantsUtil.ZERO.toString());
	}

	public Optional<PageHeader> findPageHeaderByPageId(String pageId) {
		return this.pageHeaderRepository.findById(pageId);
	}

	public List<PageHeader> getHotelsByArraysIds(List<String> items){
		return this.pageHeaderRepository.findByIdInAndCategoryIdAndStateNotAndPublish(items, TemplateType.HOTELS_DETAIL.getCode(),
				StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

	@Override
	public List<Page> findHotelsRestaurantsByDescriptorInCustomized(List<String> descriptors, String language) {
		List<String> categories = new ArrayList<>();
		categories.add(TemplateType.HOTELS_DETAIL.getCode());
		categories.add(TemplateType.RESTAURANTS_DETAIL.getCode());
		return pageRepository.findHotelsRestaurantsByDescriptorInAndStateNotCustomized(descriptors,
				categories, StatusPageType.DELETED.getCode(), language, ConstantsUtil.ZERO.toString());
	}

	@Override
	public List<Page> getHotelsByDestinationIdForSite(String destinationId, String language){
		return pageRepository.findHotelsByDestinationAndLanguageCustomized(TemplateType.HOTELS_DETAIL.getCode(),
				destinationId, StatusPageType.DELETED.getCode(), language);
	}

	@Override
	public PageHeader updatePageHeaderInEdition(PageHeader pageHeader) throws CaBusinessException {

		Optional<PageHeader> pageHeaderOptional = pageHeaderRepository.findById(pageHeader.getId());

		if(!pageHeaderOptional.isPresent()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

		if(userPrincipal.getUsername().equals(pageHeaderOptional.get().getUserEdition())){
			PageHeader pageHeaderSave = pageHeaderOptional.get();
			pageHeaderSave.setInEdition(pageHeader.getInEdition());

			return pageHeaderRepository.save(pageHeaderSave);
		}
		return new PageHeader();
	}

	@Override
	public List<PageHeader> findPageHeaderHotelsByDestinationsIds(List<String> destinationIds){
		if(CaUtil.validateListIsNotNullOrEmpty(destinationIds)){
			return pageHeaderRepository.customFindHotelsByDestinationsIds(TemplateType.HOTELS_DETAIL.getCode(),
					StatusPageType.DELETED.getCode(), Boolean.TRUE, destinationIds);
		} else {
			return pageHeaderRepository.customFindDestinationsByStateNotPublish(TemplateType.HOTELS_DETAIL.getCode(),
					StatusPageType.DELETED.getCode(), Boolean.TRUE);
		}
	}

	@Override
	public List<Page> getHotelsWithLivingToEvents(String destinationId, String language) {
		return this.pageRepository.findHotelsByDestinationAndLanguageCustomized(TemplateType.HOTELS_DETAIL.getCode(),
				destinationId, StatusPageType.DELETED.getCode(), language);
	}

	@Override
	public List<Page> findHotelsByDestinationAndDescriptorCustomized(String destination, String descriptor) {
		return pageRepository.findHotelsByDestinationAndDescriptorCustomized(TemplateType.HOTELS_DETAIL.getCode(), destination, descriptor, StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> findRestaurantsByDestinationAndDescriptorCustomized(String destination, String descriptor) {
		return pageRepository.findHotelsByDestinationAndDescriptorCustomized(TemplateType.RESTAURANTS_DETAIL.getCode(), destination, descriptor, StatusPageType.DELETED.getCode());
	}

	@Override
	public HotelDestination getHotelsByDestinationIdAndLanguageForSite(String destinationId, String language){

		Optional<Page> destinationOptional = this.pageRepository.customFindByIdAndLanguageNameElementMatch(destinationId, language);
		if(destinationOptional.isPresent()){
			LanguagePage languagePage = destinationOptional.get().getLanguages().stream().findFirst().orElse(null);
			if(Objects.nonNull(languagePage) && Objects.nonNull(languagePage.getData()))
				return getHotelDestinationFromData(languagePage.getData());
			else
				return null;
		}
		return null;
	}

	private HotelDestination getHotelDestinationFromData(Object data){
		HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);
		if(Objects.nonNull(hotelDataDto) && Objects.nonNull(hotelDataDto.getCaHotelDestination())){
			return hotelDataDto.getCaHotelDestination();
		}
		return null;
	}

	@Override
	public List<Page> findHotelsByIdsAndLanguageCustomized(List<String> ids, String language){
		return pageRepository.findHotelsByIdsAndLanguageCustomized(ids,StatusPageType.DELETED.getCode(),language);
	}

	@Override
	public List<Page> findHotelsByIdsAndLanguageCustomized(LivingDestinationHotel livingDestinationHotel){
		List<String> ids = livingDestinationHotel.getHotelCategories().stream().map(HotelCategory::getHotelId).collect(Collectors.toList());
		return pageRepository.findHotelsByIdsAndLanguageCustomized(ids,StatusPageType.DELETED.getCode(),livingDestinationHotel.getLanguage());
	}

	@Override
	public List<PageHeader> getTitle2PageFromContact(List<String> items) {
		return pageHeaderRepository.getPageContactByIds(items, StatusPageType.DELETED.getCode());
	}

	@Override
	public List<PageHeader> findDestinationsByStateNotPublishByItems(List<String> items){
		return this.pageHeaderRepository.getPageContactByIds(items, StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> customFindPageByIdsAndLanguageAndStateNot(List<String> ids, String language) {
		return this.pageRepository.customFindPageByIdsAndLanguageAndStateNot(ids, StatusPageType.DELETED.getCode(), language);
	}

	@Override
	public List<Page> getPromotionsDetail(String language){
		return this.pageRepository.customFindPageByCategoryIdAndLanguageAndStateNot(TemplateType.PROMOTIONS_DETAIL.getCode(),
				StatusPageType.DELETED.getCode(), language);
	}

	@Override
	public Page getPromotionByIdAndLanguage(String id, String language) throws CaBusinessException {
		Optional<Page> pageOptional = this.pageRepository.customFindByIdAndLanguageAndStateNot(id, language, StatusPageType.DELETED.getCode());

		if(!pageOptional.isPresent()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		return pageOptional.get();
	}

	@Override
	public List<Page> customFindHotelsByDestinationIdAndDescriptorIdAndLanguage(String destinationId, String descriptorId, String language) {
		return this.pageRepository.customFindHotelsByDestinationIdAndDescriptorIdAndLanguage(TemplateType.HOTELS_DETAIL.getCode(),
				destinationId,descriptorId,language, ConstantsUtil.ZERO.toString());
	}

	@Override
	public List<Page> customFindDestinationByStateAndLanguage(String language) {
		return this.pageRepository.customFindDestinationByStateAndLanguage(TemplateType.DESTINATIONS_DETAIL.getCode(),
				StatusPageType.DELETED.getCode(), language);
	}

	@Override
	public List<Page> customFindDescriptorByHotelsAndDestinationIds(List<String> items){

		Set<Page> pageSet = new HashSet<>();

		List<Page> pages = this.pageRepository.customFindDescriptorByHotelsAndDestinationIdsAndRoibackNot(TemplateType.HOTELS_DETAIL.getCode(),
				items, StatusPageType.DELETED.getCode(), ConstantsUtil.ZERO.toString());
		if(CaUtil.validateListIsNotNullOrEmpty(pages)){
			pageSet.addAll(pages);
		}
		return pageSet.stream().collect(Collectors.toList());
	}

	@Override
	public List<Page> getContactByPages(List<String> ids, String language) throws CaRequiredException {
		return this.pageRepository.getContactByPage(ids, StatusPageType.DELETED.getCode(), language);
	}

	@Override
	public List<Page> getHotelsByDestinationAndDescriptorAndStateNot(String destinationId, String descriptorId) {
		return this.pageRepository.getHotelsByDestinationAndDescriptorAndStateNot(TemplateType.HOTELS_DETAIL.getCode(),
				destinationId, descriptorId, StatusPageType.DELETED.getCode(), ConstantsUtil.ZERO.toString());
	}

	@Override
	public List<Page> getPromotionBell(List<String> promotions, String language){
		return this.pageRepository.getPromotionBell(promotions, language, StatusPageType.DELETED.getCode());
	}

	@Override
	public List<PageHeader> getDestinationsAll() {
		return this.pageHeaderRepository.customFindDestinationsByStateNotPublish(TemplateType.DESTINATIONS_DETAIL.getCode(),
				StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

	@Override
	public List<Page> findHotelsByListDestinationsAndLanguageCustomized(List<String> destinations, String language){
		if(Objects.nonNull(destinations) && !destinations.isEmpty()){
			return pageRepository.findHotelsByListDestinationsAndLanguageCustomized(TemplateType.HOTELS_DETAIL.getCode(),
					destinations, StatusPageType.DELETED.getCode(), language);
		} else {
			return pageRepository.findHotelsByLanguageCustomized(TemplateType.HOTELS_DETAIL.getCode(),
					StatusPageType.DELETED.getCode(), language);
		}
	}

	@Override
	public List<Page> getPromotionsByPromotionType(String promotionType, String language) {
		return this.pageRepository.findPromotionByPromotionType(TemplateType.PROMOTIONS_DETAIL.getCode(), promotionType, language,
				StatusPageType.DELETED.getCode());
	}

	@Override
	public com.casaandina.cms.util.pagination.Page<PagePromotionView> getFeaturedPromotionsByFilters(FeaturedPromotion featuredPromotion,
																									 HostIpCountry hostIpCountry, Pageable pageable) {
		List<String> items = featuredPromotion.getItems();
		List<String> destinations = featuredPromotion.getDestinations();
		List<String> hotels = featuredPromotion.getHotels();
		List<String> descriptors = featuredPromotion.getDescriptors();
		List<Descriptor> descriptorList = getDescriptorsByHotelsAll();

		org.springframework.data.domain.Page<PagePromotionView> promotions = getPagePromotionByFilters(items, destinations, hotels,
				descriptors, descriptorList, featuredPromotion.getLanguage(), hostIpCountry, pageable);

		return getPagePromotionView(promotions, featuredPromotion.getCurrency());
	}

	private com.casaandina.cms.util.pagination.Page<PagePromotionView> getPagePromotionView(
			org.springframework.data.domain.Page<PagePromotionView> promotions, String currency){

		List<PagePromotionView> pagePromotionViews = promotions.getContent();
		if(Objects.nonNull(pagePromotionViews) && !pagePromotionViews.isEmpty()){

			Optional<Currency> currencyOptional = currencyRepository.findByCodeIsoAndStatusCustomized(currency, StatusType.ACTIVE.getCode());
			Double currencyValue = currencyOptional.isPresent() ? currencyOptional.get().getValue() : Double.valueOf(ConstantsUtil.ONE.toString());

			List<String> descriptors = pagePromotionViews.stream().map(this::getDescriptorFromPagePromotionView).collect(Collectors.toList());
			List<Descriptor> descriptorList = descriptorRepository.customFindDescriptorByIds(descriptors);
			Map<String, Descriptor> descriptorMap = descriptorList.stream().collect(Collectors.toMap(Descriptor::getId, Function.identity()));
			pagePromotionViews.forEach(pro -> {
				if(Objects.nonNull(pro.getItem())) {
					Descriptor des = descriptorMap.get(pro.getItem().getDescriptor());

					if(Objects.nonNull(pro.getItem())) {
						pro.getItem().setDescriptorTitle(des.getTitle());
						pro.getItem().setDescriptorColor(des.getColor());
						pro.getItem().setPrice(getPriceWithCurrency(pro.getItem().getPrice(), currencyValue));
					}
					if(Objects.nonNull(pro.getInfo()))
						pro.getInfo().setPrice(getPriceWithCurrency(pro.getInfo().getPrice(), currencyValue));

					if (Objects.nonNull(pro.getLanguagesPage()) && !pro.getLanguagesPage().isEmpty()) {
						LanguagePage languagePage = pro.getLanguagesPage().stream()
								.filter(x -> pro.getLanguage().equals(x.getLanguage()))
								.findFirst().orElse(null);
						if(Objects.nonNull(languagePage) && Objects.nonNull(pro.getItem())) {
							pro.getItem().setTitleHotel(Objects.nonNull(languagePage.getTitle()) ? languagePage.getTitle() : ConstantsUtil.EMPTY);
							if(!(Objects.nonNull(pro.getItem().getImageMain()) && !pro.getItem().getImageMain().isEmpty()) && Objects.nonNull(languagePage.getData())){
								pro.getItem().setImageMain(getImageMainFromData(languagePage.getData()));
							}
						}
					}
				}
			});
			return PageUtil.pageToResponse(promotions, pagePromotionViews);
		}
		return PageUtil.pageToResponse(promotions);
	}

	private List<Object> getImageMainFromData(Object data){
		HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);
		if(Objects.nonNull(hotelDataDto) && Objects.nonNull(Objects.nonNull(hotelDataDto.getCaHotelSummary())))
			return Objects.nonNull(hotelDataDto.getCaHotelSummary().getImageMain())
					? ObjectMapperUtils.mapAll(hotelDataDto.getCaHotelSummary().getImageMain(), Object.class)
					: null;
		return null;
	}

	private String getDescriptorFromPagePromotionView(PagePromotionView pagePromotionView){
		if(Objects.nonNull(pagePromotionView) && Objects.nonNull(pagePromotionView.getItem())){
			return pagePromotionView.getItem().getDescriptor();
		}
		return ConstantsUtil.EMPTY;
	}

	private org.springframework.data.domain.Page<PagePromotionView> getPagePromotionByFilters(List<String> items, List<String> destinations,
																							  List<String> hotels, List<String> descriptors,
																							  List<Descriptor> descriptorList, String language,
																							  HostIpCountry hostIpCountry, Pageable pageable){
		List<String> domains = new ArrayList<>();
		List<String> clusters = new ArrayList<>();
		List<String> countries = new ArrayList<>();

		domains.add(ConstantsUtil.DOMAIN_ALL);
		clusters.add(ConstantsUtil.DOMAIN_ALL);
		countries.add(ConstantsUtil.DOMAIN_ALL);

		Optional<Domain> domain = this.domainRepository.findByPathAndState(hostIpCountry.getUriHost(), StatusType.ACTIVE.getCode());
		if(!domain.isPresent()){
			return this.pagePromotionViewRepository.findById(null, pageable);
		} else {
			domains.add(domain.get().getId());
		}

		Optional<Country> country = this.countryRepository.findByCodeAndStatusCustomized(hostIpCountry.getIsoCountry(), Boolean.TRUE);
		if(country.isPresent()){
			countries.add(country.get().getId());
			List<String> clustersByCountryId = getClusterByCountryId(country.get().getId());
			if(Objects.nonNull(clustersByCountryId) && !clustersByCountryId.isEmpty())
				clusters.addAll(clustersByCountryId);
		}

		if(!(Objects.nonNull(hotels) && !hotels.isEmpty())){
			hotels = getHotelsIdByDestinations(destinations);
		}

		if(!(Objects.nonNull(descriptors) && !descriptors.isEmpty())){
			descriptors = descriptorList.stream().map(Descriptor::getId).collect(Collectors.toList());
		}

		Sort sort = new Sort(Sort.Direction.DESC, "modifiedDate");
		Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

		LocalDateTime today = LocalDateTime.now();

		if(!(Objects.nonNull(items) && !items.isEmpty())){
			return this.pagePromotionViewRepository.getPagePromotionByFilters(hotels, descriptors, language, domains, clusters, countries, today, pageableSort);
		} else {
			return this.pagePromotionViewRepository.getPagePromotionByFilters(items, hotels, descriptors, language, domains, clusters, countries, today, pageableSort);
		}
	}

	private List<String> getClusterByCountryId(String countryId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("countries.id").is(countryId));
		List<Cluster> clusters = mongoTemplate.find(query, Cluster.class);

		if(Objects.nonNull(clusters) && !clusters.isEmpty()){
			return clusters.stream().map(Cluster::getId).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private List<String> getHotelsIdByDestinations(List<String> destinations){
		List<PageHeader> pageHeaders = null;
		if(Objects.nonNull(destinations) && !destinations.isEmpty()){
			pageHeaders = this.pageHeaderRepository.customFindHotelsByDestinationsIds(TemplateType.HOTELS_DETAIL.getCode(),
					StatusPageType.DELETED.getCode(), Boolean.TRUE, destinations);
		} else {
			pageHeaders = this.pageHeaderRepository.customFindDestinationsByStateNotPublish(TemplateType.HOTELS_DETAIL.getCode(),
					StatusPageType.DELETED.getCode(), Boolean.TRUE);
		}
		if(Objects.nonNull(pageHeaders) && !pageHeaders.isEmpty()){
			return pageHeaders.stream().map(PageHeader::getId).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private List<Descriptor> getDescriptorsByHotelsAll(){
		return this.descriptorRepository.customFindDescriptorByCategory(TemplateType.HOTELS_DETAIL.getCode());
	}

	@Override
	public List<PromotionUrlReservation> getUrlReservationByPromotionAndHotelsAndRestaurants(String promotions, String currency, String language){
		DataPromotionUrlDto dataPromotionUrlDto = new Gson().fromJson(promotions, DataPromotionUrlDto.class);

		if(Objects.nonNull(dataPromotionUrlDto) && Objects.nonNull(dataPromotionUrlDto.getData())
				&& !dataPromotionUrlDto.getData().isEmpty()){

			List<PromotionUrlReservation> promotionUrlReservations = new ArrayList<>();

			List<PromotionUrlReservation> data = dataPromotionUrlDto.getData()
											.stream().filter(x-> !ConstantsUtil.EMPTY.equals(x.getIdPromotion()))
											.collect(Collectors.toList());

			if(Objects.nonNull(data) && !data.isEmpty()){
				Set<String> listPromotions = new HashSet<>();

				listPromotions.addAll(
						data.stream()
								.map(PromotionUrlReservation::getIdPromotion)
								.collect(Collectors.toList())
				);

				String domain = getDomainInConfigurations();

				List<Page>  pagePromotions = this.pageRepository.getPromotionBell(listPromotions.stream().collect(Collectors.toList()), language, StatusPageType.DELETED.getCode());
				Map<String, Page> pagesMap = pagePromotions.stream().collect(Collectors.toMap(Page::getId, Function.identity()));
				Optional<Currency> currencyOptional = currencyRepository.findByCodeIsoAndStatusCustomized(currency, StatusType.ACTIVE.getCode());
				Double currencyValue = currencyOptional.isPresent() ? currencyOptional.get().getValue() : Double.valueOf(ConstantsUtil.ONE.toString());

				data.forEach( x -> {
					Page page = pagesMap.get(x.getIdPromotion());
					if(Objects.nonNull(page)){
						LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
						PromotionDataDto promotionDataDto =  Objects.nonNull(languagePage) ? getHotelRestPromotionItemFromData(languagePage.getData()) : null;
						if(Objects.nonNull(x.getIdHotelRest())){
							PromotionUrlReservation promotionUrlReservation = getPromotionsWithHotelRestUrlAndPriceReservation(x.getIdHotelRest(),promotionDataDto, currencyValue, x.getIdPromotion());
							if(Objects.nonNull(promotionUrlReservation)){
								promotionUrlReservation.setUrlPromotion(languagePage.getPath());
								promotionUrlReservation.setDomain(domain);
								promotionUrlReservations.add(promotionUrlReservation);
							}
						}
					}
				});
			}
			return promotionUrlReservations;
		}

		return Collections.emptyList();
	}

	@Override
	public List<Page> getPageWithDescriptor(List<String> items, String language) {
		return this.pageRepository.getPageWithDescriptor(items, language);
	}

	private PromotionDataDto getHotelRestPromotionItemFromData(Object data){
		if(Objects.nonNull(data)){
			return ObjectMapperUtils.map(data, PromotionDataDto.class);
		}
		return null;
	}

	private PromotionUrlReservation getPromotionsWithHotelRestUrlAndPriceReservation(String idHotelRest, PromotionDataDto promotionDataDto, Double currencyValue, String idPromotion){
		if(Objects.nonNull(promotionDataDto) && Objects.nonNull(promotionDataDto.getCaHotelRestPromotion())&&
				Objects.nonNull(promotionDataDto.getCaHotelRestPromotion().getItems()) &&
				!promotionDataDto.getCaHotelRestPromotion().getItems().isEmpty()){
			List<HotelRestPromotionItemDto> items = promotionDataDto.getCaHotelRestPromotion().getItems();

			HotelRestPromotionItemDto hotelRest = items.stream().filter(x -> idHotelRest.equals(x.getHotel())).findFirst().orElse(null);
			if(!Objects.nonNull(hotelRest)){
				hotelRest = items.stream().filter(x -> idHotelRest.equals(x.getRestaurant())).findFirst().orElse(null);
			}

			if(Objects.nonNull(hotelRest)) {
				return PromotionUrlReservation.builder()
						.idPromotion(idPromotion)
						.idHotelRest(idHotelRest)
						.urlReservation(hotelRest.getUrlReservation())
						.price(getPriceWithCurrency(hotelRest.getPrice(), currencyValue))
						.isRestriction(hotelRest.getIsRestriction())
						.imageRestriction((Objects.nonNull(hotelRest.getImageRestriction()) && !hotelRest.getImageRestriction().isEmpty())
								? hotelRest.getImageRestriction().get(ConstantsUtil.ZERO)
								: null)
						.bodyRestriction(hotelRest.getBodyRestriction())
						.footerRestriction(hotelRest.getFooterRestriction())
						.discount(hotelRest.getDiscount())
						.days(hotelRest.getDays())
						.nights(hotelRest.getNights())
						.build();
			}
			return null;
		}
		return null;
	}

	private Double getPriceWithCurrency(Double price, Double currencyValue){
		if(Objects.nonNull(price)){
			Double priceResult = price * currencyValue;
			return CaUtil.roundDouble(priceResult, ConstantsUtil.TWO, Boolean.TRUE);
		}
		return null;
	}

	@Override
	public List<Page> findPromotionsByStateAndLanguageCustomized(String language){
		return pageRepository.findPromotionsByStateAndLanguageCustomized(TemplateType.PROMOTIONS_DETAIL.getCode(),
					StatusPageType.DELETED.getCode(), language);
	}

	@Override
	public DraftPage getDraftPageByIdAndLanguage(String id, String language) throws CaSiteErrorException {
		Optional<DraftPage> draftPageOptional = this.draftPageRepository.customFindByIdAndLanguage(id, language);
		if(!draftPageOptional.isPresent())
			throw new CaSiteErrorException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), language, HttpStatus.NOT_FOUND.value());
		return draftPageOptional.get();
	}

	private String getDomainInConfigurations(){
		List<Configurations> configurations = this.configurationsRepository.customFindAllDomain();
		if(Objects.nonNull(configurations) &&  !configurations.isEmpty()){
			Configurations conf = configurations.stream().findFirst().orElse(null);
			if(Objects.nonNull(conf))
				return conf.getDomain();
		}
		return ConstantsUtil.EMPTY;
	}

	public List<PageHeader> findAllPageHome(){
		return this.pageHeaderRepository.customFindByCategoryIdAndStateAndPublish(TemplateType.HOME.getCode(),
				StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

	public List<PageHeader> findAllPagePromotionAll(){
		return this.pageHeaderRepository.customFindByCategoryIdAndStateAndPublish(TemplateType.PROMOTIONS.getCode(),
				StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

	public List<PageHeader> findAllPageByCategoryId(String categoryId){
		return this.pageHeaderRepository.customFindByCategoryIdAndStateAndPublish(categoryId,
				StatusPageType.DELETED.getCode(), Boolean.TRUE);
	}

	@Override
	public Optional<Page> customFindPathsBySEO(String id) {
		return this.pageRepository.customFindPathsBySEO(id);
	}

	@Override
	public Optional<Page> customFindPagePathByIdAndLanguage(String id, String language) {
		return this.pageRepository.customFindPathsByIdAndLanguage(id, language);
	}

	@Override
	public List<Page> findPromotionsByStateAndLanguageCustomizedFilter(String language){
		List<Page> pages = pageRepository.findPromotionsByStateAndLanguageCustomizedFilter(TemplateType.PROMOTIONS_DETAIL.getCode(),
				StatusPageType.DELETED.getCode(), language, ConstantsUtil.PROMOTION_TYPE_EXCLUSION, LocalDateTime.now());
		List<Page> pagesNew = new ArrayList<>();
		if(Objects.nonNull(pages) && !pages.isEmpty()){
			pages.forEach( x -> {
				if(Objects.nonNull(x.getSecondExpiration())){
					if(!CaUtil.isBeforeDate(x.getSecondExpiration())){
						pagesNew.add(x);
					}
				} else if (!(Objects.nonNull(x.getFirstExpiration()) && CaUtil.isBeforeDate(x.getFirstExpiration()))) {
					pagesNew.add(x);
				}
			});
		}
		return pagesNew;
	}

	@Override
	public List<Page> getHotelsByIdAndDescriptorAndStateNot(String hotelId) {
		return this.pageRepository.getHotelsByIdAndDescriptorAndStateNot(TemplateType.HOTELS_DETAIL.getCode(),
				hotelId, StatusPageType.DELETED.getCode());
	}

	@Override
	public List<Page> findAllHotels(String language){
		return pageRepository.findHotelsByLanguageCustomizedAndRoibackNot(TemplateType.HOTELS_DETAIL.getCode(), StatusPageType.DELETED.getCode(), language, ConstantsUtil.ZERO.toString());
	}

	@Override
	public PageHeader liberatePage(String id) throws CaBusinessException {

		Optional<PageHeader> pageHeaderOptional = pageHeaderRepository.findById(id);

		if(!pageHeaderOptional.isPresent()){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
		}

		UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();
		String username = userPrincipal.getUsername();

		PageHeader pageHeaderSave = pageHeaderOptional.get();
		pageHeaderSave.setInEdition(Boolean.FALSE);
		pageHeaderSave.setUserSetFree(username);
		pageHeaderSave.setUserEdition(username);
		pageHeaderSave.setLastUpdate(LocalDateTime.now());

		return pageHeaderRepository.save(pageHeaderSave);
	}

}
