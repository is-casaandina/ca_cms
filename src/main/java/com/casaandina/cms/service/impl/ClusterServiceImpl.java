package com.casaandina.cms.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Cluster;
import com.casaandina.cms.repository.ClusterRepository;
import com.casaandina.cms.service.ClusterService;
import com.casaandina.cms.util.ConstantsUtil;

@Service
public class ClusterServiceImpl implements ClusterService {
	
	private final ClusterRepository clusterRepository;
	
	@Autowired
	public ClusterServiceImpl(ClusterRepository clusterRepository) {
		this.clusterRepository = clusterRepository;
	}
	
	@Override
	public Cluster saveCluster(final Cluster cluster) throws CaBusinessException {
		return this.clusterRepository.insert(cluster);
	}

	@Override
	@Cacheable("cluster")
	public Cluster getClusterById(final String clusterId) throws CaRequiredException {
		
		Optional<Cluster> optional = this.clusterRepository.findById(clusterId);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
		
	}

	@Override
	@Transactional
	public Cluster updateCluster(final String clusterId,
			final Cluster cluster) throws CaBusinessException, CaRequiredException {
		
		Optional<Cluster> optional = this.clusterRepository.findById(clusterId);
		
		if (optional.isPresent()) {
			Cluster clusterOld = optional.get();
			BeanUtils.copyProperties(cluster, clusterOld, "id");
			return this.clusterRepository.save(clusterOld);
			
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
		
	}
	
	@Override
	public List<Cluster> getAllClusters() throws CaBusinessException {
		return this.clusterRepository.findAll();
	}

	@Override
	public List<Cluster> getAllClusterCustomized() throws CaBusinessException {
		return this.clusterRepository.findAllCustomized();
	}

	@Override
	public void deleteCluster(String clusterId) throws CaRequiredException {
		Optional<Cluster> optional = this.clusterRepository.findById(clusterId);
		if (optional.isPresent()) {
			this.clusterRepository.deleteById(clusterId);
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
		
	}

}
