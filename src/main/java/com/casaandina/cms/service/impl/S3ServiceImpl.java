package com.casaandina.cms.service.impl;

import com.casaandina.cms.aws.AmazonClient;
import com.casaandina.cms.dto.DimensionDto;
import com.casaandina.cms.dto.ResizeImageProperties;
import com.casaandina.cms.exception.AwsS3Exception;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.repository.*;
import com.casaandina.cms.rest.UserAuthentication;
import com.casaandina.cms.rest.request.PageTrayRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.ImageS3Response;
import com.casaandina.cms.rest.response.RangeDateResponse;
import com.casaandina.cms.rest.response.S3Parameter;
import com.casaandina.cms.security.UserPrincipal;
import com.casaandina.cms.service.S3Service;
import com.casaandina.cms.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jcodec.api.FrameGrab;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class S3ServiceImpl implements S3Service {

    private final AmazonClient amazonClient;

    private final IconS3Repository iconS3Repository;

    private final ImageS3Repository imageS3Repository;

    private final VideoS3Repository videoS3Repository;

    private final MessageProperties messageProperties;

    private final ParameterRepository parameterRepository;

    private final DocumentRepository documentRepository;

    private final JSRepository JSRepository;

    private final UserAuthentication userAuthentication;

    private static Logger logger = LoggerFactory.getLogger(S3ServiceImpl.class);

    @Value("${aws.endpointUrl}")
    private String endpointUrl;

    @Value("${aws.bucketName}")
    private String bucketName;

    @Value("${image.max-file-size-image}")
    private long maxFileSizeImage;

    @Value("${video.max-file-size-video}")
    private long maxFileSizeVideo;

    private static final String DIRECTORY_ICON = "icons";
    private static final String DIRECTORY_IMAGES = "images";
    private static final String DIRECTORY_VIDEO = "video";
    private static final String SYMBOL_X = "X";
    private static final String SYMBOL_SUBGUION = "_";
    private static final String SYMBOL_GUION = "-";
    private static final String NUMBER_ONE_STRING = "01";
    private static final String POINT_STRING = ".";
    private static final String FRAME_NAME = "frame";

    private static final String CREATE_DATE = "createDate";

    @SuppressWarnings("rawtypes")
    @Override
    public GenericResponse uploadFavicon(MultipartFile fileIcon, Integer userId) throws CaBusinessException {
        String fileName;
        File file;
        try {

            file = amazonClient.convertMultiPartToFile(fileIcon);
            fileName = amazonClient.generateFileName(fileIcon);
            validateExistIconInS3(fileName);
            String fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR
                    + DIRECTORY_ICON + ConstantsUtil.SEPARATOR + fileName;

            DimensionDto originalImgSize = CaUtil.getOriginalImageSize(fileName);
            amazonClient.uploadFileToS3Bucket(DIRECTORY_ICON, fileName, file);

            IconS3 iconS3 = IconS3.builder()
                    .url(fileUrl)
                    .type(IconsType.FAVICON.getCode())
                    .width(originalImgSize.getWidth())
                    .height(originalImgSize.getHeigth())
                    .name(fileName)
                    .mimeType(getMimeTypeFromFile(file))
                    .createdBy(userId)
                    .updatedBy(userId)
                    .build();

            Files.deleteIfExists(Paths.get(fileName));

            iconS3Repository.save(iconS3);
            return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_SAVE_UPLOAD_FAVICON_SUCCESFUL), iconS3);

        } catch (Exception e) {
            throw new CaBusinessException(
                    messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_FAVICON_UPLOAD_ERROR),
                    HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    private void validateExistIconInS3(String name) throws CaBusinessException {
        Optional<IconS3> imageS3Optional = iconS3Repository.findByName(name);
        if (imageS3Optional.isPresent()) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICON_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public String getFavicon() {
        List<IconS3> listFavicon = iconS3Repository.findByTypeAndStatusOrderByUpdatedDate(IconsType.FAVICON.getCode(),
                StatusType.ACTIVE.getCode());

        IconS3 icon = listFavicon.get(0);

        InputStream inputStream = amazonClient.getFile(DIRECTORY_ICON, icon.getName());

        return CaUtil.encodeFileInBase64(inputStream, ConstantsUtil.FORMAT_IMAGE_PNG);
    }

    @SuppressWarnings({"rawtypes"})
    @Override
    public GenericResponse uploadImagen(MultipartFile fileImage, PropertyImage properties) throws CaBusinessException {

        validateMaxFileSize(fileImage, ConstantsUtil.TYPE_IMAGE, maxFileSizeImage);
        String fileName;
        File file;

        UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

        ImageS3 imageS3 = new ImageS3();
        imageS3.setProperties(PropertyImage.builder()
                .title(properties.getTitle())
                .alt(properties.getAlt())
                .description(properties.getDescription()).build());
        try {
            fileName = amazonClient.generateFileName(fileImage);
            validateExistImageInS3(fileName);
            file = amazonClient.convertMultiPartToFile(fileImage);

            DimensionDto originalImgSize = CaUtil.getOriginalImageSize(file.getName());

            imageS3.setName(fileName);
            imageS3.setMimeType(properties.getType());
            fileName = originalImgSize.getWidth() + SYMBOL_X + originalImgSize.getHeigth() + SYMBOL_SUBGUION + fileName;
            amazonClient.uploadFileToS3BucketWithType(DIRECTORY_IMAGES, fileName, file, properties.getType());

            String fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR
                    + DIRECTORY_IMAGES + ConstantsUtil.SEPARATOR + fileName;
            imageS3.setLarge(getImage(originalImgSize, fileName, fileUrl, Double.valueOf(file.length())));
            imageS3.setCreateDate(LocalDateTime.now());

            String fileNameOrig = file.getName();

            ResizeImageProperties resizeImageProperties = getResizeImageProperties();

            Arrays.stream(CaUtil.getImageSizePercentage()).forEach(size -> {
                try {
                    buildImageBySize(size, file, fileImage, imageS3, resizeImageProperties);
                } catch (CaBusinessException e) {
                    logger.error(e.getMessage());
                }
            });

            Files.deleteIfExists(Paths.get(fileNameOrig));

        } catch (CaBusinessException cb) {
            throw new CaBusinessException(cb.getMessage(), ConstantsUtil.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            throw new CaBusinessException(
                    messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR),
                    HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        imageS3.setUserName(userPrincipal.getUsername());
        imageS3Repository.save(imageS3);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_SAVE_UPLOAD_IMAGEN_SUCCESFUL), imageS3);
    }

    private ResizeImageProperties getResizeImageProperties(){
        Parameter parameter = this.parameterRepository.customFindRoibackWebServiceByCode(ParameterType.RESIZE_IMAGES.getCode());
        return ObjectMapperUtils.map(parameter.getIndicator(), ResizeImageProperties.class);
    }

    @Override
    public ImageS3 updateImage(ImageS3 imageS3) throws CaBusinessException{
        Optional<ImageS3> imageS3Optional = imageS3Repository.findById(imageS3.getId());
        if(imageS3Optional.isPresent()){
            ImageS3 imageS3Update = imageS3Optional.get();
            imageS3Update.setProperties(imageS3.getProperties());
            imageS3Update.setUpdateDate(LocalDateTime.now());
            return imageS3Repository.save(imageS3Update);
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_UPDATE_NOT_EXIST), HttpStatus.NOT_ACCEPTABLE.value());
        }
    }

    private void validateExistImageInS3(String name) throws CaBusinessException {
        Optional<ImageS3> imageS3Optional = imageS3Repository.findByNameShort(name);
        if (imageS3Optional.isPresent()) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    private Image getImage(DimensionDto dimension, String fileName, String imageUri, Double size) {

        return Image.builder()
                .name(fileName)
                .imageUri(imageUri)
                .width(dimension.getWidth())
                .height(dimension.getHeigth())
                .size(size)
                .build();
    }

    private void buildImageBySize(String size, File file, MultipartFile multipartFile,
                                  ImageS3 imageS3, ResizeImageProperties resizeImageProperties)
            throws CaBusinessException {
        try {
            if (ConstantsUtil.LETTER_SIZE_IMAGE_SMALL.equalsIgnoreCase(size)) {
                imageS3.setSmall(buildImage(multipartFile, size, resizeImageProperties));
            } else if (ConstantsUtil.LETTER_SIZE_IMAGE_MEDIUM.equalsIgnoreCase(size)) {
                imageS3.setMedium(buildImage(multipartFile, size, resizeImageProperties));
            } else if(ConstantsUtil.LETTER_SIZE_IMAGE_EXTRA_SMALL.equalsIgnoreCase(size)) {
                imageS3.setExtraSmall(buildImage(multipartFile, size, resizeImageProperties));
            }
        } catch (Exception e) {
            try {
                Files.deleteIfExists(Paths.get(file.getName()));
            } catch (IOException e1) {
                logger.error(e1.getMessage());
            }
            logger.error(e.getMessage());
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    private Image buildImage(MultipartFile multipartFile, String size, ResizeImageProperties resizeImageProperties) throws CaBusinessException {
        String fileUrl;
        String fileName;

        try {

            File file = amazonClient.convertMultiPartToFile(multipartFile);
            DimensionDto originalImgSize = CaUtil.getOriginalImageSize(file.getName());
            BufferedImage image = ImageIO.read(new File(file.getName()));
            Image imageDimension = CaUtil.getImageSizeByPercentage(size, originalImgSize, resizeImageProperties);
            BufferedImage impTmp = ResizeImageUtil.resize(image, imageDimension.getWidth(), imageDimension.getHeight());

            String fileExtension = FileUtil.getExtension(file.getName());
            ImageIO.write(impTmp, fileExtension, file);

            if(ConstantsUtil.LETTER_SIZE_IMAGE_EXTRA_SMALL.equalsIgnoreCase(size)){
                String name = amazonClient.generateFileName(multipartFile);
                String nameWithoutExtension = name.substring(ConstantsUtil.ZERO,name.lastIndexOf(ConstantsUtil.POINT));
                String extension = FilenameUtils.getExtension(name);
                fileName = nameWithoutExtension + ConstantsUtil.FILE_NAME_EXTRA_SMALL + ConstantsUtil.POINT + extension;
            } else {
                fileName = imageDimension.getWidth() + SYMBOL_X + imageDimension.getHeight() + SYMBOL_SUBGUION + amazonClient.generateFileName(multipartFile);
            }
            amazonClient.uploadFileToS3Bucket(DIRECTORY_IMAGES, fileName, file);

            fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR + DIRECTORY_IMAGES
                    + ConstantsUtil.SEPARATOR + fileName;

            long length = file.length();

            Files.deleteIfExists(Paths.get(file.getName()));

            return getImage(new DimensionDto(imageDimension.getWidth(), imageDimension.getHeight()), fileName, fileUrl, Double.valueOf(length));

        } catch (Exception e) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    private void validateMaxFileSize(MultipartFile fileImage, Integer typeFile, long maxSizeFile) throws CaBusinessException {

        long sizeFile = fileImage.getSize();

        Integer sizeMB = (int) (maxSizeFile / ConstantsUtil.SIZE_MB);

        String message = "";

        if (ConstantsUtil.TYPE_IMAGE.equals(typeFile)) {
            message = messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_MAX_SIZE_IMAGE) + sizeMB + " " + ConstantsUtil.SYMBOL_MB;
        } else if (ConstantsUtil.TYPE_VIDEO.equals(typeFile)) {
            message = messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_MAX_SIZE_VIDEO) + sizeMB + " " + ConstantsUtil.SYMBOL_MB;
        } else if(ConstantsUtil.TYPE_DOCUMENT.equals(typeFile)){
            message = messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_MAX_SIZE_VIDEO) + sizeMB + " " + ConstantsUtil.SYMBOL_MB;
        }

        if (maxSizeFile < sizeFile) {
            throw new CaBusinessException(message, ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<ImageS3Response> findAllImagesS3() {
        List<ImageS3> listImageS3 = imageS3Repository.findAllCustomized();
        return convertListImageS3ToListImageS3Response(listImageS3);
    }

    @Override
    public RangeDateResponse getRangeDatesImages() {
        Optional<ImageS3> imageS3DateMin = imageS3Repository.findTopByOrderByCreateDateAsc();
        Optional<ImageS3> imageS3DateMax = imageS3Repository.findTopByOrderByCreateDateDesc();

        if (!imageS3DateMin.isPresent()) {
            return new RangeDateResponse();
        } else {
            LocalDate dateMin = imageS3DateMin.get().getCreateDate().toLocalDate();
            LocalDate dateMax = null;
            if (imageS3DateMax.isPresent()) {
                dateMax = imageS3DateMax.get().getCreateDate().toLocalDate();
            }
            return new RangeDateResponse(dateMin, dateMax);
        }
    }

    @Override
    public Page<ImageS3> getImagesS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException {
        Page<ImageS3> lstImageS3;
        Sort sort = new Sort(Sort.Direction.DESC, CREATE_DATE);
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

        if (CaUtil.validateIsNotNullAndNotEmpty(date)) {
            try {
                String[] splitDate = date.split(SYMBOL_GUION);
                String anio = splitDate[0];
                String month = splitDate[1];

                int lastDayMonth = CaUtil.getLastDayMonth(Integer.parseInt(anio), Integer.parseInt(month));

                LocalDate beginDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + NUMBER_ONE_STRING);
                LocalDate endDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + lastDayMonth).plusDays(1);
                lstImageS3 = imageS3Repository.findByCreateDateBetweenCustomized(beginDate.atStartOfDay(), endDate.atStartOfDay(), value, pageableSort);
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException io) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_FORMAT), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            lstImageS3 = imageS3Repository.findAllPaginated(value, pageableSort);
        }

        return lstImageS3;
    }

    private List<ImageS3Response> convertListImageS3ToListImageS3Response(List<ImageS3> listImageS3) {
        List<ImageS3Response> lstImageS3Response = new ArrayList<>();
        listImageS3.stream().forEach(x -> {
            ImageS3Response obj = new ImageS3Response();
            BeanUtils.copyProperties(x, obj);
            lstImageS3Response.add(obj);
        });
        return lstImageS3Response;
    }

    @SuppressWarnings({"rawtypes"})
    @Override
    public GenericResponse deleteImages(String imageId) throws CaBusinessException {
        Optional<ImageS3> imageS3Optional = imageS3Repository.findById(imageId);
        if (imageS3Optional.isPresent()) {
            try {

                ImageS3 imageS3 = imageS3Optional.get();
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_IMAGES, imageS3.getSmall().getName());
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_IMAGES, imageS3.getMedium().getName());
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_IMAGES, imageS3.getLarge().getName());

                imageS3Repository.delete(imageS3);

            } catch (Exception e) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_DELETE_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_NOT_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_DELETE_SUCCESFUL), null);
    }

    @SuppressWarnings({"rawtypes"})
    public GenericResponse uploadDocument(final MultipartFile multipartFile, String type) throws IOException, CaRequiredException, CaBusinessException {

        final String fileName = multipartFile.getOriginalFilename();
        final String extension = FilenameUtils.getExtension(fileName);
        String nameDocument = CaUtil.replaceEspecialCharacters(fileName);
        nameDocument = nameDocument.replace(" ", "_");

        validateExistDocumentInS3(nameDocument);

        S3Parameter s3Parameter = getS3Parameter(ParameterType.S3_DOCUMENT.getCode(), "es");
        validateMaxFileSize(multipartFile, ConstantsUtil.TYPE_DOCUMENT, s3Parameter.getMaxSize());

        List<String> extensions = Arrays.asList(s3Parameter.getExtensions().split(ConstantsUtil.COMMA));

        Document document = null;

        UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

        if (extensions.contains(extension)) {
            final File file = amazonClient.convertMultiPartToFile(multipartFile);

            final String fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR + s3Parameter.getDirectory()
                    + ConstantsUtil.SEPARATOR + nameDocument;

            amazonClient.uploadFileToS3BucketWithType(s3Parameter.getDirectory(), nameDocument, file, type);

            document = Document.builder()
                    .name(nameDocument)
                    .mimeType(type)
                    .extension(extension)
                    .directory(s3Parameter.getDirectory())
                    .url(fileUrl)
                    .userName(userPrincipal.getUsername())
                    .size(Double.valueOf(file.length()))
                    .build();

            this.documentRepository.insert(document);

            Files.deleteIfExists(Paths.get(fileName));
        }

        return CaUtil.getResponseGeneric("Se guardo el archivo " + fileName + " correctamente", document);
    }

    @SuppressWarnings({"rawtypes"})
    public GenericResponse uploadFilejs(final MultipartFile multipartFile, String type, String name) throws IOException, CaRequiredException, CaBusinessException {
        final String fileName = name;
        final String extension = FilenameUtils.getExtension(fileName);
        String nameDocument = CaUtil.replaceEspecialCharacters(fileName);
        nameDocument = nameDocument.replace(" ", "_");
        System.out.println("name document: "+nameDocument);
        System.out.println("name fileName: "+fileName);
        System.out.println("name extension: "+extension);
        validateExistDocumentInS3(nameDocument);

        S3Parameter s3Parameter = getS3Parameter(ParameterType.S3_JS.getCode(), "es");
        validateMaxFileSize(multipartFile, ConstantsUtil.TYPE_JS, s3Parameter.getMaxSize());

        List<String> extensions = Arrays.asList(s3Parameter.getExtensions().split(ConstantsUtil.COMMA));

        FileJS document = null;

        UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

        if (extensions.contains(extension)) {
            final File file = amazonClient.convertMultiPartToFile(multipartFile);

            final String fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR + s3Parameter.getDirectory()
                    + ConstantsUtil.SEPARATOR + nameDocument;

            amazonClient.uploadFileToS3BucketWithType(s3Parameter.getDirectory(), nameDocument, file, type);

            document = FileJS.builder()
                    .name(nameDocument)
                    .mimeType(type)
                    .extension(extension)
                    .directory(s3Parameter.getDirectory())
                    .url(fileUrl)
                    .userName(userPrincipal.getUsername())
                    .size(Double.valueOf(file.length()))
                    .build();

            this.JSRepository.insert(document);

            Files.deleteIfExists(Paths.get(fileName));
        }

        return CaUtil.getResponseGeneric("Se guardo el archivo " + fileName + " correctamente", document);
    }


    private void validateExistDocumentInS3(String name) throws CaRequiredException {
        Optional<Document> documentOptional = documentRepository.findByNameShort(name);
        //System.out.println("si existe"+documentOptional.isPresent());
        if (documentOptional.isPresent()) {
            throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_DOCUMENT_EXIST));
        }else{
            try {
                deleteFilejs(name);
                System.out.println("si existe"+documentOptional.isPresent());
            } catch (CaBusinessException e) {
                e.printStackTrace();
            }
        }
    }

    private S3Parameter getS3Parameter(final String code, final String languageCode) throws CaRequiredException {
        Optional<Parameter> optional = this.parameterRepository.findByCodeAndLanguageCode(code, languageCode);
        if (optional.isPresent()) {
            return new ObjectMapper().convertValue(optional.get().getIndicator(), S3Parameter.class);
        } else {
            throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
        }

    }

    @Override
    public List<com.casaandina.cms.model.Document> getAllTheDocument() {
        return this.documentRepository.findAll();
    }

    @Override
    public List<com.casaandina.cms.model.FileJS> getAllTheFilejs() {
        return this.JSRepository.findAll();
    }

    @Override
    public ResponseEntity<InputStreamResource> downloadDocument(String documentId) throws CaBusinessException {
        com.casaandina.cms.model.Document fileModel = this.documentRepository.findById(documentId).orElse(null);
        if (Objects.isNull(fileModel)) {
            throw new CaBusinessException("El documento no existe", HttpStatus.EXPECTATION_FAILED.value());
        } else {

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Cache-Control", "no-cache, no-store, must-revalidate");
            httpHeaders.add("Pragma", "no-cache");
            httpHeaders.add("Expires", "0");
            httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileModel.getName());

            return ResponseEntity.ok()
                    .headers(httpHeaders)
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(new InputStreamResource(this.amazonClient.getFile(fileModel.getDirectory(), fileModel.getName())));
        }
    }

    @Override
    public ResponseEntity<InputStreamResource> downloadFilejs(String filejsId) throws CaBusinessException {
        filejsId = CaUtil.replaceEspecialCharacters(filejsId);
        com.casaandina.cms.model.FileJS fileModel = this.JSRepository.findByNameShort(filejsId).orElse(null);
        if (Objects.isNull(fileModel)) {
            throw new CaBusinessException("El documento no existe", HttpStatus.EXPECTATION_FAILED.value());
        } else {

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Cache-Control", "no-cache, no-store, must-revalidate");
            httpHeaders.add("Pragma", "no-cache");
            httpHeaders.add("Expires", "0");
            httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileModel.getName());

            return ResponseEntity.ok()
                    .headers(httpHeaders)
                    .contentType(MediaType.parseMediaType("application/javascript"))
                    .body(new InputStreamResource(this.amazonClient.getFile(fileModel.getDirectory(), fileModel.getName())));
        }
    }

    @SuppressWarnings({"rawtypes"})
    @Override
    public GenericResponse validateExistImage(String filename) throws CaBusinessException {
        try {
            Optional<ImageS3> imageS3Optional = imageS3Repository.findByNameCustomized(filename);

            if (imageS3Optional.isPresent()) {
                ImageS3 imageS3 = imageS3Optional.get();
                amazonClient.validateFile(DIRECTORY_IMAGES, imageS3.getSmall().getName());
                amazonClient.validateFile(DIRECTORY_IMAGES, imageS3.getMedium().getName());
                amazonClient.validateFile(DIRECTORY_IMAGES, imageS3.getLarge().getName());
            }
        } catch (AwsS3Exception e) {
            throw new CaBusinessException(e.getDescription(), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_FILE_NOT_EXIST), Boolean.FALSE);
    }

    @SuppressWarnings({"rawtypes"})
    @Override
    public GenericResponse uploadVideo(MultipartFile fileVideo, PropertyImage properties) throws CaBusinessException {

        validateMaxFileSize(fileVideo, ConstantsUtil.TYPE_VIDEO, maxFileSizeVideo);

        String fileName;
        String fileNameImage;
        File file;

        VideoS3 videoS3 = new VideoS3();
        UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();

        try {
            fileName = amazonClient.generateFileName(fileVideo);
            validateExistVideoInS3(fileName);
            file = amazonClient.convertMultiPartToFile(fileVideo);

            amazonClient.uploadFileToS3BucketWithType(DIRECTORY_VIDEO, fileName, file, properties.getType());

            String fileUrlVideo = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR
                    + DIRECTORY_VIDEO + ConstantsUtil.SEPARATOR + fileName;

            videoS3.setName(fileName);
            videoS3.setUrl(fileUrlVideo);
            videoS3.setMimeType(properties.getType());
            videoS3.setSize(Double.valueOf(file.length()));

            File fileImage = getThumbnailFromVideo(file, fileName, CaUtil.validateIsNotNullAndPositive(properties.getNumberFrame()) ? properties.getNumberFrame() : 100);
            DimensionDto originalImgSize = CaUtil.getOriginalImageSize(fileImage.getName());

            fileNameImage = originalImgSize.getWidth() + SYMBOL_X + originalImgSize.getHeigth() + SYMBOL_SUBGUION + fileImage.getName();
            String fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR
                    + DIRECTORY_IMAGES + ConstantsUtil.SEPARATOR + fileNameImage;

            amazonClient.uploadFileToS3BucketWithType(DIRECTORY_IMAGES, fileNameImage, fileImage, ConstantsUtil.FORMAT_IMAGE_PNG);
            videoS3.setLarge(getImage(originalImgSize, fileNameImage, fileUrl, Double.valueOf(fileImage.length())));
            videoS3.setProperties(new PropertyImage(properties.getTitle(), properties.getAlt(), properties.getDescription()));

            Files.deleteIfExists(Paths.get(file.getName()));

            ResizeImageProperties resizeImageProperties = getResizeImageProperties();

            Arrays.stream(CaUtil.getImageSizePercentage()).forEach(size -> {
                try {
                    buildImageBySizeFromVideoThumbnail(size, fileImage, videoS3, originalImgSize, resizeImageProperties);
                } catch (CaBusinessException e) {
                    logger.error(e.getMessage());
                }
            });
            Files.deleteIfExists(Paths.get(fileImage.getName()));

        } catch (CaBusinessException cb) {
            throw new CaBusinessException(cb.getMessage(), ConstantsUtil.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VIDEO_UPLOAD_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        videoS3.setUserName(userPrincipal.getUsername());
        videoS3Repository.save(videoS3);

        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_SAVE_UPLOAD_VIDEO_SUCCESFUL), videoS3);
    }

    private File getThumbnailFromVideo(File videoFile, String filename, Integer numberFrame) throws CaBusinessException {
        try {
            Picture picture = FrameGrab.getFrameFromFile(videoFile, numberFrame);

            filename = filename.substring(0, filename.lastIndexOf(POINT_STRING));

            File fileReturn = new File(filename + SYMBOL_SUBGUION + FRAME_NAME + numberFrame + ConstantsUtil.EXTENSION_JPG);

            BufferedImage bufferedImage = AWTUtil.toBufferedImage(picture);
            ImageIO.write(bufferedImage, "jpg", fileReturn);

            return fileReturn;

        } catch (Exception e) {
            try {
                Files.deleteIfExists(Paths.get(videoFile.getName()));
            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
            throw new CaBusinessException(e.getMessage(), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    private void buildImageBySizeFromVideoThumbnail(String size, File file, VideoS3 videoS3,
                                                    DimensionDto originalImgSize, ResizeImageProperties resizeImageProperties)
            throws CaBusinessException {
        try {
            if (ConstantsUtil.LETTER_SIZE_IMAGE_SMALL.equalsIgnoreCase(size)) {
                videoS3.setSmall(buildImageFromVideoThumbnail(file, size, originalImgSize, resizeImageProperties));
            } else if (ConstantsUtil.LETTER_SIZE_IMAGE_MEDIUM.equalsIgnoreCase(size)) {
                videoS3.setMedium(buildImageFromVideoThumbnail(file, size, originalImgSize, resizeImageProperties));
            }
        } catch (Exception e) {
            try {
                Files.deleteIfExists(Paths.get(file.getName()));
            } catch (IOException e1) {
                logger.error(e1.getMessage());
            }
            logger.error(e.getMessage());
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    private Image buildImageFromVideoThumbnail(File file, String size, DimensionDto originalImgSize,
                                               ResizeImageProperties resizeImageProperties) throws CaBusinessException {
        String fileUrl;
        String fileName;

        try {

            fileName = file.getName();
            BufferedImage image = ImageIO.read(file);
            Image imageTo = CaUtil.getImageSizeByPercentage(size, originalImgSize, resizeImageProperties);

            Integer width = imageTo.getWidth();
            Integer heigth = imageTo.getHeight();
            BufferedImage impTmp = ResizeImageUtil.resize(image, width, heigth);

            String fileExtension = FileUtil.getExtension(fileName);
            ImageIO.write(impTmp, fileExtension, file);

            fileName = width + SYMBOL_X + heigth + SYMBOL_SUBGUION + fileName;

            amazonClient.uploadFileToS3BucketWithType(DIRECTORY_IMAGES, fileName, file, ConstantsUtil.FORMAT_IMAGE_JPG);

            fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR + DIRECTORY_IMAGES
                    + ConstantsUtil.SEPARATOR + fileName;

            long length = file.length();

            return getImage(new DimensionDto(width, heigth), fileName, fileUrl, Double.valueOf(length));

        } catch (Exception e) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    private void validateExistVideoInS3(String name) throws CaBusinessException {
        Optional<VideoS3> videoS3Optional = videoS3Repository.findByNameShort(name);
        if (videoS3Optional.isPresent()) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VIDEO_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public RangeDateResponse getRangeDatesVideos() {
        Optional<VideoS3> videoS3DateMin = videoS3Repository.findTopByOrderByCreateDateAsc();
        Optional<VideoS3> videoS3DateMax = videoS3Repository.findTopByOrderByCreateDateDesc();

        if (!videoS3DateMin.isPresent()) {
            return new RangeDateResponse();
        } else {
            LocalDate dateMin = videoS3DateMin.get().getCreateDate().toLocalDate();
            LocalDate dateMax = null;
            if (videoS3DateMax.isPresent()) {
                dateMax = videoS3DateMax.get().getCreateDate().toLocalDate();
            }
            return new RangeDateResponse(dateMin, dateMax);
        }
    }

    @Override
    public Page<VideoS3> getVideosS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException {
        Page<VideoS3> lstVideoS3;
        Sort sort = new Sort(Sort.Direction.DESC, CREATE_DATE);
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

        if (CaUtil.validateIsNotNullAndNotEmpty(date)) {
            try {
                String[] splitDate = date.split(SYMBOL_GUION);
                String anio = splitDate[0];
                String month = splitDate[1];

                int lastDayMonth = CaUtil.getLastDayMonth(Integer.parseInt(anio), Integer.parseInt(month));

                LocalDate beginDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + NUMBER_ONE_STRING);
                LocalDate endDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + lastDayMonth).plusDays(1);

                lstVideoS3 = videoS3Repository.findByCreateDateBetweenCustomized(beginDate.atStartOfDay(), endDate.atStartOfDay(), value, pageableSort);
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException io) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_FORMAT), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            lstVideoS3 = videoS3Repository.findAllPaginated(value, pageableSort);
        }

        return lstVideoS3;
    }

    @SuppressWarnings({"rawtypes"})
    @Override
    public GenericResponse validateExistVideo(String filename) throws CaBusinessException {
        try {
            Optional<VideoS3> videoS3ToOptional = videoS3Repository.findByNameCustomized(filename);

            if (videoS3ToOptional.isPresent()) {
                VideoS3 videoS3 = videoS3ToOptional.get();
                amazonClient.validateFile(DIRECTORY_VIDEO, videoS3.getName());
            }
        } catch (AwsS3Exception e) {
            throw new CaBusinessException(e.getDescription(), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_FILE_NOT_EXIST), Boolean.FALSE);
    }

    @SuppressWarnings({"rawtypes"})
    @Override
    public GenericResponse deleteVideo(String videoId) throws CaBusinessException {
        Optional<VideoS3> videoS3Optional = videoS3Repository.findById(videoId);
        if (videoS3Optional.isPresent()) {
            try {

                VideoS3 videoS3 = videoS3Optional.get();
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_VIDEO, videoS3.getName());
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_IMAGES, videoS3.getSmall().getName());
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_IMAGES, videoS3.getMedium().getName());
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_IMAGES, videoS3.getLarge().getName());

                videoS3Repository.delete(videoS3);

            } catch (Exception e) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VIDEO_DELETE_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VIDEO_NOT_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VIDEO_DELETE_SUCCESFUL), null);
    }

    private String getMimeTypeFromFile(File file) {
        MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
        return fileTypeMap.getContentType(file.getName());
    }

    public RangeDateResponse getRangeDatesDocuments() {
        Optional<Document> documentDateMin = documentRepository.findTopByOrderByCreatedDateAsc();
        Optional<Document> documentDateMax = documentRepository.findTopByOrderByCreatedDateDesc();

        if (!documentDateMin.isPresent()) {
            return new RangeDateResponse();
        } else {
            LocalDate dateMin = documentDateMin.get().getCreatedDate().toLocalDate();
            LocalDate dateMax = null;
            if (documentDateMax.isPresent()) {
                dateMax = documentDateMax.get().getCreatedDate().toLocalDate();
            }
            return new RangeDateResponse(dateMin, dateMax);
        }
    }

    @Override
    public RangeDateResponse getRangeDatesFilejs() {
        Optional<FileJS> documentDateMin = JSRepository.findTopByOrderByCreatedDateAsc();
        Optional<FileJS> documentDateMax = JSRepository.findTopByOrderByCreatedDateDesc();

        if (!documentDateMin.isPresent()) {
            return new RangeDateResponse();
        } else {
            LocalDate dateMin = documentDateMin.get().getCreatedDate().toLocalDate();
            LocalDate dateMax = null;
            if (documentDateMax.isPresent()) {
                dateMax = documentDateMax.get().getCreatedDate().toLocalDate();
            }
            return new RangeDateResponse(dateMin, dateMax);
        }
    }

    @Override
    public Page<Document> getDocumentsS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException {
        Page<Document> pageDocument;
        Sort sort = new Sort(Sort.Direction.DESC, "createdDate");
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

        if (CaUtil.validateIsNotNullAndNotEmpty(date)) {
            try {
                String[] splitDate = date.split(SYMBOL_GUION);
                String anio = splitDate[0];
                String month = splitDate[1];

                int lastDayMonth = CaUtil.getLastDayMonth(Integer.parseInt(anio), Integer.parseInt(month));

                LocalDate beginDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + NUMBER_ONE_STRING);
                LocalDate endDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + lastDayMonth).plusDays(1);

                pageDocument = documentRepository.findByCreatedAtBetweenCustomized(beginDate.atStartOfDay(), endDate.atStartOfDay(), value, pageableSort);
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException io) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_FORMAT), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            pageDocument = documentRepository.findAllPaginated(value, pageableSort);
        }

        return pageDocument;
    }

    @Override
    public Page<FileJS> getFilejsS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException {
        Page<FileJS> pageDocument;
        Sort sort = new Sort(Sort.Direction.DESC, "createdDate");
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

        if (CaUtil.validateIsNotNullAndNotEmpty(date)) {
            try {
                String[] splitDate = date.split(SYMBOL_GUION);
                String anio = splitDate[0];
                String month = splitDate[1];

                int lastDayMonth = CaUtil.getLastDayMonth(Integer.parseInt(anio), Integer.parseInt(month));

                LocalDate beginDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + NUMBER_ONE_STRING);
                LocalDate endDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + lastDayMonth).plusDays(1);

                pageDocument = JSRepository.findByCreatedAtBetweenCustomized(beginDate.atStartOfDay(), endDate.atStartOfDay(), value, pageableSort);
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException io) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_FORMAT), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            pageDocument = JSRepository.findAllPaginated(value, pageableSort);
        }

        return pageDocument;
    }

    @Override
    public GenericResponse deleteDocument(String videoId) throws CaBusinessException {
        Optional<Document> documentOptional = documentRepository.findById(videoId);
        if (documentOptional.isPresent()) {
            try {
                S3Parameter s3Parameter = getS3Parameter(ParameterType.S3_DOCUMENT.getCode(), "es");
                Document document = documentOptional.get();
                amazonClient.deleteFileFromS3Bucket(s3Parameter.getDirectory(), document.getName());

                documentRepository.delete(document);

            } catch (Exception e) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_DOCUMENT_DELETE_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_DOCUMENT_NOT_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_DOCUMENT_DELETE_SUCCESFUL), null);
    }

    @Override
    public GenericResponse deleteFilejs(String filejsId) throws CaBusinessException {
        Optional<FileJS> filejsOptional = JSRepository.findByNameShort(filejsId);
        if (filejsOptional.isPresent()) {
            try {
                S3Parameter s3Parameter = getS3Parameter(ParameterType.S3_JS.getCode(), "es");
                FileJS filejs = filejsOptional.get();
                amazonClient.deleteFileFromS3Bucket(s3Parameter.getDirectory(), filejs.getName());

                JSRepository.delete(filejs);

            } catch (Exception e) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_FILEJS_DELETE_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_FILEJS_NOT_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_FILEJS_DELETE_SUCCESFUL), null);

    }

    @Override
    public IconS3 uploadIcon(MultipartFile fileImage, PropertyImage properties) throws CaBusinessException {

        validateMaxFileSize(fileImage, ConstantsUtil.TYPE_IMAGE, maxFileSizeImage);
        String fileName;
        File file;

        IconS3 iconS3 = new IconS3();
        iconS3.setProperties(PropertyImage.builder()
                .title(properties.getTitle())
                .alt(properties.getAlt())
                .description(properties.getDescription()).build());
        try {

            UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();

            LocalDate currentDate = LocalDateTime.now().toLocalDate();

            fileName = amazonClient.generateFileName(fileImage);
            validateExistIconInS3(fileName);
            file = amazonClient.convertMultiPartToFile(fileImage);

            DimensionDto originalImgSize = CaUtil.getOriginalImageSize(file.getName());

            iconS3.setName(fileName);
            iconS3.setMimeType(properties.getType());
            fileName = originalImgSize.getWidth() + SYMBOL_X + originalImgSize.getHeigth() + SYMBOL_SUBGUION + fileName;
            amazonClient.uploadFileToS3Bucket(DIRECTORY_ICON, fileName, file);

            String fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR
                    + DIRECTORY_ICON + ConstantsUtil.SEPARATOR + fileName;

            iconS3.setWidth(originalImgSize.getWidth());
            iconS3.setHeight(originalImgSize.getHeigth());
            iconS3.setUrl(fileUrl);
            iconS3.setCreateDate(currentDate);
            iconS3.setUpdatedDate(currentDate);
            iconS3.setCreatedBy(userPrincipal.getId());
            iconS3.setUpdatedBy(userPrincipal.getId());
            iconS3.setUserName(userPrincipal.getUsername());
            iconS3.setType(IconsType.NORMAL.getCode());

            String fileNameOrig = file.getName();
            Files.deleteIfExists(Paths.get(fileNameOrig));

        } catch (CaBusinessException cb) {
            throw new CaBusinessException(cb.getMessage(), ConstantsUtil.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            throw new CaBusinessException(
                    messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR),
                    HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return iconS3Repository.save(iconS3);
    }

    @Override
    public RangeDateResponse getRangeDatesIcons() {
        Optional<IconS3> iconS3DateMin = iconS3Repository.findTopByTypeOrderByCreateDateAsc(IconsType.NORMAL.getCode());
        Optional<IconS3> iconS3DateMax = iconS3Repository.findTopByTypeOrderByCreateDateDesc(IconsType.NORMAL.getCode());

        if (!iconS3DateMin.isPresent()) {
            return new RangeDateResponse();
        } else {
            LocalDate dateMin = iconS3DateMin.get().getCreateDate();
            LocalDate dateMax = null;
            if (iconS3DateMax.isPresent()) {
                dateMax = iconS3DateMax.get().getCreateDate();
            }
            return new RangeDateResponse(dateMin, dateMax);
        }
    }

    @Override
    public Page<IconS3> getIconsS3ByFilters(String date, String value, Pageable pageable) throws CaBusinessException {
        Page<IconS3> lstIconsS3;
        Sort sort = new Sort(Sort.Direction.DESC, CREATE_DATE);
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

        if(CaUtil.validateIsNotNullAndNotEmpty(date)){
            try {
                String[] splitDate = date.split(SYMBOL_GUION);
                String anio = splitDate[0];
                String month = splitDate[1];

                int lastDayMonth = CaUtil.getLastDayMonth(Integer.parseInt(anio), Integer.parseInt(month));

                LocalDate beginDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + NUMBER_ONE_STRING);
                LocalDate endDate = CaUtil.convertStringtoLocalDate(date + SYMBOL_GUION + lastDayMonth).plusDays(1);

                lstIconsS3 = iconS3Repository.findByCreateDateBetweenCustomized(beginDate, endDate, value, IconsType.NORMAL.getCode(), pageableSort);

            } catch (ArrayIndexOutOfBoundsException | NumberFormatException io){
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_VALID_FORMAT), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            lstIconsS3 = iconS3Repository.findAllPaginated(value, IconsType.NORMAL.getCode(), pageableSort);
        }
        return lstIconsS3;
    }

    @Override
    public GenericResponse validateExistIcon(String filename) throws CaBusinessException {
        try {
            Optional<IconS3> iconS3Optional = iconS3Repository.findByNameCustomized(filename);

            if (iconS3Optional.isPresent()) {
                IconS3 iconS3 = iconS3Optional.get();
                String name = iconS3.getWidth() + SYMBOL_X + iconS3.getHeight() + SYMBOL_SUBGUION +  iconS3.getName();
                amazonClient.validateFile(DIRECTORY_ICON, name);
            }
        } catch (AwsS3Exception e) {
            throw new CaBusinessException(e.getDescription(), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_FILE_NOT_EXIST), Boolean.FALSE);
    }

    @Override
    public GenericResponse deleteIcon(String iconId) throws CaBusinessException {
        Optional<IconS3> iconS3Optional = iconS3Repository.findById(iconId);
        if (iconS3Optional.isPresent()) {
            try {

                IconS3 iconS3 = iconS3Optional.get();
                String name = iconS3.getWidth() + SYMBOL_X + iconS3.getHeight() + SYMBOL_SUBGUION +  iconS3.getName();
                amazonClient.deleteFileFromS3Bucket(DIRECTORY_ICON, name);

                iconS3Repository.delete(iconS3);

            } catch (Exception e) {
                throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_DELETE_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
            }
        } else {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_NOT_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_DELETE_SUCCESFUL), null);
    }

    @Override
    public void resizeAllImages(PageTrayRequest pageTrayRequest){

        Date startDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getStartDate());
        Date endDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getEndDate());

        if(Objects.nonNull(endDate))
            endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);

        List<ImageS3> imageS3s = this.imageS3Repository.findByCreateDateBetween(startDate, endDate);

        if(Objects.nonNull(imageS3s) && !imageS3s.isEmpty()){
            UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();
            ResizeImageProperties resizeImageProperties = getResizeImageProperties();
            List<ImageS3> imageS3List = new ArrayList<>();

            imageS3s.forEach( imageS3 ->
                    imageS3List.add(resizeImageS3(imageS3, resizeImageProperties, userPrincipal.getUsername()))
            );

            this.imageS3Repository.saveAll(imageS3List);
        }
    }

    public ImageS3 resizeImageS3(ImageS3 imageS3, ResizeImageProperties resizeImageProperties, String username) {
        try {
            String fileName = imageS3.getLarge().getName();
            InputStream inputStream = amazonClient.getFile(DIRECTORY_IMAGES, fileName);

            File file = new File(imageS3.getName());
            FileUtils.copyInputStreamToFile(inputStream, file);

            File fileMedium = new File(imageS3.getMedium().getName());
            File fileSmall = new File(imageS3.getSmall().getName());

            FileUtils.copyFile(file, fileMedium);
            FileUtils.copyFile(file, fileSmall);

            Image imageMedium = buildImageResize(fileMedium, ConstantsUtil.LETTER_SIZE_IMAGE_MEDIUM, resizeImageProperties, imageS3.getMedium(), imageS3.getMimeType());
            Image imageSmall = buildImageResize(fileSmall, ConstantsUtil.LETTER_SIZE_IMAGE_SMALL, resizeImageProperties, imageS3.getSmall(), imageS3.getMimeType());

            imageS3.setMedium(imageMedium);
            imageS3.setSmall(imageSmall);
            imageS3.setUpdateDate(LocalDateTime.now());
            imageS3.setUserName(username);

            Files.deleteIfExists(Paths.get(file.getName()));

        } catch(Exception ex ){
            logger.error(String.format("An error occurred when changing image size: %s", ex.getMessage()));
        }
        return imageS3;
    }

    private Image buildImageResize(File file, String size, ResizeImageProperties resizeImageProperties, Image entityImage, String type) throws CaBusinessException {

        String fileName;

        try {

            DimensionDto originalImgSize = CaUtil.getOriginalImageSize(file.getName());
            BufferedImage image = ImageIO.read(new File(file.getName()));
            Image imageDimension = CaUtil.getImageSizeByPercentage(size, originalImgSize, resizeImageProperties);
            BufferedImage impTmp = ResizeImageUtil.resize(image, imageDimension.getWidth(), imageDimension.getHeight());

            String fileExtension = FileUtil.getExtension(file.getName());
            ImageIO.write(impTmp, fileExtension, file);

            fileName = entityImage.getName();

            amazonClient.uploadFileToS3BucketWithType(DIRECTORY_IMAGES, fileName, file, type);

            long length = file.length();

            Files.deleteIfExists(Paths.get(file.getName()));

            return getImage(new DimensionDto(imageDimension.getWidth(), imageDimension.getHeight()), fileName, entityImage.getImageUri(), Double.valueOf(length));

        } catch (Exception e) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void resizeAllImagesExtraSmall(PageTrayRequest pageTrayRequest){
        Date startDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getStartDate());
        Date endDate = CaUtil.convertStringtoDateFormatGuion(pageTrayRequest.getEndDate());

        if(Objects.nonNull(endDate))
            endDate = CaUtil.addDateFormatGuion(endDate, ConstantsUtil.ONE);

        List<ImageS3> imageS3s = this.imageS3Repository.findByCreateDateBetween(startDate, endDate);

        if(Objects.nonNull(imageS3s) && !imageS3s.isEmpty()){
            UserPrincipal userPrincipal = userAuthentication.getUserPrincipal();
            ResizeImageProperties resizeImageProperties = getResizeImageProperties();
            List<ImageS3> imageS3List = new ArrayList<>();

            imageS3s.forEach( imageS3 ->
                    imageS3List.add(resizeImageS3ExtraSmall(imageS3, resizeImageProperties, userPrincipal.getUsername()))
            );

            this.imageS3Repository.saveAll(imageS3List);
        }
    }

    public ImageS3 resizeImageS3ExtraSmall(ImageS3 imageS3, ResizeImageProperties resizeImageProperties, String username) {
        try {
            String fileName = imageS3.getLarge().getName();
            InputStream inputStream = amazonClient.getFile(DIRECTORY_IMAGES, fileName);

            File file = new File(imageS3.getName());
            FileUtils.copyInputStreamToFile(inputStream, file);

            File fileExtraSmall = new File(imageS3.getSmall().getName());

            FileUtils.copyFile(file, fileExtraSmall);

            Image imageMedium = buildImageResizeExtraSmall(fileExtraSmall, ConstantsUtil.LETTER_SIZE_IMAGE_EXTRA_SMALL, resizeImageProperties, imageS3.getName(), imageS3.getMimeType());

            imageS3.setExtraSmall(imageMedium);
            imageS3.setUpdateDate(LocalDateTime.now());
            imageS3.setUserName(username);

            Files.deleteIfExists(Paths.get(file.getName()));

        } catch(Exception ex ){
            logger.error(String.format("An error occurred when changing image size: %s", ex.getMessage()));
        }
        return imageS3;
    }

    private Image buildImageResizeExtraSmall(File file, String size, ResizeImageProperties resizeImageProperties, String name, String type) throws CaBusinessException {

        String fileName;

        try {

            DimensionDto originalImgSize = CaUtil.getOriginalImageSize(file.getName());
            BufferedImage image = ImageIO.read(new File(file.getName()));
            Image imageDimension = CaUtil.getImageSizeByPercentage(size, originalImgSize, resizeImageProperties);
            BufferedImage impTmp = ResizeImageUtil.resize(image, imageDimension.getWidth(), imageDimension.getHeight());

            String fileExtension = FileUtil.getExtension(file.getName());
            ImageIO.write(impTmp, fileExtension, file);

            String nameWithoutExtension = name.substring(ConstantsUtil.ZERO,name.lastIndexOf(ConstantsUtil.POINT));
            String extension = FilenameUtils.getExtension(name);
            fileName = nameWithoutExtension + ConstantsUtil.FILE_NAME_EXTRA_SMALL + ConstantsUtil.POINT + extension;

            String fileUrl = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR + DIRECTORY_IMAGES
                    + ConstantsUtil.SEPARATOR + fileName;

            amazonClient.uploadFileToS3BucketWithType(DIRECTORY_IMAGES, fileName, file, type);

            long length = file.length();

            Files.deleteIfExists(Paths.get(file.getName()));

            return getImage(new DimensionDto(imageDimension.getWidth(), imageDimension.getHeight()), fileName, fileUrl, Double.valueOf(length));

        } catch (Exception e) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGE_UPLOAD_ERROR), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
    }
}
