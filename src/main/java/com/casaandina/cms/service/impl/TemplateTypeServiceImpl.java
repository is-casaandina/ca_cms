package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.TemplateType;
import com.casaandina.cms.repository.TemplateTypeRepository;
import com.casaandina.cms.service.TemplateTypeService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TemplateTypeServiceImpl implements TemplateTypeService {

    private final TemplateTypeRepository templateTypeRepository;
    private final MessageProperties messageProperties;

    @Autowired
    public TemplateTypeServiceImpl(TemplateTypeRepository templateTypeRepository, MessageProperties messageProperties) {
        this.templateTypeRepository = templateTypeRepository;
        this.messageProperties = messageProperties;
    }

    @Override
    public TemplateType saveTemplateType(TemplateType templateType) throws CaBusinessException {
        if (!CaUtil.validateIsNotNullAndNotEmpty(templateType.getName())) {
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_TEMPLATE_TYPE_NAME_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        Optional<TemplateType> templateTypeOptional = templateTypeRepository.findByName(templateType.getName());
        if(templateTypeOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_TEMPLATE_TYPE_NAME_EXIST), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }
        UUID uuid = UUID.randomUUID();
        return templateTypeRepository.save(
            TemplateType.builder()
                .id(uuid.toString())
                .name(templateType.getName())
                .system(Boolean.FALSE)
                .viewPage(templateType.getViewPage())
                .createdDate(LocalDateTime.now())
                .build()
        );
    }

    @Override
    public List<TemplateType> getTemplateTypeAll() {
        return templateTypeRepository.findByViewPage(Boolean.TRUE);
    }

    @Override
    public void deleteTemplateType(final String templateTypeId) throws CaRequiredException {
        Optional<TemplateType> optional = this.templateTypeRepository.findById(templateTypeId);
        if (optional.isPresent()) {
            this.templateTypeRepository.deleteById(templateTypeId);
        } else {
            throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
        }
    }
}
