package com.casaandina.cms.service.impl;

import com.casaandina.cms.dto.UserTo;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Role;
import com.casaandina.cms.model.User;
import com.casaandina.cms.model.VerificationToken;
import com.casaandina.cms.repository.RoleRepository;
import com.casaandina.cms.repository.UserRepository;
import com.casaandina.cms.repository.VerificationTokenRepository;
import com.casaandina.cms.service.EmailService;
import com.casaandina.cms.service.SequenceService;
import com.casaandina.cms.service.UserService;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.Page;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	
	private final RoleRepository roleRepository;
	
	private final VerificationTokenRepository verificationTokenRepository;
	
	private final SequenceService sequenceService;
	
	private final PasswordEncoder passwordEncoder;
	
	private final MessageProperties messageProperties; 
	
	private final EmailService emailService;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, VerificationTokenRepository verificationTokenRepository, SequenceService sequenceService, PasswordEncoder passwordEncoder, MessageProperties messageProperties, EmailService emailService) {
		this.roleRepository = roleRepository;
		this.userRepository = userRepository;
		this.verificationTokenRepository = verificationTokenRepository;
		this.sequenceService = sequenceService;
		this.passwordEncoder = passwordEncoder;
		this.messageProperties = messageProperties;
		this.emailService = emailService;
	}

	@Override
	public Optional<User> findBydId(Integer userId) {
		return this.userRepository.findById(userId);
	}

	@Override
	public List<User> findAll() throws CaRequiredException {		
		return userRepository.findByStatusOrderByUsername(StatusType.ACTIVE.getCode());
	}

	@Override
	public User saveUser(User user) throws CaBusinessException, CaRequiredException {
		validateUser(user);

		Integer sequence = sequenceService.getNextSequence(ConstantsUtil.USER_SEQUENCE);

		//Seteamos valores de Registro
		user.setId(sequence);
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setCreatedDate(LocalDateTime.now());
		user.setStatus(StatusType.ACTIVE.getCode());
		
		return userRepository.insert(user);
	}

	@Override
	public User updateUser(User user) throws CaBusinessException, CaRequiredException {
		validateUserUpdate(user);
		
		Optional<User> userExist = userRepository.findByIdAndStatus(user.getId(), StatusType.ACTIVE.getCode());
		
		if(userExist.isPresent()) {
			Optional<Role> roleExist = roleRepository.findByRoleCodeAndStatus(user.getRoleCode(), StatusType.ACTIVE.getCode());
            if (!roleExist.isPresent())
                throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_NOT_EXIST));
            User userUpdate = userExist.get();
			userUpdate.setName(user.getName());
			userUpdate.setLastname(user.getLastname());
			userUpdate.setEmail(user.getEmail());
            userUpdate.setRoleCode(user.getRoleCode());

			return userRepository.save(userUpdate);
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_USER_NOT_REGISTERED_UPDATE), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	@Override
	public User updateProfile(User user) throws CaBusinessException, CaRequiredException {

		Optional<User> userExist = userRepository.findByIdAndStatus(user.getId(), StatusType.ACTIVE.getCode());

		if(userExist.isPresent()) {
			User userUpdate = userExist.get();
			userUpdate.setName(user.getName());
			userUpdate.setLastname(user.getLastname());
			userUpdate.setEmail(user.getEmail());

			return userRepository.save(userUpdate);
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_USER_NOT_REGISTERED_UPDATE), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}
	
	@Override
	public User updateStatus(Integer id) throws CaRequiredException  {
		Optional<User> userExist = userRepository.findByIdAndStatus(id, StatusType.ACTIVE.getCode());
		
		if(!userExist.isPresent()) {
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_USER_NOT_REGISTERED));
		}
		User userUpdate = userExist.get();
		userUpdate.setStatus(StatusType.INACTIVE.getCode());
		return userRepository.save(userUpdate);
	}

	@Override
	public Page<User> listUserPaginated(String[] rolesCode, Pageable pageable) {
		org.springframework.data.domain.Page<User> userPage;

		if (rolesCode != null && rolesCode.length == 0) {
			userPage = userRepository.findByStatus(StatusType.ACTIVE.getCode(), pageable);
		} else {
			userPage = userRepository.findByStatusAndRoleCodeIn(StatusType.ACTIVE.getCode(), rolesCode, pageable);
		}

		return PageUtil.pageToResponse(userPage);
	}

	@Override
	@Transactional
	public String recoveryPassword(String email) throws CaBusinessException, CaRequiredException {
		
		if(StringUtils.isEmpty(email) || email.trim().isEmpty()) {
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_USER_MAIL_REQUIRED));
		}
		
		Optional<User> user = userRepository.findByEmailAndStatus(email, StatusType.ACTIVE.getCode());
		
		if(!user.isPresent()) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_USER_MAIL_NOT_EXIST), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		
		VerificationToken token = new VerificationToken();
		token.setUserId(user.get().getId());
		token = verificationTokenRepository.save(token);
		
		emailService.sendVerificationTokenMail(user.get(), token.getToken());
		
		return messageProperties.getMessage(PropertiesConstants.MESSAGE_EMAIL_SEND_NOTIFICATION);
	}
	
	@Override
	@Transactional
	public String updatePasswordWithToken(UserTo userTo) throws CaBusinessException {
		
		Optional<VerificationToken> tokenOptional = verificationTokenRepository.findByToken(userTo.getToken());
		
		VerificationToken tokenBean = tokenOptional.isPresent() ?  tokenOptional.get() : null;
		if(tokenBean != null) {
			if (tokenBean.getExpirationDate().isBefore(LocalDateTime.now())) {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TOKEN_EXPIRED), HttpStatus.INTERNAL_SERVER_ERROR.value());
			}
			if(StatusTokenType.VERIFIED.getCode().equals(tokenBean.getStatus())) {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TOKEN_MODIFIED_PASS), HttpStatus.INTERNAL_SERVER_ERROR.value());
			}
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_TOKEN_NOT_EXIST), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		
		tokenBean.setConfirmationDate(LocalDateTime.now());
		tokenBean.setStatus(StatusTokenType.VERIFIED.getCode());

		verificationTokenRepository.save(tokenBean);
		
		Optional<User> userOptional = userRepository.findByIdAndStatus(tokenBean.getUserId(), StatusType.ACTIVE.getCode()); 
		
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			user.setPassword(passwordEncoder.encode(userTo.getPassword()));
			user.setUpdatedDate(LocalDateTime.now());
			userRepository.save(user);
			return messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_MODIFIED_PASS);
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_USER_NOT_REGISTERED), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	@Override
	public String resetPassword(UserTo user) throws CaBusinessException {

		if (user.getPassword() == null || user.getPassword().trim().isEmpty()) {
			throw new CaBusinessException(messageProperties.getMessage(
					PropertiesConstants.VALID_USER_EMPTY_PASS), HttpStatus.INTERNAL_SERVER_ERROR.value());
		} else {
			Optional<User> userQuery = userRepository.findByIdAndStatus(user.getId(), StatusType.ACTIVE.getCode());
			if (userQuery.isPresent()) {
				User userUpdate = userQuery.get();
				userUpdate.setPassword(this.passwordEncoder.encode(user.getPassword()));
				userRepository.save(userUpdate);
				return this.messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_MODIFIED_PASS);
			} else {
				throw new CaBusinessException(messageProperties.getMessage(
						PropertiesConstants.VALID_USER_NOT_REGISTERED_UPDATE), HttpStatus.INTERNAL_SERVER_ERROR.value());
			}
		}
	}

	@Override
	@Transactional
	public String passwordChange(Integer userId, String currentPassword, String password, String confirmPassword) throws CaBusinessException {

		User user = this.userRepository.findById(userId).get();

		if (!this.passwordEncoder.matches(currentPassword, user.getPassword())) {
			throw new CaBusinessException("La contraseña es incorrecta.", HttpStatus.INTERNAL_SERVER_ERROR.value());
		} else {
			if (!password.equals(confirmPassword)) {
				throw new CaBusinessException("La confirmacion de la contraseña es incorrecta.", HttpStatus.INTERNAL_SERVER_ERROR.value());
			} else {
				user.setPassword(this.passwordEncoder.encode(password));
				userRepository.save(user);
				return this.messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_MODIFIED_PASS);
			}
		}
	}

	private void validateUser(User user) throws CaBusinessException, CaRequiredException {
		if (StringUtils.isEmpty(user.getUsername()))
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_USER_REQUIRED));

		if (StringUtils.isEmpty(user.getEmail()))
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_USER_REQUIRED));

		if (StringUtils.isEmpty(user.getRoleCode()))
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_CODE_REQUIRED));

		Optional<User> userExist = userRepository.findByUsernameAndStatus(user.getUsername(), StatusType.ACTIVE.getCode());
		if (userExist.isPresent())
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_USER_REGISTERED_EXIST), HttpStatus.INTERNAL_SERVER_ERROR.value());

		Optional<Role> roleExist = roleRepository.findById(user.getRoleCode());
		if (!roleExist.isPresent())
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_CODE_INVALID), HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

	private void validateUserUpdate(User user) throws CaBusinessException, CaRequiredException {
		if (StringUtils.isEmpty(user.getUsername()))
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_USER_REQUIRED));

		if (StringUtils.isEmpty(user.getEmail()))
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_USER_REQUIRED));

		if (StringUtils.isEmpty(user.getRoleCode()))
			throw new CaRequiredException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_CODE_REQUIRED));

		Optional<Role> roleExist = roleRepository.findById(user.getRoleCode());
		if (!roleExist.isPresent())
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.VALID_ROLE_CODE_INVALID), HttpStatus.INTERNAL_SERVER_ERROR.value());
	}
	
}
