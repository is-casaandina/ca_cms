package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Parameter;
import com.casaandina.cms.model.User;
import com.casaandina.cms.repository.ParameterRepository;
import com.casaandina.cms.service.EmailService;
import com.casaandina.cms.util.*;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Component
public class EmailServiceImpl implements EmailService {
	
	private final JavaMailSender emailSender;
	private final MessageProperties messageProperties;
	private final ParameterRepository parameterRepository;
	@Value("${spring.mail.name}")
	private String fromMailName;
	@Value("${spring.mail.username}")
	private String fromMail;

	@Autowired
	public EmailServiceImpl(JavaMailSender emailSender, MessageProperties messageProperties, ParameterRepository parameterRepository) {
		this.emailSender = emailSender;
		this.messageProperties = messageProperties;
		this.parameterRepository = parameterRepository;
	}

	@Override
	public Boolean sendVerificationTokenMail(User user, String token) throws CaBusinessException {
		try {
			String subject = messageProperties.getMessage(PropertiesConstants.MESSAGE_EMAIL_SUBJECT);
			String body = "";
			String urlFront = getUrlFrontParameter();
			
			Parameter parameter =  this.parameterRepository.customFindRoibackWebServiceByCode(ParameterType.RECOVERY_PASSWORD_MAIL.getCode());
			if(Objects.nonNull(parameter)){

				StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
				String nameTemplate = messageProperties.getMessage(PropertiesConstants.TEMPLATE_EMAIL_VERIFICATION);
				String freeMakerTemplate = ObjectMapperUtils.map(parameter.getIndicator(), String.class);
				stringTemplateLoader.putTemplate(nameTemplate, freeMakerTemplate);

				Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
				configuration.setTemplateLoader(stringTemplateLoader);
				Template t = configuration.getTemplate(nameTemplate);

				Map<String, String> map = new HashMap<>();
				map.put("VERIFICATION_URL", urlFront + messageProperties.getMessage(PropertiesConstants.URL_VERIFICATION_TOKEN) + token);

				body = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);

				return sendMail(user.getEmail(), subject, body);
			} else {
				throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_EMAIL_ERROR_SEND), HttpStatus.INTERNAL_SERVER_ERROR.value());
			}

		} catch (Exception ex) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_EMAIL_ERROR_SEND), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}		
	}
	
	public Boolean sendMail(String toEmail, String subject, String body) throws CaBusinessException {
		try {
			
			MimeMessage message = emailSender.createMimeMessage();

			message.setFrom(new InternetAddress(fromMail, fromMailName, "UTF8"));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
			message.setSubject(subject);
			message.setContent(body, "text/html");

			emailSender.send(message);

			return Boolean.TRUE;
		} catch (Exception e) {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_EMAIL_ERROR_SEND), HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	private String getUrlFrontParameter(){
		Optional<Parameter> parameterOptional = this.parameterRepository.findByCodeAndLanguageCode(ParameterType.URL_FRONT_PASSWORD.getCode(), LanguageType.SPANISH.getCode());
		if(parameterOptional.isPresent()){
			return ObjectMapperUtils.map(parameterOptional.get().getIndicator(), String.class);
		} else {
			return ConstantsUtil.EMPTY;
		}
	}
}
