package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.LivingRoom;
import com.casaandina.cms.repository.LivingRoomRepository;
import com.casaandina.cms.repository.PageRepository;
import com.casaandina.cms.service.LivingRoomService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class LivingRoomServiceImpl implements LivingRoomService {
	
	private final LivingRoomRepository livingRoomRepository;
	private final PageRepository pageRepository;
	private final MessageProperties messageProperties;

	private static final Integer NOT_ACCEPTABLE = HttpStatus.NOT_ACCEPTABLE.value();
	
	@Autowired
	public LivingRoomServiceImpl(LivingRoomRepository livingRoomRepository, PageRepository pageRepository, MessageProperties messageProperties) {
		this.livingRoomRepository = livingRoomRepository;
		this.pageRepository = pageRepository;
		this.messageProperties = messageProperties;
	}

	@Override
	public LivingRoom registerLivingRoom(final LivingRoom livingRoom) throws CaBusinessException {
		validateLivingRoom(livingRoom);

		Optional<com.casaandina.cms.model.Page> page = this.pageRepository.findById(livingRoom.getHotelId());
		if (page.isPresent()) {
			livingRoom.setDestinationId(page.get().getDestinationId());
		} else {
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_LIVING_ROOM_HOTEL_NOT_EXIST), NOT_ACCEPTABLE);
		}
		livingRoom.setLastUpdate(LocalDateTime.now());
		return this.livingRoomRepository.insert(livingRoom);
	}

	private void validateLivingRoom(final LivingRoom livingRoom) throws CaBusinessException{
		if(!CaUtil.validateIsNotNullAndNotEmpty(livingRoom.getName())){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_LIVING_ROOM_NAME_REQUIRED), NOT_ACCEPTABLE);
		}

		if(!CaUtil.validateIsNotNullAndNotEmpty(livingRoom.getHotelId())){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_LIVING_ROOM_HOTEL_REQUIRED), NOT_ACCEPTABLE);
		}

		if(!CaUtil.validateIsNotNullAndNotEmpty(livingRoom.getCategory())){
			throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_LIVING_ROOM_CATEGORY_REQUIRED), NOT_ACCEPTABLE);
		}
	}

	@Override
	public LivingRoom getLivingRoom(final String livingRoomId) throws CaRequiredException {
		Optional<LivingRoom> optional = this.livingRoomRepository.findById(livingRoomId);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public LivingRoom updateLivingRoom(final LivingRoom livingRoom) throws CaBusinessException {

		validateLivingRoom(livingRoom);

		Optional<LivingRoom> optional = this.livingRoomRepository.findById(livingRoom.getId());
		if (optional.isPresent()) {
			LivingRoom livingRoom2 = optional.get();
			if (livingRoom.getHotelId() != livingRoom2.getHotelId()) {
				Optional<com.casaandina.cms.model.Page> page = this.pageRepository.findById(livingRoom.getHotelId());
				if (page.isPresent()) {
					livingRoom.setDestinationId(page.get().getDestinationId());
				}
			}
			BeanUtils.copyProperties(livingRoom, livingRoom2, ConstantsUtil.ID);
			livingRoom2.setLastUpdate(LocalDateTime.now());
			return this.livingRoomRepository.save(livingRoom2);
		} else {
			throw new CaBusinessException(ConstantsUtil.NOT_FOUND_ID, NOT_ACCEPTABLE);
		}
	}

	@Override
	public void deleteLivingRoom(final String livingRoomId) throws CaRequiredException {
		Optional<LivingRoom> optional = this.livingRoomRepository.findById(livingRoomId);
		if (optional.isPresent()) {
			this.livingRoomRepository.deleteById(livingRoomId);
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_ID);
		}
	}

	@Override
	public Page<LivingRoom> getAllTheLivingRooms(final List<String> destinationsId,
			final List<String> hotelsId, final List<Integer> capacityRange, Pageable pageable) throws CaRequiredException {
		
		if (Objects.nonNull(destinationsId)) {
			return getAllByDestinationNonNull(destinationsId, hotelsId, capacityRange, pageable);
		} else {
			return getAllByDestinationNull(hotelsId, capacityRange, pageable);
		}
	}

	@Override
	public Page<LivingRoom> findAllLivingRoomsPaginated(Pageable pageable, String hotelId) {
		Sort sort = new Sort(Sort.Direction.DESC, "lastUpdate");
		Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
		if(CaUtil.validateIsNotNullAndNotEmpty(hotelId))
			return this.livingRoomRepository.findByHotelId(hotelId, pageableSort);
		else
			return this.livingRoomRepository.findAll(pageableSort);
	}

	@Override
	public Page<LivingRoom> findLivingRoomsByHotelAndCategoryPaginated(Pageable pageable, String hotelId, String category) {
		Sort sort = new Sort(Sort.Direction.DESC, "lastUpdate");
		Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
		List<String> categories = new ArrayList<>();
		if(CaUtil.validateIsNotNullAndNotEmpty(category)){
			categories.add(category);
		} else {
			categories.add(ConstantsUtil.LivingRoomCategory.CORPORATE);
			categories.add(ConstantsUtil.LivingRoomCategory.SOCIAL);
			categories.add(ConstantsUtil.LivingRoomCategory.VARIOUS);
		}
		if(CaUtil.validateIsNotNullAndNotEmpty(hotelId)){

			return this.livingRoomRepository.findByHotelIdAndCategoryIn(hotelId,categories, pageableSort);
		}
		else{
			return this.livingRoomRepository.findByCategoryIn(categories, pageableSort);
		}
	}
	
	private Page<LivingRoom> getAllByDestinationNonNull(final List<String> destinationsId,
			final List<String> hotelsId, final List<Integer> capacityRange, Pageable pageable) {
		
		if (Objects.nonNull(hotelsId)) {
			if (validateCapacityRange(capacityRange)) {
				return this.livingRoomRepository.findAllByDestinationIdInAndHotelIdInAndTeatherBetween(
						destinationsId, hotelsId, capacityRange.get(0), capacityRange.get(1), pageable);
			} else {
				return this.livingRoomRepository.findAllByDestinationIdInAndHotelIdIn(destinationsId, hotelsId, pageable);
			}
		} else {
			if (validateCapacityRange(capacityRange)) {
				return this.livingRoomRepository.findAllByDestinationIdInAndTeatherBetween(
						destinationsId, capacityRange.get(0), capacityRange.get(1), pageable);
			} else {
				return this.livingRoomRepository.findAllByDestinationIdIn(destinationsId, pageable);
			}
		}
	}
	
	private Page<LivingRoom> getAllByDestinationNull(final List<String> hotelsId, final List<Integer> capacityRange, Pageable pageable){
		if (Objects.nonNull(hotelsId)) {
			if (validateCapacityRange(capacityRange)) {
				return this.livingRoomRepository.findAllByHotelIdInAndTeatherBetween(
						hotelsId, capacityRange.get(0), capacityRange.get(1), pageable);
			} else {
				return this.livingRoomRepository.findAllByHotelIdIn(hotelsId, pageable);
			}
		} else {
			if (validateCapacityRange(capacityRange)) {
				return this.livingRoomRepository.findAllByTeatherBetween(capacityRange.get(0), capacityRange.get(1), pageable);
			} else {
				return this.livingRoomRepository.findAll(pageable);
			}
		}
	}
	
	private boolean validateCapacityRange(final List<Integer> capacityRange) {
		return Objects.nonNull(capacityRange) && capacityRange.size() == 2;
	}

}
