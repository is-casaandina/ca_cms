package com.casaandina.cms.service.impl;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Assistance;
import com.casaandina.cms.model.AssistanceTitle;
import com.casaandina.cms.repository.AssistanceRepository;
import com.casaandina.cms.rest.request.AssistanceSiteRequest;
import com.casaandina.cms.service.AssistanceService;
import com.casaandina.cms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class AssistanceServiceImpl implements AssistanceService {

    private final AssistanceRepository assistanceRepository;
    private final MessageProperties messageProperties;

    @Autowired
    public AssistanceServiceImpl(AssistanceRepository assistanceRepository, MessageProperties messageProperties) {
        this.assistanceRepository = assistanceRepository;
        this.messageProperties = messageProperties;
    }

    @Override
    public Assistance createAssistance(Assistance assistance) throws CaBusinessException {
        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getIconClass())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_ICON_CLASS_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getEnglishTitle())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_ENGLISH_TITLE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getSpanishTitle())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_SPANISH_TITLE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getPortugueseTitle())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_PORTUGUESE_TITLE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        assistance.setAssistanceTitles(getListAssistanceTitle(assistance));
        assistance.setCreatedDate(LocalDateTime.now());
        assistance.setStatus(StatusType.ACTIVE.getCode());
        return assistanceRepository.insert(assistance);
    }

    private List<AssistanceTitle> getListAssistanceTitle(Assistance assistance) {
        List<AssistanceTitle> listAssistanceTitle = new ArrayList<>();
        listAssistanceTitle.add(AssistanceTitle.builder().language(LanguageType.ENGLISH.getCode()).title(assistance.getEnglishTitle()).build());
        listAssistanceTitle.add(AssistanceTitle.builder().language(LanguageType.SPANISH.getCode()).title(assistance.getSpanishTitle()).build());
        listAssistanceTitle.add(AssistanceTitle.builder().language(LanguageType.PORTUGUESE.getCode()).title(assistance.getPortugueseTitle()).build());
        return listAssistanceTitle;
    }

    @Override
    public Assistance updateAssistance(String assistanceId, Assistance assistance) throws CaBusinessException {
        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getIconClass())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_ICON_CLASS_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getEnglishTitle())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_ENGLISH_TITLE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getSpanishTitle())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_SPANISH_TITLE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        if(!CaUtil.validateIsNotNullAndNotEmpty(assistance.getPortugueseTitle())){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_PORTUGUESE_TITLE_REQUIRED), ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        Optional<Assistance> assistanceOptional = assistanceRepository.findById(assistanceId);
        if(!assistanceOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_NOT_EXIST),ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        Assistance assistanceUpdate = assistanceOptional.get();

        assistanceUpdate.setAssistanceTitles(getListAssistanceTitle(assistance));
        assistanceUpdate.setIconClass(assistance.getIconClass());
        assistanceUpdate.setSpanishTitle(assistance.getSpanishTitle());
        assistanceUpdate.setEnglishTitle(assistance.getEnglishTitle());
        assistanceUpdate.setPortugueseTitle(assistance.getPortugueseTitle());

        return assistanceRepository.save(assistanceUpdate);
    }

    @Override
    public Page<Assistance> getPageAssistanceByType(Integer type, String title, Pageable pageable){
        return assistanceRepository.customFindByStatusAndTypeAndTitle(StatusType.ACTIVE.getCode(), type, Objects.nonNull(title) ? title : ConstantsUtil.EMPTY, pageable);
    }

    @Override
    public List<Assistance> getListAssistanceByType(Integer type){
        return assistanceRepository.findByStatusAndType(StatusType.ACTIVE.getCode(), type);
    }

    @Override
    public List<Assistance> getListAssistanceByIdsAndTypeAndLanguage(AssistanceSiteRequest assistanceSiteRequest) {
        return this.assistanceRepository.customFindByIdsAndTypeAndLanguage(assistanceSiteRequest.getItems(), StatusType.ACTIVE.getCode(),assistanceSiteRequest.getLanguage());
    }

    @Override
    public void deleteAssistance(String id) throws CaBusinessException {
        Optional<Assistance> assistanceOptional = assistanceRepository.findById(id);
        if(!assistanceOptional.isPresent()){
            throw new CaBusinessException(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_NOT_EXIST),ConstantsUtil.INTERNAL_SERVER_ERROR);
        }

        Assistance assistanceUpdate = assistanceOptional.get();
        assistanceUpdate.setStatus(StatusType.INACTIVE.getCode());

        assistanceRepository.save(assistanceUpdate);
    }
}
