package com.casaandina.cms.service.impl;

import com.casaandina.cms.model.Robots;
import com.casaandina.cms.repository.RobotsRepository;
import com.casaandina.cms.service.RobotsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RobotsServiceImpl implements RobotsService {

    private final RobotsRepository robotsRepository;

    @Autowired
    public RobotsServiceImpl(RobotsRepository robotsRepository) {
        this.robotsRepository = robotsRepository;
    }

    @Override
    public List<Robots> getAll() {
        return this.robotsRepository.findAll();
    }

    @Override
    public Robots addValueAgent(Robots robots) {
        return this.robotsRepository.insert(robots);
    }

    @Override
    public void deleteValueAgents(String robotsId) {
        Optional<Robots> robots = this.robotsRepository.findById(robotsId);
        if (robots.isPresent()) {
            this.robotsRepository.delete(robots.get());
        }
    }
}
