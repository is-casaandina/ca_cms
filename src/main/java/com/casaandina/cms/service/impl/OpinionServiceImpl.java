package com.casaandina.cms.service.impl;

import com.casaandina.cms.model.Opinion;
import com.casaandina.cms.repository.OpinionRepository;
import com.casaandina.cms.service.OpinionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OpinionServiceImpl implements OpinionService {

	private final OpinionRepository opinionRepository;
	
	@Autowired
	public OpinionServiceImpl(OpinionRepository opinionRepository) {
		this.opinionRepository = opinionRepository;
	}
	
	@Override
	public List<Opinion> getAllOpinionsByHotelCode(String hotelCode) {
		return this.opinionRepository.findAllByHotelCode(hotelCode);
	}

}
