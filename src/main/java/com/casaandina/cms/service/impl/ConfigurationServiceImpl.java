package com.casaandina.cms.service.impl;

import com.casaandina.cms.aws.AmazonClient;
import com.casaandina.cms.dto.InputStreamParameter;
import com.casaandina.cms.dto.ParameterSiteMap;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Configurations;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.Parameter;
import com.casaandina.cms.repository.ConfigurationsRepository;
import com.casaandina.cms.repository.PageRepository;
import com.casaandina.cms.repository.ParameterRepository;
import com.casaandina.cms.service.ConfigurationService;
import com.casaandina.cms.util.*;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
@Log4j2
public class ConfigurationServiceImpl implements ConfigurationService {
	
	private final ParameterRepository parameterRepository;
	private final ConfigurationsRepository configurationsRepository;
	private final PageRepository pageRepository;
	private final AmazonClient amazonClient;

	@Value("${aws.endpointUrl}")
	private String endpointUrl;

	@Value("${aws.bucketName}")
	private String bucketName;

	private static final String DIRECTORY_SITE_MAP = "siteMap";

	public ConfigurationServiceImpl(ConfigurationsRepository configurationsRepository, ParameterRepository parameterRepository, PageRepository pageRepository, AmazonClient amazonClient) {
		this.configurationsRepository = configurationsRepository;
		this.parameterRepository = parameterRepository;
		this.pageRepository = pageRepository;
		this.amazonClient = amazonClient;
	}

	@Override
	public Configurations get() {
		return this.configurationsRepository.findAll().stream().findFirst().get();
	}

	@Override
	public Configurations save(Configurations configurations) throws CaBusinessException, CaRequiredException {
		return this.configurationsRepository.save(configurations);
	}

	@Override
	public List<Parameter> saveServiceRoiback(List<Parameter> parameters) throws CaBusinessException, CaRequiredException {
		parameters.stream().forEach(p -> p.setCode(ParameterType.ROIBACK.getCode()));
		return this.parameterRepository.saveAll(parameters);
	}
	
	@Override
	public Parameter saveSocialNetworks(Parameter parameter) throws CaBusinessException, CaRequiredException {
		
		parameter.setCode(ConstantsUtil.SOCIAL_NETWORKS_CODE);
		parameter.setDescription(ConstantsUtil.SOCIAL_NETWORKS);
		parameter.setLanguageCode(ConstantsUtil.SPANISH_LANGUAGE_CODE);
		return this.parameterRepository.insert(parameter);
		
	}
	
	@Override
	public List<Parameter> savePriceRoiback(List<Parameter> parametersList) throws CaBusinessException, CaRequiredException {
		return this.parameterRepository.saveAll(parametersList);
	}
	
	private Parameter findParameterByCodeAndLanguage(final String code, final String languageCode) throws CaRequiredException {
		Optional<Parameter> optional = this.parameterRepository.findByCodeAndLanguageCode(code, languageCode);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
		}
	}
	
	@Override
	public List<Parameter> getPriceRoiback() {
		return parameterRepository.findByCode(ParameterType.PRICE_ROIBACK.getCode());
	}
	
	@Override
	public Parameter getSocialNetworking() throws CaRequiredException {
		return findParameterByCodeAndLanguage(ParameterType.SOCIAL_NETWORK.getCode(), LanguageType.SPANISH.getCode());
	}
	
	@Override
	public List<Parameter> getServiceRoiback() {
		return parameterRepository.findByCode(ParameterType.ROIBACK.getCode());
	}

	@Override
	public void siteMapGenerate(Boolean acceptExternalPages){
		Parameter parameter = this.parameterRepository.customFindRoibackWebServiceByCode(ParameterType.SITE_MAP.getCode());
		if(Objects.nonNull(parameter)){
			ParameterSiteMap parameterSiteMap = ObjectMapperUtils.map(parameter.getIndicator(), ParameterSiteMap.class);
			List<Page> pages = getAllPage(acceptExternalPages);
			if(Objects.nonNull(pages) && !pages.isEmpty()){
				List<Configurations> configurations = this.configurationsRepository.customFindAllDomain();
				if(Objects.nonNull(configurations) && !configurations.isEmpty()){
					Map<String, StringBuilder> mapXmlLanguages = generateXmlFromPage(parameterSiteMap, pages, configurations.get(ConstantsUtil.ZERO).getDomain());
					convertMapXmlToFileXmlAndUpload(parameterSiteMap, mapXmlLanguages,  configurations.get(ConstantsUtil.ZERO).getDomain());
				}
			}
		}
	}

	private void convertMapXmlToFileXmlAndUpload(ParameterSiteMap parameterSiteMap, Map<String, StringBuilder> mapXmlLanguages, String domain){
		StringBuilder sbSpanish = mapXmlLanguages.get(LanguageType.SPANISH.getCode());
		StringBuilder sbEnglish= mapXmlLanguages.get(LanguageType.ENGLISH.getCode());
		StringBuilder sbPortuguese = mapXmlLanguages.get(LanguageType.PORTUGUESE.getCode());

		uploadXmlToS3(sbSpanish.toString(), LanguageType.SPANISH.getCode());
		uploadXmlToS3(sbEnglish.toString(), LanguageType.ENGLISH.getCode());
		uploadXmlToS3(sbPortuguese.toString(), LanguageType.PORTUGUESE.getCode());

		String uriSpanish = generatePathPostXML(domain, LanguageType.SPANISH.getCode());
		String uriEnglish = generatePathPostXML(domain, LanguageType.ENGLISH.getCode());
		String uriPortuguese = generatePathPostXML(domain, LanguageType.PORTUGUESE.getCode());

		StringBuilder sbSiteMapIndex = buildSiteMapIndex(parameterSiteMap, uriSpanish, uriEnglish, uriPortuguese);

		uploadXmlToS3(sbSiteMapIndex.toString(), null);
	}

	private String generatePathPostXML(String domain,String language){
		String urlBase = domain;
		if(!ConstantsUtil.SEPARATOR.equals(urlBase.substring(urlBase.length() - 1)))
			urlBase = urlBase + ConstantsUtil.SEPARATOR;

		StringBuilder sbFileName = new StringBuilder(urlBase);
		sbFileName.append(ConstantsUtil.SiteMapXml.POST);
		sbFileName.append(ConstantsUtil.SiteMapXml.SUBGUION);
		sbFileName.append(language).append(ConstantsUtil.SiteMapXml.SUBGUION).append(ConstantsUtil.SiteMapXml.SITE_MAP);
		sbFileName.append(ConstantsUtil.SiteMapXml.EXTENSION);

		return sbFileName.toString();
	}

	private StringBuilder buildSiteMapIndex(ParameterSiteMap parameterSiteMap, String uriSpanish, String uriEnglish, String uriPortuguese){
		StringBuilder sbSiteMapIndex = new StringBuilder(parameterSiteMap.getBeginIndexXml());
		ZoneId zoneId = ZoneId.of("America/Lima");
		LocalDateTime now = LocalDateTime.now(zoneId);
		sbSiteMapIndex.append("\n").append(getBodyUrlSet(parameterSiteMap.getBodyIndexXml(), uriSpanish, now));
		sbSiteMapIndex.append("\n").append(getBodyUrlSet(parameterSiteMap.getBodyIndexXml(), uriEnglish, now));
		sbSiteMapIndex.append("\n").append(getBodyUrlSet(parameterSiteMap.getBodyIndexXml(), uriPortuguese, now));
		sbSiteMapIndex.append("\n").append(parameterSiteMap.getEndIndexXml());

		return sbSiteMapIndex;
	}

	private String uploadXmlToS3(String xmlString, String language){
		byte[] byteSpanish = xmlString.getBytes(StandardCharsets.UTF_8);
		InputStream source = new ByteArrayInputStream(byteSpanish);

		StringBuilder sbFileName;
		if(Objects.nonNull(language)){
			sbFileName = new StringBuilder(ConstantsUtil.SiteMapXml.POST);
			sbFileName.append(ConstantsUtil.SiteMapXml.SUBGUION);
			sbFileName.append(language).append(ConstantsUtil.SiteMapXml.SUBGUION).append(ConstantsUtil.SiteMapXml.SITE_MAP);
			sbFileName.append(ConstantsUtil.SiteMapXml.EXTENSION);
		} else {
			sbFileName = new StringBuilder(ConstantsUtil.SiteMapXml.SITE_MAP);
			sbFileName.append(ConstantsUtil.SiteMapXml.EXTENSION);
		}

		InputStreamParameter inputStreamParameter = InputStreamParameter.builder()
				.contentType(ConstantsUtil.SiteMapXml.CONTENT_TYPE)
				.length(byteSpanish.length)
				.fileName(sbFileName.toString())
				.fis(source)
				.build();

		String fileUrlXml = endpointUrl + ConstantsUtil.SEPARATOR + bucketName + ConstantsUtil.SEPARATOR
				+ DIRECTORY_SITE_MAP + ConstantsUtil.SEPARATOR + sbFileName.toString();

		amazonClient.uploadInputStream(DIRECTORY_SITE_MAP, inputStreamParameter);
		return fileUrlXml;
	}

	private Map<String, StringBuilder> generateXmlFromPage(ParameterSiteMap parameterSiteMap, List<Page> pages, String domain){
		StringBuilder sbSpanish = new StringBuilder(parameterSiteMap.getBeginUrlSetXml());
		StringBuilder sbEnglish = new StringBuilder(parameterSiteMap.getBeginUrlSetXml());
		StringBuilder sbPortuguese = new StringBuilder(parameterSiteMap.getBeginUrlSetXml());
		pages.forEach(page -> {
			if(Objects.nonNull(page.getLanguages()) && !page.getLanguages().isEmpty()){
				page.getLanguages().forEach(language -> {
					if(Objects.nonNull(language.getPath())){
						String path;
						if(TemplateType.LINK.getCode().equals(page.getCategoryId())){
							path = language.getPath();
						} else {
							path = Objects.nonNull(domain) ? (domain + language.getPath()) : language.getPath();
						}
						String urlSet = getBodyUrlSet(parameterSiteMap.getBodyUrlSetXml(), path, page.getModifiedDate());
						if(LanguageType.SPANISH.getCode().equals(language.getLanguage()) && Objects.nonNull(urlSet))
							sbSpanish.append("\n").append(urlSet);
						if(LanguageType.ENGLISH.getCode().equals(language.getLanguage()) && Objects.nonNull(urlSet))
							sbEnglish.append("\n").append(urlSet);
						if(LanguageType.PORTUGUESE.getCode().equals(language.getLanguage()) && Objects.nonNull(urlSet))
							sbPortuguese.append("\n").append(urlSet);
					}
				});
			}
		});

		return getMapXmlSIte(sbSpanish, sbEnglish, sbPortuguese, parameterSiteMap);
	}

	private Map<String, StringBuilder> getMapXmlSIte(StringBuilder sbSpanish, StringBuilder sbEnglish,
													 StringBuilder sbPortuguese, ParameterSiteMap parameterSiteMap){

		sbSpanish.append("\n").append(parameterSiteMap.getEndUrlSetXml());
		sbEnglish.append("\n").append(parameterSiteMap.getEndUrlSetXml());
		sbPortuguese.append("\n").append(parameterSiteMap.getEndUrlSetXml());

		Map<String, StringBuilder> mapXmlLanguages = new HashMap<>();

		mapXmlLanguages.put(LanguageType.SPANISH.getCode(), sbSpanish);
		mapXmlLanguages.put(LanguageType.ENGLISH.getCode(), sbEnglish);
		mapXmlLanguages.put(LanguageType.PORTUGUESE.getCode(), sbPortuguese);

		return mapXmlLanguages;
	}

	private List<Page> getAllPage(Boolean acceptExternalPages){

		List<String> categories = TemplateType.getListCode();
		if(!acceptExternalPages){
			categories.removeIf(x -> TemplateType.LINK.getCode().equals(x));
		}
		return this.pageRepository.customFindAllPageForSiteMap(categories);
	}

	private String getBodyUrlSet(String bodyUrlSet, String path, LocalDateTime modifiedDate){
		try {
			StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
			String nameTemplate = "bodyUrlSet.ftl";
			String freeMakerTemplate = ObjectMapperUtils.map(bodyUrlSet, String.class);
			stringTemplateLoader.putTemplate(nameTemplate, freeMakerTemplate);

			Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
			configuration.setTemplateLoader(stringTemplateLoader);
			Template t = configuration.getTemplate(nameTemplate);

			path = path.replace("&", "&amp;").replace("<", "&lt;");
			path = path.replace(">", "&gt;").replace("\"", "&quot;");
			path = path.replace("'", "&apos;");

			Map<String, String> map = new HashMap<>();
			map.put("PATH", path);
			map.put("MODIFIED_DATE", modifiedDate.toString());

			return FreeMarkerTemplateUtils.processTemplateIntoString(t, map);

		} catch(Exception e){
			log.error("ocurrio un error al parsear cadena xml urlSet" + e.getMessage());
			return null;
		}
	}

	@Override
	public void uploadTxtToS3(String robotsTxt){
		byte[] byteSpanish = robotsTxt.getBytes(StandardCharsets.UTF_8);
		InputStream source = new ByteArrayInputStream(byteSpanish);

		StringBuilder sbFileName = new StringBuilder(ConstantsUtil.SiteMapXml.ROBOTS_NAME);
		sbFileName.append(ConstantsUtil.SiteMapXml.EXTENSION_TXT);

		InputStreamParameter inputStreamParameter = InputStreamParameter.builder()
				.contentType(ConstantsUtil.SiteMapXml.CONTENT_TYPE_TXT)
				.length(byteSpanish.length)
				.fileName(sbFileName.toString())
				.fis(source)
				.build();

		amazonClient.uploadInputStream(DIRECTORY_SITE_MAP, inputStreamParameter);
	}
}
