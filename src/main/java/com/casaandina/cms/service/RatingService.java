package com.casaandina.cms.service;

import com.casaandina.cms.rest.response.RatingResponse;

public interface RatingService {
	
	RatingResponse getAllRatingByHotelCode(String hotelCode);

}
