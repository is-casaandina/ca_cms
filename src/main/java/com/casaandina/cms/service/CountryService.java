package com.casaandina.cms.service;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Country;

import java.util.List;

public interface CountryService {
	
	Country saveCountry(Country country)
			throws CaBusinessException, CaRequiredException;
	
	Country getCountry(String countryId) throws CaRequiredException;
	
	Country updateCountry(String countryId, Country country)
			throws CaBusinessException, CaRequiredException;
	
	void deleteCountry(String countryId) throws CaRequiredException;
	
	Country getCountryByCode(String country) throws CaRequiredException;

	List<Country> customFindAllByStatus();
	
	List<Country> getCountriesList() throws CaBusinessException;

}
