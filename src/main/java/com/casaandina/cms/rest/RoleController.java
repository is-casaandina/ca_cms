package com.casaandina.cms.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.casaandina.cms.model.Permission;
import com.casaandina.cms.rest.request.RoleAndPermissionRequest;
import com.casaandina.cms.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Role;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.RoleDetailResponse;
import com.casaandina.cms.service.RoleService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.PropertiesConstants;
import com.casaandina.cms.util.pagination.Page;
import com.casaandina.cms.util.pagination.PageUtil;

@RestController
@RequestMapping("/api/roles")
public class RoleController {
	
	private final RoleService roleService;
	private final PermissionService permissionService;
	private final MessageProperties messageProperties;
	
	@Autowired
	public RoleController(RoleService roleService, PermissionService permissionService, MessageProperties messageProperties) {
		this.roleService = roleService;
		this.permissionService = permissionService;
		this.messageProperties = messageProperties;
	}
	
	@GetMapping
	public List<Role> findAll() throws CaBusinessException {
		return roleService.findAll();
	}

	@GetMapping("/paginated")
	public Page<Role> findAllRolesPaginated(Pageable pageable) throws CaBusinessException {
		return roleService.findAllRolesPaginated(PageUtil.getPageable(pageable.getPageNumber(), pageable.getPageSize()));
	}

	@GetMapping("/detail")
	public List<RoleDetailResponse> findAllRoleDetail() throws CaBusinessException {
		return this.convertRoleToRoleDetailResponse(this.roleService.findRolesPermissionDetail());
	}

	@GetMapping("/detail/paginated")
	public Page<RoleDetailResponse> findAllRoleDetailPaginated(Pageable pageable) throws CaBusinessException {
		Page<Role> rolePaginated = roleService.findRolesPermissionDetailPaginated(
			PageUtil.getPageable(
				pageable.getPageNumber(),
				pageable.getPageSize()
			)
		);

		List<RoleDetailResponse> roles = convertRoleToRoleDetailResponse(rolePaginated.getContent());

		return PageUtil.pageToResponse(rolePaginated, roles);
	}

	@SuppressWarnings("rawtypes")
	@PostMapping
	public GenericResponse saveRole(@RequestBody RoleAndPermissionRequest roleAndPermissionRequest) throws CaRequiredException, CaBusinessException {
		roleService.saveRole(this.convertRequestToRole(roleAndPermissionRequest));
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.SAVE_ROLE_SUCCESFUL));
	}

	@PutMapping("/{roleCode}")
    public GenericResponse updateRole(@PathVariable String roleCode, @RequestBody RoleAndPermissionRequest roleAndPermissionRequest) throws CaRequiredException, CaBusinessException {
		Role role = this.convertRequestToRole(roleAndPermissionRequest);
		role.setRoleCode(roleCode);
		roleService.updateRole(role);
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.UPDATE_ROLE_SUCCESFUL));
    }
	
	@DeleteMapping("/{roleCode}")
	public GenericResponse deleteRole(@PathVariable String roleCode) throws CaRequiredException, CaBusinessException {
		roleService.deleteRole(roleCode);
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.DELETE_ROLE_SUCCESFUL));
	}

	private Role convertRequestToRole(RoleAndPermissionRequest roleAndPermissionRequest) {
		return Role.builder()
			.roleCode(roleAndPermissionRequest.getRoleCode())
			.roleName(roleAndPermissionRequest.getRoleName())
			.permissionCodeList(roleAndPermissionRequest.getPermissionCodeList())
			.build();
	}

	private List<RoleDetailResponse> convertRoleToRoleDetailResponse(List<Role> listRole){
		List<RoleDetailResponse> lstRoleDetailResponse = new ArrayList<>();

		if(CaUtil.validateListIsNotNullOrEmpty(listRole)) {

			List<Permission> listPermission = this.permissionService.findAllActive();

			lstRoleDetailResponse = listRole.stream().map(role -> {
				List<Permission> permissionCodeList = new ArrayList<>();
				if(CaUtil.validateListIsNotNullOrEmpty(role.getPermissionCodeList())) {
					permissionCodeList = getListPermissionByRole(listPermission, role.getPermissionCodeList());
				}

				return RoleDetailResponse.builder()
						.roleCode(role.getRoleCode())
						.roleName(role.getRoleName())
						.status(role.getStatus())
						.permissionCodeList(permissionCodeList)
						.build();
			})
			.collect(Collectors.toList());
		}

		return lstRoleDetailResponse;
	}

	private Predicate<Permission> inPermissionRole(List<String> permissionList) {
		return p -> permissionList.contains(p.getPermissionCode());
	}

	private List<Permission> getListPermissionByRole(List<Permission> permissions, List<String> permissionsRole) {
		return permissions.stream().filter(inPermissionRole(permissionsRole)).collect(Collectors.toList());
	}
}
