package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Benefits;
import com.casaandina.cms.rest.request.BenefitsRequest;
import com.casaandina.cms.rest.response.BenefitsResponse;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.service.BenefitsService;
import com.casaandina.cms.util.ConstantsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/benefits")
public class BenefitsController {

    private final BenefitsService benefitsService;

    @Autowired
    public BenefitsController(BenefitsService benefitsService) {
        this.benefitsService = benefitsService;
    }

    @GetMapping
    public BenefitsResponse getBenefits() {
        Optional<Benefits> benefits = this.benefitsService.get();

        if (benefits.isPresent()) {
            Benefits response = benefits.get();
            return BenefitsResponse.builder()
                    .textEs(response.getTextEs())
                    .linkEs(response.getLinkEs())
                    .textEn(response.getTextEn())
                    .linkEn(response.getLinkEn())
                    .textPr(response.getTextPr())
                    .linkPr(response.getLinkPr())
                    .detail(response.getDetail())
                    .build();
        } else {
            return new BenefitsResponse();
        }

    }

    @PostMapping
    public GenericResponse<Benefits> saveBenefits(@RequestBody final BenefitsRequest benefitsRequest) throws CaBusinessException, CaRequiredException {
        Optional<Benefits> benefits = this.benefitsService.get();

        Benefits benefitsUpd = new Benefits();

        if (benefits.isPresent()) {
            benefitsUpd = benefits.get();
        }

        benefitsUpd.setTextEs(benefitsRequest.getTextEs());
        benefitsUpd.setLinkEs(benefitsRequest.getLinkEs());
        benefitsUpd.setTextEn(benefitsRequest.getTextEn());
        benefitsUpd.setLinkEn(benefitsRequest.getLinkEn());
        benefitsUpd.setTextPr(benefitsRequest.getTextPr());
        benefitsUpd.setLinkPr(benefitsRequest.getLinkPr());
        benefitsUpd.setDetail(benefitsRequest.getDetail());

        GenericResponse<Benefits> genericResponse = new GenericResponse<>();
        genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
        genericResponse.setData(this.benefitsService.save(benefitsUpd));

        return genericResponse;
    }
}
