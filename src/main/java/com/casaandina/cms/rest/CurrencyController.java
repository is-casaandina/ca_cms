package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Currency;
import com.casaandina.cms.rest.request.CurrencyRequest;
import com.casaandina.cms.rest.response.CurrencyResponse;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.service.CurrencyService;
import com.casaandina.cms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/currencies")
public class CurrencyController {

	private final CurrencyService currencyService;
	
	private final MessageProperties messageProperties;

	@Autowired
	public CurrencyController(CurrencyService currencyService, MessageProperties messageProperties) {
		this.currencyService = currencyService;
		this.messageProperties = messageProperties;
	}

	@SuppressWarnings("rawtypes")
	@PostMapping
	public GenericResponse saveCurrency(@RequestBody CurrencyRequest currencyRequest) throws CaBusinessException, CaRequiredException {
		Currency currency = currencyService.saveCurrency(ObjectMapperUtils.map(currencyRequest, Currency.class));
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_SAVE_SUCCESFUL),ObjectMapperUtils.map(currency, CurrencyResponse.class));
	}

	@GetMapping
	public List<CurrencyResponse> findByStatus() {
		return ObjectMapperUtils.mapAll(currencyService.findByStatus(), CurrencyResponse.class);
	}
	
	@SuppressWarnings("rawtypes")
	@PutMapping
	public GenericResponse<List<CurrencyResponse>> updateListCurrency(@RequestBody List<CurrencyRequest> listCurrencyRequest) throws CaBusinessException {
		List<Currency> listCurrency = currencyService.updateListCurrency(ObjectMapperUtils.mapAll(listCurrencyRequest, Currency.class));
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.VALID_CURRENCY_UPDATE_SUCCESFUL), ObjectMapperUtils.mapAll(listCurrency, CurrencyResponse.class));
	}
	
	@GetMapping("/currency-converter")
	public CurrencyResponse currencyConverter(@RequestParam("codeIsoOrigin") String codeIsoOrigin, @RequestParam("codeIsoTarget") String codeIsoTarget, @RequestParam("amount") Double amount) throws CaBusinessException{
		Double result = currencyService.currencyConverter(codeIsoOrigin, codeIsoTarget, amount);
		return CurrencyResponse.builder().value(result).build();
	}

}
