package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Component;
import com.casaandina.cms.rest.response.ComponentResponse;
import com.casaandina.cms.rest.response.ComponentShortResponse;
import com.casaandina.cms.service.ComponentService;
import com.casaandina.cms.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/components")
public class ComponentController {
	
	private final ComponentService componentService;
	
	@Autowired
	public ComponentController(ComponentService componentService) {
		this.componentService = componentService;
	}
	
	@GetMapping
	public List<ComponentShortResponse> findComponentShortByStatus() throws CaBusinessException {
		List<Component> listComponent = componentService.findComponentShortByStatus();
		return ObjectMapperUtils.mapAll(listComponent, ComponentShortResponse.class);
	}
	
	@GetMapping("/{id}")
	public ComponentResponse findById(@PathVariable String id) throws CaRequiredException {
		Component component = componentService.findByIdAndStatus(id);
		return ObjectMapperUtils.map(component, ComponentResponse.class);
	}
	
	@GetMapping("/code/{code}")
	public ComponentResponse findByCode(@PathVariable String code) throws CaRequiredException {
		return ObjectMapperUtils.map(componentService.findByCodeAndStatus(code), ComponentResponse.class);
	}

	@GetMapping("/type/{type}")
	public List<ComponentResponse> findByType(@PathVariable Integer type) {
		return ObjectMapperUtils.mapAll(componentService.findComponentByTypeAndStatus(type), ComponentResponse.class);
	}
}
