package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.exception.CaSiteErrorException;
import com.casaandina.cms.model.LanguagePage;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.PageError;
import com.casaandina.cms.rest.response.ExceptionResponse;
import com.casaandina.cms.service.PageErrorService;
import com.casaandina.cms.util.ConstantsUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@ControllerAdvice
@RestController
@RequiredArgsConstructor
public class CustomExceptionAdviceController extends ResponseEntityExceptionHandler {

    private final PageErrorService pageErrorService;

    @ExceptionHandler(CaBusinessException.class)
    public ResponseEntity<Object> handleCaBusinessException(CaBusinessException ex, WebRequest request){
        ExceptionResponse exceptionResponse = ExceptionResponse.builder()
                .timestamp(new Date())
                .message(ex.getMessage())
                .details(request.getDescription(false))
                .build();

        return new ResponseEntity(exceptionResponse, getHttpStatusByCodeException(ex.getStatus()));
    }

    @ExceptionHandler(CaRequiredException.class)
    public ResponseEntity<Object> handleCaRequiredException(CaRequiredException ex, WebRequest request){
        ExceptionResponse exceptionResponse = ExceptionResponse.builder()
                .timestamp(new Date())
                .message(ex.getMessage())
                .details(request.getDescription(false))
                .build();

        return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private HttpStatus getHttpStatusByCodeException(int code){

        HttpStatus httpStatus = null;
        switch(code){
            case 406 :
            case 500 :
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
               break;
            case 404:
                httpStatus = HttpStatus.NOT_FOUND;
                break;
            default:
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return httpStatus;
    }

    @ExceptionHandler(CaSiteErrorException.class)
    public ResponseEntity<Object> handleCaSiteErrorException(CaSiteErrorException ex, WebRequest request){
        String details = request.getDescription(false);
        ExceptionResponse exceptionResponse = ExceptionResponse.builder()
                .timestamp(new Date())
                .message(ex.getMessage())
                .details(details)
                .uriError(getUriError(details, ex.getLanguage(), ex.getStatus()))
                .build();

        return new ResponseEntity(exceptionResponse, getHttpStatusByCodeException(ex.getStatus()));
    }

    private String getUriError(String fromUri, String language, Integer errorCode){
        if(fromUri.contains(ConstantsUtil.URI_EXCEPTION_SITE)){
                Optional<PageError> pageErrorOptional = this.pageErrorService.getPageErrorByCodeAndState(errorCode);
                if(pageErrorOptional.isPresent()) {
                    PageError pageError = pageErrorOptional.get();
                    Optional<Page> pageOptional = this.pageErrorService.customFindPagePathByIdAndLanguage(pageError.getPageId(), language);
                    if(pageOptional.isPresent()){
                        Page page = pageOptional.get();
                        if(Objects.nonNull(page.getLanguages()) && !page.getLanguages().isEmpty()){
                            LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
                            if(Objects.nonNull(language)){
                                return languagePage.getPath();
                            }
                        }
                    }
                }
        }
        return null;
    }
}
