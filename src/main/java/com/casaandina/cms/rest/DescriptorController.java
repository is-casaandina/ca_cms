package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Descriptor;
import com.casaandina.cms.rest.request.TagRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.rest.response.TagResponse;
import com.casaandina.cms.service.DescriptorService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.TemplateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/descriptors")
public class DescriptorController {
	
	private final DescriptorService tagService;
	
	@Autowired
	public DescriptorController(DescriptorService tagService) {
		this.tagService = tagService;
	}
	
	@PostMapping
	public GenericResponse<TagResponse> createTag(@RequestBody final TagRequest tagRequest)
			throws CaRequiredException {
		GenericResponse<TagResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(ObjectMapperUtils.map(this.tagService.createTag(
				ObjectMapperUtils.map(tagRequest, Descriptor.class)), TagResponse.class));
		
		return genericResponse;
	}
	
	@GetMapping("/{id}")
	public TagResponse readTag(@PathVariable("id") final String tagId) throws CaRequiredException {
		return ObjectMapperUtils.map(this.tagService.readTag(tagId), TagResponse.class);
	}
	
	@PutMapping("/{id}")
	public GenericResponse<TagResponse> updateTag(@PathVariable("id") final String tagId,
			@RequestBody final TagRequest tagRequest) throws CaRequiredException {
		
		Descriptor tag = ObjectMapperUtils.map(tagRequest, Descriptor.class);
		tag.setId(tagId);
		
		GenericResponse<TagResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_UPDATE);
		genericResponse.setData(ObjectMapperUtils.map(this.tagService.updateTag(tag), TagResponse.class));
		
		return genericResponse;
	}
	
	@DeleteMapping("/{id}")
	public MessageResponse deleteTag(@PathVariable("id") final String tagId) {
		this.tagService.deleteTag(tagId);
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setMessage(ConstantsUtil.SUCCESSFUL_DELETE);
		return messageResponse;
	}
	
	@GetMapping
	public List<TagResponse> getAllTheTagsByLanguageCode() {
	    List<Descriptor> listDescriptor = this.tagService.getAllTheTags();
	    List<TagResponse> response = ObjectMapperUtils.mapAll(listDescriptor, TagResponse.class);
		response.forEach(x-> {
	        TemplateType templateType = TemplateType.get(x.getCategory());
	        if(templateType != null)
	            x.setCategoryName(templateType.getValue());
        });
		return response;
	}

	@GetMapping("/categories/{categoryId}")
	public List<SelectResponse> getDescriptorByCategoryId(@PathVariable String categoryId){
		if(TemplateType.HOTELS_DESCRIPTOR.getCode().equals(categoryId)){
			categoryId = TemplateType.HOTELS_DETAIL.getCode();
		}
		if(TemplateType.RESTAURANTS_DESCRIPTOR.getCode().equals(categoryId)){
			categoryId = TemplateType.RESTAURANTS_DETAIL.getCode();
		}
		List<Descriptor> listDescriptor =  tagService.getAllByCategory(categoryId);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listDescriptor.forEach(x ->
				listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(x.getTitle()).build())
		);
		return listSelectResponse;
	}

	@GetMapping("/categories")
	public List<SelectResponse> getDescriptorsByCategory(@RequestParam(value ="type-descriptor", required = false) String category, @RequestParam(value = "type", required = false) String type){
		String categoryId = "";
		if(CaUtil.validateIsNotNullAndNotEmpty(type))
			category = type;
		if(CaUtil.validateIsNotNullAndNotEmpty(category)){
			if(ConstantsUtil.Page.HOTELES.equals(category)){
				category = TemplateType.HOTELS_DETAIL.getValue();
			} else if(ConstantsUtil.Page.RESTAURANTS.equals(category)){
				category = TemplateType.RESTAURANTS_DETAIL.getValue();
			}
			TemplateType templateType = TemplateType.getByValue(category);
			categoryId = templateType.getCode();
		}
		List<Descriptor> listDescriptor = this.tagService.getAllByCategory(categoryId);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listDescriptor.forEach(x ->
				listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(x.getTitle()).build())
		);
		return listSelectResponse;
	}

}
