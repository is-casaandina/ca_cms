package com.casaandina.cms.rest;

import com.casaandina.cms.dto.HotelRestPromotionItemDto;
import com.casaandina.cms.dto.PromotionDataDto;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.DraftPage;
import com.casaandina.cms.model.LanguagePage;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.rest.request.PageRequest;
import com.casaandina.cms.rest.response.*;
import com.casaandina.cms.service.PageService;
import com.casaandina.cms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pages")
@SuppressWarnings("rawtypes")
public class PageController {

	private final PageService pageService;
	
	private final UserAuthentication userAuthentication;
	
	private final MessageProperties messageProperties;
	
	@Autowired
	public PageController(PageService pageService, UserAuthentication userAuthentication, MessageProperties messageProperties) {
		this.pageService = pageService;
		this.userAuthentication = userAuthentication;
		this.messageProperties = messageProperties;
	}
	
	@PostMapping
	public GenericResponse savePage(@RequestBody PageRequest pageRequest) throws CaBusinessException {
		DraftPage draftPage = pageService.savePage(ObjectMapperUtils.map(pageRequest, Page.class));
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_SAVE_SUCCESSFUL), ObjectMapperUtils.map(draftPage, PageResponse.class));
	}
	
	@PostMapping("/publish/{id}")
	public GenericResponse publishPage(@PathVariable String id) throws CaBusinessException {
		Page page = pageService.publishPage(id);
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_PUBLISH_SUCCESSFUL), ObjectMapperUtils.map(page, PageResponse.class));
	}
	
	@GetMapping("/{idPage}")
	public PageResponse getPageForEdit(@PathVariable String idPage) throws CaBusinessException {
		String username = userAuthentication.getUserPrincipal().getUsername();
		return ObjectMapperUtils.map(pageService.getPageForEdit(idPage, username), PageResponse.class);
	}
	
	@GetMapping("/tray")
	public List<PageHeaderResponse> getListPageHeaderForTray() {
		return ObjectMapperUtils.mapAll(pageService.getListPageHeaderForTray(), PageHeaderResponse.class);
	}
	
	@GetMapping("/url")
	public List<PageUrlResponse> findPageByLanguageAndPath(@RequestParam String language, @RequestParam String value, @RequestParam Integer limit) {
		List<Page> listPage = pageService.findPageByLanguageAndPath(language, value, limit);
		List<PageUrlResponse> listPageUrlResponse = new ArrayList<>();
		listPage.forEach(x -> 
			listPageUrlResponse.add(PageUrlResponse.builder().isExternal(x.getExternal()).url(x.getLanguages().stream().findFirst().get().getPath()).build())
		);
		return listPageUrlResponse;
	}

	@GetMapping("/destinations")
	public List<SelectResponse> findPageDestinationByCountryId(@RequestParam(required = false) String countryId,
															   @RequestParam(required = false) String country,
															   @RequestParam String language){
		if(CaUtil.validateIsNotNullAndNotEmpty(country)){
			countryId = country;
		}
		List<Page> listPage = pageService.findDestinationByCountryIdAndLanguageCustomized(countryId);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listPage.forEach(x->
			x.getLanguages().forEach(k -> {
				if (k.getLanguage().equals(language)) {
					listSelectResponse.add(SelectResponse.builder().text(k.getTitle()).value(x.getId()).build());
				}
			})
		);
		return listSelectResponse;
	}

	@GetMapping("/{destination}/hotels")
	public List<SelectResponse> findHotelsByDestinationCustomized(@PathVariable String destination, @RequestParam String language){
		List<Page> listPage = pageService.findHotelsByDestinationCustomized(destination);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listPage.forEach(x->
				x.getLanguages().forEach(k -> {
					if (k.getLanguage().equals(language)) {
						listSelectResponse.add(SelectResponse.builder().text(k.getTitle()).value(x.getId()).build());
					}
				})
		);
		return listSelectResponse;
	}

	@GetMapping("/promotions")
	public List<SelectResponse> findPromotionsByDescriptorAndCategory(@RequestParam("type-descriptor") String category, @RequestParam String descriptor, @RequestParam String language){
		String categoryId = "";
		if(CaUtil.validateIsNotNullAndNotEmpty(category)) {
			TemplateType templateType = TemplateType.getByValue(category);
			if(templateType != null)
				categoryId = templateType.getCode();
		}
		List<Page> listPage = pageService.findPromotionsByDescriptorAndCategory(descriptor, categoryId);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listPage.forEach(x->
				x.getLanguages().forEach(k -> {
					if (k.getLanguage().equals(language)) {
						listSelectResponse.add(SelectResponse.builder().text(k.getTitle()).value(x.getId()).build());
					}
				})
		);
		return listSelectResponse;
	}

	@GetMapping("/hotels")
	public List<SelectResponse> findHotelsByDescriptor(@RequestParam String descriptor, @RequestParam String language){
		List<Page> listPage = pageService.findHotelsByDescriptorCustomized(descriptor);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listPage.forEach(x -> {
			LanguagePage languagePage = x.getLanguages().stream().findFirst().orElse(null);
			listSelectResponse.add(SelectResponse.builder()
					.text(languagePage != null ? languagePage.getTitle() : ConstantsUtil.EMPTY)
					.url(languagePage != null ? languagePage.getPath() : ConstantsUtil.EMPTY)
					.value(x.getId())
					.build());
		});
		return listSelectResponse;
	}

	@DeleteMapping("/{id}")
	public GenericResponse deletePage(@PathVariable String id) throws CaBusinessException{
		this.pageService.deletePage(id);
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_DELETE_SUCCESFUL));
	}

	@GetMapping("/view/{id}/{state}")
	public PageResponse getPageForView(@PathVariable String id, @PathVariable Integer state) throws CaRequiredException {
		return ObjectMapperUtils.map(pageService.getPageForView(id, state), PageResponse.class);
	}

	@PostMapping("/unPublish/{id}")
	public GenericResponse unPublishPage(@PathVariable String id) throws CaBusinessException {
		DraftPage page = pageService.unPublishPage(id);
		page.setCategoryName(CaUtil.getTemplateTypeName(page.getCategoryId()));
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_UNPUBLISH_SUCCESSFUL), ObjectMapperUtils.map(page, PageResponse.class));
	}

	@GetMapping("/hotels/rooms")
	public List<SelectResponse> findPageToRooms(@RequestParam String language, @RequestParam String value,@RequestParam Integer type, @RequestParam Integer limit) {
		List<SelectResponse> listPageUrlResponse = new ArrayList<>();
		if(ConstantsUtil.ONE.equals(type)) {
			String languageNew = CaUtil.validateIsNotNullAndNotEmpty(language) ? language : LanguageType.SPANISH.getCode();
			List<Page> listPage = pageService.findHotelsByLanguageAndName(languageNew, value, limit);
			listPage.forEach(x -> {
				LanguagePage languagePage = x.getLanguages().stream().findFirst().get();
				listPageUrlResponse.add(SelectResponse.builder().value(x.getId()).text(languagePage.getTitle()).build());
			});
		} else {
			List<LanguagePage> languages = pageService.customFindByIdAndLanguageCode(value, language);
			String name = "";
			if(CaUtil.validateListIsNotNullOrEmpty(languages)){
				name = languages.stream().findFirst().get().getTitle();
			}
			listPageUrlResponse.add(SelectResponse.builder().value(value).text(name).build());
		}
		return listPageUrlResponse;
	}

	@GetMapping("/hotels/destinations")
	public List<PageHeaderResponse> getPageHotelsByNameHotelAndNameDestinations(@RequestParam String name){
		return  ObjectMapperUtils.mapAll(this.pageService.findPageHotelsByNameHotelAndNameDestinationsToPage(name), PageHeaderResponse.class);
	}

	@GetMapping("/hotels/name")
	public List<PageHeaderResponse> getPageHotelsByName(@RequestParam String name){
		List<PageHeader> listPageHeader = this.pageService.getPageHotelsByName(name);
		return listPageHeader.parallelStream().map(this::getPageHeaderResponse).collect(Collectors.toList());
	}

	private PageHeaderResponse getPageHeaderResponse(PageHeader pageHeader){
		return PageHeaderResponse.builder()
				.id(pageHeader.getId())
				.name(pageHeader.getDestinationName() + ConstantsUtil.Page.SEPARATOR_GUION_WITH_SPACES + pageHeader.getName())
				.build();
	}

	@GetMapping("/select")
	public List<SelectResponse> getAllPages(@RequestParam String language, @RequestParam String value, @RequestParam Integer type, @RequestParam Integer limit){
		List<SelectResponse> listPageUrlResponse = new ArrayList<>();

		if(ConstantsUtil.ONE.equals(type)){
			List<Page> listPage = pageService.findPageByLanguageAndPathAndExternalNot(language, value, limit);
			listPage.forEach(x ->
					listPageUrlResponse.add(SelectResponse.builder().text(x.getLanguages().stream().findFirst().get().getPath()).value(x.getId()).build())
			);
		} else {
			List<LanguagePage> languages = pageService.findPageByIdAndLanguage(value, language);
			String name = "";
			if(CaUtil.validateListIsNotNullOrEmpty(languages)){
				name = languages.stream().findFirst().get().getPath();
			}
			listPageUrlResponse.add(SelectResponse.builder().value(value).text(name).build());
		}

		return listPageUrlResponse;
	}

	@GetMapping("/select/with/external")
	public List<SelectResponse> getAllPagesWithExternal(@RequestParam String language, @RequestParam String value, @RequestParam Integer type, @RequestParam Integer limit){
		List<SelectResponse> listPageUrlResponse = new ArrayList<>();

		if(ConstantsUtil.ONE.equals(type)){
			List<Page> listPage = pageService.findPageByLanguageAndPathAndExternal(language, value, limit);
			listPage.forEach(x ->
					listPageUrlResponse.add(SelectResponse.builder().text(x.getLanguages().stream().findFirst().get().getPath()).value(x.getId()).build())
			);
		} else {
			List<LanguagePage> languages = pageService.findPageByIdAndLanguage(value, language);
			String name = "";
			if(CaUtil.validateListIsNotNullOrEmpty(languages)){
				name = languages.stream().findFirst().get().getPath();
			}
			listPageUrlResponse.add(SelectResponse.builder().value(value).text(name).build());
		}

		return listPageUrlResponse;
	}

	@PutMapping("/header")
	public GenericResponse updatePageHeaderInEdition(@RequestBody PageRequest pageRequest) throws CaBusinessException{
		PageHeader pageHeader = ObjectMapperUtils.map(pageRequest, PageHeader.class);
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_SAVE_SUCCESSFUL), pageService.updatePageHeaderInEdition(pageHeader));
	}

	@GetMapping("/{destination}/{descriptor}/hotels")
	public List<SelectResponse> findHotelsByDestinationCustomized(@PathVariable String destination,@PathVariable String descriptor, @RequestParam String language){
		List<Page> listPage = pageService.findHotelsByDestinationAndDescriptorCustomized(destination, descriptor);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listPage.forEach(x->
				x.getLanguages().forEach(k -> {
					if (k.getLanguage().equals(language)) {
						listSelectResponse.add(SelectResponse.builder().text(k.getTitle()).value(x.getId()).build());
					}
				})
		);
		return listSelectResponse;
	}

	@GetMapping("/{destination}/{descriptor}/restaurants")
	public List<SelectResponse> findRestaurantsByDestinationCustomized(@PathVariable String destination,@PathVariable String descriptor, @RequestParam String language){
		List<Page> listPage = pageService.findRestaurantsByDestinationAndDescriptorCustomized(destination, descriptor);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listPage.forEach(x->
				x.getLanguages().forEach(k -> {
					if (k.getLanguage().equals(language)) {
						listSelectResponse.add(SelectResponse.builder().text(k.getTitle()).value(x.getId()).build());
					}
				})
		);
		return listSelectResponse;
	}

	@GetMapping("/promotions/detail")
	public List<SelectResponse> getPromotionsDetail(@RequestParam String language){
		List<Page> pages = this.pageService.getPromotionsDetail(language);
		if(CaUtil.validateListIsNotNullOrEmpty(pages)){
			List<SelectResponse> selectResponses = new ArrayList<>();
			pages.forEach(page -> {
				selectResponses.add(SelectResponse.builder()
						.value(page.getId())
						.text(CaUtil.validateListIsNotNullOrEmpty(page.getLanguages())
								? page.getLanguages().get(ConstantsUtil.ZERO).getTitle()
								: ConstantsUtil.EMPTY)
						.build());
			});
			return selectResponses;
		} else {
			return Collections.emptyList();
		}
	}

	@GetMapping("/promotions/{promotion}/detail")
	public List<SelectResponse> getHotelRestaurantByPromotionAndDescriptor(@PathVariable String promotion,
														   @RequestParam String category,
														   @RequestParam String language) throws CaBusinessException{

		List<SelectResponse> selectResponses = new ArrayList<>();
		Page page = this.pageService.getPromotionByIdAndLanguage(promotion, language);
		if(CaUtil.validateIsNotNullAndNotEmpty(page)){
			LanguagePage languagePage = page.getLanguages().stream()
					.filter(x -> language.equals(x.getLanguage())).findFirst().orElse(null);
			if(CaUtil.validateIsNotNullAndNotEmpty(language)){
				List<String> idsPage = getHotelRestPromotionFromData(languagePage.getData(), category);
				if(CaUtil.validateListIsNotNullOrEmpty(idsPage)){
					List<Page> pages = this.pageService.customFindPageByIdsAndLanguageAndStateNot(idsPage, language);
					Map<String, Page> pageMap = pages.stream().collect(Collectors.toMap(Page::getId, Function.identity()));
					idsPage.forEach( x -> {
						Page pageGet = pageMap.get(x);
						if(Objects.nonNull(pageGet)) {
							selectResponses.add(SelectResponse.builder()
									.value(pageGet.getId())
									.text(CaUtil.validateListIsNotNullOrEmpty(pageGet.getLanguages())
											? pageGet.getLanguages().get(ConstantsUtil.ZERO).getTitle() : ConstantsUtil.EMPTY)
									.build());
						}
					});
				}
			}
		}
		return selectResponses;
	}

	private List<String> getHotelRestPromotionFromData(Object data, String category){
		PromotionDataDto promotionDataDto = ObjectMapperUtils.map(data, PromotionDataDto.class);
		List<String> ids = new ArrayList<>();
		if(CaUtil.validateIsNotNullAndNotEmpty(promotionDataDto) && CaUtil.validateIsNotNullAndNotEmpty(promotionDataDto.getCaHotelRestPromotion()) &&
			CaUtil.validateListIsNotNullOrEmpty(promotionDataDto.getCaHotelRestPromotion().getItems())){

			List<HotelRestPromotionItemDto> hotelRestPromotionItems = null;

			if(ConstantsUtil.Page.PROMOTION_CAMPANIAS.equals(category)){
				promotionDataDto.getCaHotelRestPromotion().getItems().forEach(page -> {
					if(ConstantsUtil.Page.PROMOTION_HOTELES.equals(page.getType()))
						ids.add(page.getHotel());
					else if(ConstantsUtil.Page.PROMOTION_RESTAURANTES.equals(page.getType()))
						ids.add(page.getRestaurant());
				});
			} else if (ConstantsUtil.Page.PROMOTION_HOTELES.equals(category)) {
				hotelRestPromotionItems = promotionDataDto.getCaHotelRestPromotion().getItems().stream()
						.filter(x -> ConstantsUtil.Page.PROMOTION_HOTELES.equals(x.getType())).collect(Collectors.toList());
				if(Objects.nonNull(hotelRestPromotionItems) && !hotelRestPromotionItems.isEmpty()){
					hotelRestPromotionItems.forEach(hotel -> ids.add(hotel.getHotel()));
				}
			} else if(ConstantsUtil.Page.PROMOTION_RESTAURANTES.equals(category)){
				hotelRestPromotionItems = promotionDataDto.getCaHotelRestPromotion().getItems().stream()
						.filter(x-> ConstantsUtil.Page.PROMOTION_RESTAURANTES.equals(x.getType())).collect(Collectors.toList());
				if(Objects.nonNull(hotelRestPromotionItems) && !hotelRestPromotionItems.isEmpty()){
					hotelRestPromotionItems.forEach(restaurant -> ids.add(restaurant.getRestaurant()));
				}
			}
		}
		return ids;
	}

	@GetMapping("/{promotionType}/promotions")
	public List<SelectResponse> getPromotionsByPromotionType(@PathVariable String promotionType,
															 @RequestParam String language) {
		List<Page> pages = this.pageService.getPromotionsByPromotionType(promotionType, language);
		List<SelectResponse> selectResponses = new ArrayList<>();
		if(Objects.nonNull(pages) && !pages.isEmpty()){
			pages.forEach( page -> {
				LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
				if(Objects.nonNull(languagePage)){
					selectResponses.add(SelectResponse.builder().value(page.getId()).text(languagePage.getTitle()).build());
				}
			});
		}
		return selectResponses;
	}

	@GetMapping("/autocomplete/hotels")
	public List<SelectResponse> getPageHotelsByNameHotel(@RequestParam String value){
		List<PageHeader> listPageHeader = this.pageService.findPageAutocompleteByNameAndCategoryToPage(value, TemplateType.HOTELS_DETAIL.getCode());
		if(Objects.nonNull(listPageHeader) && !listPageHeader.isEmpty()){
			List<SelectResponse> selectResponses = new ArrayList<>();
			listPageHeader.forEach(x ->
					selectResponses.add(
							SelectResponse.builder().value(x.getId()).text(x.getName()).build()
					)
			);
			return selectResponses;
		}
		return Collections.emptyList();
	}

	@GetMapping("/home/select")
	public List<SelectResponse> findAllPageHomeSelect(){
		List<PageHeader> listPageHeader = this.pageService.findAllPageHome();
		if(Objects.nonNull(listPageHeader) && !listPageHeader.isEmpty()){
			List<SelectResponse> selectResponses = new ArrayList<>();
			listPageHeader.forEach(x -> selectResponses.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build()));
			return selectResponses;
		}
		return Collections.emptyList();
	}

	@GetMapping("/promotions/all/select")
	public List<SelectResponse> findAllPagePromotionAllSelect(){
		List<PageHeader> listPageHeader = this.pageService.findAllPagePromotionAll();
		if(Objects.nonNull(listPageHeader) && !listPageHeader.isEmpty()){
			List<SelectResponse> selectResponses = new ArrayList<>();
			listPageHeader.forEach(x -> selectResponses.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build()));
			return selectResponses;
		}
		return Collections.emptyList();
	}

	@GetMapping("/select/{categoryId}")
	public List<SelectResponse> findPageByCategorySelect(@PathVariable String categoryId){
		List<PageHeader> listPageHeader = this.pageService.findAllPageByCategoryId(categoryId);
		if(Objects.nonNull(listPageHeader) && !listPageHeader.isEmpty()){
			List<SelectResponse> selectResponses = new ArrayList<>();
			listPageHeader.forEach(x -> selectResponses.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build()));
			return selectResponses;
		}
		return Collections.emptyList();
	}

	@PutMapping("/setFree/{id}")
	public GenericResponse updatePageHeaderInEdition(@PathVariable String id) throws CaBusinessException{
		return CaUtil.getResponseGeneric(messageProperties.getMessage(
				PropertiesConstants.MESSAGE_PAGE_LIBERATE_SUCCESSFUL), pageService.liberatePage(id)
		);
	}

}
