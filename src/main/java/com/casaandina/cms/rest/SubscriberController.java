package com.casaandina.cms.rest;

import com.casaandina.cms.dto.SubscriberTray;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Country;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.Parameter;
import com.casaandina.cms.model.Subscriber;
import com.casaandina.cms.rest.request.SubscriberContactRequest;
import com.casaandina.cms.rest.request.SubscriberPersonLegalRequest;
import com.casaandina.cms.rest.request.SubscriberPersonRequest;
import com.casaandina.cms.rest.request.SubscriberTrayRequest;
import com.casaandina.cms.rest.response.*;
import com.casaandina.cms.service.SubscriberService;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/subscriber")
@SuppressWarnings("rawtypes")
public class SubscriberController {
	
	private final SubscriberService subscriberService;
	private final MessageProperties messageProperties;
	
	@Autowired
	public SubscriberController(SubscriberService subscriberService, MessageProperties messageProperties) {
		this.subscriberService = subscriberService;
		this.messageProperties = messageProperties;
	}
	
	@GetMapping("/motives")
	public List<SubscriberParameterResponse> findMotiveByStatusAndLanguages(@RequestParam("language") String language) throws CaBusinessException {
		List<Parameter> listMotives = subscriberService.findMotiveByStatusAndLanguage(language);
		List<SubscriberParameterResponse> listSubscriberParameter = new ArrayList<>();
		listMotives.forEach(x -> 
			listSubscriberParameter.add(SubscriberParameterResponse.builder().id(x.getId()).description((String) x.getIndicator()).build())
		);
		return listSubscriberParameter;
	}
	
	@GetMapping("/genders")
	public List<SubscriberParameterResponse> findGenderByStatusAndLanguages(@RequestParam("language") String language) throws CaBusinessException {
		List<Parameter> listMotives = subscriberService.findGenderByStatusAndLanguage(language);
		List<SubscriberParameterResponse> listSubscriberParameter = new ArrayList<>();
		listMotives.forEach(x -> 
			listSubscriberParameter.add(SubscriberParameterResponse.builder().id(x.getId()).description((String) x.getIndicator()).build())
		);
		return listSubscriberParameter;
	}
	
	@GetMapping("/all")
	public List<Subscriber> getActiveSubscriptionsList() throws CaBusinessException {
		return this.subscriberService.getActiveSubscriptionsList();
	}
	
	@PostMapping("/contact")
	public GenericResponse saveSubscriberContact(@RequestBody SubscriberContactRequest subscriberContactRequest) throws CaBusinessException {
		Subscriber subscriber = ObjectMapperUtils.map(subscriberContactRequest, Subscriber.class);
		subscriber.setCountry(Country.builder().id(subscriberContactRequest.getCountryId()).build());
		subscriber.setDestination(Page.builder().id(subscriberContactRequest.getPageDestinationId()).build());
		
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_SUBSCRIBER_CONTACT_SAVE_SUCCESFUL), ObjectMapperUtils.map(subscriberService.saveSubscriberContact(subscriber), SubscriberResponse.class));
	}

	@PostMapping("/person")
	public GenericResponse subscribePerson(@RequestBody final SubscriberPersonRequest subscriberPersonRequest) throws CaBusinessException {
		Subscriber subscriber = ObjectMapperUtils.map(subscriberPersonRequest, Subscriber.class);
		subscriber.setCountry(Country.builder().id(subscriberPersonRequest.getCountryId()).build());
		SubscriberResponse subscriberResponse = ObjectMapperUtils.map(this.subscriberService.subscribePerson(subscriber), SubscriberResponse.class);
		this.subscriberService.sendMailPerson(subscriber, ParameterType.NATURAL_PERSON_MAIL.getCode());
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_SUBSCRIBER_CONTACT_SAVE_SUCCESFUL), subscriberResponse);
	}
	
	@PostMapping("/legal-person")
	public GenericResponse subscribeLegalPerson(@RequestBody final SubscriberPersonLegalRequest subscriberPersonLegalRequest) throws CaBusinessException {
		Subscriber subscriber = ObjectMapperUtils.map(subscriberPersonLegalRequest, Subscriber.class);
		subscriber.setCountry(Country.builder().id(subscriberPersonLegalRequest.getCountryId()).build());
		SubscriberResponse subscriberResponse = ObjectMapperUtils.map(this.subscriberService.subscribeLegalPerson(subscriber), SubscriberResponse.class);
		this.subscriberService.sendMailPerson(subscriber, ParameterType.LEGAL_PERSON_MAIL.getCode());
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_SUBSCRIBER_CONTACT_SAVE_SUCCESFUL), subscriberResponse) ;
	}

	@GetMapping("/contacts")
	public List<SubscriberContactResponse> findContactByStatus() {
		return  ObjectMapperUtils.mapAll(subscriberService.findContactByStatus(), SubscriberContactResponse.class) ;
	}

	@GetMapping("/list")
	public com.casaandina.cms.util.pagination.Page<SubscriberTrayResponse> getSubscriberTray(SubscriberTrayRequest subscriberTrayRequest){
		SubscriberTray subscriberTray = ObjectMapperUtils.map(subscriberTrayRequest, SubscriberTray.class);
		org.springframework.data.domain.Page<Subscriber> subscriberPage = this.subscriberService.findSubscriberByFilters(subscriberTray,
				PageUtil.getPageable(subscriberTrayRequest.getPage(), subscriberTrayRequest.getSize()));

		List<Subscriber> subscribers = subscriberPage.getContent();
		return PageUtil.pageToResponse(subscriberPage, getSubscriberTrayResponse(subscribers));
	}

	private List<SubscriberTrayResponse> getSubscriberTrayResponse(List<Subscriber> subscribers){
		if(Objects.nonNull(subscribers) && !subscribers.isEmpty()){
			List<SubscriberTrayResponse> subscriberTrayResponses = new ArrayList<>();
			subscribers.forEach(subscriber -> {
				SubscriberType subscriberType = SubscriberType.get(subscriber.getTypeSuscriber());
				String name;

				if(Objects.nonNull(subscriber.getRuc()) && Objects.nonNull(subscriber.getSurname())){
					name = (Objects.nonNull(subscriber.getName()) ? subscriber.getName() : ConstantsUtil.EMPTY) +
							ConstantsUtil.SPACE + subscriber.getSurname();
				} else {
					name = subscriber.getName();
				}

				subscriberTrayResponses.add(SubscriberTrayResponse.builder()
						.id(subscriber.getId())
						.name(name)
						.phone(subscriber.getPhone())
						.registerDate(subscriber.getRegisterDate())
						.typeSuscriber(Objects.nonNull(subscriberType) ? subscriberType.getValue() : ConstantsUtil.EMPTY)
						.countryName(Objects.nonNull(subscriber.getCountry()) ? subscriber.getCountry().getName() : ConstantsUtil.EMPTY)
						.build()
				);
			});
			return subscriberTrayResponses;
		}

		return Collections.emptyList();
	}

	@PostMapping("/downloads")
	public ResponseEntity downloadExcel(@RequestBody SubscriberTrayRequest subscriberTrayRequest){
		SubscriberTray subscriberTray = ObjectMapperUtils.map(subscriberTrayRequest, SubscriberTray.class);
		return this.subscriberService.downloadExcelByFilters(subscriberTray);
	}

	@GetMapping("/downloads")
	public GenericResponse downloadExcelGet(SubscriberTrayRequest subscriberTrayRequest){
		SubscriberTray subscriberTray = ObjectMapperUtils.map(subscriberTrayRequest, SubscriberTray.class);
		return GenericResponse
			.builder()
			.data(this.subscriberService.bytesExcelByFilters(subscriberTray))
			.build();
	}

	@DeleteMapping("/{id}")
	public GenericResponse deleteSubscriber(@PathVariable String id)throws CaBusinessException {
		this.subscriberService.deleteSubscriber(id);
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_VALID_SUBSCRIBER_DELETE_SUCCESFUL));
	}

}
