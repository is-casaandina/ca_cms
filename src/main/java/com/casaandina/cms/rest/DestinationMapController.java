package com.casaandina.cms.rest;

import com.casaandina.cms.model.DestinationMap;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.DestinationMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.casaandina.cms.util.CaUtil.validateListIsNotNullOrEmpty;

@RestController
@RequestMapping("/api/destination/maps")
@RequiredArgsConstructor
public class DestinationMapController {

    private final DestinationMapService destinationMapService;

    @GetMapping
    public List<SelectResponse> getDestinationMap(@RequestParam String countryId){
        List<DestinationMap> destinationMaps = this.destinationMapService.findByCountryIdAndState(countryId);
        if(validateListIsNotNullOrEmpty(destinationMaps)){
            List<SelectResponse> selectResponses = new ArrayList<>();
            destinationMaps.forEach(x ->
                selectResponses.add(SelectResponse.builder()
                        .value(x.getId())
                        .text(x.getName())
                        .build())
            );
            return selectResponses.stream()
                    .sorted(Comparator.comparing(SelectResponse::getText))
                    .collect(Collectors.toList());
        } else
            return Collections.emptyList();
    }

}
