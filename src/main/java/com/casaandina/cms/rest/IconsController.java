package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.IconsResponse;
import com.casaandina.cms.service.IconsService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/icons")
public class IconsController {
	
	private final IconsService iconsService;
	private final MessageProperties messageProperties;
	
	@Autowired
	public IconsController(IconsService iconsService, MessageProperties messageProperties) {
		this.iconsService = iconsService;
		this.messageProperties = messageProperties;
	}
	
	@PostMapping
	public GenericResponse uploadIconsFilesFromZip(@RequestPart(value = "file") MultipartFile fileZip) throws CaBusinessException {
		iconsService.uploadIconsFilesFromZip(fileZip);
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_ICONS_SVG_UPLOAD_SUCCESFUL), null);
	}
	
	@GetMapping
	public List<IconsResponse> findAllIcons() {
		return ObjectMapperUtils.mapAll(iconsService.findAllIcons(), IconsResponse.class);
	}
}
