package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Assistance;
import com.casaandina.cms.rest.request.AssistanceRequest;
import com.casaandina.cms.rest.response.AssistanceResponse;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.AssistanceService;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/services")
public class AssistanceController {

    private final AssistanceService assistanceService;
    private final MessageProperties messageProperties;

    @Autowired
    public AssistanceController(AssistanceService assistanceService, MessageProperties messageProperties) {
        this.assistanceService = assistanceService;
        this.messageProperties = messageProperties;
    }

    @PostMapping
    public GenericResponse createAssistance(@RequestBody AssistanceRequest assistanceRequest) throws CaBusinessException {
        Assistance assistance = ObjectMapperUtils.map(assistanceRequest, Assistance.class);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_SAVE_SUCCESFUL), ObjectMapperUtils.map(assistanceService.createAssistance(assistance), AssistanceResponse.class));
    }

    @PutMapping("/{id}")
    public GenericResponse updateAssistance(@PathVariable("id") String assistanceId, @RequestBody AssistanceRequest assistanceRequest) throws CaBusinessException {
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_UPDATE_SUCCESFUL), ObjectMapperUtils.map(assistanceService.updateAssistance(assistanceId, ObjectMapperUtils.map(assistanceRequest, Assistance.class)), AssistanceResponse.class));
    }

    @DeleteMapping("{id}")
    public MessageResponse deleteAssistance(@PathVariable("id") String assistanceId) throws CaBusinessException {
        assistanceService.deleteAssistance(assistanceId);
        return CaUtil.getMessgeResponse(messageProperties.getMessage(PropertiesConstants.MESSAGE_ASSISTANCE_DELETE_SUCCESFUL));
    }

    @GetMapping("/paginated/{typeAssistance}")
    public com.casaandina.cms.util.pagination.Page<AssistanceResponse> getAssistancePaginated(@PathVariable Integer typeAssistance,@RequestParam(required = false) String title, Pageable pageable){
        Page<Assistance> pageAssistance = assistanceService.getPageAssistanceByType(typeAssistance, title, PageUtil.getPageable(pageable.getPageNumber(), pageable.getPageSize()));
        List<Assistance> listAssistance = pageAssistance
                .getContent()
                .stream()
                .map(this::buildAssistanceTitle)
                .collect(Collectors.toList());
        List<AssistanceResponse> listAssistanceResponse = ObjectMapperUtils.mapAll(listAssistance, AssistanceResponse.class);
        return PageUtil.pageToResponse(pageAssistance, listAssistanceResponse);
    }

    @GetMapping("/hotels/select")
    public List<SelectResponse> getAssistanceHotelsByLanguage(@RequestParam String language){
        List<Assistance> listAssistance = assistanceService.getListAssistanceByType(AssistanceType.HOTELS.getCode());
        return getAssistanceSelectResponse(listAssistance, language);
    }

    @GetMapping("/bedrooms/select")
    public List<SelectResponse> getAssistanceBedroomsByLanguage(@RequestParam String language){
        List<Assistance> listAssistance = assistanceService.getListAssistanceByType(AssistanceType.BEDROOMS.getCode());
        return getAssistanceSelectResponse(listAssistance, language);
    }

    private List<SelectResponse> getAssistanceSelectResponse(List<Assistance> listAssistance, String language){
        List<SelectResponse> listSelectResponse = new ArrayList<>();
        listAssistance.forEach(x->{
            x.getAssistanceTitles().forEach(k->{
                if(language.equals(k.getLanguage())){
                    listSelectResponse.add(SelectResponse.builder().icon(x.getIconClass()).value(x.getId()).text(k.getTitle()).build());
                }
            });
        });
        return listSelectResponse.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
    }

    private Assistance buildAssistanceTitle(Assistance assistance){
        assistance.getAssistanceTitles().forEach(x->{
            if(LanguageType.ENGLISH.getCode().equals(x.getLanguage()))
                assistance.setEnglishTitle(x.getTitle());
            if(LanguageType.SPANISH.getCode().equals(x.getLanguage()))
                assistance.setSpanishTitle(x.getTitle());
            if(LanguageType.PORTUGUESE.getCode().equals(x.getLanguage()))
                assistance.setPortugueseTitle(x.getTitle());
        });
        return assistance;
    }
}
