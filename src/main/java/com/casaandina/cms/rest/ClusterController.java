package com.casaandina.cms.rest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.casaandina.cms.rest.response.SelectResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Cluster;
import com.casaandina.cms.rest.request.ClusterRequest;
import com.casaandina.cms.rest.response.ClusterResponse;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.service.ClusterService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;

@RestController
@RequestMapping("/api/clusters")
public class ClusterController {
	
	private final ClusterService clusterService;
	
	@Autowired
	public ClusterController(ClusterService clusterService) {
		this.clusterService = clusterService;
	}
	
	@PostMapping
	public GenericResponse<ClusterResponse> saveCluster(@RequestBody final ClusterRequest clusterRequest)
			throws CaBusinessException {
		GenericResponse<ClusterResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(ObjectMapperUtils.map(this.clusterService.saveCluster(
				ObjectMapperUtils.map(clusterRequest, Cluster.class)), ClusterResponse.class));
		return genericResponse;
	}
	
	@GetMapping("/{id}")
	public ClusterResponse getClusterById(@PathVariable("id") final String clusterId)throws CaRequiredException {
		return ObjectMapperUtils.map(this.clusterService.getClusterById(clusterId), ClusterResponse.class);
	}
	
	@PutMapping("/{id}")
	public GenericResponse<ClusterResponse> updateCluster(@PathVariable("id") final String clusterId,
			@RequestBody final ClusterRequest clusterRequest) throws CaBusinessException, CaRequiredException {
		GenericResponse<ClusterResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_UPDATE);
		genericResponse.setData(ObjectMapperUtils.map(this.clusterService.updateCluster(
				clusterId, ObjectMapperUtils.map(clusterRequest, Cluster.class)), ClusterResponse.class));
		return genericResponse;
	}
	
	@DeleteMapping("/{id}")
	public MessageResponse deleteCluster(@PathVariable("id") final String clusterId) throws CaRequiredException {
		this.clusterService.deleteCluster(clusterId);
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setMessage(ConstantsUtil.SUCCESSFUL_DELETE);
		return messageResponse;
	}
	
	@GetMapping
	public List<ClusterResponse> getAllClusters() throws CaBusinessException {
		return ObjectMapperUtils.mapAll(this.clusterService.getAllClusters(), ClusterResponse.class);
	}

	@GetMapping("/select")
	public List<SelectResponse> findAllClusterToSelect() throws  CaBusinessException {
		List<Cluster> listCluster = this.clusterService.getAllClusterCustomized();
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listCluster.forEach(x ->
				listSelectResponse.add(SelectResponse.builder().text(x.getName()).value(x.getId()).build())
		);
		return listSelectResponse;
	}

	@GetMapping("/{clusterId}/countries")
	public List<SelectResponse> findCountriesByCluesterId(@PathVariable String clusterId) throws CaRequiredException {
		Cluster cluster = this.clusterService.getClusterById(clusterId);
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		cluster.getCountries().forEach(x ->
				listSelectResponse.add(SelectResponse.builder().text(x.getName()).value(x.getId()).build())
		);
		return listSelectResponse.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
	}

}
