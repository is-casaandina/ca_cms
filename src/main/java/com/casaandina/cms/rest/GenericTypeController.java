package com.casaandina.cms.rest;

import com.casaandina.cms.model.LanguageDescription;
import com.casaandina.cms.model.PromotionType;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.PromotionTypeService;
import com.casaandina.cms.util.ConstantsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/generic/types")
public class GenericTypeController {

    private final PromotionTypeService promotionTypeService;

    @Autowired
    public GenericTypeController(PromotionTypeService promotionTypeService) {
        this.promotionTypeService = promotionTypeService;
    }

    @GetMapping("/promotions")
    public List<SelectResponse> getGenericTypeByLanguageAndTemplateType(@RequestParam String language){
        return getPromotionTypeByLanguage(language);
    }

    private List<SelectResponse> getPromotionTypeByLanguage(String language){
        List<PromotionType> listPromotionType = this.promotionTypeService.getPromotionTypeByLanguage(language);
        List<SelectResponse> listSelectResponse = new ArrayList<>();
        listPromotionType.forEach(x -> {
            LanguageDescription languageDescription = x.getLanguages().stream().findFirst().orElse(null);
            listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(languageDescription != null ? languageDescription.getDescription() : ConstantsUtil.EMPTY).build());
        });
        return listSelectResponse;
    }
}
