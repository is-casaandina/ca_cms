package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Template;
import com.casaandina.cms.rest.request.TemplateRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.TemplateResponse;
import com.casaandina.cms.service.TemplateService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/templates")
public class TemplateController {

	private final TemplateService templateService;
	private final MessageProperties messageProperties;

	@Autowired
	public TemplateController(TemplateService templateService, MessageProperties messageProperties) {
		this.templateService = templateService;
		this.messageProperties = messageProperties;
	}

	@PostMapping
	public GenericResponse saveTemplate(@RequestBody TemplateRequest templateRequest) throws CaRequiredException {
		Template template = templateService.saveTemplate(ObjectMapperUtils.map(templateRequest, Template.class));
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_SAVE_SUCCESFUL),template);
	}

	@GetMapping
	public List<TemplateResponse> listViewTemplates(@RequestParam(required = false) List<String> categories) {
		List<Template> listTemplate = templateService.listViewTemplates(categories);
		return  ObjectMapperUtils.mapAll(listTemplate, TemplateResponse.class);
	}

	@GetMapping("/{code}")
	public TemplateResponse getTemplateForEdit(@PathVariable String code) throws CaBusinessException  {
		return ObjectMapperUtils.map(templateService.getTemplateForEdit(code), TemplateResponse.class);
	}
}
