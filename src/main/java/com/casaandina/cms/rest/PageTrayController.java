package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.model.PageJoinView;
import com.casaandina.cms.rest.request.PageTrayRequest;
import com.casaandina.cms.rest.response.PageHeaderResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.PageTrayService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.TemplateType;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/pages/tray")
public class PageTrayController {

    private final PageTrayService pageTrayService;

    @Autowired
    public PageTrayController(PageTrayService pageTrayService) {
        this.pageTrayService = pageTrayService;
    }

    @GetMapping("/list")
    public com.casaandina.cms.util.pagination.Page<PageHeaderResponse> listTrayPage(PageTrayRequest pageTrayRequest) throws CaRequiredException {
        Page<PageHeader> pageHeader = pageTrayService.listPageTrayByFilters(pageTrayRequest,
                PageUtil.getPageable(pageTrayRequest.getPage(), pageTrayRequest.getSize()));
        List<PageHeaderResponse> listPageHeaderResponse = ObjectMapperUtils.mapAll(pageHeader.getContent(), PageHeaderResponse.class);
        listPageHeaderResponse.forEach(x-> {
            if(x.getCategoryId()!= null){
                TemplateType templateType = TemplateType.get(x.getCategoryId());
                x.setCategoryName(templateType.getValue());
            }
        }
        );
        return PageUtil.pageToResponse(pageHeader, listPageHeaderResponse);
    }

    @GetMapping("/hotels/list")
    public com.casaandina.cms.util.pagination.Page<PageHeaderResponse> listHotelsTrayPage(PageTrayRequest pageTrayRequest) throws CaRequiredException {
        Page<PageHeader> pageHeader = pageTrayService.listHotelsTrayByFilters(pageTrayRequest,
                PageUtil.getPageable(pageTrayRequest.getPage(), pageTrayRequest.getSize()));
        List<PageHeaderResponse> listPageHeaderResponse = ObjectMapperUtils.mapAll(pageHeader.getContent(), PageHeaderResponse.class);
        listPageHeaderResponse.forEach(x-> {
                    if(x.getCategoryId()!= null){
                        TemplateType templateType = TemplateType.get(x.getCategoryId());
                        x.setCategoryName(templateType.getValue());
                    }
                }
        );
        return PageUtil.pageToResponse(pageHeader, listPageHeaderResponse);
    }

    @GetMapping("/destinations/select")
    public List<SelectResponse> findDestinationsAll(){
        List<PageHeader> listDestinations = this.pageTrayService.findDestinationsByState();
        List<SelectResponse> selectResponses = new ArrayList<>();
        listDestinations.forEach(x ->
                selectResponses.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build())
        );
        return selectResponses;
    }

    @GetMapping("/destinations/countries")
    public List<SelectResponse> findDestinationByCountryIdList(
            @RequestParam() List<String> categories,
            @RequestParam(required = false) List<String> countries
    ){
        Sort sort = new Sort(Sort.Direction.ASC, "name");
        List<PageJoinView> listPage;

        if(countries != null && countries.size() > ConstantsUtil.ZERO) {
           listPage = this.pageTrayService.findPageByCategoriesAndCountries(categories, countries, sort);
        }else {
            listPage = this.pageTrayService.findPageByCategories(categories, sort);
        }

        List<SelectResponse> selectResponses = new ArrayList<>();
        listPage.forEach(x ->
                selectResponses.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build())
        );
        return selectResponses;
    }
}
