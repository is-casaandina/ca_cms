package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.rest.request.ImageS3Request;
import com.casaandina.cms.rest.request.PageTrayRequest;
import com.casaandina.cms.rest.response.*;
import com.casaandina.cms.service.S3Service;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/s3")
public class S3Controller {

    private final S3Service s3Service;
    private final UserAuthentication userAuthentication;
    private final MessageProperties messageProperties;

    @Autowired
    public S3Controller(S3Service s3Service, UserAuthentication userAuthentication, MessageProperties messageProperties) {
        this.s3Service = s3Service;
        this.userAuthentication = userAuthentication;
        this.messageProperties = messageProperties;
    }

    @SuppressWarnings("rawtypes")
    @PostMapping("/favicons/upload")
    public GenericResponse uploadFavicon(@RequestPart(value = "file") MultipartFile file) throws CaBusinessException {
        Integer userId = userAuthentication.getUserPrincipal().getId();
        return s3Service.uploadFavicon(file, userId);
    }

    @GetMapping("/favicons")
    public String getFavicon() {
        return s3Service.getFavicon();
    }

    @SuppressWarnings("rawtypes")
    @PostMapping("/images")
    public GenericResponse uploadImage(@RequestPart(value = "file") MultipartFile file,
                                       @RequestPart String type,
                                       @RequestParam(required = false) String title,
                                       @RequestParam(required = false) String alt,
                                       @RequestParam(required = false) String description) throws CaBusinessException {

        return s3Service.uploadImagen(file, PropertyImage.builder()
                .title(title)
                .alt(alt)
                .description(description)
                .type(type).build());
    }

    @PutMapping("/images")
    public GenericResponse updateImage(@RequestBody ImageS3Request imageS3Request) throws CaBusinessException{
        ImageS3 imageS3 = this.s3Service.updateImage(ObjectMapperUtils.map(imageS3Request, ImageS3.class));
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_IMAGES_UPDATE_SUCCESFUL),
                ObjectMapperUtils.map(imageS3, ImageS3Response.class));
    }

    @GetMapping("/images")
    public List<ImageS3Response> findAllImagesS3() {
        return s3Service.findAllImagesS3();
    }

    @GetMapping("/images/dates")
    public RangeDateResponse getRangeDatesImages() {
        return s3Service.getRangeDatesImages();
    }

    @GetMapping("/images/filters")
    public com.casaandina.cms.util.pagination.Page<ImageS3Response> getImagesS3ByFilters(@RequestParam String date,
                                                                                         @RequestParam String value,
                                                                                         @RequestParam Integer page,
                                                                                         @RequestParam Integer size) throws CaBusinessException {
        Page<ImageS3> pageImageS3 = s3Service.getImagesS3ByFilters(date, value, PageUtil.getPageable(page, size));
        List<ImageS3Response> imageS3Response = ObjectMapperUtils.mapAll(pageImageS3.getContent(), ImageS3Response.class);
        return PageUtil.pageToResponse(pageImageS3, imageS3Response);
    }

    @SuppressWarnings("rawtypes")
    @DeleteMapping("/images/{imageId}")
    public GenericResponse deleteImages(@PathVariable String imageId) throws CaBusinessException {
        return s3Service.deleteImages(imageId);
    }

    @SuppressWarnings("rawtypes")
    @GetMapping("/images/validate/{filename}")
    public GenericResponse validateExistImage(@PathVariable String filename) throws CaBusinessException {
        return s3Service.validateExistImage(filename);
    }

    @GetMapping(value = "/documents")
    public List<Document> getAllTheDocuments() {
        return this.s3Service.getAllTheDocument();
    }

    @GetMapping(value = "/document/{documentId}")
    public ResponseEntity<InputStreamResource> downloadDocument(@PathVariable("documentId") String documentId) throws CaBusinessException {
        return this.s3Service.downloadDocument(documentId);
    }

    @SuppressWarnings("rawtypes")
    @PostMapping(value = "/documents")
    public GenericResponse uploadDocument(@RequestPart MultipartFile file, @RequestParam String type) throws IOException, CaRequiredException, CaBusinessException {
        return this.s3Service.uploadDocument(file, type);
    }

    @GetMapping(value = "/filejs")
    public List<Document> getAllTheFilejs() {
        return this.s3Service.getAllTheDocument();
    }

    @GetMapping(value = "/filejs/{filejsId}")
    public ResponseEntity<InputStreamResource> downloadFilejs(@PathVariable("filejsId") String filejsId) throws CaBusinessException {
        return this.s3Service.downloadFilejs(filejsId);
    }
    @PostMapping(value = "/filejs")
    public GenericResponse uploadFileJS(@RequestPart MultipartFile file, @RequestParam String type, @RequestParam String name) throws IOException, CaRequiredException, CaBusinessException {
        return this.s3Service.uploadFilejs(file, type, name);
    }

    @SuppressWarnings("rawtypes")
    @PostMapping("/videos")
    public GenericResponse uploadVideo(@RequestPart MultipartFile file,
                                       @RequestParam String type,
                                       @RequestParam(required = false) String title,
                                       @RequestParam(required = false) String alt,
                                       @RequestParam(required = false) String description,
                                       @RequestParam(required = false) Integer numberFrames) throws CaBusinessException {
        return s3Service.uploadVideo(file, PropertyImage.builder()
                .title(title)
                .alt(alt)
                .description(description)
                .type(type)
                .numberFrame(numberFrames).build());
    }

    @GetMapping("/videos/dates")
    public RangeDateResponse getRangeDatesVideos() {
        return s3Service.getRangeDatesVideos();
    }

    @GetMapping("/videos/filters")
    public com.casaandina.cms.util.pagination.Page<VideoS3Response> getVideosS3ByFilters(@RequestParam("date") String date, @RequestParam("value") String value,
                                                                                         @RequestParam("page") Integer page, @RequestParam("size") Integer size) throws CaBusinessException {
        Page<VideoS3> pageVideoS3 = s3Service.getVideosS3ByFilters(date, value, PageUtil.getPageable(page, size));
        List<VideoS3Response> videoS3Response = ObjectMapperUtils.mapAll(pageVideoS3.getContent(), VideoS3Response.class);
        return PageUtil.pageToResponse(pageVideoS3, videoS3Response);
    }

    @SuppressWarnings("rawtypes")
    @GetMapping("/videos/validate/{filename}")
    public GenericResponse validateExistVideo(@PathVariable String filename) throws CaBusinessException {
        return s3Service.validateExistVideo(filename);
    }

    @SuppressWarnings("rawtypes")
    @DeleteMapping("/videos/{videoId}")
    public GenericResponse deleteVideo(@PathVariable String videoId) throws CaBusinessException {
        return s3Service.deleteVideo(videoId);
    }

    @GetMapping("/documents/filters")
    public com.casaandina.cms.util.pagination.Page<DocumentResponse> getDocumentsS3ByFilters(@RequestParam("date") String date, @RequestParam("value") String value,
                                                                                             @RequestParam("page") Integer page, @RequestParam("size") Integer size) throws CaBusinessException {
        Page<Document> pageDocument = s3Service.getDocumentsS3ByFilters(date, value, PageUtil.getPageable(page, size));
        List<DocumentResponse> documentResponse = ObjectMapperUtils.mapAll(pageDocument.getContent(), DocumentResponse.class);
        return PageUtil.pageToResponse(pageDocument, documentResponse);
    }

    @GetMapping("/documents/dates")
    public RangeDateResponse getRangeDatesDocuments() {
        return s3Service.getRangeDatesDocuments();
    }

    @GetMapping("/filejs/dates")
    public RangeDateResponse getRangeDatesFilejs() {
        return s3Service.getRangeDatesFilejs();
    }

    @DeleteMapping("/documents/{documentId}")
    public GenericResponse deleteDocument(@PathVariable String documentId) throws CaBusinessException {
        return s3Service.deleteDocument(documentId);
    }

    @DeleteMapping("/filejs/{filejsId}")
    public GenericResponse deleteFilejs(@PathVariable String filejsId) throws CaBusinessException {
        return s3Service.deleteFilejs(filejsId);
    }

    @PostMapping("/icons")
    public GenericResponse uploadIcon(@RequestPart(value = "file") MultipartFile file,
                                      @RequestPart String type,
                                      @RequestParam(required = false) String title,
                                      @RequestParam(required = false) String alt,
                                      @RequestParam(required = false) String description) throws CaBusinessException {

        IconS3 icons3 = s3Service.uploadIcon(file, PropertyImage.builder()
                .title(title)
                .alt(alt)
                .description(description)
                .type(type).build());
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_S3_SAVE_UPLOAD_ICON_SUCCESFUL), ObjectMapperUtils.map(icons3, IconS3Response.class));
    }

    @GetMapping("/icons/dates")
    public RangeDateResponse getRangeDatesIcons() {
        return s3Service.getRangeDatesIcons();
    }

    @GetMapping("/icons/filters")
    public com.casaandina.cms.util.pagination.Page<IconS3Response> getIconsS3ByFilters(@RequestParam("date") String date, @RequestParam("value") String value,
                                                                                       @RequestParam("page") Integer page, @RequestParam("size") Integer size) throws CaBusinessException {
        Page<IconS3> pageIconS3 = s3Service.getIconsS3ByFilters(date, value, PageUtil.getPageable(page, size));
        List<IconS3Response> iconS3Response = ObjectMapperUtils.mapAll(pageIconS3.getContent(), IconS3Response.class);
        return PageUtil.pageToResponse(pageIconS3, iconS3Response);
    }

    @GetMapping("/icons/validate/{filename}")
    public GenericResponse validateExistIcon(@PathVariable String filename) throws CaBusinessException {
        return s3Service.validateExistIcon(filename);
    }

    @DeleteMapping("/icons/{iconId}")
    public GenericResponse deleteIcons(@PathVariable String iconId) throws CaBusinessException {
        return s3Service.deleteIcon(iconId);
    }

    @PostMapping("/images/resize")
    public GenericResponse imagesResizeAll(PageTrayRequest pageTrayRequest){
        this.s3Service.resizeAllImages(pageTrayRequest);
        return CaUtil.getResponseGeneric(ConstantsUtil.SUCCESSFUL_UPDATE);
    }

    @PostMapping("/images/resize/extraSmall")
    public GenericResponse imagesResizeAllExtraSmall(PageTrayRequest pageTrayRequest){
        this.s3Service.resizeAllImagesExtraSmall(pageTrayRequest);
        return CaUtil.getResponseGeneric(ConstantsUtil.SUCCESSFUL_UPDATE);
    }

}
