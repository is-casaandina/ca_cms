package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.LanguageDescription;
import com.casaandina.cms.model.PromotionType;
import com.casaandina.cms.rest.request.PromotionTypeRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.PromotionTypeResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.PromotionTypeService;
import com.casaandina.cms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/promotions")
public class PromotionTypeController {

    private final PromotionTypeService promotionTypeService;
    private final MessageProperties messageProperties;

    @Autowired
    public PromotionTypeController(PromotionTypeService promotionTypeService, MessageProperties messageProperties) {
        this.promotionTypeService = promotionTypeService;
        this.messageProperties = messageProperties;
    }

    @GetMapping("/types/all")
    public List<PromotionTypeResponse> findAll(){
        return ObjectMapperUtils.mapAll(this.promotionTypeService.findByStateAll(), PromotionTypeResponse.class);
    }

    @GetMapping("/types")
    public List<SelectResponse> getPromotionTypeByLanguage(@RequestParam String language){
        List<PromotionType> listPromotionType = this.promotionTypeService.getPromotionTypeByLanguage(language);
        List<SelectResponse> listSelectResponse = new ArrayList<>();
        listPromotionType.forEach(x -> {
            LanguageDescription languageDescription = x.getLanguages().stream().findFirst().orElse(null);
            listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(languageDescription != null ? languageDescription.getDescription() : ConstantsUtil.EMPTY).build());
        });
        return listSelectResponse;
    }

    @PostMapping("/types")
    public GenericResponse savePromotionType(@RequestBody PromotionTypeRequest promotionTypeRequest) throws CaBusinessException {
        PromotionType promotionType = this.promotionTypeService.savePromotion(ObjectMapperUtils.map(promotionTypeRequest, PromotionType.class));
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_PROMOTION_TYPE_SAVE_SUCCESFUL), ObjectMapperUtils.map(promotionType, PromotionTypeResponse.class));
    }
}
