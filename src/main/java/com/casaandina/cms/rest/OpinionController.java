package com.casaandina.cms.rest;

import com.casaandina.cms.rest.response.OpinionResponse;
import com.casaandina.cms.service.OpinionService;
import com.casaandina.cms.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/opinions")
public class OpinionController {
	
	private final OpinionService opinionService;
	
	@Autowired
	public OpinionController(OpinionService opinionService) {
		this.opinionService = opinionService;
	}
	
	@GetMapping("/{hotelCode}")
	public List<OpinionResponse> getAllOpinionsByHotelCode(@PathVariable final String hotelCode) {
		return ObjectMapperUtils.mapAll(this.opinionService.getAllOpinionsByHotelCode(hotelCode), OpinionResponse.class);
	}

}
