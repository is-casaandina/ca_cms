package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.TemplateType;
import com.casaandina.cms.rest.request.TemplateTypeRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.rest.response.TemplateTypeResponse;
import com.casaandina.cms.service.TemplateTypeService;
import com.casaandina.cms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/categories")
public class TemplateTypeController {

    private final TemplateTypeService templateTypeService;
    private final MessageProperties messageProperties;

    @Autowired
    public TemplateTypeController(TemplateTypeService templateTypeService, MessageProperties messageProperties) {
        this.templateTypeService = templateTypeService;
        this.messageProperties = messageProperties;
    }

    @PostMapping
    public GenericResponse saveTemplateType(@RequestBody TemplateTypeRequest templateTypeRequest) throws CaBusinessException {
        TemplateType templateType = templateTypeService.saveTemplateType(ObjectMapperUtils.map(templateTypeRequest, TemplateType.class));
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_TYPE_SAVE_SUCCESFUL), ObjectMapperUtils.map(templateType, TemplateTypeResponse.class));
    }

    @GetMapping
    public List<TemplateTypeResponse> getTemplateTypeAll(){
        List<TemplateType> listTemplateType =  templateTypeService.getTemplateTypeAll();
        List<TemplateTypeResponse> responses = ObjectMapperUtils.mapAll(listTemplateType, TemplateTypeResponse.class);
        return responses.stream().sorted(Comparator.comparing(TemplateTypeResponse::getName)).collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    public MessageResponse deleteScript(@PathVariable("id") final String templateTypeId) throws CaRequiredException {
        this.templateTypeService.deleteTemplateType(templateTypeId);
        return MessageResponse.builder().message(ConstantsUtil.SUCCESSFUL_DELETE).build();
    }
}
