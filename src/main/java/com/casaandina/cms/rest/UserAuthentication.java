package com.casaandina.cms.rest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.casaandina.cms.security.UserPrincipal;

@Component
public class UserAuthentication {

	public UserAuthentication() {
		super();
	}
	
	public UserPrincipal getUserPrincipal() {
		return (UserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
	}
	
}
