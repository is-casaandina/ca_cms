package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.LanguagePageNotification;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.model.PageNotification;
import com.casaandina.cms.rest.request.PageNotificationRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.PageNotificationDetailResponse;
import com.casaandina.cms.rest.response.PageNotificationResponse;
import com.casaandina.cms.service.PageNotificationService;
import com.casaandina.cms.service.PageService;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.Page;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/page/notification")
public class PageNotificationController {

    private final PageNotificationService pageNotificationService;
    private final PageService pageService;
    private final MessageProperties messageProperties;

    @Autowired
    public PageNotificationController(PageNotificationService pageNotificationService,
                                      MessageProperties messageProperties,
                                      PageService pageService) {
        this.pageNotificationService = pageNotificationService;
        this.messageProperties = messageProperties;
        this.pageService = pageService;
    }

    @PostMapping("")
    public GenericResponse savePageNotifitacion(@RequestBody PageNotificationRequest pageNotificationRequest)
            throws CaBusinessException, CaRequiredException {
        pageNotificationService.insertPageNotification(ObjectMapperUtils.map(pageNotificationRequest, PageNotification.class));

        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.SAVE_PAGE_NOTIFICATION_SUCCESFUL),null);
    }

    @PutMapping("{id}")
    public GenericResponse updatePageNotifitacion(@PathVariable("id") Integer pageNotificationId, @RequestBody PageNotificationRequest pageNotificationRequest)
            throws CaBusinessException, CaRequiredException {
        pageNotificationService.updatePageNotification(pageNotificationId, ObjectMapperUtils.map(pageNotificationRequest, PageNotification.class));

        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.UPDATE_PAGE_NOTIFICATION_SUCCESFUL),null);
    }

    @DeleteMapping("{pageNotificationId}")
    public GenericResponse deletePageNotifitacion(@PathVariable Integer pageNotificationId)
            throws CaBusinessException, CaRequiredException{
        pageNotificationService.deletePageNotification(pageNotificationId);

        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.DELETE_PAGE_NOTIFICATION_SUCCESFUL),null);
    }

    @GetMapping("")
    public Page<PageNotificationResponse> findPaginated(@RequestParam Integer page, @RequestParam Integer size){
        Page<PageNotification> pagePageNotification = this.pageNotificationService.listPaginated(ConstantsUtil.ACTIVE, PageUtil.getPageable(page, size));
        List<PageNotificationResponse> responsePageNotification = ObjectMapperUtils.mapAll(pagePageNotification.getContent(), PageNotificationResponse.class);
        return PageUtil.pageToResponse(pagePageNotification, this.setPage(responsePageNotification));
    }



    private List<PageNotificationResponse> setPage(List<PageNotificationResponse> pageNotificationResponseList) {
        return pageNotificationResponseList.stream()
                .map(this::buildPageNotificationResponse)
                .collect(Collectors.toList());
    }

    private PageNotificationResponse buildPageNotificationResponse(PageNotificationResponse livingRoomResponse) {
        Optional<PageHeader> pageHeader = this.pageService.findPageHeaderByPageId(livingRoomResponse.getPageId());

        if (pageHeader.isPresent()) {
            livingRoomResponse.setPage(pageHeader.get().getName());
        }

        return livingRoomResponse;
    }

    @GetMapping("{pageNotificationId}")
    public PageNotificationResponse findByPageId(@PathVariable Integer pageNotificationId){
        return ObjectMapperUtils.map(pageNotificationService.getByPageNotificationId(pageNotificationId), PageNotificationResponse.class);
    }

    @GetMapping("/site/{languageCode}/{pageId}")
    public List<PageNotificationDetailResponse> findByPageId(@PathVariable String languageCode, @PathVariable String pageId){
        List<LanguagePageNotification> listPageNotification = pageNotificationService.listByPageId(languageCode, pageId);
        return ObjectMapperUtils.mapAll(listPageNotification, PageNotificationDetailResponse.class);
    }

}
