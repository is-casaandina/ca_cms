package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.ConstructorTemplate;
import com.casaandina.cms.rest.request.ConstructorTemplateRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.ConstructorTemplateResponse;
import com.casaandina.cms.service.ConstructorService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/constructortemplate")
public class ConstructorController {
    
        private final ConstructorService constructorService;
	private final MessageProperties messageProperties;

	@Autowired
	public ConstructorController(ConstructorService constructorService, MessageProperties messageProperties) {
		this.constructorService = constructorService;
		this.messageProperties = messageProperties;
	}

	@PostMapping
	public GenericResponse saveTemplate(@RequestBody ConstructorTemplateRequest constructortemplateRequest) throws CaRequiredException {
		ConstructorTemplate constructorT = constructorService.saveConstructorTemplate(ObjectMapperUtils.map(constructortemplateRequest, ConstructorTemplate.class));
		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_TEMPLATE_SAVE_SUCCESFUL),constructorT);
	}

	@GetMapping
	public List<ConstructorTemplateResponse> listViewTemplates(@RequestParam(required = false) List<String> categories) {
		List<ConstructorTemplate> listConstructorTemplate = constructorService.listViewConstructorTemplates(categories);
		return  ObjectMapperUtils.mapAll(listConstructorTemplate, ConstructorTemplateResponse.class);
	}

	@GetMapping("/{code}")
	public ConstructorTemplateResponse getTemplateForEdit(@PathVariable String code) throws CaBusinessException  {
		return ObjectMapperUtils.map(constructorService.getConstructorTemplateForEdit(code), ConstructorTemplateResponse.class);
	}
}
