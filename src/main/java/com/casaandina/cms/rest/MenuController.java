package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Menu;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.rest.request.MenuRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MenuResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.service.MenuService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.TemplateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/menus")
public class MenuController {
	
	private final MenuService menuService;
	
	@Autowired
	public MenuController(MenuService headerService) {
		this.menuService = headerService;
	}
	
	@PostMapping
	public GenericResponse<MenuResponse> saveMenu(@RequestBody final MenuRequest menuRequest) throws CaRequiredException {
		
		GenericResponse<MenuResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);

		MenuResponse menuResponse = ObjectMapperUtils.map(this.menuService.createMenu(
				ObjectMapperUtils.map(menuRequest, Menu.class)), MenuResponse.class);

		Optional<PageHeader> pageHeader = this.menuService.getPageHeaderWithCategory(menuResponse.getPageId());
		if(pageHeader.isPresent()){
			menuResponse.setExternal(TemplateType.LINK.getCode().equals(pageHeader.get().getCategoryId()));
		}

		genericResponse.setData(menuResponse);
		
		return genericResponse;
	}
	
	@GetMapping("/{id}")
	public MenuResponse getMenu(@PathVariable("id") final String menuId) throws CaRequiredException {
		return ObjectMapperUtils.map(this.menuService.readMenu(menuId), MenuResponse.class);
	}
	
	@PutMapping("/{id}")
	public GenericResponse<MenuResponse> updateMenu(@PathVariable("id") final String menuId, 
			@RequestBody final MenuRequest menuRequest) throws CaRequiredException {
		
		Menu menu = ObjectMapperUtils.map(menuRequest, Menu.class);
		menu.setId(menuId);
		
		GenericResponse<MenuResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_UPDATE);

		MenuResponse menuResponse = ObjectMapperUtils.map(this.menuService.updateMenu(menu), MenuResponse.class);

		Optional<PageHeader> pageHeader = this.menuService.getPageHeaderWithCategory(menuResponse.getPageId());
		if(pageHeader.isPresent()) {
			menuResponse.setExternal(TemplateType.LINK.getCode().equals(pageHeader.get().getCategoryId()));
		}

		genericResponse.setData(menuResponse);
		
		return genericResponse;
	}
	
	@DeleteMapping("/{id}")
	public MessageResponse deleteMenu(@PathVariable("id") final String menuId) throws CaRequiredException {
		this.menuService.deleteMenu(menuId);
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setMessage(ConstantsUtil.SUCCESSFUL_DELETE);
		return messageResponse;
	}
	
	@GetMapping
	public List<MenuResponse> getAllTheHeaders(@RequestParam("language") final String languageCode) throws CaRequiredException {
		List<MenuResponse> menuResponses = ObjectMapperUtils.mapAll(this.menuService.getAllMenusByLanguageCode(languageCode), MenuResponse.class);
		if(Objects.nonNull(menuResponses) && !menuResponses.isEmpty()){

			List<String> ids = menuResponses.stream().map(MenuResponse::getPageId).collect(Collectors.toList());
			List<PageHeader> pageHeaders = this.menuService.getPageHeaderWithCategory(ids);
			Map<String, PageHeader> pageHeaderMap = pageHeaders.stream().collect(Collectors.toMap(PageHeader::getId, Function.identity()));

			menuResponses.forEach( menu -> {
				PageHeader pageHeader = pageHeaderMap.get(menu.getPageId());
				if(Objects.nonNull(pageHeader))
					menu.setExternal(TemplateType.LINK.getCode().equals(pageHeader.getCategoryId()));
			});
			return menuResponses;
		} else {
			return Collections.emptyList();
		}
	}

}
