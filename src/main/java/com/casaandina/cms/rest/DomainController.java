package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Domain;
import com.casaandina.cms.rest.request.DomainRequest;
import com.casaandina.cms.rest.response.DomainResponse;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.DomainService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/domains")
public class DomainController {

    private final DomainService domainService;
    private final MessageProperties messageProperties;

    @Autowired
    public DomainController(DomainService domainService, MessageProperties messageProperties) {
        this.domainService = domainService;
        this.messageProperties = messageProperties;
    }

    @GetMapping
    public List<DomainResponse> findAll(){
        return ObjectMapperUtils.mapAll(this.domainService.getAllDomainsByState(), DomainResponse.class);
    }

    @GetMapping("/select")
    public List<SelectResponse> getDomainByState(){
        List<Domain> listDomain = this.domainService.getAllDomainsByState();
        List<SelectResponse> listSelectResponse = new ArrayList<>();
        listDomain.forEach(x ->
            listSelectResponse.add(SelectResponse.builder()
                    .value(x.getId())
                    .text(x.getPath())
                    .build())
        );
        return listSelectResponse;
    }

    @PostMapping
    public GenericResponse insertDomain(@RequestBody DomainRequest domainRequest) throws CaBusinessException {
        Domain domain = ObjectMapperUtils.map(domainRequest, Domain.class);
        DomainResponse domainResponse = ObjectMapperUtils.map(this.domainService.insertDomain(domain), DomainResponse.class);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_DOMAIN_INSERT_SUCCESFUL), domainResponse);
    }

    @PutMapping("/{id}")
    public GenericResponse updateDomain(@PathVariable String id, @RequestBody DomainRequest domainRequest) throws CaBusinessException {
        Domain domain = ObjectMapperUtils.map(domainRequest, Domain.class);
        domain.setId(id);
        DomainResponse domainResponse = ObjectMapperUtils.map(this.domainService.updateDomain(domain), DomainResponse.class);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_DOMAIN_UPDATE_SUCCESFUL), domainResponse);
    }

    @DeleteMapping("/{id}")
    public GenericResponse deleteDomain(@PathVariable String id) throws CaBusinessException{
        this.domainService.deleteDomain(id);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_DOMAIN_DELETE_SUCCESFUL));
    }
}
