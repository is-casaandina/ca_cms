package com.casaandina.cms.rest.response;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

@Data
public class ClusterResponse {
	
	private String id;
	
	private String name;
	
	private Boolean status;
	
	private List<CountryResponse> countries;
	
	private Integer createdBy;
	
	private LocalDateTime createdDate;
	
	private Integer updatedBy;
	
	private LocalDateTime updatedDate;

}
