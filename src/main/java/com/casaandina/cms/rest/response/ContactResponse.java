package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.ImageS3;
import com.casaandina.cms.model.LanguageDetailContact;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ContactResponse {

    private String title1;
    private String title2;
    private ImageS3 imageMain;
    private String url;
    private List<LanguageDetailContact> languageDetailContacts;
}
