package com.casaandina.cms.rest.response;

import lombok.Data;

@Data
public class SocialNetworkResponse {

	private String title;
	private String url;
	private String iconClass;

}
