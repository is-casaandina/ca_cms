package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.PropertyImage;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class IconS3Response {

    private String id;
    private String name;
    private String mimeType;
    private PropertyImage properties;
    private Integer type;
    private Integer width;
    private Integer height;
    private String url;
    private LocalDate createDate;
    private Integer status;
}
