package com.casaandina.cms.rest.response;

import lombok.Data;

@Data
public class ScriptResponse {
	
	private String id;
	
	private String name;
	
	private String header;
	
	private String body;
	
	private String footer;

}
