package com.casaandina.cms.rest.response;

import lombok.Data;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class PageNotificationResponse {
    private Integer id;
    private String pageId;
    private String page;
    private String name;
    private LocalDateTime beginDate;
    private LocalDateTime endDate;
    private List<LanguagePageNotificationResponse> languages;

    private Boolean status;
    private Integer createdBy;
    private LocalDateTime createdDate;
    private Integer updatedBy;
    private LocalDateTime updatedDate;
}
