package com.casaandina.cms.rest.response;

import lombok.Data;

@Data
public class LanguagePageNotificationResponse {
    private String language;
    private String title;
    private String detail;
}
