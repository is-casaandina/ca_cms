package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.Permission;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPermissionResponse {
    private String username;
    private String fullname;
    private String roleName;
    private String roleCode;
    private List<Permission> permissionList;
}
