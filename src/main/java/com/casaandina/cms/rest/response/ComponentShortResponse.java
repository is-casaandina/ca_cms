package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.Property;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ComponentShortResponse {
	
	private String id;
	private String name;
	private String code;
	private String description;
	private String imageUri;
	private String template;
	private Integer status;
	private Integer type;
	private List<Property> properties;

}
