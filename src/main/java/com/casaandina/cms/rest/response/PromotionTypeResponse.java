package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.LanguageDescription;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PromotionTypeResponse {

    private String id;
    private List<LanguageDescription> languages;
    private Integer state;
    private LocalDateTime createdDate;
}
