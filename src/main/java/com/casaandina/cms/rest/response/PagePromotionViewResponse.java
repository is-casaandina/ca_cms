package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PagePromotionViewResponse {

    private String id;
    private LocalDateTime modifiedDate;
    private String genericType;
    private String language;
    private String path;
    @JsonIgnore
    private String domainId;
    @JsonIgnore
    private String clusterId;
    @JsonIgnore
    private String countryId;
    private String roiback;
    private PagePromotionInfoViewResponse info;
    private PagePromotionItemViewResponse item;
}
