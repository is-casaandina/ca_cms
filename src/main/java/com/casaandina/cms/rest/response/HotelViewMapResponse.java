package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class HotelViewMapResponse {

    private String name;
    private String description;
    private ImageS3Response imageMain;
    private String urlMap;
    private String urlRoiback;
    private String revinate;
}
