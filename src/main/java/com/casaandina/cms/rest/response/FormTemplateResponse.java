package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.LanguageFormTemplate;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class FormTemplateResponse {

    private String id;

    private String code;

    private String name;

    private List<LanguageFormTemplate> languages;
}
