package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.LanguagePage;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class DomainResponse {

    private String id;
    private String path;
    private Integer state;
    private LocalDateTime createdDate;
    private Double tax;
    private String promotionAllId;
    private String language;
    private String currency;
    private Boolean hideLanguage;
    private List<LanguagePage> promotionPaths;
}
