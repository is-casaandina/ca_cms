package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PagePromotionInfoViewResponse {

    private Double price;
    private Double discount;
    private String title;
    private List<Object> imagePromotion;
    private Boolean isRestriction;
    private List<Object> imageRestriction;
    private String bodyRestriction;
    private String textButtonRestriction;
    private String footerRestriction;
    private String urlReservation;
}
