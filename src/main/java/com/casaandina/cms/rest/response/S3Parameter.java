package com.casaandina.cms.rest.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class S3Parameter {
	
	private long maxSize;
	private String directory;
	private String extensions;

}
