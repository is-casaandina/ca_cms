package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserResponse {
    private Integer id;
    private String username;
    private String password;
    private String fullname;
    private String name;
    private String lastname;
    private String email;
    private String roleCode;
    private Integer status;
    private String token;
}
