package com.casaandina.cms.rest.response;

import com.casaandina.cms.dto.InfoPromotionDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PromotionBellResponse {

    private String id;
    private String url;
    private InfoPromotionDto infoPromotion;
}
