package com.casaandina.cms.rest.response;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.casaandina.cms.model.Image;
import com.casaandina.cms.model.PropertyImage;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
@Document(collection = "videoS3")
public class VideoS3Response {

	@Id
	private String id;

	private String name;
	private String mimeType;
	private PropertyImage properties;
	private String url;
	private Image small;
	private Image medium;
	private Image large;
	private Double size;

	@JsonIgnore
	private LocalDateTime createDate;

	@JsonIgnore
	private LocalDateTime updateDate;

	public VideoS3Response() {
		super();
		LocalDateTime now = LocalDateTime.now();
		this.createDate = now;
		this.updateDate = now;
	}
}
