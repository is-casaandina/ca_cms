package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class TagResponse {
	
	private String id;
	
	private String name;
	
	private String url;

    private String urlBanner;

    private String typographyStyle;
	
	private String color;
	
	private String title;

	private String category;

	private String categoryName;

	private LocalDateTime createdDate;
	
	private LocalDateTime updatedDate;

}
