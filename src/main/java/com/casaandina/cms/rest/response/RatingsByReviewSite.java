package com.casaandina.cms.rest.response;

import lombok.Data;

@Data
public class RatingsByReviewSite {
	
	private String reviewSiteName;
	
	private Double avarageRating;
	
	private String rankingMessage;

}
