package com.casaandina.cms.rest.response;

import java.util.ArrayList;
import java.util.List;

import com.casaandina.cms.model.LanguageOption;

public class OptionResponse {

	private Integer id;
	private String name;
	private String description;
	private Integer level;
	private Integer idOptionParent;
	private String idPage;
	private Integer status;
	private String lastUpdate;
	private String username;
	private String owner;
	private Integer order;
	private List<OptionResponse> listOptionResponse = new ArrayList<>();
	private List<LanguageOption> languages;

	public OptionResponse() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getIdOptionParent() {
		return idOptionParent;
	}

	public void setIdOptionParent(Integer idOptionParent) {
		this.idOptionParent = idOptionParent;
	}

	public String getIdPage() {
		return idPage;
	}

	public void setIdPage(String idPage) {
		this.idPage = idPage;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public List<OptionResponse> getListOptionResponse() {
		return listOptionResponse;
	}

	public void setListOptionResponse(List<OptionResponse> listOptionResponse) {
		this.listOptionResponse = listOptionResponse;
	}

	public List<LanguageOption> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageOption> languages) {
		this.languages = languages;
	}

}
