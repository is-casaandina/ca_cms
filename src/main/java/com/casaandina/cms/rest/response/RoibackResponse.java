package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RoibackResponse {

	private String languageCode;
	
	@JsonProperty("url")
	private String indicator;
}
