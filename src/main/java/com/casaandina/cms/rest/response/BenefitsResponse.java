package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.DetailBenefit;
import com.casaandina.cms.model.Link;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BenefitsResponse {
    private String id;
    private String textEs;
    private Link linkEs;
    private String textEn;
    private Link linkEn;
    private String textPr;
    private Link linkPr;
    private List<DetailBenefit> detail;
}
