package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.Image;
import com.casaandina.cms.model.PropertyImage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ImageS3Response {

	private String id;
	private String name;
	private String mimeType;
	private PropertyImage properties;
	private Image small;
	private Image medium;
	private Image large;
	private LocalDateTime createDate;
	private LocalDateTime updateDate;

	private String userName;

}
