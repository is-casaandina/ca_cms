package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class TemplateResponse {

    private String id;
    private String code;
    private String name;
    private String description;
    private String imageUri;
    private String html;
    private String categoryId;
    private List<String> refComponents;
}
