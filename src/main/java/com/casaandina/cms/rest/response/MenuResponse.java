package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class MenuResponse {
	
	private String id;
	
	private String name;
	
	private String type;
	
	private String parentId;
	
	private String action;
	
	private Integer order;
	
	private String languageCode;

	private String pageId;

	private String pagePath;

	private String descriptorId;

	private Boolean sectionFooter;

	private Boolean external;

	private String phone;

	private String email;

}
