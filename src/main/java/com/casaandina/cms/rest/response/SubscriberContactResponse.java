package com.casaandina.cms.rest.response;

import lombok.Data;

@Data
public class SubscriberContactResponse {

	private String id;
	private String name;
	private String email;
	private String phone;
	private String countryId;
	private String pageDestinationId;
	private Integer gender;
	private Integer motive;
	private String address;
	private String message;
	private Boolean sendNotification;
	private Boolean sendNew;

}
