package com.casaandina.cms.rest.response;

import java.util.List;

import com.casaandina.cms.model.Permission;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleDetailResponse {

	private String roleCode;
	private String roleName;
	private List<Permission> permissionCodeList;
	private Integer status;

}
