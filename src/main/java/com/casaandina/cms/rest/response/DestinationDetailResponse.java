package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude
public class DestinationDetailResponse {

    private String descriptorId;
    private String descriptorName;
    private String descriptorColor;
    private List<HotelDestinationResponse> hotels;

}
