package com.casaandina.cms.rest.response;

import java.time.LocalDate;

public class RangeDateResponse {

	private LocalDate dateMin;
	private LocalDate dateMax;

	public RangeDateResponse() {
		super();
	}

	public RangeDateResponse(LocalDate dateMin, LocalDate dateMax) {
		super();
		this.dateMin = dateMin;
		this.dateMax = dateMax;
	}

	public LocalDate getDateMin() {
		return dateMin;
	}

	public void setDateMin(LocalDate dateMin) {
		this.dateMin = dateMin;
	}

	public LocalDate getDateMax() {
		return dateMax;
	}

	public void setDateMax(LocalDate dateMax) {
		this.dateMax = dateMax;
	}

	@Override
	public String toString() {
		return "RangeDate [dateMin=" + dateMin + ", dateMax=" + dateMax + "]";
	}

}
