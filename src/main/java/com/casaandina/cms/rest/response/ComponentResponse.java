package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.Property;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ComponentResponse {

    private String id;
    private String name;
    private String code;
    private String description;
    private Integer type;
    private String category;
    private String imageUri;
    private Integer status;
    private String ref;
    private String template;
    private List<Property> properties;
}
