package com.casaandina.cms.rest.response;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class CountryResponse {
	
	private String id;
	
	private String name;
	
	private String code;
	
	private Boolean status;
	
	private Integer createdBy;
	
	private LocalDateTime createdDate;
	
	private Integer updatedBy;
	
	private LocalDateTime updatedDate;
	
}
