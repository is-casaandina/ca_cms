package com.casaandina.cms.rest.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RatingResponse {
	
	private String ratingType;
	
	private Double avarageRating;
	
	private String rankingMessage;
	
	private List<RatingsByReviewSite> ratingsByReviewSites;

}
