package com.casaandina.cms.rest.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HotelRestPromotionResponse {

    private String id;
    private String name;
    private String nameDescriptor;
    private String colorDescriptor;
    private String url;
    private ImageS3Response imageMain;
    private Double tax;
    private String roiback;
}
