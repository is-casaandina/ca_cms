package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@JsonInclude(value = Include.NON_NULL)
@Data
public class LivingRoomResponse {
	
	private String id;
	
	private String name;
	
	private String squareMeters;
	
	private String lengthy;
	
	private String width;
	
	private String high;
	
	private Integer teather;
	
	private String school;
	
	private String banquet;
	
	private String directory;
	
	private String cocktail;
	
	private String hotelId;

	private String hotel;
	
	private String destinationId;

	private Integer order;

	private String destination;

	private List<LivingRoomSliderResponse> sliders;

	private LocalDateTime lastUpdate;

	private String category;

}
