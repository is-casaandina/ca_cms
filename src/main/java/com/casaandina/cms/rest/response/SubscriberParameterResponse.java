package com.casaandina.cms.rest.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubscriberParameterResponse {

	private String id;
	private String description;

}
