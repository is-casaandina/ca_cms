package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PageErrorResponse {

    private String id;
    private String pageId;
    private Integer errorCode;
    private Integer state;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private String owner;
    private String user;
}
