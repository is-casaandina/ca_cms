package com.casaandina.cms.rest.response;

import java.time.LocalDate;
import java.util.List;

import com.casaandina.cms.model.Subscriber;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuoteEventResponse {
	
	private PageResponse page;
	private Integer quantityAssistants;
	private LocalDate beginDate;
	private LocalDate endDate;
	private String beginHour;
	private String endHour;
	private List<String> armedTypes;
	private List<String> foodOptions;
	private List<String> audiovisualEquipments;
	private Subscriber subscriber;

}
