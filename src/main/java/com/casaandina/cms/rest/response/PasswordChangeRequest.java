package com.casaandina.cms.rest.response;

import lombok.Data;

@Data
public class PasswordChangeRequest {
    private String currentPassword;
    private String password;
    private String confirmPassword;
}
