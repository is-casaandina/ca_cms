package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PageHeaderResponse {

	private String id;
	private String name;
	private String username;
	private String fullname;
	private Integer state;
	private List<String> languages;
	private LocalDateTime createdDate;
	private LocalDateTime lastUpdate;
	private String lastUpdateString;
	private String categoryId;
	private String destinationId;
	private String categoryName;
	private Boolean publish;
	private String roiback;
	private String revinate;
	private String destinationName;
	private Boolean inEdition;
	private String countryId;
	private String userSetFree;

}
