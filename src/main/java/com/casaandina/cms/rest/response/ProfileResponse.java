package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileResponse {
    private String username;
    private String fullname;
    private String name;
    private String lastname;
    private String email;
    private Role role;
}
