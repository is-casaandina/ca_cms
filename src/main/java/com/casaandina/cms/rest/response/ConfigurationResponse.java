package com.casaandina.cms.rest.response;

import com.casaandina.cms.model.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConfigurationResponse {
    private String sitename;
    private String company;
    private String logo;
    private String icon;
    private String copyright;
    private String css;
    private List<String> languages;
    private String defaultLanguage;
    private Metadata metadata;
    private Script script;
    private SocialNetworking socialNetworking;
    private Roiback roiback;
    private RoibackPrice roibackPrice;
    private String keywords;
    private List<String> iconsPay;
    private String fontIcons;
    private String domain;
    private ContactConfigurations contact;
    private String homeId;
}
