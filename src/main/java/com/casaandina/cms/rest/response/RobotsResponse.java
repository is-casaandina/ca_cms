package com.casaandina.cms.rest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RobotsResponse {
    private String id;
    private String userAgent;
    private String type;
    private String value;
}
