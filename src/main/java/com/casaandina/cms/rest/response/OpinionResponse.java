package com.casaandina.cms.rest.response;

import com.casaandina.cms.serviceclient.response.ReviewSite;

import lombok.Data;

@Data
public class OpinionResponse {
	
	private String title;
	
	private String body;
	
	private String author;
	
	private Integer dateReview;
	
	private Double rating;
	
	private ReviewSite reviewSite;

}
