package com.casaandina.cms.rest.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class DocumentResponse {
	
	private String id;
	
	private String name;
	
	private String mimeType;
	
	private String extension;
	
	private String directory;
	
	private String url;

	private Double size;
	
	private LocalDateTime createdDate;

}
