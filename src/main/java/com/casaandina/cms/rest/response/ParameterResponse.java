package com.casaandina.cms.rest.response;

import lombok.Data;

@Data
public class ParameterResponse {
	
	private String id;
	
	private String description;
	
	private String code;
	
	private String languageCode;
	
	private Object indicator;

}
