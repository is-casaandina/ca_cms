package com.casaandina.cms.rest.response;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.casaandina.cms.model.Country;
import com.casaandina.cms.model.Page;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class SubscriberResponse {
	
	private String id;
	private String name;
	private String email;
	private String phone;
	private String surname;
	private String businessName;
	private String ruc;
	private Integer segment;
	private Country country;
	private Page destination;
	private String city;	
	private Integer gender;
	private Integer motive;
	private String address;
	private String message;
	private String typeSuscriber;
	private Boolean sendNotification;
	private Boolean sendNew;
	private Integer status;
	private LocalDate birthdate;
	private LocalDateTime registerDate;

}
