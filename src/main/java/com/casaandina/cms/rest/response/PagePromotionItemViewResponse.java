package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PagePromotionItemViewResponse{

    private String descriptor;
    private String descriptorColor;
    private String descriptorTitle;
    private String hotel;
    private String titleHotel;
    private Double price;
    private Double discount;
    private Integer days;
    private Integer nights;
    private Boolean isRestriction;
    private String restriction;
    private String bodyRestriction;
    private String footerRestriction;
    private List<Object> backgroundImage;
    private String urlReservation;
    private List<Object> imageRestriction;
    private List<Object> imageMain;


}
