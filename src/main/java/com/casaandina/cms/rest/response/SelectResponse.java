package com.casaandina.cms.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value =  JsonInclude.Include.NON_NULL)
public class SelectResponse {

    private Object value;
    private String text;
    private String icon;
    private String url;
    private Boolean principal;
}
