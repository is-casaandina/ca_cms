package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.*;
import com.casaandina.cms.rest.request.*;
import com.casaandina.cms.rest.response.*;
import com.casaandina.cms.service.ConfigurationService;
import com.casaandina.cms.service.RobotsService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/configurations")
public class ConfigurationController {
	
	private final ConfigurationService configurationService;
	private final RobotsService robotsService;
	private String patternFormat = "%1$s: %2$s\n";
	
	@Autowired
	public ConfigurationController(ConfigurationService configurationService, RobotsService robotsService) {
		this.configurationService = configurationService;
		this.robotsService = robotsService;
	}

	@GetMapping
	public ConfigurationResponse getConfiguration() {
		Configurations configuration = this.configurationService.get();

		return ObjectMapperUtils.map(configuration, ConfigurationResponse.class);
	}

	@PostMapping("/identity")
	public GenericResponse<Configurations> saveSiteIdentity(@RequestBody final SiteIdentityRequest siteIdentityRequest) throws CaBusinessException, CaRequiredException{

		Configurations configurations = this.configurationService.get();

		configurations.setSitename(siteIdentityRequest.getSitename());
		configurations.setContact(siteIdentityRequest.getContact());
		configurations.setIcon(siteIdentityRequest.getIconSite());
		Metadata metadata = configurations.getMetadata();
		if (metadata == null) metadata = new Metadata();
		metadata.setTitle(siteIdentityRequest.getSitename());
		metadata.setDescription(siteIdentityRequest.getDescription());
		metadata.setKeywords(siteIdentityRequest.getKeywords());

		GenericResponse<Configurations> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(this.configurationService.save(configurations));

		return genericResponse;
	}

	@PostMapping("/script")
	public GenericResponse<Configurations> saveScript(@RequestBody final ScriptRequest scriptRequest) throws CaBusinessException, CaRequiredException{

		Script script = Script.builder()
				.tracing(scriptRequest.getTracing())
				.header(scriptRequest.getHeader())
				.body(scriptRequest.getBody())
				.footer(scriptRequest.getFooter())
				.build();

		Configurations configurations = this.configurationService.get();
		configurations.setScript(script);

		GenericResponse<Configurations> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(this.configurationService.save(configurations));

		return genericResponse;
	}

	@PostMapping("/roiback")
	public GenericResponse<Configurations> saveServiceRoiback(@RequestBody final RoibackRequest roibackRequest) throws CaBusinessException, CaRequiredException{

		Roiback roiback = Roiback.builder()
				.urlReservationEng(roibackRequest.getUrlReservationEng())
				.urlReservationEsp(roibackRequest.getUrlReservationEsp())
				.urlReservationPor(roibackRequest.getUrlReservationPor())
				.build();

		Configurations configurations = this.configurationService.get();
		configurations.setRoiback(roiback);

		GenericResponse<Configurations> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(this.configurationService.save(configurations));

		return genericResponse;
	}

	@PostMapping("/social-networking")
	public GenericResponse saveSocialNetworking(@RequestBody final SocialNetworkingRequest socialNetworkingRequest) throws CaBusinessException, CaRequiredException{

		SocialNetworking socialNetworking = SocialNetworking.builder()
				.urlFacebook(socialNetworkingRequest.getUrlFacebook())
				.urlFlickr(socialNetworkingRequest.getUrlFlickr())
				.urlInstagram(socialNetworkingRequest.getUrlInstagram())
				.urlTwitter(socialNetworkingRequest.getUrlTwitter())
				.urlYoutube(socialNetworkingRequest.getUrlYoutube())
				.urlWhatsapp(socialNetworkingRequest.getUrlWhatsapp())
				.build();

		Configurations configurations = this.configurationService.get();
		configurations.setSocialNetworking(socialNetworking);

		GenericResponse<Configurations> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(this.configurationService.save(configurations));

		return genericResponse;
	}

	@PostMapping("/roiback/prices")
	public GenericResponse savePriceRoiback(@RequestBody final RoibackPriceRequest roibackPriceRequest) throws CaBusinessException, CaRequiredException{

		RoibackPrice roibackPrice = RoibackPrice.builder()
			.urlPricePen(roibackPriceRequest.getUrlPricePen())
			.urlPriceUsd(roibackPriceRequest.getUrlPriceUsd())
			.urlPriceBrl(roibackPriceRequest.getUrlPriceBrl())
			.build();

		Configurations configurations = this.configurationService.get();
		configurations.setRoibackPrice(roibackPrice);

		GenericResponse<Configurations> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(this.configurationService.save(configurations));

		return genericResponse;
	}

	@PostMapping("/icons-pay")
	public GenericResponse saveIconsPay(@RequestBody final List<String> iconsPay) throws CaBusinessException, CaRequiredException {
		Configurations configurations = this.configurationService.get();
		configurations.setIconsPay(iconsPay);

		GenericResponse<Configurations> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(this.configurationService.save(configurations));

		return genericResponse;
	}
	
	@GetMapping("/parameters/roiback/price")
	public List<PriceRoibackResponse> getPriceRoiback() throws CaRequiredException {
		return ObjectMapperUtils.mapAll(configurationService.getPriceRoiback(), PriceRoibackResponse.class);	
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/parameters/social/networking")
	public List<SocialNetworkResponse> getSocialNetworking() throws CaRequiredException {
		return (List<SocialNetworkResponse>) configurationService.getSocialNetworking().getIndicator();
	}
	
	@GetMapping("/parameters/roiback")
	public List<RoibackResponse> getServiceRoiback() {
		return ObjectMapperUtils.mapAll(this.configurationService.getServiceRoiback(), RoibackResponse.class);
	}

	@GetMapping("/robots")
	public List<RobotsResponse> getAllRobots() {
		return ObjectMapperUtils.mapAll(this.robotsService.getAll(), RobotsResponse.class);
	}

	@PostMapping("/robots")
	public MessageResponse addValueRobots(@RequestBody final RobotsRequest robotsRequest) {

		Robots robots = ObjectMapperUtils.map(robotsRequest, Robots.class);

		this.robotsService.addValueAgent(robots);

		return MessageResponse.builder()
				.message(ConstantsUtil.SUCCESSFUL_REGISTER)
				.build();
	}

	@DeleteMapping("/robots/{id}")
	public MessageResponse deleteRobots(@PathVariable("id") final String robotsId) {

		this.robotsService.deleteValueAgents(robotsId);

		return MessageResponse.builder()
				.message(ConstantsUtil.SUCCESSFUL_DELETE)
				.build();
	}

	@GetMapping("/robots-generate")
	public GenericResponse generateRobots() {

		//Aqui se debe generar el robots.txt
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);


		StringBuilder robotsTxt = new StringBuilder();
		robotsTxt.append("# Generator - CMS-MX\n");
		robotsTxt.append(String.format("# Date %s", sdf.format(new Date())));
		robotsTxt.append("\n");

		List<Robots> robots = this.robotsService.getAll();

		Arrays.stream(ConstantsUtil.USER_AGENTS)
				.forEach(userAgent -> {
					List<Robots> robotsAgent = robots.stream().filter(r -> r.getUserAgent().equalsIgnoreCase(userAgent)).collect(Collectors.toList());
					if (robotsAgent.stream().count() > 0) {

						robotsTxt.append(String.format("User-Agent: %s", userAgent));
						robotsTxt.append("\n");

						robotsAgent.stream()
							.sorted(Comparator.comparing(Robots::getType))
							.forEach(r -> robotsTxt.append(String.format(patternFormat, r.getType(), r.getValue())));

						robotsTxt.append("\n");
					}
				});

		List<Robots> robotsSitemap = robots.stream().filter(r -> r.getType().equalsIgnoreCase("sitemap")).collect(Collectors.toList());
		if (robotsSitemap.stream().count() > 0) {
			robotsTxt.append("\n");

			robotsSitemap.stream()
				.sorted(Comparator.comparing(Robots::getType))
				.forEach(r -> robotsTxt.append(String.format(patternFormat, r.getType(), r.getValue())));
		}

		this.configurationService.uploadTxtToS3(robotsTxt.toString());

		return GenericResponse.builder()
				.data(robotsTxt.toString().getBytes())
				.build();
	}

	@PostMapping("/robots-generate")
	public MessageResponse generateFileRobots() {

		//Aqui se debe generar el robots.txt
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);


		StringBuilder robotsTxt = new StringBuilder();
		robotsTxt.append("# Generator - CMS-MX\n");
		robotsTxt.append(String.format("# Date %s", sdf.format(new Date())));
		robotsTxt.append("\n");

		List<Robots> robots = this.robotsService.getAll();

		Arrays.stream(ConstantsUtil.USER_AGENTS)
			.forEach(userAgent -> {
				List<Robots> robotsAgent = robots.stream().filter(r -> r.getUserAgent().equalsIgnoreCase(userAgent)).collect(Collectors.toList());
				if (robotsAgent.stream().count() > 0) {

					robotsTxt.append(String.format("User-Agent: %s", userAgent));
					robotsTxt.append("\n");

					robotsAgent.stream()
							.sorted(Comparator.comparing(Robots::getType))
							.forEach(r -> robotsTxt.append(String.format(patternFormat, r.getType(), r.getValue())));

					robotsTxt.append("\n");
				}
			});

		List<Robots> robotsSitemap = robots.stream().filter(r -> r.getType().equalsIgnoreCase("sitemap")).collect(Collectors.toList());
		if (robotsSitemap.stream().count() > 0) {
			robotsTxt.append("\n");

			robotsSitemap.stream()
					.sorted(Comparator.comparing(Robots::getType))
					.forEach(r -> robotsTxt.append(String.format(patternFormat, r.getType(), r.getValue())));
		}

		this.configurationService.uploadTxtToS3(robotsTxt.toString());

		return MessageResponse.builder()
				.message(ConstantsUtil.SUCCESSFUL_GENERATE)
				.build();
	}

	@PostMapping("/sitemap-generate")
	public MessageResponse generateSitemap(@RequestBody SiteMapRequest siteMapRequest) {

		this.configurationService.siteMapGenerate(siteMapRequest.getAcceptExternalPages());

		return MessageResponse.builder()
				.message(ConstantsUtil.SUCCESSFUL_GENERATE)
				.build();
	}

	@PutMapping("/domain")
	public GenericResponse updateDomain(@RequestBody DomainRequest domainRequest) throws CaBusinessException, CaRequiredException {
		Configurations configurations = this.configurationService.get();
		configurations.setDomain(domainRequest.getPath());

		this.configurationService.save(configurations);

		return CaUtil.getResponseGeneric(ConstantsUtil.SUCCESSFUL_UPDATE, configurations);
	}

	@PutMapping("/home/{id}")
	public GenericResponse updateHome(@PathVariable String id) throws CaBusinessException, CaRequiredException {
		Configurations configurations = this.configurationService.get();
		configurations.setHomeId(id);

		this.configurationService.save(configurations);

		return CaUtil.getResponseGeneric(ConstantsUtil.SUCCESSFUL_UPDATE, configurations);
	}

}
