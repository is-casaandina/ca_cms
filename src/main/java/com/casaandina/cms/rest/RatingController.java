package com.casaandina.cms.rest;

import com.casaandina.cms.rest.response.RatingResponse;
import com.casaandina.cms.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/ratings")
public class RatingController {
	
	private RatingService ratingService;
	
	@Autowired
	public RatingController(RatingService ratingService) {
		this.ratingService = ratingService;
	}
	
	@GetMapping("/{hotelCode}")
	public RatingResponse getAllRatingByHotelCode(@PathVariable final String hotelCode) {
		return this.ratingService.getAllRatingByHotelCode(hotelCode);
	}

}
