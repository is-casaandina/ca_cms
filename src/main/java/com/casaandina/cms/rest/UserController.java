package com.casaandina.cms.rest;

import com.casaandina.cms.dto.UserTo;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Permission;
import com.casaandina.cms.model.Role;
import com.casaandina.cms.model.User;
import com.casaandina.cms.rest.request.ProfileRequest;
import com.casaandina.cms.rest.request.UserRequest;
import com.casaandina.cms.rest.request.UserSearchPaginatedRequest;
import com.casaandina.cms.rest.response.*;
import com.casaandina.cms.service.PermissionService;
import com.casaandina.cms.service.RoleService;
import com.casaandina.cms.service.UserService;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.Page;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {
	
	private final UserService userService;
	private final RoleService roleService;
	private final PermissionService permissionService;
	private final UserAuthentication userAuthentication;

	private final MessageProperties messageProperties;
	
	@Autowired
	public UserController(UserService userService, RoleService roleService, PermissionService permissionService, UserAuthentication userAuthentication, MessageProperties messageProperties) {
		this.userService = userService;
		this.roleService = roleService;
		this.permissionService = permissionService;
		this.userAuthentication = userAuthentication;
		this.messageProperties = messageProperties;
	}
	
	@GetMapping
	public GenericResponse findAll() throws CaRequiredException {
		GenericResponse rpta = new GenericResponse();
		List<User> users = userService.findAll();
		rpta.setData(users);
		return rpta;
	}
	
	@GetMapping("/paginated")
	public Page<UserRolResponse> findUserPaginated(UserSearchPaginatedRequest userSearchPaginatedRequest){

		Page<User> usersPaginated = userService.listUserPaginated(
				userSearchPaginatedRequest.getRolesCode(),
				PageUtil.getPageable(userSearchPaginatedRequest.getPage(), userSearchPaginatedRequest.getSize())
		);

		List<UserRolResponse> users = usersPaginated
				.getContent()
				.stream()
				.map(this::buildUserRolResponse)
				.collect(Collectors.toList());


		return PageUtil.pageToResponse(usersPaginated, users);
	}

	@GetMapping("/list")
	public List<UserResponse> listUsers() throws CaRequiredException {
		List<User> listUsers = this.userService.findAll();
		return ObjectMapperUtils.mapAll(listUsers, UserResponse.class);
	}
	
	@PostMapping
	public GenericResponse<UserResponse> saveUser(@RequestBody UserRequest userRequest) throws CaBusinessException, CaRequiredException {

		User user = convertUserResquestToUser(userRequest);

		// Se registra un Usuario
		User userResult = userService.saveUser(user);

		return CaUtil.getResponseGeneric(
				messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_SAVE_SUCCESS),
				convertUserToUserResponse(userResult)
				);
	}
	
	@PutMapping("/{userId}")
	public GenericResponse<UserResponse> updateUser(@PathVariable Integer userId, @RequestBody UserRequest userRequest) throws CaBusinessException, CaRequiredException {
		User user = convertUserResquestToUser(userRequest);
		user.setId(userId);

		// Se actualiza información de Usuario
		User userResult = userService.updateUser(user);

		
		return CaUtil.getResponseGeneric(
				messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_UPDATE_SUCCESS),
				convertUserToUserResponse(userResult)
				);
	}

	@GetMapping("/profile")
	public ProfileResponse getProfile() throws CaRequiredException {
		Optional<User> user = this.userService.findBydId(this.userAuthentication.getUserPrincipal().getId());

		return convertToProfileResponse(user.get());
	}

	@PutMapping("/profile")
	public GenericResponse<ProfileResponse> updateProfile(@RequestBody ProfileRequest profileRequest) throws CaBusinessException, CaRequiredException {
		User user = convertProfileResquestToUser(profileRequest);
		user.setId(this.userAuthentication.getUserPrincipal().getId());

		// Se actualiza información de Usuario
		User userResult = userService.updateProfile(user);

		return CaUtil.getResponseGeneric(
				messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_UPDATE_SUCCESS),
				convertToProfileResponse(userResult)
		);
	}
	
	@PostMapping("/{userId}/reset-password")
	public GenericResponse resetPassword(@PathVariable Integer userId, @RequestBody UserTo user) throws CaBusinessException {
		GenericResponse rpta = new GenericResponse();
		user.setId(userId);
		String message = this.userService.resetPassword(user);
		rpta.setMessage(message);
		return rpta;
	}
	
	@DeleteMapping("/{userId}")
	public GenericResponse deleteMapping(@PathVariable Integer userId) throws CaRequiredException {
		userService.updateStatus(userId);

		return GenericResponse.builder()
			.message(messageProperties.getMessage(PropertiesConstants.MESSAGE_USER_DELETE_SUCCESS))
			.build();
	}
	
	@PostMapping("/send-mail")
	public GenericResponse recoveryPassword(@RequestBody UserTo userTo) throws CaBusinessException, CaRequiredException {
		GenericResponse rpta = new GenericResponse();
		String message = userService.recoveryPassword(userTo.getEmail());
		rpta.setMessage(message);
		return rpta;
	}
	
	@PostMapping("/update-password-token")
	public GenericResponse updatePasswordWithToken(@RequestBody UserTo userTo) throws CaBusinessException {
		GenericResponse rpta = new GenericResponse();
		String message = userService.updatePasswordWithToken(userTo);
		rpta.setMessage(message);
		return rpta;
	}

	@PutMapping("/password-change")
	public MessageResponse updatePasswordChange(@RequestBody PasswordChangeRequest passwordChangeRequest) throws CaBusinessException {
		Integer userId = this.userAuthentication.getUserPrincipal().getId();

		return MessageResponse.builder()
				.message(this.userService.passwordChange(
						userId,
						passwordChangeRequest.getCurrentPassword(),
						passwordChangeRequest.getPassword(),
						passwordChangeRequest.getConfirmPassword()
				))
				.build();
	}

	@GetMapping("/permissions")
	public UserPermissionResponse getUserPermissions() throws CaBusinessException {
		Optional<User> user = this.userService.findBydId(this.userAuthentication.getUserPrincipal().getId());
		if (user.isPresent()) {
			Optional<Role> role = this.roleService.findById(user.get().getRoleCode());
			if (role.isPresent()) {
				List<Permission> permissionList = this.permissionService.findAllActive();
				List<String> permissionCodeList = role.get().getPermissionCodeList();

				return UserPermissionResponse.builder()
						.fullname(user.get().getFullname())
						.username(user.get().getUsername())
						.roleCode(role.get().getRoleCode())
						.roleName(role.get().getRoleName())
						.permissionList(permissionList.stream().filter(inPermissionRole(permissionCodeList)).collect(Collectors.toList()))
						.build();
			}
		}

		return null;
	}

	private Predicate<Permission> inPermissionRole(List<String> permissionList) {
		return p -> permissionList.contains(p.getPermissionCode());
	}

	private UserRolResponse buildUserRolResponse(User user) {
		Optional<Role> role = this.roleService.findById(user.getRoleCode());

		return UserRolResponse.builder()
			.id(user.getId())
			.username(user.getUsername())
            .fullname(user.getFullname())
            .name(user.getName())
			.lastname(user.getLastname())
			.email(user.getEmail())
			.status(user.getStatus())
			.role(role.isPresent() ? role.get() : null)
			.build();
	}

	private ProfileResponse convertToProfileResponse(User user) {
		Optional<Role> role = this.roleService.findById(user.getRoleCode());
		return ProfileResponse.builder()
				.username(user.getUsername())
                .fullname(user.getFullname())
                .name(user.getName())
				.lastname(user.getLastname())
				.email(user.getEmail())
				.role(role.isPresent() ? role.get() : null)
				.build();
	}

	private UserResponse convertUserToUserResponse(User user) {
		return UserResponse.builder()
			.id(user.getId())
			.username(user.getUsername())
            .fullname(user.getFullname())
            .name(user.getName())
			.lastname(user.getLastname())
			.password(user.getPassword())
			.email(user.getEmail())
			.roleCode(user.getRoleCode())
			.build();
	}

	// Transformación de Request a modelo User
	private User convertUserResquestToUser(UserRequest userRequest) {
		return User.builder()
			.username(userRequest.getUsername())
			.name(userRequest.getName())
			.lastname(userRequest.getLastname())
			.password(userRequest.getPassword())
			.email(userRequest.getEmail())
			.roleCode(userRequest.getRoleCode())
			.build();
	}

	private User convertProfileResquestToUser(ProfileRequest profileRequest) {
		return User.builder()
				.name(profileRequest.getName())
				.lastname(profileRequest.getLastname())
				.email(profileRequest.getEmail())
				.build();
	}
}
