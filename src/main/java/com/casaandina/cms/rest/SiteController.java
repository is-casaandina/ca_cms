package com.casaandina.cms.rest;

import com.casaandina.cms.dto.*;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.exception.CaSiteErrorException;
import com.casaandina.cms.model.Currency;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.*;
import com.casaandina.cms.rest.request.AssistanceSiteRequest;
import com.casaandina.cms.rest.request.FeaturedPromotionRequest;
import com.casaandina.cms.rest.request.LivingDestinationHotelRequest;
import com.casaandina.cms.rest.request.PaginatedRequest;
import com.casaandina.cms.rest.response.*;
import com.casaandina.cms.service.*;
import com.casaandina.cms.serviceclient.RoibackServiceClient;
import com.casaandina.cms.util.TemplateType;
import com.casaandina.cms.util.*;
import com.casaandina.cms.util.pagination.PageUtil;
import com.maxmind.geoip2.model.CountryResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/site")
@RequiredArgsConstructor
@Log4j2
public class SiteController {

    private final MenuService menuService;
    private final PageService pageService;
    private final ParameterService parameterService;
    private final AssistanceService assistanceService;
    private final DescriptorService descriptorService;
    private final MessageProperties messageProperties;
    private final SiteService siteService;
    private final GeoLocationService geoLocationService;
    private final RoibackServiceClient roibackServiceClient;

    @GetMapping("/menus")
    public List<MenuResponse> getAllTheHeaders(@RequestParam("language") final String languageCode) throws CaRequiredException {
        List<MenuResponse> menuResponses = ObjectMapperUtils.mapAll(this.menuService.getAllMenusByLanguageCode(languageCode), MenuResponse.class);
        if(Objects.nonNull(menuResponses) && !menuResponses.isEmpty()){
            Set<String> items = menuResponses.stream().map(MenuResponse::getPageId).collect(Collectors.toSet());
            if(Objects.nonNull(items) && !items.isEmpty()) {
                List<Page> pages = this.pageService.getPageWithDescriptor(items.stream().collect(Collectors.toList()), languageCode);
                if (Objects.nonNull(pages) && !pages.isEmpty()) {
                    Map<String, Page> mapPage = pages.stream().collect(Collectors.toMap(Page::getId, Function.identity()));
                    menuResponses.forEach(menu -> {
                        if (Objects.nonNull(menu.getPageId())) {
                            Page page = mapPage.get(menu.getPageId());
                            menu.setDescriptorId(Objects.nonNull(page) ? page.getDescriptorId() : null);
                            if(Objects.nonNull(page))
                                menu.setExternal(TemplateType.LINK.getCode().equals(page.getCategoryId()) ? Boolean.TRUE : Boolean.FALSE);
                            if(Objects.nonNull(page) && Objects.nonNull(page.getLanguages()) && !page.getLanguages().isEmpty()){
                                LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
                                if(Objects.nonNull(languagePage)){
                                    menu.setPagePath(languagePage.getPath());
                                }
                            }
                        }
                    });
                }
            }
        }
        return menuResponses;
    }

    @GetMapping("/pages/{pageId}")
    public PageResponse getPageById(@PathVariable String pageId){
        Page page = this.pageService.findByIdAndState(pageId);
        if(page != null)
            return ObjectMapperUtils.map(page, PageResponse.class);
        return null;
    }

    private List<LanguagePage> getListLanguagePage(String id){
        Optional<Page> pageOptional = this.pageService.customFindPathsBySEO(id);
        if(pageOptional.isPresent()){
            return pageOptional.get().getLanguages();
        }
        return Collections.emptyList();
    }

    @GetMapping("/pages")
    public PageResponse getPageByPath(HttpServletRequest request,
                                      @RequestParam(required = false) String path, //es/ofertas/paga-con-puntos&language=es
                                      @RequestParam(required = false) String id,
                                      @RequestParam(required = false) String language) throws CaSiteErrorException{
        path = CaUtil.evaluatePath(path);
        Page page = this.pageService.findByPathAndState(path,id, language);
        if(page != null){
            PageResponse pageResponse = ObjectMapperUtils.map(page, PageResponse.class);
            pageResponse.setPaths(getListLanguagePage(pageResponse.getId()));
            if(TemplateType.PROMOTIONS_DETAIL.getCode().equals(page.getCategoryId())){
                validatePromotion(pageResponse, language);
                return getPagePromotionsByDomain(request, pageResponse, language);
            } else
                return pageResponse;
        } else
            throw new CaSiteErrorException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST),
                    language, HttpStatus.NOT_FOUND.value());
    }

    private void validatePromotion(PageResponse pageResponse, String language) throws CaSiteErrorException {
        if(Objects.nonNull(pageResponse.getSecondExpiration())){
            if(CaUtil.isBeforeDate(pageResponse.getSecondExpiration()))
                throw new CaSiteErrorException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST),
                    language, HttpStatus.NOT_FOUND.value());
        } else if(Objects.nonNull(pageResponse.getFirstExpiration()) && CaUtil.isBeforeDate(pageResponse.getFirstExpiration())) {
            throw new CaSiteErrorException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST),
                    language, HttpStatus.NOT_FOUND.value());
        }
    }

    private PageResponse getPagePromotionsByDomain(HttpServletRequest request, PageResponse pageResponse, String language)
            throws CaSiteErrorException {
        HostIpCountry hostIpCountry = getHostIpCountryByRequest(request);
        if(CaUtil.validateIsNotNullAndNotEmpty(hostIpCountry.getUriHost())) {
            Domain domain = this.siteService.getDomainByPath(hostIpCountry.getUriHost());
            log.info("DOMAIN: " + hostIpCountry.getUriHost());
            if (Objects.nonNull(domain)) {
                if (CaUtil.validateIsNotNullAndNotEmpty(pageResponse.getDomainId())) {
                    if (domain.getId().equals(pageResponse.getDomainId())) {
                        return getPagePromotionsFromDomain(pageResponse, hostIpCountry, domain.getPromotionAllId(), language);
                    } else
                        throw new CaSiteErrorException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST),
                                language, HttpStatus.NOT_FOUND.value());
                } else {
                    return getPagePromotionsFromDomain(pageResponse, hostIpCountry, domain.getPromotionAllId(), language);
                }
            } else
                throw new CaSiteErrorException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST),
                        language, HttpStatus.NOT_FOUND.value());
        } else
            return pageResponse;
    }

    private PageResponse getPagePromotionsFromDomain(PageResponse pageResponse, HostIpCountry hostIpCountry, String promotionAllId, String language){
        if(!CaUtil.validateIsNotNullAndNotEmpty(pageResponse.getClusterId())){
            return pageResponse;
        } else {
            return getPagePromotionsByCountry(pageResponse, hostIpCountry, promotionAllId, language);
        }
    }

    private PageResponse getPagePromotionsByCountry(PageResponse pageResponse, HostIpCountry hostIpCountry, String promotionAllId, String language) {
        Cluster cluster = this.siteService.getClusterByClusterId(pageResponse.getClusterId());
        Country country = existCountryInCluster(cluster, hostIpCountry.getIsoCountry());
        if(Objects.nonNull(country)){
            if(CaUtil.validateIsNotNullAndNotEmpty(pageResponse.getCountryId())){
                if(pageResponse.getCountryId().equals(country.getId())){
                    return pageResponse;
                } else {
                    return returnPageResponseWithInvalidCountry(pageResponse, country.getName(), promotionAllId, language);
                }
            } else
                return pageResponse;
        } else {
            return returnPageResponseWithInvalidCountry(pageResponse, hostIpCountry.getCountryName(), promotionAllId, language);
        }
    }

    private PageResponse returnPageResponseWithInvalidCountry(PageResponse pageResponse, String countryName, String promotionAllId, String language){
        if(CaUtil.validateIsNotNullAndNotEmpty(pageResponse.getCountryId())){
            Country country = this.siteService.getCountryById(pageResponse.getCountryId());
            pageResponse.setCountryName(Objects.nonNull(country) ? country.getName() : ConstantsUtil.EMPTY);
        }
        pageResponse.setInvalidCountry(Boolean.TRUE);
        pageResponse.setCountryRequest(countryName);
        pageResponse.setPromotionAllId(getPathByPageIdAndLanguage(promotionAllId, language));
        return pageResponse;
    }

    private String getPathByPageIdAndLanguage(String id, String language){
        Optional<Page> pageOptional = this.pageService.customFindPagePathByIdAndLanguage(id, language);
        if(pageOptional.isPresent()){
            Page page = pageOptional.get();
            LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
            if(Objects.nonNull(language))
                return languagePage.getPath();
        }
        return ConstantsUtil.EMPTY;
    }

    private Country existCountryInCluster(Cluster cluster, String isoCountry){
        return cluster.getCountries().stream()
                .filter(x -> x.getCode().equals(isoCountry))
                .findFirst().orElse(null);
    }

    @GetMapping("/pages/draft")
    public PageResponse getDraftPageByIdAndLanguage(@RequestParam(required = false) String id,
                                                    @RequestParam(required = false) String language) throws CaSiteErrorException {
        DraftPage draftPage = this.pageService.getDraftPageByIdAndLanguage(id, language);
        return ObjectMapperUtils.map(draftPage, PageResponse.class);
    }

    @GetMapping("/parameters/{code}")
    public Object getStatesByCode(@PathVariable final String code) throws CaRequiredException {
        return this.parameterService.getStatesAll(code).getIndicator();
    }

    @GetMapping("/parameters/{code}/{language}")
    public Object getStatesByCodeAndLanguage(@PathVariable final String code, @PathVariable final String language) throws CaRequiredException {
        return this.parameterService.getStateByCodeAndLanguage(code, language).getIndicator();
    }

    @GetMapping("/pages/descriptors/ids")
    public List<HotelRestaurantDescriptorResponse> findHotelsRestaurantsByDescriptor(@RequestParam List<String> descriptors, @RequestParam String language){
        List<Page> listPage = pageService.findHotelsRestaurantsByDescriptorInCustomized(descriptors, language);
        return getHotelRestaurantsByPages(listPage);
    }

    @GetMapping("/pages/hotels/destinations")
    public List<PageHeaderResponse> getPageHotelsByNameHotelAndNameDestinations(@RequestParam String name){
        List<PageHeaderResponse> pageHeaderResponses = ObjectMapperUtils.mapAll(this.pageService.findPageHotelsByNameHotelAndNameDestinations(name), PageHeaderResponse.class);
        return groupByDestinationsWithHotels(pageHeaderResponses);
    }

    @GetMapping("/pages/autocomplete/hotels")
    public List<PageHeaderResponse> getPageHotelsByNameHotel(@RequestParam String name, @RequestParam(required = false) String destinationId){
        List<PageHeaderResponse> pageHeaderResponses;
        if(CaUtil.validateIsNotNullAndNotEmpty(destinationId)){
            pageHeaderResponses = ObjectMapperUtils.mapAll(this.pageService.findPageAutocompleteByNameAndCategoryAndDestinationId(name, TemplateType.HOTELS_DETAIL.getCode(), destinationId), PageHeaderResponse.class);
        } else {
            pageHeaderResponses = ObjectMapperUtils.mapAll(this.pageService.findPageAutocompleteByNameAndCategory(name, TemplateType.HOTELS_DETAIL.getCode()), PageHeaderResponse.class);
        }
        return pageHeaderResponses.stream()
                .sorted(Comparator.comparing(PageHeaderResponse::getName))
                .collect(Collectors.toList());
    }

    @GetMapping("/pages/autocomplete/destinations")
    public List<PageHeaderResponse> getPageHotelsByNameDestination(@RequestParam String name){
        List<PageHeaderResponse> pageHeaderResponses = ObjectMapperUtils.mapAll(this.pageService.findPageAutocompleteByNameAndCategory(name, TemplateType.DESTINATIONS_DETAIL.getCode()), PageHeaderResponse.class);
        return  pageHeaderResponses.stream().sorted(Comparator.comparing(PageHeaderResponse::getName)).collect(Collectors.toList());
    }

    @GetMapping("/services")
    public List<SelectResponse> getServicesByIdsAndTypeAndLanguage(AssistanceSiteRequest assistanceSiteRequest){
        List<Assistance> listAssistance = this.assistanceService.getListAssistanceByIdsAndTypeAndLanguage(assistanceSiteRequest);
        if(Objects.nonNull(listAssistance) && !listAssistance.isEmpty()){
            List<SelectResponse> selectResponses = new ArrayList<>();
            Map<String, Assistance> assistanceMap = listAssistance.stream()
                    .collect(Collectors.toMap(Assistance::getId, Function.identity()));
            assistanceSiteRequest.getItems().forEach(x -> {
                Assistance assistance = assistanceMap.get(x);
                if(Objects.nonNull(assistance))
                    selectResponses.add(buildAssistanceToSelectResponse(assistance));
            });
            return selectResponses;
        }
        return Collections.emptyList();
    }

    private SelectResponse buildAssistanceToSelectResponse(Assistance assistance){

        AssistanceTitle assistanceTitle = assistance.getAssistanceTitles().stream().findFirst().orElse(null);

        return SelectResponse.builder()
                .value(assistance.getId())
                .text(assistanceTitle != null ? assistanceTitle.getTitle() : ConstantsUtil.EMPTY)
                .icon(assistance.getIconClass())
                .build();
    }

    @GetMapping("/descriptors/{id}")
    public TagResponse readTag(@PathVariable("id") final String tagId) throws CaRequiredException {
        return ObjectMapperUtils.map(this.descriptorService.readTag(tagId), TagResponse.class);
    }

    @GetMapping("/pages/view/external")
    public RedirectView viewPageExternal(@RequestParam String id,
                                         @RequestParam String language,
                                         @RequestParam(required = false) String domain) throws CaSiteErrorException {
        try {
            Page page = this.pageService.viewPageExternal(id, language);
            LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
            if(!CaUtil.validateIsNotNullAndNotEmpty(languagePage)){
                throw new CaSiteErrorException(messageProperties.getMessage(PropertiesConstants.MESSAGE_PAGE_NOT_EXIST), language, HttpStatus.NOT_FOUND.value());
            }
            RedirectView redirectView = new RedirectView();
            String url;
            if(TemplateType.LINK.getCode().equals(page.getCategoryId())){
                url = languagePage!= null ? languagePage.getPath(): ConstantsUtil.EMPTY;
            } else {
                url = languagePage!= null ? (domain + languagePage.getPath()) : ConstantsUtil.EMPTY;
            }
            redirectView.setUrl(url);
            return redirectView;
        } catch (CaBusinessException ex) {
            throw new CaSiteErrorException(ex.getMessage(), language, ex.getStatus());
        }
    }

    @GetMapping("/pages/destinations/hotels")
    public List<DestinationSiteResponse> getDestinationsWithHotelsSite(){
        List<DestinationSiteResponse> destinationSiteResponses = new ArrayList<>();
        List<PageHeader> listDestinations = pageService.findDestinationsByStateNotPublish();
        listDestinations.forEach(x -> {
            List<Page> pagesHotels = pageService.findHotelsByDestinationAndLanguage(x.getId(), LanguageType.SPANISH.getCode());
            List<HotelSiteResponse> hotelSiteResponses = new ArrayList<>();
            pagesHotels.forEach(h -> {
                LanguagePage languagePage = h.getLanguages().stream().findFirst().orElse(null);
                hotelSiteResponses.add(HotelSiteResponse.builder()
                        .name(languagePage != null ? languagePage.getTitle() : ConstantsUtil.EMPTY)
                        .descriptorId(h.getDescriptorId())
                        .path(languagePage != null ? languagePage.getPath() : ConstantsUtil.EMPTY)
                        .roiback(h.getRoiback())
                        .build());
            });

            destinationSiteResponses.add(DestinationSiteResponse.builder()
                    .name(x.getName())
                    .roiback(x.getRoiback())
                    .hotels(hotelSiteResponses)
                    .build());
        });

        return destinationSiteResponses;
    }

    @GetMapping("/pages/hotels/ids")
    public List<SelectResponse> getHotelsByArrayIds(@RequestParam List<String> items){
        List<PageHeader> listPageHeader = pageService.getHotelsByArraysIds(items);
        List<SelectResponse> selectResponses = new ArrayList<>();
        listPageHeader.forEach(x ->
            selectResponses.add(SelectResponse.builder()
                    .value(x.getId())
                    .text(x.getName())
                    .build())
        );
        return selectResponses;
    }

    private List<HotelRestaurantDescriptorResponse> getHotelRestaurantsByPages(List<Page> listPage){

        List<HotelRestaurantDescriptorResponse> listHotelsRestaurants = new ArrayList<>();

        Map<String, List<Page>> mapPage = listPage.stream().collect(Collectors.groupingBy(Page::getDescriptorId));
        mapPage.forEach((key, value) -> {

            List<SelectResponse> selectResponses = new ArrayList<>();

            value.forEach(x -> {
                LanguagePage languagePage = x.getLanguages().stream().findFirst().orElse(null);
                selectResponses.add(SelectResponse.builder()
                        .text(languagePage != null ? languagePage.getTitle() : ConstantsUtil.EMPTY)
                        .url(languagePage != null ? languagePage.getPath() : ConstantsUtil.EMPTY)
                        .value(x.getId())
                        .build());
            });

            listHotelsRestaurants.add(HotelRestaurantDescriptorResponse.builder()
                    .descriptorId(key)
                    .pages(selectResponses.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList()))
                    .build());
        });

        return listHotelsRestaurants;
    }

    @GetMapping("/pages/roiback/reservations")
    public RoibackLowPriceResponse getPriceRoibackHotelsReservation(HttpServletRequest request,
                                                                    @RequestParam String roiback,
                                                                    @RequestParam String currencyIso,
                                                                    @RequestParam String language) throws CaRequiredException{
        RoibackLowPrice roibackLowPrice = this.siteService.getPriceByRoibackAndCurrency(roiback, currencyIso, language);

        if(roibackLowPrice != null){
            Price price;
            if(roibackLowPrice.getPrices() != null){
                price = roibackLowPrice.getPrices().stream().findFirst().orElse(null);
            } else {
                price = null;
            }

            Currency currency = this.siteService.findByCodeIsoAndStatus(currencyIso);
            Double exchangeRate;
            if(ConstantsUtil.CurrencyType.USD_CURRENCY.equals(currencyIso)){
                Currency currencyPEN = this.siteService.findByCodeIsoAndStatus(ConstantsUtil.CurrencyType.PEN_CURRENCY);
                exchangeRate = currencyPEN.getValue();
            } else {
                exchangeRate = currency.getValue();
            }

            String host = CaUtil.getHostAndServerNameFromRequest(request);
            Double tax = getTaxByDomain(host);

            Integer priceFrom = Objects.nonNull(price)  ? price.getFrom() : ConstantsUtil.ZERO;
            priceFrom = priceFrom.equals(ConstantsUtil.ZERO) ? priceFrom : getPriceFromWithTax(priceFrom, tax);

            return RoibackLowPriceResponse.builder()
                    .currency(currency.getSymbol())
                    .from(priceFrom)
                    .urlReservation(roibackLowPrice.getUrlReservation())
                    .exchangeRate(exchangeRate)
                    .build();
        } else{
            String urlReservation = this.siteService.getUrlRoibackHotel(language, roiback);
            return RoibackLowPriceResponse.builder().urlReservation(urlReservation).build();
        }
    }

    private Integer getPriceFromWithTax(Integer priceFrom, Double tax){
        tax = Objects.nonNull(tax) ? tax : ConstantsUtil.ONE.doubleValue();
        Double result = CaUtil.roundDouble(priceFrom.doubleValue() * tax, ConstantsUtil.ZERO, Boolean.TRUE);
        return result.intValue();
    }

    private List<HotelDestinationServiceResponse> getHotelServicesFromData(Object data, String language, Map<String, Assistance> assistanceMap){

        HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);

        List<HotelDestinationServiceResponse> assistanceResponses = new ArrayList<>();

        if(hotelDataDto != null && hotelDataDto.getCaServices() != null){

            hotelDataDto.getCaServices().getItems().forEach(x-> {
                Assistance assistance = assistanceMap.get(x.getService());
                if(Objects.nonNull(assistance)){
                    String title = ConstantsUtil.EMPTY;
                    if(LanguageType.SPANISH.getCode().equals(language)){
                        title = assistance.getSpanishTitle();
                    } else if(LanguageType.ENGLISH.getCode().equals(language)){
                        title = assistance.getEnglishTitle();
                    } else if(LanguageType.PORTUGUESE.getCode().equals(language)){
                        title = assistance.getPortugueseTitle();
                    }
                    assistanceResponses.add(HotelDestinationServiceResponse.builder().icon(assistance.getIconClass()).name(title).build());
                }
            });
        }
        return assistanceResponses;
    }

    private String getUrlImageFromData(Object data){
        HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);
        if(hotelDataDto!= null && hotelDataDto.getCaHotelSummary() != null &&
                hotelDataDto.getCaHotelSummary().getImageMain() !=null &&
                hotelDataDto.getCaHotelSummary().getImageMain().get(ConstantsUtil.ZERO).getLarge() != null){
            return hotelDataDto.getCaHotelSummary().getImageMain().get(ConstantsUtil.ZERO).getLarge().getImageUri();
        }
        return ConstantsUtil.EMPTY;
    }

    @GetMapping("/living-rooms/destinations")
    public List<SelectResponse> getDestinationsByLivingRooms(){

        List<LivingRoomDestination> livingRoomDestinations = this.siteService.customFindAllDestinationWithLivingRoom();

        List<SelectResponse> listDestinations = new ArrayList<>();
        livingRoomDestinations.forEach( x ->
            listDestinations.add(SelectResponse.builder()
                    .value(x.getDestinationId())
                    .text(x.getDestination())
                    .build())
        );
        return listDestinations.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
    }

    @GetMapping("/living-rooms/hotels")
    public List<SelectResponse> getHotelsToLivingRooms(@RequestParam List<String> destinations){
        List<PageHeader> listPageHeader = this.siteService.findPageHeaderHotelsByDestinationsIds(destinations);//List<PageHeader> listPageHeader = pageService.findPageHeaderHotelsByDestinationsIds(destinations);

        List<SelectResponse> listDestinations = new ArrayList<>();
        listPageHeader.forEach( x ->
            listDestinations.add(SelectResponse.builder()
                    .value(x.getId())
                    .text(x.getName())
                    .build())
        );
        return listDestinations.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
    }

    @GetMapping("/living-rooms/range")
    public RangeTheaterResponse getRangeTheater() throws CaRequiredException {
        return siteService.getRangeTheater();
    }

    @GetMapping("/living-rooms/filters")
    public com.casaandina.cms.util.pagination.Page<LivingRoomResponse> getAllTheLivingRoom(
            @RequestParam(value = "destination", required = false) final List<String> destinationsId,
            @RequestParam(value = "hotel", required = false) final List<String> hotelsId,
            @RequestParam(value = "capacity", required = false) final List<Integer> capacityRange, PaginatedRequest pageable) throws CaRequiredException {

        org.springframework.data.domain.Page<LivingRoom> rooms =  this.siteService.getAllTheLivingRooms(
                destinationsId, hotelsId, capacityRange, PageUtil.getPageable(pageable.getPage(), pageable.getSize()));
        List<LivingRoom> listRoom = rooms.getContent();
        List<LivingRoomResponse> listRoomResponse = ObjectMapperUtils.mapAll(listRoom, LivingRoomResponse.class);
        return PageUtil.pageToResponse(rooms, listRoomResponse);
    }

    @PostMapping("/pages/events/destinations/hotels/living")
    public PageEventSocialDestinationResponse getHotelsWithLivingRoomToEvents(@RequestBody LivingDestinationHotelRequest livingDestinationHotelRequest){
        LivingDestinationHotel livingDestinationHotel = ObjectMapperUtils.map(livingDestinationHotelRequest, LivingDestinationHotel.class);
        List<Page> pages = pageService.findHotelsByIdsAndLanguageCustomized(livingDestinationHotel);

        Optional<PageHeader> pageHeader = pageService.findPageHeaderByPageId(livingDestinationHotelRequest.getDestinationId());
        List<PageEventSocialResponse> pageEventSocialResponses = new ArrayList<>();

        if(Objects.nonNull(pages) && !pages.isEmpty()){
            Map<String, Page> pageMap = pages.stream().collect(Collectors.toMap(Page::getId, Function.identity()));

            Map<String, List<LivingRoom>> livingRoomByHotel = getLivingRoomByHotelDestinationCategory(livingDestinationHotel);

            Map<String, PageEventSocialResponse> pageEventSocialResponseMap = new HashMap<>();

            livingDestinationHotel.getHotelCategories().forEach(hotelCategory -> {
                if(!pageEventSocialResponseMap.containsKey(hotelCategory.getHotelId())){
                    Page page = pageMap.get(hotelCategory.getHotelId());
                    if(Objects.nonNull(page)){
                        LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
                        PageEventSocialResponse pageEventSocialResponse = new PageEventSocialResponse();
                        if (Objects.nonNull(languagePage)) {
                            pageEventSocialResponse.setId(page.getId());
                            pageEventSocialResponse.setNameHotel(languagePage.getTitle());
                            getDescriptionAndUrlImageFromData(pageEventSocialResponse,languagePage.getData());
                        }
                        List<LivingRoom> livingRooms = livingRoomByHotel.get(page.getId());
                        pageEventSocialResponse.setLivings(ObjectMapperUtils.mapAll(livingRooms, LivingEventSocialResponse.class));
                        pageEventSocialResponseMap.put(page.getId(), pageEventSocialResponse);
                        pageEventSocialResponses.add(pageEventSocialResponse);
                    }
                }
            });
        }

        return  PageEventSocialDestinationResponse.builder()
                .hotels(pageEventSocialResponses)
                .destinationName(pageHeader.isPresent() ? pageHeader.get().getName() : ConstantsUtil.EMPTY)
                .build();
    }

    private Map<String, List<LivingRoom>> getLivingRoomByHotelDestinationCategory(LivingDestinationHotel livingDestinationHotel){
        Map<String, List<HotelCategory>> hotelCategoryRequestMap = livingDestinationHotel.getHotelCategories().stream()
                .collect(Collectors.groupingBy(HotelCategory::getHotelId));

        Map<String, List<LivingRoom>> livingRoListMap = new HashMap<>();

        hotelCategoryRequestMap.forEach( (key, value) -> {
            List<String> categories = value.stream().map(HotelCategory::getCategory).collect(Collectors.toList());
            List<LivingRoom> livingRooms = this.siteService.customFindLivingRoomByDestinationIdAndHotelIdAndCategory(
                    livingDestinationHotel.getDestinationId(), key, categories);
            livingRoListMap.put(key, livingRooms);
        });

        return livingRoListMap;
    }

    private PageEventSocialResponse getDescriptionAndUrlImageFromData(PageEventSocialResponse pageEventSocialResponse ,Object data){
        HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);

        if(hotelDataDto != null && hotelDataDto.getCaHotelSummary()!= null){
            pageEventSocialResponse.setDescription(hotelDataDto.getCaHotelSummary().getDescriptionLivingRoom());
            pageEventSocialResponse.setUrlImage360(hotelDataDto.getCaHotelSummary().getImageTotalUrl());
        }
        return pageEventSocialResponse;
    }

    @GetMapping("/pages/{destination}/hotels")
    public List<DestinationDetailResponse> getDestinationWithHotelsAndServices(@PathVariable String destination, @RequestParam String language){
        HotelDestination hotelDestination = pageService.getHotelsByDestinationIdAndLanguageForSite(destination, language);

        if(CaUtil.validateIsNotNullAndNotEmpty(hotelDestination)){
            Map<String, Descriptor> descriptorMaps = getDescriptors(hotelDestination.getItems());
            Map<String, Page> pageMap = getPagesMapFromHotelDestination(hotelDestination.getItems(), language);
            Map<String, Assistance> assistanceMap = getAssistancesFromHotels(pageMap, language);
            List<DestinationDetailResponse> destinationDetailResponses = new ArrayList<>();

            hotelDestination.getItems().forEach( x -> {

                Descriptor descriptor = descriptorMaps.get(x.getDescriptor());

                List<HotelDestinationResponse> hotelDestinationResponses = new ArrayList<>();

                List<String> pages = x.getHotels().stream().map(this::buildHotels).collect(Collectors.toList());

                pages.forEach( id -> {
                    Page page = pageMap.get(id);
                    if(Objects.nonNull(page)){
                        LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
                        hotelDestinationResponses.add(HotelDestinationResponse.builder()
                                .name(languagePage.getTitle())
                                .roiback(page.getRoiback())
                                .services(getHotelServicesFromData(languagePage.getData(), language, assistanceMap))
                                .urlImage(getUrlImageFromData(languagePage.getData()))
                                .url(languagePage.getPath())
                                .build());
                    }
                });

                destinationDetailResponses.add(DestinationDetailResponse.builder()
                        .descriptorId(x.getDescriptor())
                        .descriptorName(descriptor.getName())
                        .descriptorColor(descriptor.getColor())
                        .hotels(hotelDestinationResponses)
                        .build());

            });
            return destinationDetailResponses;
        } else {
            return Collections.emptyList();
        }
    }

    private Map<String, Descriptor> getDescriptors(List<HotelDestinationItem> hotelDestinationItems){
        List<String> descriptor = hotelDestinationItems.stream().map(this::buildDescriptor).collect(Collectors.toList());
        List<Descriptor> descriptors = this.siteService.customFindDescriptorByIds(descriptor);
        Map<String, Descriptor> descriptorMaps = new HashMap<>();
        descriptors.forEach( x-> descriptorMaps.put(x.getId(), x));
        return descriptorMaps;
    }

    private Map<String, Page> getPagesMapFromHotelDestination(List<HotelDestinationItem> hotelDestinationItems, String language){

        Set<String> idsPage = new HashSet<>();
        hotelDestinationItems.forEach( x ->
            x.getHotels().forEach(hotel -> idsPage.add(hotel.getHotel()))
        );

        List<Page> pages = this.pageService.findHotelsByIdsAndLanguageCustomized(idsPage.stream().collect(Collectors.toList()), language);
        if(CaUtil.validateListIsNotNullOrEmpty(pages))
            return pages.stream().collect(Collectors.toMap(Page::getId, Function.identity()));
        else
            return new HashMap<>();
    }

    private Map<String, Assistance> getAssistancesFromHotels(Map<String, Page> pageMap, String language){

        Set<String> ids = new HashSet<>();
        pageMap.forEach((key, value) -> {
             LanguagePage languagePage = value.getLanguages().stream().findFirst().orElse(null);
             if(Objects.nonNull(languagePage)){
                 HotelDataDto hotelDataDto = ObjectMapperUtils.map(languagePage.getData(), HotelDataDto.class);
                 if(hotelDataDto != null && hotelDataDto.getCaServices() != null){
                     hotelDataDto.getCaServices().getItems().forEach(service -> ids.add(service.getService()));
                 }
             }
        });

        List<Assistance> assistanceList = siteService.getAssistanceByIdsAndTypeAndLanguage(ids.stream().collect(Collectors.toList()), language);
        if(CaUtil.validateListIsNotNullOrEmpty(assistanceList))
            return assistanceList.stream().collect(Collectors.toMap(Assistance::getId, Function.identity()));
        else
            return new HashMap<>();
    }

    private String buildDescriptor(HotelDestinationItem hotelDestinationItem){
        return hotelDestinationItem.getDescriptor();
    }

    private String buildHotels(HotelDestinationItemHotel hotelDestinationItemHotel){
        return hotelDestinationItemHotel.getHotel();
    }

    @GetMapping("/pages/title/ids")
    public List<SelectResponse> getTitle2PageFromContact(@RequestParam List<String> items){
        List<PageHeader> pages = this.pageService.getTitle2PageFromContact(items);
        List<SelectResponse> selectResponses = new ArrayList<>();
        pages.forEach(x ->
            selectResponses.add(SelectResponse.builder()
                    .value(x.getId())
                    .text(x.getName())
                    .build())
        );
        return selectResponses;
    }

    @GetMapping("/events/destinations")
    public List<SelectResponse> getDestinationsByLivingRooms(@RequestParam List<String> items){
        List<PageHeader> listPageHeader = pageService.findDestinationsByStateNotPublishByItems(items);
        List<SelectResponse> listDestinations = new ArrayList<>();
        listPageHeader.forEach( x ->
                listDestinations.add(SelectResponse.builder()
                        .value(x.getId())
                        .text(x.getName())
                        .build())
        );
        return listDestinations.stream().sorted(Comparator.comparing(SelectResponse :: getText)).collect(Collectors.toList());
    }

    @GetMapping("/promotions/detail/ids")
    public List<HotelRestPromotionResponse> getPageToPromotionDetail(HttpServletRequest request,
                                                                     @RequestParam List<String> ids,
                                                                     @RequestParam String language){
        List<Page> pages = this.pageService.customFindPageByIdsAndLanguageAndStateNot(ids, language);
        if(CaUtil.validateListIsNotNullOrEmpty(pages)){

            String host = CaUtil.getHostAndServerNameFromRequest(request);
            Double tax = getTaxByDomain(host);

            List<HotelRestPromotionResponse> hotelRestPromotionResponses = new ArrayList<>();
            Map<String, Page> pageMap = pages.stream().collect(Collectors.toMap(Page::getId, Function.identity()));
            List<String> descriptorsString = pages.stream().map(this::buildDescriptorFromPage).collect(Collectors.toList());
            List<Descriptor> descriptors = this.siteService.customFindDescriptorByIds(descriptorsString);
            Map<String, Descriptor> descriptorMap = descriptors.stream().collect(Collectors.toMap(Descriptor::getId, Function.identity()));
            ids.forEach(x -> {
                Page page = pageMap.get(x);
                if(Objects.nonNull(page)){
                    Descriptor descriptor = descriptorMap.get(page.getDescriptorId());
                    LanguagePage languagePage = CaUtil.validateListIsNotNullOrEmpty(page.getLanguages())
                            ? page.getLanguages().stream().findFirst().orElse(null)
                            : null;
                    hotelRestPromotionResponses.add(HotelRestPromotionResponse.builder()
                            .id(x)
                            .name(Objects.nonNull(languagePage) ? languagePage.getTitle() : ConstantsUtil.EMPTY)
                            .url(Objects.nonNull(languagePage) ? languagePage.getPath() : ConstantsUtil.EMPTY)
                            .nameDescriptor(descriptor.getTitle())
                            .colorDescriptor(descriptor.getColor())
                            .imageMain(getImageS3FromData(languagePage.getData(), page.getCategoryId()))
                            .tax(tax)
                            .roiback(page.getRoiback())
                            .build());
                }

            });
            return hotelRestPromotionResponses;
        } else {
            return Collections.emptyList();
        }
    }

    private ImageS3Response getImageS3FromData(Object data, String categoryId){
        if(Objects.nonNull(data)){
            ImageS3 imageS3 = null;
            if(TemplateType.HOTELS_DETAIL.getCode().equals(categoryId)){
                HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);
                if(Objects.nonNull(hotelDataDto) && Objects.nonNull(hotelDataDto.getCaHotelSummary())
                    && Objects.nonNull(hotelDataDto.getCaHotelSummary().getImageMain())
                    && !hotelDataDto.getCaHotelSummary().getImageMain().isEmpty()){
                    imageS3 = hotelDataDto.getCaHotelSummary().getImageMain().get(ConstantsUtil.ZERO);
                }
            } else if(TemplateType.RESTAURANTS_DETAIL.getCode().equals(categoryId)){
                RestaurantDataDto restaurantDataDto = ObjectMapperUtils.map(data, RestaurantDataDto.class);
                if(Objects.nonNull(restaurantDataDto) && Objects.nonNull(restaurantDataDto.getCaInfoRestaurant())
                        && Objects.nonNull(restaurantDataDto.getCaInfoRestaurant().getImageMain())
                        && !restaurantDataDto.getCaInfoRestaurant().getImageMain().isEmpty()){
                    imageS3 = restaurantDataDto.getCaInfoRestaurant().getImageMain().get(ConstantsUtil.ZERO);
                }
            }
            return Objects.nonNull(imageS3) ? ObjectMapperUtils.map(imageS3, ImageS3Response.class) : null;
        }
        return ImageS3Response.builder().build();
    }

    private String buildDescriptorFromPage(Page page){
        return page.getDescriptorId();
    }

    @GetMapping("/countries/select")
    public List<SelectResponse> getCountriesAll(){
        List<Country> listCountry = this.siteService.customFindAllByStatus();
        List<SelectResponse> listSelectResponse = new ArrayList<>();
        listCountry.forEach(x ->
                listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build())
        );
        return listSelectResponse.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
    }

    @GetMapping("/configurations")
    public ConfigurationResponse getConfiguration() {
        return ObjectMapperUtils.map(this.siteService.getConfigurations(), ConfigurationResponse.class);
    }

    @GetMapping("/configurations/contact/page")
    public List<ConfigurationContactPageResponse> contactPage() throws CaBusinessException {
        Configurations configurations = this.siteService.getConfigurations();
        List<ConfigurationContactPageResponse> list = new ArrayList<>();

        if(configurations.getContact() != null){
            String contactPageId = configurations.getContact().getFormPage();
            if(contactPageId != null) {
                Optional<Page> pageContact = this.pageService.customFindPathsBySEO(contactPageId);
                if(pageContact.isPresent()) {
                    return (List) pageContact.get().getLanguages();
                }
            }
        }
        return Collections.emptyList();
    }

    @GetMapping("/pages/destinations/view/maps")
    public List<HotelViewMapResponse> getHotelsByDestinationAndDescriptors(@RequestParam String destinationId,
                                                             @RequestParam String descriptorId,
                                                             @RequestParam String language) throws CaRequiredException{
        List<Page> pages = this.pageService
                .customFindHotelsByDestinationIdAndDescriptorIdAndLanguage(destinationId, descriptorId, language);

        List<HotelViewMapResponse> hotelViewMapResponses = new ArrayList<>();

        if(CaUtil.validateListIsNotNullOrEmpty(pages)){

            String urlRoiback = this.siteService.getUrlRoibackHotel(language);
            String rangeDates = this.siteService.getRangeDateRoiback();

            pages.forEach( x -> {
                LanguagePage languagePage = x.getLanguages().stream().findFirst().orElse(null);
                if(Objects.nonNull(languagePage)){
                    HotelViewMapResponse hotelViewMapResponse = getHotelViewMapResponseFromData(languagePage.getData());
                    hotelViewMapResponse.setName(languagePage.getTitle());
                    hotelViewMapResponse.setRevinate(x.getRevinate());
                    hotelViewMapResponse.setUrlRoiback(urlRoiback+ x.getRoiback() + ConstantsUtil.SEPARATOR + rangeDates);
                    hotelViewMapResponses.add(hotelViewMapResponse);
                }
            });
        }

        return hotelViewMapResponses;
    }

    private HotelViewMapResponse getHotelViewMapResponseFromData(Object data){
        HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);

        if(hotelDataDto != null && hotelDataDto.getCaHotelSummary()!= null){
            HotelSummaryDto hotelSummaryDto = hotelDataDto.getCaHotelSummary();

            ImageS3Response imageS3Response = CaUtil.validateListIsNotNullOrEmpty(hotelSummaryDto.getImageMain())
                    ? ObjectMapperUtils.map(hotelSummaryDto.getImageMain().get(ConstantsUtil.ZERO), ImageS3Response.class)
                    : new ImageS3Response();

            return HotelViewMapResponse.builder()
                    .description(hotelSummaryDto.getDescription())
                    .urlMap(hotelSummaryDto.getMapUrl())
                    .imageMain(imageS3Response)
                    .build();
        }

        return HotelViewMapResponse.builder().build();
    }

    @GetMapping("/pages/destinations/map")
    public List<DestinationDescriptorMapResponse> getDestinationsPageFrom(@RequestParam String language){
        List<Page> pagesDestination = this.pageService.customFindDestinationByStateAndLanguage(language);
        if(CaUtil.validateListIsNotNullOrEmpty(pagesDestination)){
            List<String> destinationMapIds = pagesDestination.stream().map(Page::getDestinationMapId).collect(Collectors.toList());
            Map<String, DestinationMap> destinationMap = getDestinationMapByDestination(destinationMapIds);

            List<String> itemsDestinations = pagesDestination.stream().map(Page::getId).collect(Collectors.toList());
            List<Page> pagesDescriptors = this.pageService.customFindDescriptorByHotelsAndDestinationIds(itemsDestinations);
            List<String> itemDescriptors = pagesDescriptors.stream().map(Page::getDescriptorId).collect(Collectors.toList());
            Map<String, List<Page>> pagesDescriptorMaps = pagesDescriptors.stream().collect(Collectors.groupingBy(Page::getDestinationId));
            Map<String, Descriptor> descriptorMap = getDescriptorByItemDescriptor(itemDescriptors);

            List<DestinationDescriptorMapResponse> destinationDescriptorMapResponses = new ArrayList<>();
            pagesDestination.forEach(des -> {
                    List<Page> desPage = pagesDescriptorMaps.get(des.getId());
                    List<DescriptorResponse> descriptorResponses = new ArrayList<>();
                    if(CaUtil.validateListIsNotNullOrEmpty(desPage)){
                        desPage.forEach(x->{
                            Descriptor descriptor = descriptorMap.get(x.getDescriptorId());
                            descriptorResponses.add(DescriptorResponse.builder().id(descriptor.getId()).color(descriptor.getColor()).build());
                        });
                    }

                    LanguagePage languagePage = des.getLanguages().stream().findFirst().orElse(null);
                    DestinationMap destinationMapClassName = destinationMap.get(des.getDestinationMapId());

                destinationDescriptorMapResponses.add(DestinationDescriptorMapResponse.builder()
                        .destination(Objects.nonNull(languagePage) ? languagePage.getTitle() : ConstantsUtil.EMPTY)
                        .destinationId(des.getId())
                        .className(Objects.nonNull(destinationMapClassName) ? destinationMapClassName.getClassName() : ConstantsUtil.EMPTY)
                        .descriptors(descriptorResponses)
                        .build());
            });
            return destinationDescriptorMapResponses.stream()
                    .sorted(Comparator.comparing(DestinationDescriptorMapResponse::getDestination))
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private Map<String, DestinationMap> getDestinationMapByDestination(List<String> destinationMapIds){
        if(CaUtil.validateListIsNotNullOrEmpty(destinationMapIds)){
            List<DestinationMap> destinations = this.siteService.getDestinationMapByIds(destinationMapIds);
            return destinations.stream().collect(Collectors.toMap(DestinationMap::getId, Function.identity()));
        }
        return new HashMap<>();
    }

    private Map<String, Descriptor> getDescriptorByItemDescriptor(List<String> itemDescriptors){
        if (CaUtil.validateListIsNotNullOrEmpty(itemDescriptors)) {
            List<Descriptor> descriptors = this.siteService.customFindDescriptorByIds(itemDescriptors);
            return descriptors.stream().collect(Collectors.toMap(Descriptor::getId, Function.identity()));
        }
        return new HashMap<>();
    }

    @GetMapping("/pages/hotels/contact")
    public List<ContactResponse> getContactByHotelId(@RequestParam String destinationId,
                                               @RequestParam String descriptorId,
                                               @RequestParam String language)
        throws CaRequiredException {
        List<Page> pageHeaders = this.pageService.getHotelsByDestinationAndDescriptorAndStateNot(destinationId, descriptorId);
        List<ContactResponse> contactResponses = new ArrayList<>();

        if(Objects.nonNull(pageHeaders) && !pageHeaders.isEmpty()){
            List<String> ids = pageHeaders.stream().map(Page::getId).collect(Collectors.toList());
            List<Page> pages = this.pageService.getContactByPages(ids, language);

            if(Objects.nonNull(pages) && !pages.isEmpty()){
                pages.forEach(page -> {
                    ContactResponse contactResponse = null;
                    LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
                    if(Objects.nonNull(page.getContact())){
                        contactResponse = getContactResponseFromContact(page.getContact(), language);
                    }
                    contactResponse = Objects.nonNull(contactResponse) ? contactResponse : new ContactResponse();
                    if(Objects.nonNull(languagePage)){
                        contactResponse.setImageMain(getImageS3FromData(languagePage.getData()));
                        contactResponse.setUrl(languagePage.getPath());
                    }
                    contactResponses.add(contactResponse);
                });
            }
        }
        return contactResponses;
    }

    private ContactResponse getContactResponseFromContact(Contact contact,final String language){
        List<LanguageDetailContact> languageDetailContacts = new ArrayList<>();
        if(Objects.nonNull(contact.getLanguages()) && !contact.getLanguages().isEmpty()){
            LanguageContact languageContact = contact.getLanguages().stream()
                    .filter(x -> language.equals(x.getLanguage()))
                    .findFirst().orElse(null);
            if(Objects.nonNull(languageContact) && Objects.nonNull(languageContact.getDetails())
                    && !languageContact.getDetails().isEmpty()){
                languageDetailContacts.addAll(languageContact.getDetails());
            }
        }

        return ContactResponse.builder()
                .title1(contact.getTitle1())
                .title2(contact.getTitle2())
                .languageDetailContacts(languageDetailContacts)
                .build();
    }

    private ImageS3 getImageS3FromData(Object data){
        HotelDataDto hotelDataDto = ObjectMapperUtils.map(data, HotelDataDto.class);
        if(hotelDataDto!= null && hotelDataDto.getCaHotelSummary() != null &&
                hotelDataDto.getCaHotelSummary().getImageMain() !=null &&
                hotelDataDto.getCaHotelSummary().getImageMain().get(ConstantsUtil.ZERO).getLarge() != null){
            return hotelDataDto.getCaHotelSummary().getImageMain().get(ConstantsUtil.ZERO);
        }
        return new ImageS3();
    }

    @GetMapping("/pages/promotions/bells")
    public List<PromotionBellResponse> getPromotionBellByLanguage(HttpServletRequest request,
                                                                  @RequestParam List<String> promotions,
                                                                  @RequestParam String currency,
                                                                  @RequestParam String language){
        List<Page> pages = this.pageService.getPromotionBell(promotions,language);
        List<PromotionBellResponse> promotionBellResponses = new ArrayList<>();

        String host = CaUtil.getHostAndServerNameFromRequest(request);
        Double tax = getTaxByDomain(host);

        if(Objects.nonNull(pages) && !pages.isEmpty()){
            Currency currencyObject = this.siteService.findByCodeIsoAndStatus(currency);
            Double currencyValue = (Objects.nonNull(currencyObject) && Objects.nonNull(currencyObject.getValue()))
                    ? currencyObject.getValue() : Double.valueOf(ConstantsUtil.ONE.toString());

            pages.forEach(x -> {
                LanguagePage languagePage = x.getLanguages().stream().findFirst().orElse(null);
                if(Objects.nonNull(languagePage)){
                    promotionBellResponses.add(PromotionBellResponse.builder()
                            .id(x.getId())
                            .url(languagePage.getPath())
                            .infoPromotion(getInfoPromotionFromData(languagePage.getData(), tax, currencyValue))
                            .build());
                }
            });
        }
        return promotionBellResponses;
    }

    private InfoPromotionDto getInfoPromotionFromData(Object data, Double tax, Double currency){
        if(Objects.nonNull(data)){
            PromotionDataDto promotionDataDto = ObjectMapperUtils.map(data, PromotionDataDto.class);
            if(Objects.nonNull(promotionDataDto) && Objects.nonNull(promotionDataDto.getCaInfoPromotion())){
                InfoPromotionDto infoPromotionDto = promotionDataDto.getCaInfoPromotion();
                infoPromotionDto.setPrice(getPriceWithTaxWithChangeRate(infoPromotionDto.getPrice(), tax, currency));
                return infoPromotionDto;
            }
        }
        return null;
    }

    //region Featured Promotion
    @GetMapping("/promotions/types")
    public List<SelectResponse> getPromotionTypeByLanguage(@RequestParam String language){
        List<PromotionType> listPromotionType = this.siteService.getPromotionTypeByLanguage(language);
        List<SelectResponse> listSelectResponse = new ArrayList<>();
        listPromotionType.forEach(x -> {
            LanguageDescription languageDescription = x.getLanguages().stream().findFirst().orElse(null);
            listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(languageDescription != null ? languageDescription.getDescription() : ConstantsUtil.EMPTY).build());
        });
        return listSelectResponse;
    }

    @GetMapping("/pages/promotions/details")
    public List<SelectResponse> getPromotionDetails(@RequestParam  String language){
         List<Page> pages = this.pageService.findPromotionsByStateAndLanguageCustomized(language);

        List<SelectResponse> selectResponses = new ArrayList<>();
        if(Objects.nonNull(pages) && !pages.isEmpty()){
            pages.forEach(x -> {
                LanguagePage languagePage = x.getLanguages().stream().findFirst().orElse(null);
                if(Objects.nonNull(languagePage) && CaUtil.validateIsNotNullAndNotEmpty(languagePage.getTitle())){
                    selectResponses.add(SelectResponse.builder().value(x.getId()).text(languagePage.getTitle()).build());
                }
            });
        }
        return selectResponses;
    }

    @GetMapping("/pages/destinations/all")
    public List<SelectResponse> getDestinationsAll(){
        List<PageHeader> pageHeaders = this.pageService.getDestinationsAll();
        List<SelectResponse> selectResponses = new ArrayList<>();
        if(Objects.nonNull(pageHeaders) && !pageHeaders.isEmpty()){
            pageHeaders.forEach(x -> selectResponses.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build()));
        }
        return selectResponses;
    }

    @GetMapping("/pages/promotions/destinations/hotels")
    public List<SelectResponse> getHotelsByListDestination(@RequestParam List<String> destinations,@RequestParam String language){
        List<Page> pages = this.pageService.findHotelsByListDestinationsAndLanguageCustomized(destinations, language);
        List<SelectResponse> selectResponses = new ArrayList<>();
        if(Objects.nonNull(pages) && !pages.isEmpty()){
            pages.forEach(x -> {
                LanguagePage languagePage = x.getLanguages().stream().findFirst().orElse(null);
                if(Objects.nonNull(languagePage) && CaUtil.validateIsNotNullAndNotEmpty(languagePage.getTitle())){
                    selectResponses.add(SelectResponse.builder().value(x.getId()).text(languagePage.getTitle()).build());
                }
            });
        }
        return selectResponses;
    }

    @GetMapping("/hotels/descriptors")
    public List<SelectResponse> getDescriptorsByCategoryId(){
        List<Descriptor> listDescriptor =  descriptorService.getAllByCategory(TemplateType.HOTELS_DETAIL.getCode());
        List<SelectResponse> listSelectResponse = new ArrayList<>();
        listDescriptor.forEach(x ->
                listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(x.getTitle()).build())
        );
        return listSelectResponse;
    }

    @GetMapping("/pages/promotions/featured")
    public com.casaandina.cms.util.pagination.Page<PagePromotionViewResponse> getFeaturesPromotions(HttpServletRequest request,
            FeaturedPromotionRequest featuredPromotionRequest, Pageable pageable){

        HostIpCountry hostIpCountry = getHostIpCountryByRequest(request);

        com.casaandina.cms.util.pagination.Page<PagePromotionView> pagePromotionViewPage = this.pageService.getFeaturedPromotionsByFilters(ObjectMapperUtils.map(featuredPromotionRequest, FeaturedPromotion.class),
                hostIpCountry,
                PageUtil.getPageable(pageable.getPageNumber(), pageable.getPageSize()));

        List<PagePromotionViewResponse> pagePromotionViewResponses = ObjectMapperUtils.mapAll(pagePromotionViewPage.getContent(),
                PagePromotionViewResponse.class);

        Double tax = getTaxByDomain(hostIpCountry.getUriHost());

        pagePromotionViewResponses.forEach(prom -> {
            if(Objects.nonNull(prom.getInfo())){
                prom.getInfo().setPrice(getPriceWithTax(prom.getInfo().getPrice(), tax));
            }
            if(Objects.nonNull(prom.getItem())){
                prom.getItem().setPrice(getPriceWithTax(prom.getItem().getPrice(), tax));
            }
        });

        return PageUtil.pageToResponse(pagePromotionViewPage, pagePromotionViewResponses);
    }

    //endregion

    @GetMapping("/pages/promotions/details/url")
    public List<PromotionUrlReservationResponse> getUrlReservationByPromotionAndHotelsAndRestaurants(HttpServletRequest request,
                                                                    @RequestParam String promotions,
                                                                    @RequestParam String currency,
                                                                    @RequestParam String language){
        List<PromotionUrlReservation> promotionUrlReservations = this.pageService.getUrlReservationByPromotionAndHotelsAndRestaurants(promotions, currency, language);

        String host = CaUtil.getHostAndServerNameFromRequest(request);
        Double tax = getTaxByDomain(host);

        promotionUrlReservations.forEach(prom -> {
            if(Objects.nonNull(prom.getPrice())){
                prom.setPrice(getPriceWithTax(prom.getPrice(), tax));
            }
        });

        return ObjectMapperUtils.mapAll(promotionUrlReservations, PromotionUrlReservationResponse.class);
    }

    @GetMapping("/configurations/languages")
    public ConfigurationLanguageResponse getLanguages(){
        Configurations configurations = siteService.getLanguagesForSite();
        return ObjectMapperUtils.map(configurations, ConfigurationLanguageResponse.class);
    }

    @GetMapping("/currencies/select")
    public List<SelectResponse> getCurrencies(){
        List<Currency> currencies = this.siteService.getCurrenciesForSite();
        if(Objects.nonNull(currencies) && !currencies.isEmpty()){
            Set<SelectResponse> selectResponseSet = new HashSet<>();
            currencies.forEach(currency ->
                selectResponseSet.add(
                        SelectResponse.builder()
                                .value(currency.getCodeIso())
                                .text(currency.getSymbol())
                                .build()
                )
            );
            return selectResponseSet.stream().collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @GetMapping("/form/template")
    public List<FieldFormTemplate> getFormTemplateByCodeAndLanguage(@RequestParam String code, @RequestParam String language){
        FormTemplate formTemplate = this.siteService.getFormTemplateByCodeAndLanguage(code, language);
        if(Objects.nonNull(formTemplate)){
            LanguageFormTemplate languageFormTemplate = formTemplate.getLanguages().stream().findFirst().orElse(null);
            if(Objects.nonNull(languageFormTemplate))
                return languageFormTemplate.getFields();
        }
        return Collections.emptyList();
    }

    @GetMapping("/test")
    public HostIpCountry getHostName(HttpServletRequest request){
        return getHostIpCountryByRequest(request);
    }

    private HostIpCountry getHostIpCountryByRequest(HttpServletRequest request){
        try {
            String ip = CaUtil.extractIp(request);

            HostIpCountry hostIpCountry = new HostIpCountry();
            hostIpCountry.setUriHost(CaUtil.getHostAndServerNameFromRequest(request));
            log.info(hostIpCountry.getUriHost());

            CountryResponse countryResponse = geoLocationService.getIpLocation(ip);

            if(Objects.nonNull(countryResponse) && Objects.nonNull(countryResponse.getCountry())) {
                hostIpCountry.setIsoCountry(countryResponse.getCountry().getIsoCode());
                hostIpCountry.setCountryName(countryResponse.getCountry().getNames().get(LanguageType.SPANISH.getCode()));
            }

            if(Objects.nonNull(countryResponse) && Objects.nonNull(countryResponse.getTraits())) {
                hostIpCountry.setIpPublic(countryResponse.getTraits().getIpAddress());
            }

            return hostIpCountry;

        } catch (Exception e) {
            log.error(String.format(ConstantsUtil.AN_ERROR_OCCURRED, e.getMessage()));
            return new HostIpCountry();
        }
    }

    private Double getPriceWithTax(Double price, Double tax){
        if(Objects.nonNull(price)){
            Double priceWithTax = price * tax;
            return CaUtil.roundDouble(priceWithTax, ConstantsUtil.ZERO, Boolean.TRUE);
        }
        return price;
    }

    private Double getPriceWithTaxWithChangeRate(Double price, Double tax, Double currency){
        if(Objects.nonNull(price)){
            Double priceWithTax = price * tax * currency;
            return CaUtil.roundDouble(priceWithTax, ConstantsUtil.ZERO, Boolean.TRUE);
        }
        return price;
    }

    private Double getTaxByDomain(String path){
        Domain domain = this.siteService.getDomainByPath(path);
        if(Objects.nonNull(domain) && Objects.nonNull(domain.getTax())){
            return domain.getTax();
        }
        return Double.valueOf(ConstantsUtil.ONE.toString());
    }

    @GetMapping("/domains")
    public DomainResponse getDomainByPath(@RequestParam  String path){
        Domain domain = this.siteService.customFindByPath(path);
        if(Objects.nonNull(domain)) {
            DomainResponse domainResponse = ObjectMapperUtils.map(domain, DomainResponse.class);

            if(Objects.nonNull(domainResponse) && Objects.nonNull(domainResponse.getPromotionAllId())){
                Optional<Page> page = this.pageService.customFindPathsBySEO(domainResponse.getPromotionAllId());
                if(page.isPresent()){
                    domainResponse.setPromotionPaths(page.get().getLanguages());
                }
            }
            return domainResponse;
        }
        return new DomainResponse();
    }

    @GetMapping("/test/roiback/{roiback}")
    public void testRoiback(@PathVariable String roiback, @RequestParam String language) throws CaRequiredException{
        this.roibackServiceClient.getRoibackElement(roiback, language);
    }

    @GetMapping("/pages/promotions/details/filter")
    public List<SelectResponse> getPromotionDetailsFilter(@RequestParam  String language){
        List<Page> pages = this.pageService.findPromotionsByStateAndLanguageCustomizedFilter(language);

        List<SelectResponse> selectResponses = new ArrayList<>();
        if(Objects.nonNull(pages) && !pages.isEmpty()){
            pages.forEach(x -> {
                LanguagePage languagePage = x.getLanguages().stream().findFirst().orElse(null);
                if(Objects.nonNull(languagePage) && CaUtil.validateIsNotNullAndNotEmpty(languagePage.getTitle())){
                    selectResponses.add(SelectResponse.builder().value(x.getId()).text(languagePage.getTitle()).build());
                }
            });
        }
        return selectResponses;
    }

    @GetMapping("/pages/hotels/contact/{hotelId}")
    public List<ContactResponse> getContactByHotelId(@PathVariable String hotelId,
                                                     @RequestParam String language)
            throws CaRequiredException {
        List<Page> pageHeaders = this.pageService.getHotelsByIdAndDescriptorAndStateNot(hotelId);
        List<ContactResponse> contactResponses = new ArrayList<>();

        if(Objects.nonNull(pageHeaders) && !pageHeaders.isEmpty()){
            List<String> ids = pageHeaders.stream().map(Page::getId).collect(Collectors.toList());
            List<Page> pages = this.pageService.getContactByPages(ids, language);

            if(Objects.nonNull(pages) && !pages.isEmpty()){
                pages.forEach(page -> {
                    ContactResponse contactResponse = null;
                    LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
                    if(Objects.nonNull(page.getContact())){
                        contactResponse = getContactResponseFromContact(page.getContact(), language);
                    }
                    contactResponse = Objects.nonNull(contactResponse) ? contactResponse : new ContactResponse();
                    if(Objects.nonNull(languagePage)){
                        contactResponse.setImageMain(getImageS3FromData(languagePage.getData()));
                        contactResponse.setUrl(languagePage.getPath());
                    }
                    contactResponses.add(contactResponse);
                });
            }
        }
        return contactResponses;
    }

    @GetMapping("/pages/hotels/all/descriptors")
    public List<HotelDescriptorInformationResponse> getHotelsByArrayIds(@RequestParam String language){
        List<Page> listPage = pageService.findAllHotels(language);
        List<Descriptor> descriptorList = this.descriptorService.customFindAllDescriptors();
        Map<String, Descriptor> descriptorMap = descriptorList.stream().collect(Collectors.toMap(Descriptor::getId, Function.identity()));

        if(Objects.nonNull(listPage) && !listPage.isEmpty()) {
            List<HotelDescriptorInformationResponse> hotelDescriptorInformationResponses = new ArrayList<>();
            listPage.forEach(page -> {
                LanguagePage languagePage = page.getLanguages().stream().findFirst().orElse(null);
                Descriptor descriptor = descriptorMap.get(page.getDescriptorId());
                String title = "";
                if(Objects.nonNull(languagePage)){
                    title = languagePage.getTitle();
                }
                hotelDescriptorInformationResponses.add(HotelDescriptorInformationResponse.builder()
                        .id(page.getId())
                        .name(title)
                        .descriptor(Objects.nonNull(descriptor) ? DescriptorResponse.builder()
                                .id(descriptor.getId())
                                .name(descriptor.getName())
                                .color(descriptor.getColor())
                                .title(descriptor.getTitle())
                                .build() : null)
                        .build());
            });
            return hotelDescriptorInformationResponses;
        }
        return Collections.emptyList();
    }

    @GetMapping("/promotions/filter/destinations")
    public List<SelectResponse> getDestinationByPromotions(@RequestParam List<String> items){
        List<PageHeader> pageHeaders = this.siteService.getDestinationByPromotions(items);

        List<SelectResponse> listDestinations = new ArrayList<>();
        pageHeaders.forEach( x ->
                listDestinations.add(SelectResponse.builder()
                        .value(x.getId())
                        .text(x.getName())
                        .build())
        );
        return listDestinations.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
    }

    @GetMapping("/promotions/filter/hotels")
    public List<SelectResponse> getHotelByDestinationsForPromotions(@RequestParam List<String> items){
        List<PageHeader> pageHeaders = this.siteService.getHotelByDestinationsForPromotions(items);

        List<SelectResponse> listHotels = new ArrayList<>();
        pageHeaders.forEach( x ->
                listHotels.add(SelectResponse.builder()
                        .value(x.getId())
                        .text(x.getName())
                        .build())
        );
        return listHotels.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
    }

    private List<PageHeaderResponse> groupByDestinationsWithHotels(List<PageHeaderResponse> pageHeaderResponses){
        if(Objects.nonNull(pageHeaderResponses) && !pageHeaderResponses.isEmpty()){

            List<PageHeaderResponse> destinations = pageHeaderResponses.stream()
                    .filter(x -> TemplateType.DESTINATIONS_DETAIL.getCode().equals(x.getCategoryId()))
                    .sorted(Comparator.comparing(PageHeaderResponse::getName))
                    .collect(Collectors.toList());

            if(Objects.nonNull(destinations) && !destinations.isEmpty()){
                List<PageHeaderResponse> result = new ArrayList<>();

                destinations.forEach(destination -> {

                    List<PageHeaderResponse> hotels = pageHeaderResponses.stream()
                            .filter(x -> destination.getId().equals(x.getDestinationId()))
                            .sorted(Comparator.comparing(PageHeaderResponse::getName))
                            .collect(Collectors.toList());

                    result.add(destination);

                    if(Objects.nonNull(hotels) && !hotels.isEmpty())
                        result.addAll(hotels);
                });
                return result;
            } else {
                pageHeaderResponses.stream().sorted(Comparator.comparing(PageHeaderResponse::getName)).collect(Collectors.toList());
            }
        }
        return pageHeaderResponses;
    }
}
