package com.casaandina.cms.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.Page;
import com.casaandina.cms.model.QuoteEvent;
import com.casaandina.cms.model.Subscriber;
import com.casaandina.cms.rest.request.QuoteEventRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.QuoteEventResponse;
import com.casaandina.cms.service.QuoteEventService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.PropertiesConstants;

@RestController
@RequestMapping("/api/quotes")
@SuppressWarnings("rawtypes")
public class QuoteEventController {
	
	private final QuoteEventService quoteEventService;
	private final MessageProperties messageProperties;
	
	@Autowired
	public QuoteEventController(QuoteEventService quoteEventService, MessageProperties messageProperties) {
		this.quoteEventService = quoteEventService;
		this.messageProperties = messageProperties;
	}
	
	@PostMapping
	public GenericResponse saveQuoteEvent(@RequestBody QuoteEventRequest quoteEventRequest) throws CaBusinessException {
		QuoteEvent quoteEvent = ObjectMapperUtils.map(quoteEventRequest, QuoteEvent.class);
		quoteEvent.setPage(Page.builder().id(quoteEventRequest.getIdPageHotel()).build());
		quoteEvent.setSubscriber(ObjectMapperUtils.map(quoteEventRequest.getSubscriberShortRequest(), Subscriber.class));

		QuoteEventResponse quoteEventResponse = ObjectMapperUtils.map(quoteEventService.saveQuoteEvent(quoteEvent), QuoteEventResponse.class);

		quoteEventService.sendMail(quoteEvent);

		return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_QUOTE_EVENT_SAVE_SUCCESFUL)
				, quoteEventResponse);
	}

}
