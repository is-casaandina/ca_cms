package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssistanceRequest {

    private String id;
    private String iconClass;
    private String englishTitle;
    private String spanishTitle;
    private String portugueseTitle;
    private Integer type;
}
