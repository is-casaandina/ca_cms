package com.casaandina.cms.rest.request;

import lombok.Data;

import java.util.List;

@Data
public class LivingRoomRequest {
	
	private String name;
	
	private String squareMeters;
	
	private String lengthy;
	
	private String width;
	
	private String high;
	
	private Integer teather;
	
	private String school;
	
	private String banquet;
	
	private String directory;
	
	private String cocktail;
	
	private String hotelId;
	
	private String destinationId;

	private Integer order;

	private List<LivingRoomSliderRequest> sliders;

	private String category;

}
