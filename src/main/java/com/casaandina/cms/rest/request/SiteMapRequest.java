package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class SiteMapRequest {

    private Boolean acceptExternalPages;
}
