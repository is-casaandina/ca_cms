package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class ProfileRequest {
    private String name;
    private String lastname;
    private String email;
}
