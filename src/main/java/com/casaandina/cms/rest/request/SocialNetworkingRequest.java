package com.casaandina.cms.rest.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SocialNetworkingRequest {
    private String urlFacebook;
    private String urlTwitter;
    private String urlInstagram;
    private String urlYoutube;
    private String urlFlickr;
    private String urlWhatsapp;
}
