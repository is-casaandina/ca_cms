package com.casaandina.cms.rest.request;

import com.casaandina.cms.model.LanguageDescription;
import lombok.Data;

import java.util.List;

@Data
public class PromotionTypeRequest {

    private List<LanguageDescription> languages;
}
