package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class UserRequest {
    private Integer id;
    private String username;
    private String password;
    private String name;
    private String lastname;
    private String email;
    private String roleCode;
    private Integer status;
    private String token;
}
