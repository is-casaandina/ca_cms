package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class RobotsRequest {
    private String userAgent;
    private String type;
    private String value;
}
