package com.casaandina.cms.rest.request;

import java.time.LocalDateTime;
import java.util.List;

import com.casaandina.cms.dto.CountryRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClusterRequest {
	
	private String name;
	
	private Boolean status;
	
	private List<CountryRequest> countries;
	
	private Integer createdBy;
	
	private LocalDateTime createdDate;
	
	private Integer updatedBy;
	
	private LocalDateTime updatedDate;
	
}
