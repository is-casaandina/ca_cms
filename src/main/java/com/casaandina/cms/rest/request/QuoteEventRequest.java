package com.casaandina.cms.rest.request;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class QuoteEventRequest {

	private String idPageHotel;

	private Integer quantityAssistants;

	private LocalDate beginDate;

	private LocalDate endDate;

	private String beginHour;

	private String endHour;

	private List<String> armedTypes;

	private List<String> foodOptions;

	private List<String> audiovisualEquipments;

	private String 	additionalComments;

	private SubscriberShortRequest subscriberShortRequest;
}
