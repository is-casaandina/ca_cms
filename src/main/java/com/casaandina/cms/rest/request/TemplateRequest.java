package com.casaandina.cms.rest.request;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TemplateRequest {

    private String id;
    private String code;
    private String name;
    private String description;
    private String imageUri;
    private String html;
    private List<String> refComponents = new ArrayList<>();
}
