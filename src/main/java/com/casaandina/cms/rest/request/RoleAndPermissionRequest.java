package com.casaandina.cms.rest.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleAndPermissionRequest {
    private String roleName;
    private String roleCode;
    private List<String> permissionCodeList;
}
