package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PageTrayRequest {

    private List<String> categories;
    private Integer[] states;
    private List<String> userList;
    private List<String> destinations;
    private List<String> countries;
    private List<String> genericType;
    private String menuCode;
    private String startDate;
    private String endDate;
    private String name;
    private Integer page;
    private Integer size;
}
