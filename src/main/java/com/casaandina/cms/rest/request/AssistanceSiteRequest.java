package com.casaandina.cms.rest.request;

import lombok.Data;

import java.util.List;

@Data
public class AssistanceSiteRequest {

    private String language;
    private List<String> items;
}
