package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class TagRequest {
	
	private String name;
	
	private String url;

    private String urlBanner;

    private String typographyStyle;
	
	private String color;
	
	private String title;
	
	private String category;

}
