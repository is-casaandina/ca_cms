package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class ParameterRequest {
	
	private String code;
	
	private String languageCode;
	
	private Object indicator;

}
