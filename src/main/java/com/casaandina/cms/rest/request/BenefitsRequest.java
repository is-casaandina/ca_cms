package com.casaandina.cms.rest.request;

import com.casaandina.cms.model.DetailBenefit;
import com.casaandina.cms.model.Link;
import lombok.Data;

import java.util.List;

@Data
public class BenefitsRequest {
    private String id;
    private String textEs;
    private Link linkEs;
    private String textEn;
    private Link linkEn;
    private String textPr;
    private Link linkPr;
    private List<DetailBenefit> detail;
}
