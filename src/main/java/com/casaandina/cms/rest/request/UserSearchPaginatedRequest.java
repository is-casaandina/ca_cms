package com.casaandina.cms.rest.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserSearchPaginatedRequest extends PaginatedRequest {
    private String[] rolesCode;
}

