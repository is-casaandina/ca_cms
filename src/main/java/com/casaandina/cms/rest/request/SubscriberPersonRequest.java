package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class SubscriberPersonRequest {

	private String name;
	private String email;
	private String phone;
	private String countryId;
	private String city;
	private Integer gender;
	
}
