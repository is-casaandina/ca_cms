package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class PageErrorRequest {

    private String pageId;
    private Integer errorCode;
}
