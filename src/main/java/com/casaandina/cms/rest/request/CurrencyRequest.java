package com.casaandina.cms.rest.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
public class CurrencyRequest {

	private String name;
	private String symbol;
	private Double value;
	private String codeIso;
	private Integer isPrincipal;
	
}
