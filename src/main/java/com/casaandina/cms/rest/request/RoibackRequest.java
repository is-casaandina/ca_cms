package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoibackRequest {
	private String urlReservationEsp;
	private String urlReservationEng;
	private String urlReservationPor;
}
