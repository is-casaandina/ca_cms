package com.casaandina.cms.rest.request;

import com.casaandina.cms.model.Contact;
import com.casaandina.cms.model.LanguagePage;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class PageRequest {

	private String id;
	private String name;
	private Integer pageType;
	private Integer type;
	private String user;
	private String owner;
	private Integer status;
	private LocalDate createdDate;
	private LocalDate publishDate;
	private LocalDate futurePublishDate;
	private LocalDate unpublishedDate;
	private LocalDate modifiedDate;
	private List<LanguagePage> languages = new ArrayList<>();
	private Boolean external;
	private String countryId;
	private String categoryId;
	private String descriptorId;
	private String destinationId;
	private String roiback;
	private String revinate;
	private LocalDateTime beginDate;
	private LocalDateTime firstExpiration;
	private LocalDateTime secondExpiration;
	private Boolean countdown;
	private String shortName;
	private String genericType;
	private String domainId;
	private String clusterId;

	private Contact contact;
	private Boolean inEdition;

	private String destinationMapId;

}
