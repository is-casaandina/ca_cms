package com.casaandina.cms.rest.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
public class LanguagePageNotificationRequest {
    private String language;
    private String title;
    private String detail;
}
