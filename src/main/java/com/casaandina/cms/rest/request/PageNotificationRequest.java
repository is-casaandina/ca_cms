package com.casaandina.cms.rest.request;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PageNotificationRequest {
    private String pageId;
    private String name;
    private LocalDateTime beginDate;
    private LocalDateTime endDate;
    private List<LanguagePageNotificationRequest> languages;
}
