package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class LivingDestinationHotelRequest {

    private String destinationId;
    private List<HotelCategoryRequest> hotelCategories;
    private String language;

}
