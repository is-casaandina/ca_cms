package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class SubscriberShortRequest {
	
	private String name;
	private String email;
	private String phone;

}
