package com.casaandina.cms.rest.request;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Data;

@Data
public class SubscriberPersonLegalRequest {

	private String name;
	private String surname;
	private String email;
	private String businessName;
	private String ruc;
	private Integer segment;
	private String phone;
	private String countryId;
	private String city;
	
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate birthdate;
	
	private Boolean sendNotification;
	private Boolean sendNew;
	private Integer gender;
}
