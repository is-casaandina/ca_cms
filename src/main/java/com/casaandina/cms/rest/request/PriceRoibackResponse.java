package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceRoibackResponse {

	private String language;
	
	@JsonProperty("price")
	private Double indicator;

}
