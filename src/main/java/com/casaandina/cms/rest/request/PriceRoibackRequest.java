package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceRoibackRequest {

	private String language;
	
	@JsonProperty("price")
	private Double indicator;

}
