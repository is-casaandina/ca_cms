package com.casaandina.cms.rest.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoibackPriceRequest {
    private String urlPricePen;
    private String urlPriceUsd;
    private String urlPriceBrl;
}
