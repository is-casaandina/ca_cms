package com.casaandina.cms.rest.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class SocialNetworkRequest {

	private String title;
	private String url;
	private String iconClass;

}
