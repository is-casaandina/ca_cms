package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class MenuRequest {
	
	private String name;
	
	private String type;
	
	private String parentId;
	
	private String action;
	
	private Integer order;
	
	private String languageCode;
	
	private String pageId;

	private Boolean sectionFooter;

	private String phone;

	private String email;

}
