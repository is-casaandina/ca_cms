package com.casaandina.cms.rest.request;

import lombok.Data;

@Data
public class DomainRequest {

    private String path;
    private Double tax;
    private String promotionAllId;
    private String language;
    private String currency;
    private Boolean hideLanguage;
}
