package com.casaandina.cms.rest.request;

import com.casaandina.cms.model.ContactConfigurations;
import lombok.Data;

@Data
public class SiteIdentityRequest {
    private String sitename;
    private String description;
    private String keywords;
    private String iconSite;
    private ContactConfigurations contact;
}
