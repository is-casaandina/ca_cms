package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScriptRequest {
	
	private String tracing;
	
	private String header;
	
	private String body;
	
	private String footer;

}
