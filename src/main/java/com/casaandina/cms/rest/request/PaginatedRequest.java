package com.casaandina.cms.rest.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PaginatedRequest {
    private Integer page;
    private Integer size;
    private String hotelId;
    private String category;
}
