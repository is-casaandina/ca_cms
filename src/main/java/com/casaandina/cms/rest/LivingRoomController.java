package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.LivingRoom;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.rest.request.LivingRoomRequest;
import com.casaandina.cms.rest.request.PaginatedRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.LivingRoomResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.service.LivingRoomService;
import com.casaandina.cms.service.PageService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.pagination.Page;
import com.casaandina.cms.util.pagination.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/living-rooms")
public class LivingRoomController {
	
	private final LivingRoomService livingRoomService;
	private final PageService pageService;
	
	@Autowired
	public LivingRoomController(LivingRoomService livingRoomService, PageService pageService) {
		this.livingRoomService = livingRoomService;
		this.pageService = pageService;
	}
	
	@PostMapping
	public GenericResponse<LivingRoomResponse> registerLivingRoom(@RequestBody final LivingRoomRequest livingRoomRequest)
			throws CaBusinessException {
		
		GenericResponse<LivingRoomResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(ObjectMapperUtils.map(this.livingRoomService.registerLivingRoom(
				ObjectMapperUtils.map(livingRoomRequest, LivingRoom.class)), LivingRoomResponse.class));
		
		return genericResponse;
				
	}
	
	@GetMapping("/{id}")
	public LivingRoomResponse getLivingRoom(@PathVariable("id") final String livingRoomId) throws CaRequiredException {
		return ObjectMapperUtils.map(this.livingRoomService.getLivingRoom(livingRoomId), LivingRoomResponse.class);
	}
	
	@PutMapping("/{id}")
	public GenericResponse<LivingRoomResponse> updateLivingRoom(@PathVariable("id") final String livingRoomId,
			@RequestBody final LivingRoomRequest livingRoomRequest) throws CaBusinessException {
		
		LivingRoom livingRoom = ObjectMapperUtils.map(livingRoomRequest, LivingRoom.class);
		livingRoom.setId(livingRoomId);
		
		GenericResponse<LivingRoomResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_UPDATE);
		genericResponse.setData(ObjectMapperUtils.map(this.livingRoomService.updateLivingRoom(livingRoom),
				LivingRoomResponse.class));
		
		return genericResponse;
	}
	
	@DeleteMapping("/{id}")
	public MessageResponse deleteLivingRoom(@PathVariable("id") final String livingRoomId) throws CaRequiredException {
		
		this.livingRoomService.deleteLivingRoom(livingRoomId);
		
		return MessageResponse.builder()
				.message(ConstantsUtil.SUCCESSFUL_DELETE)
				.build();
	}
	
	@GetMapping
	public Page<LivingRoomResponse> getAllTheLivingRoom(
			@RequestParam(value = "destination", required = false) final List<String> destinationsId,
			@RequestParam(value = "hotel", required = false) final List<String> hotelsId,
			@RequestParam(value = "capacity", required = false) final List<Integer> capacityRange, PaginatedRequest pageable) throws CaRequiredException {
		
		 org.springframework.data.domain.Page<LivingRoom> rooms = this.livingRoomService.getAllTheLivingRooms(
				destinationsId, hotelsId, capacityRange, PageUtil.getPageable(pageable.getPage(), pageable.getSize()));
		List<LivingRoom> listRoom = rooms.getContent();
		List<LivingRoomResponse> listRoomResponse = ObjectMapperUtils.mapAll(listRoom, LivingRoomResponse.class);
		return PageUtil.pageToResponse(rooms, listRoomResponse);

	}

	@GetMapping("/paginated")
	public Page<LivingRoomResponse> getAllTheLivingRoomPaginated(PaginatedRequest pageable) throws CaRequiredException {
		org.springframework.data.domain.Page<LivingRoom> rooms =
				this.livingRoomService.findLivingRoomsByHotelAndCategoryPaginated(PageUtil.getPageable(pageable.getPage(), pageable.getSize()),
						pageable.getHotelId(), pageable.getCategory());
		List<LivingRoom> listRoom = rooms.getContent();
		List<LivingRoomResponse> listRoomResponse = ObjectMapperUtils.mapAll(listRoom, LivingRoomResponse.class);
		return PageUtil.pageToResponse(rooms, this.setHotel(listRoomResponse));
	}

	private List<LivingRoomResponse> setHotel(List<LivingRoomResponse> livingRoomResponseList) {
		return livingRoomResponseList.stream()
				.map(this::buildLivingRoomResponse)
				.collect(Collectors.toList());
	}

	private LivingRoomResponse buildLivingRoomResponse(LivingRoomResponse livingRoomResponse) {
		Optional<PageHeader> pageHeader = this.pageService.findPageHeaderByPageId(livingRoomResponse.getHotelId());

		if (pageHeader.isPresent()) {
			livingRoomResponse.setHotel(pageHeader.get().getName());
		}

		return livingRoomResponse;
	}

}
