package com.casaandina.cms.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.casaandina.cms.model.Permission;
import com.casaandina.cms.service.PermissionService;

@RestController
@RequestMapping("/api/permissions")
public class PermissionController {
	
	private final PermissionService permissionService;
	
	@Autowired
	public PermissionController(PermissionService permissionService) {
		this.permissionService = permissionService;
	}
	
	@GetMapping
	public List<Permission> findAllActive(){
		return permissionService.findAllActive();
	}
}
