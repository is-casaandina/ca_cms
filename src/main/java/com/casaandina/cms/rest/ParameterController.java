package com.casaandina.cms.rest;

import com.casaandina.cms.dto.ParameterCodeValue;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Parameter;
import com.casaandina.cms.rest.request.ParameterRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.rest.response.ParameterResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.ParameterService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/parameters")
public class ParameterController {
	
	private final ParameterService parameterService;
	
	@Autowired
	public ParameterController(ParameterService parameterService) {
		this.parameterService = parameterService;
	}
	
	@PostMapping
	public GenericResponse<ParameterResponse> saveParameter(@RequestBody final ParameterRequest parameterRequest)
			throws CaRequiredException {
		
		GenericResponse<ParameterResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(ObjectMapperUtils.map(this.parameterService.createParameter(ObjectMapperUtils.map(
				parameterRequest, Parameter.class)), ParameterResponse.class));
		
		return genericResponse;
	}
	
	@GetMapping("/{id}")
	public ParameterResponse getParameter(@PathVariable("id") final String parameterId) throws CaRequiredException {
		return ObjectMapperUtils.map(this.parameterService.getParameter(parameterId), ParameterResponse.class);
	}
	
	@PutMapping("/{id}")
	public GenericResponse<ParameterResponse> updateParameter(@PathVariable("id") final String parameterId,
			@RequestBody final ParameterRequest parameterRequest) throws CaRequiredException {
		
		Parameter parameter = ObjectMapperUtils.map(parameterRequest, Parameter.class);
		
		GenericResponse<ParameterResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_UPDATE);
		genericResponse.setData(ObjectMapperUtils.map(this.parameterService.updateParameter(parameter), ParameterResponse.class));
		
		return genericResponse;
		
	}
	
	@DeleteMapping("/{id}")
	public MessageResponse deleteParameter(@PathVariable("id") final String parameterId) throws CaRequiredException {
		
		this.parameterService.deleteParameter(parameterId);
		
		return MessageResponse.builder()
				.message(ConstantsUtil.SUCCESSFUL_DELETE)
				.build();
	}
	
	@GetMapping
	public List<Parameter> getAllTheParameters() throws CaRequiredException {
		return this.parameterService.getAllTheParameters();
	}
	
	@GetMapping("/filters/{code}")
	public Object getStatesAll(@PathVariable final String code) throws CaRequiredException {
		return this.parameterService.getStatesAll(code).getIndicator();
	}

	@GetMapping("/filters/{code}/{language}")
	public Object getStatesByCodeAndLanguage(@PathVariable final String code, @PathVariable final String language) throws CaRequiredException {
		return this.parameterService.getStateByCodeAndLanguage(code, language).getIndicator();
	}

	@GetMapping("/select/living-room-category")
	public List<SelectResponse> getSelectLivingRoomCategory() throws CaRequiredException {
		List<SelectResponse> selectResponses = new ArrayList<>();
		Object indicator = this.parameterService.getStatesAll(ConstantsUtil.Parameters.LIVING_ROOM_CATEGORY).getIndicator();
		List<ParameterCodeValue> indicators = ObjectMapperUtils.mapAll((List)indicator, ParameterCodeValue.class);
		indicators.forEach(x ->
				selectResponses.add(
					SelectResponse.builder().value(x.getCode()).text(x.getValue()).build()
				)
		);
		return selectResponses.stream().sorted(Comparator.comparing(SelectResponse::getText)).collect(Collectors.toList());
	}

}
