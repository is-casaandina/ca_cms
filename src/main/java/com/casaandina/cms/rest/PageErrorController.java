package com.casaandina.cms.rest;

import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.model.PageError;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.rest.request.PageErrorRequest;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.PageErrorResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.PageErrorService;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.MessageProperties;
import com.casaandina.cms.util.ObjectMapperUtils;
import com.casaandina.cms.util.PropertiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/pages/error")
public class PageErrorController {

    private final PageErrorService pageErrorService;
    private final MessageProperties messageProperties;

    @Autowired
    public PageErrorController(PageErrorService pageErrorService, MessageProperties messageProperties) {
        this.pageErrorService = pageErrorService;
        this.messageProperties = messageProperties;
    }

    @GetMapping
    public List<PageErrorResponse> findAllByState(){
        return ObjectMapperUtils.mapAll(this.pageErrorService.findAllByState(), PageErrorResponse.class);
    }

    @PostMapping
    public GenericResponse<PageErrorResponse> insertPageError(@RequestBody PageErrorRequest pageErrorRequest)throws CaBusinessException {
        PageError pageError = ObjectMapperUtils.map(pageErrorRequest, PageError.class);
        PageErrorResponse pageErrorResponse = ObjectMapperUtils.map(this.pageErrorService.insertPageError(pageError), PageErrorResponse.class);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_INSERT_SUCCESSFUL), pageErrorResponse);
    }

    @PutMapping("/{id}")
    public GenericResponse<PageErrorResponse> updatePageError(@PathVariable String id, @RequestBody PageErrorRequest pageErrorRequest) throws CaBusinessException {
        PageError pageError = ObjectMapperUtils.map(pageErrorRequest, PageError.class);
        pageError.setId(id);
        PageErrorResponse pageErrorResponse = ObjectMapperUtils.map(this.pageErrorService.updatePageError(pageError), PageErrorResponse.class);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_UPDATE_SUCCESSFUL), pageErrorResponse);
    }

    @DeleteMapping("/{id}")
    public GenericResponse deletePageError(@PathVariable String id) throws CaBusinessException{
        this.pageErrorService.deletePageError(id);
        return CaUtil.getResponseGeneric(messageProperties.getMessage(PropertiesConstants.MESSAGE_ERROR_PAGE_DELETE_SUCCESSFUL));
    }

    @GetMapping("/select")
    public List<SelectResponse> findAllPageError(){
        List<PageHeader> listPageHeader = this.pageErrorService.findAllPageError();
        if(Objects.nonNull(listPageHeader) && !listPageHeader.isEmpty()){
            List<SelectResponse> selectResponses = new ArrayList<>();
            listPageHeader.forEach(x -> selectResponses.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build()));
            return selectResponses;
        }
        return Collections.emptyList();
    }
}
