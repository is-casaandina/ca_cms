package com.casaandina.cms.rest;

import com.casaandina.cms.dto.CountryRequest;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Country;
import com.casaandina.cms.rest.response.CountryResponse;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.casaandina.cms.rest.response.SelectResponse;
import com.casaandina.cms.service.CountryService;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryController {
	
	private final CountryService countryService;
	
	@Autowired
	public CountryController(CountryService countryService) {
		this.countryService = countryService;
	}
	
	@PostMapping
	public GenericResponse<CountryResponse> saveCountry(@Valid @RequestBody final CountryRequest countryRequest) throws CaBusinessException, CaRequiredException {
		
		GenericResponse<CountryResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_REGISTER);
		genericResponse.setData(ObjectMapperUtils.map(this.countryService.saveCountry(ObjectMapperUtils.map(countryRequest, Country.class)), CountryResponse.class));
		return genericResponse;
	}
	
	@GetMapping("/{id}")
	public CountryResponse getCountry(@PathVariable("id") final String id) throws CaRequiredException {
		return ObjectMapperUtils.map(this.countryService.getCountry(id), CountryResponse.class);
	}
	
	@PutMapping("/{id}")
	public GenericResponse<CountryResponse> updateCountry(@PathVariable("id") final String countryId,
			@Valid @RequestBody final CountryRequest countryRequest) throws CaBusinessException, CaRequiredException {
		
		GenericResponse<CountryResponse> genericResponse = new GenericResponse<>();
		genericResponse.setMessage(ConstantsUtil.SUCCESSFUL_UPDATE);
		genericResponse.setData(ObjectMapperUtils.map(this.countryService.updateCountry(countryId, ObjectMapperUtils.map(countryRequest, Country.class)), CountryResponse.class));
		return genericResponse;
	}
	
	@DeleteMapping("/{id}")
	public MessageResponse deleteCountry(@PathVariable("id") final String countryId) throws CaRequiredException {
		this.countryService.deleteCountry(countryId);
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setMessage(ConstantsUtil.SUCCESSFUL_DELETE);
		return messageResponse;
	}
	
	@GetMapping("code/{code}")
	public CountryResponse getCountryByCode(@PathVariable("code") final String countryCode) throws CaRequiredException {
		return ObjectMapperUtils.map(this.countryService.getCountryByCode(countryCode), CountryResponse.class);
	}
	
	@GetMapping
	public List<CountryResponse> getCountriesList() throws CaBusinessException {
		return ObjectMapperUtils.mapAll(this.countryService.getCountriesList(), CountryResponse.class);
	}

	@GetMapping("/select")
	public List<SelectResponse> getCountriesAll(){
		List<Country> listCountry = this.countryService.customFindAllByStatus();
		List<SelectResponse> listSelectResponse = new ArrayList<>();
		listCountry.forEach(x ->
			listSelectResponse.add(SelectResponse.builder().value(x.getId()).text(x.getName()).build())
		);
		return listSelectResponse;
	}
}
