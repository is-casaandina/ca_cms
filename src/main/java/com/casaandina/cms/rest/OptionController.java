package com.casaandina.cms.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.casaandina.cms.dto.OptionTo;
import com.casaandina.cms.exception.CaBusinessException;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.Option;
import com.casaandina.cms.rest.response.OptionResponse;
import com.casaandina.cms.service.OptionService;

@RestController
@RequestMapping("/api/options")
public class OptionController {
	
	private final OptionService optionService;
	
	private final UserAuthentication userAuthentication;
	
	@Autowired
	public OptionController(OptionService optionService, UserAuthentication userAuthentication) {
		this.optionService = optionService;
		this.userAuthentication = userAuthentication;
	}
	
	@PostMapping
	public Option saveOption(@RequestBody OptionTo optionTo) throws CaRequiredException, CaBusinessException {
		optionTo.setUsername(userAuthentication.getUserPrincipal().getUsername());
		return optionService.saveOption(optionTo);
	}	
	
	@PostMapping("/move")
	public Option moveOption(@RequestBody OptionTo optionTo) throws CaRequiredException, CaBusinessException {
		optionTo.setUsername(userAuthentication.getUserPrincipal().getUsername());
		return optionService.moveOption(optionTo);
	}
	
	@GetMapping("/test")
	public List<OptionResponse> getOptions() throws CaRequiredException, CaBusinessException {
		return optionService.getOptions();
	}
	
	@PostMapping("/rename")
	public OptionResponse renameOption(@RequestBody OptionResponse optionResponse) throws CaRequiredException, CaBusinessException {
		optionResponse.setUsername(userAuthentication.getUserPrincipal().getUsername());
		return optionService.renameOption(optionResponse);
	}
	
	@DeleteMapping("/{id}")
	public void deleteOption(@PathVariable Integer id) throws CaBusinessException {
		String username = userAuthentication.getUserPrincipal().getUsername();
		optionService.deleteOption(id,username);
	}
	
	@PostMapping("/duplicate")
	public Option duplicateOption(@RequestBody OptionResponse optionResponse) throws CaBusinessException {
		String username = userAuthentication.getUserPrincipal().getUsername();
		optionResponse.setUsername(username);
		optionResponse.setOwner(username);
		return optionService.duplicateOption(optionResponse);
	}
}
