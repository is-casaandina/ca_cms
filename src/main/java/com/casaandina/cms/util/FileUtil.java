package com.casaandina.cms.util;

public class FileUtil {
    public static String getExtension(String filename) {
        String extension = "";
        try {
            extension = filename.substring(filename.lastIndexOf(".")+1).toUpperCase();
        } catch (Exception e) {
            extension = "";
        }
        return extension;
    }
}
