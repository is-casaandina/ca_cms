package com.casaandina.cms.util;

import org.springframework.http.HttpStatus;

public class ConstantsUtil {
	
	private ConstantsUtil() {
		super();
	}
	
	public static final Integer INTERNAL_SERVER_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();
	public static final String CODE_ISO_PERU = "PE";
	public static final String TIMEZONE_UTC = "UTC";
	public static final String URI_EXCEPTION_SITE = "uri=/site";
	public static final String WELCOME_TO_CASA_ANDINA = "Bienvenido a Casa Andina";
	public static final String REQUEST_ORIGIN = "origin";
	public static final String DOMAIN_ALL = "111111111111111111111111";

	public static final Integer ZERO = 0;
	public static final Integer ONE = 1;
	public static final Integer TWO = 2;
	public static final Integer THREE = 3;
	public static final Integer LIMIT = 500;
	public static final Integer DEFAULT_PAGE = 0;
    public static final Integer DEFAULT_SIZE = 10;
    public static final String PRE_ROLE = "R";
    public static final String PRE_PERMISSION = "P";
    public static final String SEPARATOR = "/";
    public static final String DATA = "data:";
    public static final String BASE64 = ";base64, ";  
    public static final String FORMAT_IMAGE_PNG = "image/png";
    public static final String FORMAT_IMAGE_JPG = "image/jpeg";
    public static final Integer ROUND_TWO_DECIMALS = 2;
    public static final String EMPTY = "";
    public static final String SPACE = " ";
    public static final String POINT = ".";
    
    public static final String ROLE_SEQUENCE = "roleSequence";
    public static final String USER_SEQUENCE = "userSequence";
    public static final String OPTION_SEQUENCE = "optionSequence";
    public static final String COUNTRY_SEQUENCE = "countrySequence";
    public static final String CLUSTER_SEQUENCE = "clusterSequence";
    public static final String CURRENCY_SEQUENCE = "currencySequence";
    public static final String ICONS3_SEQUENCE = "iconS3Sequence";
	public static final String PAGE_NOTIFICATION_SEQUENCE = "pageNotificationSequence";

    public static final String REVINATE_USERNAME = "X-Revinate-Porter-Username";
    public static final String REVINATE_TIMESTAMP = "X-Revinate-Porter-Timestamp";
    public static final String REVINATE_KEY = "X-Revinate-Porter-Key";
    public static final String REVINATE_ENCODED = "X-Revinate-Porter-Encoded";
    public static final String REVINATE_LANGUAGE_PARAMETER = "?languageSlug=";
    public static final String REVINATE_SIZE_PARAMETER = "&size=";
    public static final String REVINATE_RATING_PARAMETER = "&ratings=";
    public static final String REVINATE_SORT_PARAMETER = "&sort=";
    public static final String REVINATE_SELF_VALUE = "self";
    public static final String COMMA = ",";
	public static final String REVINATE_OPINIONS = "revinate-opinions";
	public static final String REVINATE_RATINGS = "revinate-ratings";
	public static final String REVINATE_SYNCHRONIZATION = "Se ha sincronizado correctamente";
	public static final String POSITION = "Puesto ";
	public static final String TRIP_ADVISOR = "TripAdvisor";
	
	public static final String ID = "id";
	public static final String CREATED_DATE = "createdDate";
	
	public static final String SOCIAL_NETWORKS = "Redes Sociales";
	public static final String SOCIAL_NETWORKS_CODE = "son";

	public static final String AN_ERROR_OCCURRED = "An error occurred: %s";
	public static final String SUCCESSFUL_GENERATE =  "se ha generado correctamente";
	public static final String SUCCESSFUL_REGISTER = "Se ha registrado correctamente";
	public static final String SUCCESSFUL_UPDATE = "Se ha actualizado correctamente";
	public static final String SUCCESSFUL_DELETE = "Se ha eliminado correctamente";
	
	public static final String REGISTERING_OPINIONS_BY_HOTEL = "Registrando opiniones por hotel";
	public static final String REGISTERING_RATINGS_BY_HOTEL = "Registrando puntuaciones por hotel";
	public static final String OFFER_DESCRIPTION = "TOP 5% de los Hoteles en Arequipa";
	
	public static final String NOT_FOUND_ID = "El Id no existe";
	public static final String NOT_FOUND_CODE = "El c\u00f3digo no existe";
	public static final String NOT_FOUND_SERVICE = "El servicio no esta disponible";
	
	public static final String SPANISH_LANGUAGE_CODE = "es";

	public static final String LETTER_SIZE_IMAGE_EXTRA_SMALL = "XS";
	public static final String LETTER_SIZE_IMAGE_SMALL = "S";
	public static final String LETTER_SIZE_IMAGE_MEDIUM = "M";
	public static final String LETTER_SIZE_IMAGE_LARGE = "L";
	public static final String FILE_NAME_EXTRA_SMALL = "-xsmall";

	public static final String PNG = "PNG";
	public static final String EXTENSION_PNG = ".png";
	public static final String EXTENSION_JPG = ".jpg";
	
	public static final Integer TYPE_IMAGE = 1;
	public static final Integer TYPE_VIDEO = 2;
	public static final Integer TYPE_DOCUMENT = 3;
	public static final Integer TYPE_JS = 3;
	
	public static final long SIZE_MB = 1048576L;
	public static final String SYMBOL_MB = "MB";
	
	public static final String DUPLICATE_DATA = "No se puede duplicar datos";
	public static final String[] ARRAYS_ICONS_FILES = new String[]{"selection.json","fonts/","fonts/Casa-Andina-Icons.ttf","fonts/Casa-Andina-Icons.woff","fonts/Casa-Andina-Icons.eot","fonts/Casa-Andina-Icons.svg","style.css"};
	public static final String STYLE_CSS = "style.css";
	public static final int BUFFER_LENGTH = 4096;
	public static final String USER_DIR = "user.dir";

	public static final Boolean ACTIVE = true;
	public static final Boolean INACTIVE = false;
	public static final String DESCRIPTOR_ALL = "ALL";

	public static class MongoDb {
		public static class Collection {
			public static final String MENU = "menu";
			public static final String PAGE_NOTIFICATION = "pageNotification";
			public static final String LIVING_ROOM = "livingRoom";
		}
	}

	public static class Page {
		public static final String DEFAULT_NAME = "T\u00edtulo P\u00e1gina";
		public static final String HOTELES = "Hoteles";
		public static final String RESTAURANTS = "Restaurantes";
		public static final String SEPARATOR_GUION_WITH_SPACES = " - ";
		public static final Integer COUNT_LANGUAGES = 3;
		public static final String PROMOTION_HOTELES = "Hoteles";
		public static final String PROMOTION_RESTAURANTES = "Restaurantes";
		public static final String PROMOTION_CAMPANIAS = "Campa\u00f1as";
	}

	public static class Parameters {
		public static final String LIVING_ROOM_CATEGORY = "living-room-category";
	}

	public static class RoibackWebService {
		public static final String CON_DISPO = "con_dispo";
		public static final String BEDROOMS = "habitaciones";
		public static final String RATES = "tarifas";
		public static final String ACTIVITIES = "ocupaciones";
		public static final String PENSIONS = "pensiones";
		public static final String NHALF_PRICE = "nprecio_medio";
		public static final String REGISTERING_ROIBACK_PRICES_BY_HOTEL = "Registrando precios roiback por hotel";
	}

	public static final String[] USER_AGENTS = new String[]{"*","Googlebot", "Googlebot-Image", "MSNBot", "Slurp", "Lycos", "Seekbot", "Sistrix", "SearchmetricsBot", "BacklinkCrawler", "SEOkicks-Robot", "xovi"};

	public static class SiteMapXml {
		public static final String POST = "post";
		public static final String SUBGUION = "_";
		public static final String SITE_MAP = "sitemap";
		public static final String CONTENT_TYPE = "application/xml";
		public static final String CONTENT_TYPE_TXT = "text/plain";
		public static final String EXTENSION_TXT = ".txt";
		public static final String EXTENSION = ".xml";
		public static final String ROBOTS_NAME = "robots";

	}

	public static class CurrencyType {
		public static final String USD_CURRENCY = "USD";
		public static final String PEN_CURRENCY = "PEN";
	}

	public static class PageErrorJson {
		public static final String CONTENT_TYPE = "application/json";
		public static final String EXTENSION_JSON = ".json";
	}

	public static class LivingRoomCategory {
		public static final String CORPORATE = "1";
		public static final String SOCIAL = "2";
		public static final String VARIOUS = "3";
	}

	public static class HostRequest {
		public static final String TWO_POINTS = ":";
		public static final String TWO_SEPARATOR = "//";
	}

	public static final String PROMOTION_TYPE_EXCLUSION = "5d8556f0a504035348f2a9b0";
}
