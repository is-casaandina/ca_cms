package com.casaandina.cms.util;

public enum PermissionType {

    PAGE("P003", "Paginas"),
    ERROR("P005", "Error"),
    PROMOTIONS("P007", "Promociones"),
    HOTELS("P008", "Hoteles"),
    DESTINATIONS("P009", "Destinos"),
    RESTAURANTS("P010", "Restaurantes"),
    EVENTS("P012", "EVentos");

    private String code;
    private String value;

    private PermissionType(String code, String value){
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
