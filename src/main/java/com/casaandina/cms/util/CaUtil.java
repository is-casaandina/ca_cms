package com.casaandina.cms.util;

import com.amazonaws.util.IOUtils;
import com.casaandina.cms.dto.DimensionDto;
import com.casaandina.cms.dto.ResizeImageProperties;
import com.casaandina.cms.model.Image;
import com.casaandina.cms.rest.response.GenericResponse;
import com.casaandina.cms.rest.response.MessageResponse;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.MappingJacksonValue;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CaUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(CaUtil.class);
	
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	public static final String DATE_PATTERN_ISO_LOCAL_DATE = "yyyy-MM-dd";
	public static final String DATE_PATTERN_GUION = "dd-MM-yyyy";

	private static final String MSG_ERROR_STRING = "Ocurrio un error al convertir fecha: %s";

	private CaUtil() {
		super();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> GenericResponse<T> getResponseGeneric(String message, T data) {
		GenericResponse<T> responseGeneric = new GenericResponse();
		responseGeneric.setMessage(message);
		responseGeneric.setData(data);
		return responseGeneric;
	}

	public static GenericResponse getResponseGeneric(String message) {
		return GenericResponse.builder()
			.message(message)
			.build();
	}

	public static MessageResponse getMessgeResponse(String message) {
		return MessageResponse.builder()
			.message(message)
			.build();
	}
	
	public static Date currentDate(){
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN);
			Calendar calCurrentDate = Calendar.getInstance();
			dfDate.setTimeZone(calCurrentDate.getTimeZone());
			String strCurrentDate = dfDate.format(calCurrentDate.getTime());
			return dfDate.parse(strCurrentDate);
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	public static String currentDateString(){
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN);
			Calendar calCurrentDate = Calendar.getInstance();
			dfDate.setTimeZone(calCurrentDate.getTimeZone());
			return dfDate.format(calCurrentDate.getTime());
		} catch (Exception e) {
			return "";
		}
	}
	
	public static Date convertStringtoDate(String templateDate) {
		Date newDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN);
		try {
			if (templateDate!= null && !templateDate.isEmpty()) {
				newDate = simpleFormat.parse(templateDate);
			}
		} catch(Exception ex) {
			logger.error(String.format(MSG_ERROR_STRING, ex.getMessage()));
		}
		return newDate;
	}

	public static Date convertStringtoDateFormatGuion(String templateDate) {
		Date newDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN_GUION);
		try {
			if (templateDate!= null && !templateDate.isEmpty()) {
				newDate = simpleFormat.parse(templateDate);
			}
		} catch(Exception ex) {
			logger.error(String.format(MSG_ERROR_STRING, ex.getMessage()));
		}
		return newDate;
	}

	public static Date addDateFormatGuion(Date objDate, int daysAdd){
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN_GUION);
			Calendar calCurrentDate = Calendar.getInstance();
			calCurrentDate.setTime(objDate);
			calCurrentDate.add(Calendar.DATE, daysAdd);
			String strCurrentDate = dfDate.format(calCurrentDate.getTime());
			return dfDate.parse(strCurrentDate);
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	public static LocalDate convertStringtoLocalDate(String templateDate) {
		LocalDate newDate = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN_ISO_LOCAL_DATE);
		try {
			if (templateDate!= null && !templateDate.isEmpty()) {
				newDate = LocalDate.parse(templateDate, formatter);
			}
		} catch(Exception ex) {
			logger.error(String.format(MSG_ERROR_STRING, ex.getMessage()));
		}
		return newDate;
	}
	
	public static String convertDateToString(Date templateDate) {
		String strDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN);
		try {
			if (templateDate!= null ) {
				strDate = simpleFormat.format(templateDate);
			}
		} catch(Exception ex) {
			logger.error(String.format("Ocurrio un error al convertir fecha a texto: %S", ex.getMessage()));
		}
		return strDate;
	}
	
	public static Boolean validateListIsNotNullOrEmpty(List<?> lstObj) {
		return lstObj != null && !lstObj.isEmpty();
	}
	
	public static String generateCode(String code, int sequence) {
        if (sequence < 10)
            code = code.concat("00") + String.valueOf(sequence);
        else if (sequence < 99)
            code = code.concat("0") + String.valueOf(sequence);
        else
            code = String.valueOf(sequence);
        return code;
    }
	
	public static DimensionDto getOriginalImageSize(String fileName) throws IOException {
		BufferedImage bimg = ImageIO.read(new File(fileName));
		int widthOrigin = bimg.getWidth();
		int heightOrigin = bimg.getHeight();
		return new DimensionDto(widthOrigin, heightOrigin);
	}
	
	public static String encodeFileInBase64(InputStream inputStream, String formatName) {
        
        StringBuilder stringBuilder = new StringBuilder(ConstantsUtil.DATA);
        stringBuilder.append(formatName).append(ConstantsUtil.BASE64);
        
        try {
            byte[] bytes = IOUtils.toByteArray(inputStream);
            stringBuilder.append(new String(new Base64().encode(bytes), StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.getMessage();
        }
        
        return stringBuilder.toString();
    }
	
	public static String calculateHMAC(final String secret, final String username, final String timestamp) {
		String data = username + timestamp;
		
        try {
            SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
 
            StringBuilder encoded = new StringBuilder();
            for (final byte element : rawHmac) {
            	encoded.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
            }
            return encoded.toString();
        } catch (GeneralSecurityException e) {
            throw new IllegalArgumentException();
        }
	}
        
	public static MappingJacksonValue getMappingJacksonValue(String[] fields, String filterProvider, Object obj) {
		
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept(fields);

		FilterProvider filters = new SimpleFilterProvider().addFilter(filterProvider, filter);

		MappingJacksonValue mapping = new MappingJacksonValue(obj);

		mapping.setFilters(filters);

		return mapping;
	}
	
	public static String convertLocalDateTimeToString(LocalDateTime date) {
		try {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN_ISO_LOCAL_DATE);

	        return date.format(formatter);
		} catch (Exception e) {
			return "";
		}
	}
	
	public static BigDecimal round(BigDecimal d, int scale, boolean roundUp) {
		int mode = (roundUp) ? BigDecimal.ROUND_UP : BigDecimal.ROUND_DOWN;
		return d.setScale(scale, mode);
	}
	
	public static Double roundDouble(Double d, int scale, boolean roundUp) {

		try {
			int mode = (roundUp) ? BigDecimal.ROUND_HALF_UP : BigDecimal.ROUND_HALF_UP;

			BigDecimal value = BigDecimal.valueOf(d);
			value = value.setScale(scale, mode);

			return value.doubleValue();
		} catch(Exception e){
			return null;
		}
		
	}
	
	public static Boolean validateEmail(String email) {
		Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
 
        Matcher matcher = pattern.matcher(email);
        
        return matcher.find();
	}
	
	public static boolean validateIsNull(Object o) {
		return o == null;
	}
	
	public static boolean validateIsNotNull(Object o) {
		return !validateIsNull(o);
	}
	
	public static boolean validateIsEmpty(Object o) {
		String d = o.toString();
		return d.trim().length() == 0;
	}
	
	public static boolean validateIsNotEmpty(Object o) {
		return !validateIsEmpty(o);
	}
	
	public static boolean validateIsNullOrEmpty(Object o) {
		return (validateIsNull(o) || validateIsEmpty(o));
	}
	
	public static boolean validateIsNotNullAndNotEmpty(Object o) {
		return (validateIsNotNull(o) && validateIsNotEmpty(o));
	}
	
	public static boolean validateIsPositiveNumber(Integer num) {
		return num > 0;
	}
	
	public static boolean validateIsPositiveNumber(Long num) {
		return num > 0;
	}
	
	public static boolean validateIsPositiveNumber(BigDecimal num) {
		return BigDecimal.ZERO.compareTo(num)<0;
	}
	
	public static boolean validateIsNotNullAndPositive(Long num){
		return (validateIsNotNull(num) && validateIsPositiveNumber(num));
	}
	
	public static boolean validateIsNotNullAndPositive(BigDecimal num){
		return (validateIsNotNull(num) && validateIsPositiveNumber(num));
	}
	
	public static boolean validateIsNotNullAndPositive(Integer num){
		return (validateIsNotNull(num) && validateIsPositiveNumber(num));
	}
	
	public static String[] getImageSizePercentage() {
		return new String[] {ConstantsUtil.LETTER_SIZE_IMAGE_SMALL,ConstantsUtil.LETTER_SIZE_IMAGE_MEDIUM,ConstantsUtil.LETTER_SIZE_IMAGE_LARGE, ConstantsUtil.LETTER_SIZE_IMAGE_EXTRA_SMALL};
	}
	
	public static Image getImageSizeByPercentage(String size, DimensionDto imageSize, ResizeImageProperties resizeImageProperties){
        if (size.equalsIgnoreCase(ConstantsUtil.LETTER_SIZE_IMAGE_SMALL)) {
			Integer width = (int) (Double.valueOf(imageSize.getWidth()) / Double.valueOf(resizeImageProperties.getSmall().getWidth()));
			Integer height = (int) (Double.valueOf(imageSize.getHeigth()) / Double.valueOf(resizeImageProperties.getSmall().getHeight()));
			return new Image(width, height);
		} else if (size.equalsIgnoreCase(ConstantsUtil.LETTER_SIZE_IMAGE_MEDIUM)){
        	Integer width = (int)(Double.valueOf(imageSize.getWidth())/Double.valueOf(resizeImageProperties.getMedium().getWidth()));
        	Integer height = (int) (Double.valueOf(imageSize.getHeigth())/Double.valueOf(resizeImageProperties.getMedium().getHeight()));
        	return new Image(width, height);
		} else if (size.equalsIgnoreCase(ConstantsUtil.LETTER_SIZE_IMAGE_EXTRA_SMALL)){
			Integer width = (int)(Double.valueOf(imageSize.getWidth())/Double.valueOf(resizeImageProperties.getExtraSmall().getWidth()));
			Integer height = (int) (Double.valueOf(imageSize.getHeigth())/Double.valueOf(resizeImageProperties.getExtraSmall().getHeight()));
			return new Image(width, height);
		} else  if(size.equalsIgnoreCase(ConstantsUtil.LETTER_SIZE_IMAGE_LARGE))
            return new Image(imageSize.getWidth(),imageSize.getHeigth());

        return null;
    }
	
	public static int getLastDayMonth(int year, int month) {
		Calendar cal=Calendar.getInstance();
		cal.set(year, month-1, 1);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static String getTemplateTypeName(String code){
		TemplateType templateType = TemplateType.get(code);
		return templateType.getValue();
	}

	public static Date getDate(String dateStr, String format) {
		final DateFormat formatter = new SimpleDateFormat(format);
		try {
			return formatter.parse(dateStr);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String replaceEspecialCharacters(String especialCharacters){
		especialCharacters = especialCharacters.replace("\\", "");
		return especialCharacters.replaceAll("[-+^$()!&*%#@!/\"]*", "");
	}

	public static String extractIp(HttpServletRequest request) {
		String clientIp;
		String clientXForwardedForIp = request.getHeader("x-forwarded-for");
		if (Objects.nonNull(clientXForwardedForIp)) {
			clientIp = parseXForwardedHeader(clientXForwardedForIp);
		} else {
			clientIp = request.getRemoteAddr();
		}

		return clientIp;
	}

	private static String parseXForwardedHeader(String header) {
		return header.split(" *, *")[0];
	}

	public static String getHostAndServerNameFromRequest(HttpServletRequest request){

		if(Objects.nonNull(request.getHeader(ConstantsUtil.REQUEST_ORIGIN))) {
			StringBuilder sbUriHost = new StringBuilder(request.getHeader(ConstantsUtil.REQUEST_ORIGIN));

			String uriHost = sbUriHost.toString();
			if (!ConstantsUtil.SEPARATOR.equals(uriHost.substring(uriHost.length() - ConstantsUtil.ONE))) {
				sbUriHost.append(ConstantsUtil.SEPARATOR);
			}

			return sbUriHost.toString();
		}
		return ConstantsUtil.EMPTY;
	}

	public static String evaluatePath(String path){

		if(CaUtil.validateIsNotNullAndNotEmpty(path) && !ConstantsUtil.SEPARATOR.equals(path)
				&& path.length() >= ConstantsUtil.THREE) {
			Map<String, String> languagesMap = new HashMap<>();
			languagesMap.put(LanguageType.SPANISH.getCode(), LanguageType.SPANISH.getCode() + ConstantsUtil.SEPARATOR);
			languagesMap.put(LanguageType.ENGLISH.getCode(), LanguageType.ENGLISH.getCode() + ConstantsUtil.SEPARATOR);
			languagesMap.put(LanguageType.PORTUGUESE.getCode(), LanguageType.PORTUGUESE.getCode() + ConstantsUtil.SEPARATOR);

			String pathThree = path.substring(ConstantsUtil.ZERO, ConstantsUtil.THREE);

			if(!(pathThree.equals(languagesMap.get(LanguageType.SPANISH.getCode())) ||
					pathThree.equals(languagesMap.get(LanguageType.ENGLISH.getCode())) ||
					pathThree.equals(languagesMap.get(LanguageType.PORTUGUESE.getCode())))){
				return languagesMap.get(LanguageType.SPANISH.getCode()) + path;
			}
		}
		return path;
	}

	public static Boolean isAfterOrEqualDate(LocalDateTime date){
		LocalDateTime today = LocalDateTime.now();
		return date.isAfter(today) || today.isEqual(date);
	}

	public static Boolean isBeforeOrEqualDate(LocalDateTime date){
		LocalDateTime today = LocalDateTime.now();
		return date.isBefore(today) || today.isEqual(date);
	}

	public static Boolean isAfterDate(LocalDateTime date){
		LocalDateTime today = LocalDateTime.now();
		return date.isAfter(today);
	}

	public static Boolean isBeforeDate(LocalDateTime date){
		LocalDateTime today = LocalDateTime.now();
		return date.isBefore(today);
	}

}
