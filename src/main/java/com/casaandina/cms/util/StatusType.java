package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum StatusType {

	ACTIVE(Integer.valueOf(1), "ACTIVO"), 
	INACTIVE(Integer.valueOf(0), "INACTIVO");

	private Integer code;
	private String value;
	
	private static final List<StatusType> list=new ArrayList<>();
	
	private static final Map<Integer, StatusType> lookup=new HashMap<>();
	
	static{
		for(StatusType issu : StatusType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	public static StatusType get(Integer code) {
		return lookup.get(code);
	}

	private StatusType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
