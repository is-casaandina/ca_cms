package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum GenderType {

	MALE(Integer.valueOf(1), "Masculino"),
	FEMALE(Integer.valueOf(2), "Femenino"),
	OTHER(Integer.valueOf(3), "Otro");

	private Integer code;
	private String value;

	private static final List<GenderType> list=new ArrayList<>();

	private static final Map<Integer, GenderType> lookup=new HashMap<>();

	static{
		for(GenderType issu : GenderType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}

	public static GenderType get(Integer code) {
		return lookup.get(code);
	}

	private GenderType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
