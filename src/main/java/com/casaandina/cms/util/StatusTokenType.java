package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum StatusTokenType {

	PENDING(Integer.valueOf(1), "PENDIENTE"),

	VERIFIED(Integer.valueOf(2), "VERIFICADO");

	protected static final List<StatusTokenType> list = new ArrayList<>();

	protected static final Map<Integer, StatusTokenType> lookup = new HashMap<>();

	static {
		for (StatusTokenType c : StatusTokenType.values()) {
			lookup.put(c.getCode(), c);
			list.add(c);
		}
	}

	private Integer code;

	private String value;

	private StatusTokenType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public static List<StatusTokenType> getList() {
		return list;
	}

}
