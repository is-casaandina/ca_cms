package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SubscriberType {

	CONTACT("1", "Contacto"),
	RESERVATION("2", "Reservas"),
	CLAIMS("3", "Reclamos"),
	NEWSLETTER("4", "Newsletter"),
	QUOTE("5", "Cotizaci\u00f3n");

	private String code;
	private String value;
	
	private static final List<SubscriberType> list=new ArrayList<>();
	
	private static final Map<String, SubscriberType> lookup=new HashMap<>();
	
	static{
		for(SubscriberType issu : SubscriberType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	public static SubscriberType get(String code) {
		return lookup.get(code);
	}

	private SubscriberType(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public static List<SubscriberType> getList(){
		return list;
	}
}
