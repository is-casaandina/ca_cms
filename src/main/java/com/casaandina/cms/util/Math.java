package com.casaandina.cms.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Math {
	
	public static Double setDecimals(final Double number, final int numberOfDecimals) {
		BigDecimal bigDecimal = BigDecimal.valueOf(number).setScale(numberOfDecimals, RoundingMode.HALF_UP);
		
		return bigDecimal.doubleValue();
	}

}
