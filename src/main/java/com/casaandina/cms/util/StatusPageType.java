package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum StatusPageType {

	PUBLISHED(Integer.valueOf(1), "PUBLICADA"),
	IN_EDITION(Integer.valueOf(2), "EN EDICI\u00d3N"),
	DELETED(Integer.valueOf(3), "DESPUBLICADA"),
	DRAFT(Integer.valueOf(4), "BORRADOR");

	private Integer code;
	private String value;
	
	private static final List<StatusPageType> list=new ArrayList<>();
	private static final List<Integer> listCode = new ArrayList<>();
	
	private static final Map<Integer, StatusPageType> lookup=new HashMap<>();
	
	static{
		for(StatusPageType issu : StatusPageType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
			listCode.add(issu.getCode());
		}
	}
	
	public static StatusPageType get(Integer code) {
		return lookup.get(code);
	}

	private StatusPageType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public static List<Integer> getListCode(){
		return listCode;
	}
}
