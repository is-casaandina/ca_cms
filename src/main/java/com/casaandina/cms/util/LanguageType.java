package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum LanguageType {
	
	ENGLISH("en", "Ingl\u00e9s"),
	SPANISH("es", "Espa\u00f1ol"),
	PORTUGUESE("pt", "Portugu\u00e9s");

	private String code;
	private String value;
	
	private static final List<LanguageType> list=new ArrayList<>();
	
	private static final Map<String, LanguageType> lookup=new HashMap<>();
	
	static{
		for(LanguageType issu : LanguageType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	public static LanguageType get(String code) {
		return lookup.get(code);
	}

	private LanguageType(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public static List<LanguageType> getList(){
		return list;
	}

}
