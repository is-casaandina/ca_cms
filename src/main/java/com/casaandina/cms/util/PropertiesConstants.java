package com.casaandina.cms.util;

public class PropertiesConstants {

	private PropertiesConstants() {
		super();
	}

	/* User */
	public static final String VALID_USER_REQUIRED = "valid.user.required";
	public static final String VALID_USER_NOT_REGISTERED_UPDATE = "valid.user.not.registered.update";
	public static final String VALID_USER_NOT_REGISTERED = "valid.user.not.registered";
	public static final String VALID_USER_REGISTERED_EXIST = "valid.user.registered.exist";
	public static final String VALID_USER_MAIL_REQUIRED = "valid.user.mail.required";
	public static final String VALID_USER_MAIL_NOT_EXIST = "valid.user.mail.not.exist";
	public static final String MESSAGE_USER_MODIFIED_PASS = "message.user.modified.password";
	public static final String VALID_USER_EMPTY_PASS = "valid.user.empty.password";
	public static final String MESSAGE_USER_SAVE_SUCCESS = "message.user.save.success";
	public static final String MESSAGE_USER_UPDATE_SUCCESS = "message.user.update.success";
	public static final String MESSAGE_USER_DELETE_SUCCESS = "message.user.delete.success";
	public static final String MESSAGE_USER_INVALID = "message.user.invalid";

	/* Role */
	public static final String VALID_ROLE_NOT_EXIST = "valid.role.not.exist";
	public static final String VALID_ROLE_CODE_REQUIRED = "valid.role.code.required";
	public static final String VALID_ROLE_CODE_INVALID = "valid.role.code.invalid";
	public static final String VALID_ROLE_NAME_REQUIRED = "valid.role.name.required";
	public static final String VALID_ROLE_NOT_SELECTED = "valid.role.not.selected";
	public static final String VALID_ROLE_NAME_INCORRECT = "valid.role.name.incorrect";
	public static final String VALID_ROLE_NAME_EXIST = "valid.role.name.exist";
	public static final String SAVE_ROLE_EXCEPTION_ERROR = "save.role.exception.error";
	public static final String UPDATE_ROLE_NAME_EXIST = "update.role.name.exist";
	public static final String DELETE_ROLE_EXCEPTION_ERROR = "delete.role.exception.error";
	public static final String DELETE_ROLE_NAME_NOT_EXIST = "delete.role.name.not.exist";
	public static final String DELETE_ROLE_ASSIGN_USER_VALID = "delete.role.assign.user.valid";
	public static final String SAVE_ROLE_SUCCESFUL = "save.role.succesful";
	public static final String UPDATE_ROLE_SUCCESFUL = "update.role.succesful";
	public static final String DELETE_ROLE_SUCCESFUL = "delete.role.succesful";

	/* Permission */
	public static final String VALID_PERMISSION_CODE_INVALID = "valid.permission.code.invalid";

	/* Option */
	public static final String VALID_OPTION_REQUIRED = "valid.option.required";
	public static final String VALID_OPTION_CODE_PARENT_INVALID = "valid.option.code.parent.invalid";
	public static final String VALID_OPTION_REGISTERED_EXIST = "valid.option.registered.exist";
	public static final String VALID_OPTION_NOT_EXIST = "valid.option.not.exist";
	public static final String VALID_OPTION_CODE_PARENT_REQUIRED = "valid.option.code.parent.required";
	public static final String VALID_OPTION_CODE_PARENT_NOT_EXIST = "valid.option.code.parent.not.exist";
	public static final String VALID_OPTION_NAME_EXIST = "valid.option.name.exist";
	public static final String VALID_OPTION_DELETE_HAVE_DEPENDENCY = "valid.option.delete.have.dependency";

	/* PageNotification */
	public static final String VALID_PAGE_NOTIFICATION_NOT_REGISTERED = "valid.pagenotification.not.registered";
	public static final String SAVE_PAGE_NOTIFICATION_SUCCESFUL = "message.pagenotification.save.succesful";
	public static final String UPDATE_PAGE_NOTIFICATION_SUCCESFUL = "message.pagenotification.update.succesful";
	public static final String DELETE_PAGE_NOTIFICATION_SUCCESFUL = "message.pagenotification.delete.succesful";

	/* Component */
	public static final String VALID_COMPONENT_CODE_REQUIRED = "valid.component.code.required";
	public static final String VALID_COMPONENT_CODE_NOT_EXIST = "valid.component.code.not.exist";
	
	/* Email */
	public static final String MESSAGE_EMAIL_SUBJECT = "message.email.subject";
	public static final String TEMPLATE_EMAIL_VERIFICATION = "template.email.verification";
	public static final String URL_VERIFICATION_TOKEN = "url.verification.token";
	public static final String MESSAGE_EMAIL_ERROR_SEND = "message.email.error.send";
	public static final String MESSAGE_EMAIL_SEND_NOTIFICATION = "message.email.send.notification";
	public static final String TEMPLATE_QUOTE_EVENT_EMAIL = "template.quote.event.email";

	/* Token */
	public static final String MESSAGE_TOKEN_EXPIRED = "message.token.expired";
	public static final String MESSAGE_TOKEN_NOT_EXIST = "message.token.not.exist";
	public static final String MESSAGE_TOKEN_MODIFIED_PASS = "message.token.modified.password";
	
	/* Currency */
	public static final String VALID_CURRENCY_NAME_REQUIRED = "valid.currency.name.required";
	public static final String VALID_CURRENCY_SYMBOL_REQUIRED = "valid.currency.symbol.required";
	public static final String VALID_CURRENCY_VALUE_REQUIRED = "valid.currency.value.required";
	public static final String VALID_CURRENCY_NAME_EXIST = "valid.currency.name.exist";
	public static final String VALID_CURRENCY_LIST_REQUIRED = "valid.currency.list.required";
	public static final String VALID_CURRENCY_CODEISO_REQUIRED = "valid.currency.codeiso.required";
	public static final String VALID_CURRENCY_NOT_EXIST_PRINCIPAL = "valid.currency.not.exist.principal";
	public static final String VALID_CURRENCY_CODEISO_ORIGIN_TARGET_REQUIRED = "valid.currency.codeiso.origin.target.required";
	public static final String VALID_CURRENCY_VALUE_EXCHANGE = "valid.currency.value.exchange";
	public static final String VALID_CURRENCY_EXCHANGE_NOT_ASSIGNED = "valid.currency.exchange.not.assigned";
	public static final String VALID_CURRENCY_SAVE_SUCCESFUL = "valid.currency.save.succesful";
	public static final String VALID_CURRENCY_UPDATE_SUCCESFUL = "valid.currency.update.succesful";
	
	/* Country */
	public static final String REGISTERED_COUNTRY_CODE = "registered.country.code";
	public static final String UNREGISTERED_COUNTRY_CODE = "unregistered.country.code";
	
	/* Cluster */
	public static final String UNREGISTERED_CLUSTER_ID = "unregistered.cluster.id";
	
	/* S3 */
	public static final String MESSAGE_S3_ICON_EXIST = "message.s3.icon.exist";
	public static final String MESSAGE_S3_SAVE_UPLOAD_FAVICON_SUCCESFUL = "message.s3.save.upload.favicon.succesful";
	public static final String MESSAGE_S3_ICONS_FILE_EXIST = "message.s3.icons.file.exist";
	public static final String MESSAGE_S3_IMAGES_FILE_NOT_EXIST = "message.s3.images.file.not.exist";
	public static final String MESSAGE_S3_FAVICON_UPLOAD_ERROR = "message.s3.favicon.upload.error";
	public static final String MESSAGE_S3_IMAGE_UPLOAD_ERROR = "message.s3.image.upload.error";
	public static final String MESSAGE_S3_SAVE_UPLOAD_IMAGEN_SUCCESFUL = "message.s3.save.upload.imagen.succesful";
	public static final String MESSAGE_S3_VALID_MAX_SIZE_IMAGE = "message.s3.valid.max.size.image";
	public static final String MESSAGE_S3_VALID_MAX_SIZE_VIDEO = "message.s3.valid.max.size.video";
	public static final String MESSAGE_S3_VALID_MAX_SIZE_DOCUMENT = "message.s3.valid.max.size.document";
	public static final String MESSAGE_S3_IMAGES_EXIST = "message.s3.images.exist";
	public static final String MESSAGE_S3_VALID_FORMAT = "message.s3.valid.format";
	public static final String MESSAGE_S3_IMAGES_DELETE_ERROR = "message.s3.images.delete.error";
	public static final String MESSAGE_S3_IMAGES_NOT_EXIST = "message.s3.images.not.exist";
	public static final String MESSAGE_S3_IMAGES_DELETE_SUCCESFUL = "message.s3.images.delete.succesful";
	public static final String MESSAGE_S3_SAVE_UPLOAD_VIDEO_SUCCESFUL = "message.s3.save.upload.video.succesful";
	public static final String MESSAGE_S3_VIDEO_UPLOAD_ERROR = "message.s3.video.upload.error";
	public static final String MESSAGE_S3_VIDEO_EXIST = "message.s3.video.exist";
	public static final String MESSAGE_S3_VIDEO_DELETE_ERROR = "message.s3.video.delete.error";
	public static final String MESSAGE_S3_VIDEO_NOT_EXIST = "message.s3.video.not.exist";
	public static final String MESSAGE_S3_VIDEO_DELETE_SUCCESFUL = "message.s3.video.delete.succesful";
	public static final String MESSAGE_S3_ICONS_SVG_UPLOAD_ERROR = "message.s3.icons.svg.upload.error";
	public static final String MESSAGE_S3_ICONS_SVG_UPLOAD_SELECTION_ERROR = "message.s3.icons.svg.upload.selection.error";
	public static final String MESSAGE_S3_ICONS_SVG_UPLOAD_SUCCESFUL = "message.s3.icons.svg.upload.succesful";
	public static final String MESSAGE_S3_DOCUMENT_EXIST = "message.s3.document.exist";
	public static final String MESSAGE_S3_DOCUMENT_DELETE_ERROR = "message.s3.document.delete.error";
	public static final String MESSAGE_S3_DOCUMENT_NOT_EXIST = "message.s3.document.not.exist";
	public static final String MESSAGE_S3_DOCUMENT_DELETE_SUCCESFUL = "message.s3.document.delete.succesful";
	public static final String MESSAGE_S3_FILEJS_EXIST = "message.s3.document.exist";
	public static final String MESSAGE_S3_FILEJS_DELETE_ERROR = "message.s3.document.delete.error";
	public static final String MESSAGE_S3_FILEJS_NOT_EXIST = "message.s3.document.not.exist";
	public static final String MESSAGE_S3_FILEJS_DELETE_SUCCESFUL = "message.s3.document.delete.succesful";

	public static final String MESSAGE_S3_SAVE_UPLOAD_ICON_SUCCESFUL = "message.s3.save.upload.icon.succesful";
	public static final String MESSAGE_S3_ICONS_DELETE_ERROR = "message.s3.icons.delete.error";
	public static final String MESSAGE_S3_ICONS_NOT_EXIST = "message.s3.icons.not.exist";
	public static final String MESSAGE_S3_ICONS_DELETE_SUCCESFUL = "message.s3.icons.delete.succesful";
	public static final String MESSAGE_S3_IMAGES_UPDATE_NOT_EXIST = "message.s3.images.update.not.exist";
	public static final String MESSAGE_S3_IMAGES_UPDATE_SUCCESFUL= "message.s3.images.update.succesful";

	/* Revinate */
	public static final String SERVICE_REVINATE_NOT_AVAILABLE = "service.revinate.not.available";
	public static final String SUCCESSFUL_SYNCHRONIZATION = "successful.synchronization";
	
	/* Parameter */
	public static final String UNREGISTERED_PARAMETER_CODE = "unregistered.parameter.code";

	/* Page */
	public static final String MESSAGE_PAGE_NAME_EXIST = "message.page.name.exist";
	public static final String MESSAGE_PAGE_NAME_REQUIRED = "message.page.name.required";
	public static final String MESSAGE_PAGE_NOT_EXIST = "message.page.not.exist";
	public static final String MESSAGE_PAGE_SAVE_SUCCESSFUL = "message.page.save.successful";
	public static final String MESSAGE_PAGE_LIBERATE_SUCCESSFUL = "message.page.liberate.successful";
	public static final String MESSAGE_PAGE_PUBLISH_SUCCESSFUL = "message.page.publish.successful";
	public static final String MESSAGE_PAGE_UNPUBLISH_SUCCESSFUL = "message.page.unpublish.successful";
	public static final String MESSAGE_PAGE_USER_EDIT_EXIST = "message.page.user.edit.exist";
	public static final String MESSAGE_PAGE_NOT_EXIST_UPDATE = "message.page.not.exist.update";
	public static final String MESSAGE_PAGE_PATH_EXIST = "message.page.path.exist";
	public static final String MESSAGE_PAGE_PATH_REPEAT = "message.page.path.repeat";
	public static final String MESSAGE_PAGE_DELETE_SUCCESFUL= "message.page.delete.succesful";
	public static final String MESSAGE_PAGE_LANGUAGE_SPANISH_REQUIRED= "message.page.language.spanish.required";
	public static final String MESSAGE_PAGE_COUNT_LANGUAGES_REQUIRED= "message.page.count.languages.required";
	public static final String MESSAGE_PAGE_LANGUAGES_PAGEPATH_REQUIRED= "message.page.languages.pagepath.required";
	public static final String MESSAGE_PAGE_SET_FREE_VALIDATE = "message.page.set.free.validate";

	/* Template */
	public static final String MESSAGE_TEMPLATE_CODE_NOT_EXIST= "message.template.code.not.exist";
	public static final String MESSAGE_TEMPLATE_SAVE_SUCCESFUL = "message.template.save.succesful";
	public static final String MESSAGE_TEMPLATE_CODE_REQUIRED = "message.template.code.required";
	public static final String MESSAGE_TEMPLATE_CODE_EXIST = "message.template.code.exist";
	
	/* Subscriber */
	public static final String MESSAGE_VALID_LANGUAGE = "message.valid.language";
	
	/* Subscriber Contact */
	public static final String MESSAGE_SUBSCRIBER_CONTACT_SAVE_SUCCESFUL = "message.subscriber.contact.save.succesful";
	public static final String MESSAGE_VALID_SUBSCRIBER_NAME_REQUIRED = "message.valid.subscriber.name.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_EMAIL_REQUIRED = "message.valid.subscriber.email.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_EMAIL_VALIDATE = "message.valid.subscriber.email.validate";
	public static final String MESSAGE_VALID_SUBSCRIBER_EMAIL_EXIST = "message.valid.subscriber.email.exist";
	public static final String MESSAGE_VALID_SUBSCRIBER_PHONE_REQUIRED = "message.valid.subscriber.phone.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_COUNTRY_REQUIRED = "message.valid.subscriber.country.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_GENDER_REQUIRED = "message.valid.subscriber.gender.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_MOTIVE_REQUIRED = "message.valid.subscriber.motive.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_DESTINATION_REQUIRED = "message.valid.subscriber.destination.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_ADDRESS_REQUIRED = "message.valid.subscriber.address.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_MESSAGE_REQUIRED = "message.valid.subscriber.message.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_COUNTRY_REQUIRED_NOT_EXIST = "message.valid.subscriber.country.not.exist";
	public static final String MESSAGE_VALID_SUBSCRIBER_DESTINATION_NOT_EXIST = "message.valid.subscriber.destination.not.exist";
	public static final String MESSAGE_VALID_SUBSCRIBER_RUC_REQUIRED = "message.valid.subscriber.ruc.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_SEGMENT_REQUIRED = "message.valid.subscriber.segment.required";
	public static final String MESSAGE_VALID_SUBSCRIBER_NEWSLETTER = "message.valid.subscriber.newsletter";
	public static final String MESSAGE_VALID_SUBSCRIBER_DELETE_EXIST= "message.valid.subscriber.delete.exist";
	public static final String MESSAGE_VALID_SUBSCRIBER_DELETE_SUCCESFUL= "message.valid.subscriber.delete.succesful";

	/* Script */
	public static final String SCRIPT_CODE_VALIDATION_MESSAGE = "script.code.validation.message";
	
	/* QuoteEvent */
	public static final String MESSAGE_QUOTE_EVENT_SAVE_SUCCESFUL = "message.quote.event.save.succesful";
	public static final String MESSAGE_VALID_QUOTE_HOTEL_REQUIRED = "message.valid.quote.hotel.required";
	public static final String MESSAGE_VALID_QUOTE_QUANTITY_ASSISTANTS_REQUIRED = "message.valid.quote.quantity.assistants.required";
	public static final String MESSAGE_VALID_QUOTE_BEGIN_DATE_REQUIRED = "message.valid.quote.begin.date.required";
	public static final String MESSAGE_VALID_QUOTE_END_DATE_REQUIRED = "message.valid.quote.end.date.required";
	public static final String MESSAGE_VALID_QUOTE_BEGIN_HOUR_REQUIRED = "message.valid.quote.begin.hour.required";
	public static final String MESSAGE_VALID_QUOTE_END_HOUR_REQUIRED = "message.valid.quote.end.hour.required";
	public static final String MESSAGE_VALID_QUOTE_ARMED_TYPES_REQUIRED = "message.valid.quote.armed.types.required";
	public static final String MESSAGE_VALID_QUOTE_FOOD_OPTIONS_REQUIRED = "message.valid.quote.food.options.required";
	public static final String MESSAGE_VALID_QUOTE_AUDIOVISUAL_EQUIPMENTS_REQUIRED = "message.valid.quote.audiovisual.equipments.required";
	public static final String MESSAGE_VALID_QUOTE_SUBSCRIBER_PAGE_NOT_EXIST = "message.valid.quote.subscriber.page.not.exist";
	public static final String MESSAGE_VALID_QUOTE_END_DATE_MINOR_BEGIN_DATE = "message.valid.quote.end.date.minor.begin.date";
	public static final String MESSAGE_VALID_QUOTE_BEGIN_DATE_MINOR_CURRENT_DATE = "message.valid.quote.begin.date.minor.current.date";

	/* Servicios Roiback */
	public static final String MESSAGE_SAVE_ROIBACK_SUCCESFUL = "message.save.roiback.succesful";
	public static final String MESSAGE_VALID_ROIBACK_REQUIRED = "message.valid.roiback.required";
	public static final String MESSAGE_VALID_PRICE_ROIBACK_REQUIRED = "message.valid.price.roiback.required";
	
	/* Social Networking */
	public static final String MESSAGE_VALID_SOCIAL_NETWORKING_REQUIRED = "message.valid.social.networking.required";
	
	/* Tag */
	public static final String REGISTERED_TITLE_MESSAGE = "registered.title.message";

	/* Template Type*/
	public static final String MESSAGE_VALID_TEMPLATE_TYPE_NAME_REQUIRED = "message.valid.template.type.name.required";
	public static final String MESSAGE_VALID_TEMPLATE_TYPE_NAME_EXIST = "message.valid.template.type.name.exist";
	public static final String MESSAGE_TEMPLATE_TYPE_SAVE_SUCCESFUL = "message.template.type.save.succesful";

	/* Assistance */
	public static final String MESSAGE_ASSISTANCE_ICON_CLASS_REQUIRED = "message.assistance.icon.class.required";
	public static final String MESSAGE_ASSISTANCE_ENGLISH_TITLE_REQUIRED = "message.assistance.english.title.required";
	public static final String MESSAGE_ASSISTANCE_SPANISH_TITLE_REQUIRED = "message.assistance.spanish.title.required";
	public static final String MESSAGE_ASSISTANCE_PORTUGUESE_TITLE_REQUIRED = "message.assistance.portuguese.title.required";
	public static final String MESSAGE_ASSISTANCE_SAVE_SUCCESFUL = "message.assistance.save.succesful";
	public static final String MESSAGE_ASSISTANCE_UPDATE_SUCCESFUL = "message.assistance.update.succesful";
	public static final String MESSAGE_ASSISTANCE_NOT_EXIST = "message.assistance.not.exist";
	public static final String MESSAGE_ASSISTANCE_DELETE_SUCCESFUL = "message.assistance.delete.succesful";

	/* Promotion Type */
	public static final String MESSAGE_PROMOTION_TYPE_DESCRIPTION_REQUIRED = "message.promotion.type.description.required";
	public static final String MESSAGE_PROMOTION_TYPE_SAVE_SUCCESFUL = "message.promotion.type.save.succesful";

	/* Domain */
	public static final String MESSAGE_DOMAIN_PATH_REQUIRED = "message.domain.path.required";
	public static final String MESSAGE_DOMAIN_PATH_EXIST = "message.domain.path.exist";
	public static final String MESSAGE_DOMAIN_DELETE_PATH_NOT_EXIST = "message.domain.delete.path.not.exist";
	public static final String MESSAGE_DOMAIN_UPDATE_PATH_NOT_EXIST = "message.domain.update.path.not.exist";
	public static final String MESSAGE_DOMAIN_INSERT_SUCCESFUL = "message.domain.insert.succesful";
	public static final String MESSAGE_DOMAIN_UPDATE_SUCCESFUL = "message.domain.update.succesful";
	public static final String MESSAGE_DOMAIN_DELETE_SUCCESFUL = "message.domain.delete.succesful";

	/* Page Error */
	public static final String MESSAGE_ERROR_PAGE_UPDATE_NOT_EXIST = "message.error.page.update.not.exist";
	public static final String MESSAGE_ERROR_PAGE_DELETE_NOT_EXIST = "message.error.page.delete.not.exist";
	public static final String MESSAGE_ERROR_PAGE_PAGE_ID_REQUIRED = "message.error.page.page.id.required";
	public static final String MESSAGE_ERROR_PAGE_ERROR_CODE_REQUIRED = "message.error.page.error.code.required";
	public static final String MESSAGE_ERROR_PAGE_ERROR_CODE_EXIST = "message.error.page.error.code.exist";
	public static final String MESSAGE_ERROR_PAGE_REQUIRED = "message.error.page.required";
	public static final String MESSAGE_ERROR_PAGE_INSERT_SUCCESSFUL = "message.error.page.insert.successful";
	public static final String MESSAGE_ERROR_PAGE_UPDATE_SUCCESSFUL = "message.error.page.update.successful";
	public static final String MESSAGE_ERROR_PAGE_DELETE_SUCCESSFUL = "message.error.page.delete.successful";
	public static final String MESSAGE_ERROR_PAGE_ASSOCIATE_SUCCESSFUL = "message.error.page.associate.successful";

	/* Living Room */
	public static final String MESSAGE_VALID_LIVING_ROOM_NAME_REQUIRED = "message.valid.living.room.name.required";
	public static final String MESSAGE_VALID_LIVING_ROOM_HOTEL_REQUIRED = "message.valid.living.room.hotel.required";
	public static final String MESSAGE_VALID_LIVING_ROOM_CATEGORY_REQUIRED = "message.valid.living.room.category.required";
	public static final String MESSAGE_VALID_LIVING_ROOM_HOTEL_NOT_EXIST = "message.valid.living.room.hotel.not.exist";

}
