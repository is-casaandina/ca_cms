package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TemplateType {

    HOME("a5b68eb1-977f-4ab4-b269-8c679f631016","Home", Boolean.TRUE),
    DESTINATIONS("79c8ce33-a4a6-4fb9-a5c9-f21abdfd5292", "Destinos", Boolean.TRUE),
    DESTINATIONS_DETAIL("52ceb00a-e56d-492e-8fbd-058b7d1d750f", "Destino Detalle", Boolean.TRUE),
    HOTELS_DESCRIPTOR("5ccd4af8-a323-4cb1-9475-3b9d74d4a325", "Hotel Descriptor", Boolean.TRUE),
    HOTELS_DETAIL("dad19020-cf17-4435-9cbe-38ab9d34bf85", "Hotel Detalle", Boolean.TRUE),
    RESTAURANTS("98c073a0-783f-4c1b-8ac0-09db98298127", "Restaurantes", Boolean.TRUE),
    RESTAURANTS_DESCRIPTOR("e47f499c-fa59-42ec-b637-b4a410086fe0", "Restaurante Descriptor", Boolean.TRUE),
    RESTAURANTS_DETAIL("0f9e341c-11dc-48db-9efe-88ae7202c64b", "Restaurante Detalle", Boolean.TRUE),
    PROMOTIONS("2e6c4b19-51ad-455b-8bb6-8bec6c07a1ef", "Promociones", Boolean.TRUE),
    PROMOTIONS_DETAIL("4e8d5b70-e4c4-4974-9a6d-1397060227c2", "Promoci\u00f3n Detalle", Boolean.TRUE),
    EVENTS("7928cb6f-8661-440f-82f9-d3675c8d7596", "Eventos", Boolean.TRUE),
    EVENTS_DETAIL("7c4fe9b0-e256-44a6-b8ce-e594ec6e72f2", "Evento Detalle", Boolean.TRUE),
    ABOUT_US("ab0614a2-24df-44c5-94b9-c2caf61282fb","Quienes Somos", Boolean.TRUE),
    CONTACT("1aa5327c-5190-46d4-8a48-ed3a2a746dd5", "Contacto", Boolean.FALSE),
    PAGE_ERROR("69139d58-f29d-43d8-81fb-6f02bde932c1", "P\u00e1gina Error", Boolean.FALSE),
    FREE("f950e1ff-a05c-4c2c-becc-d082499e2590","Libre", Boolean.TRUE),
    LINK("8280867e-aa09-4d67-8254-3fb182070c53", "Enlace", Boolean.TRUE),
    BELL("8d5f1bea-6989-4a69-8fe5-d4cf5c3a6109", "Campa\u00f1as", Boolean.FALSE);

    private String code;
    private String value;
    private Boolean viewPage;

    private static final List<TemplateType> list = new ArrayList<>();
    private static final List<String> listCode = new ArrayList<>();

    private static final Map<String, TemplateType> lookup=new HashMap<>();
    private static final Map<String, TemplateType> lookupByValues=new HashMap<>();

    static{
        for(TemplateType issu : TemplateType.values()){
            lookup.put(issu.getCode(), issu);
            lookupByValues.put(issu.getValue(), issu);
            list.add(issu);
            if(issu.getViewPage())
                listCode.add(issu.getCode());
        }
    }

    public static List<String> getListCode(){
        return listCode;
    }

    public static TemplateType get(String code) {
        return lookup.get(code);
    }

    public static TemplateType getByValue(String value) {
        return lookupByValues.get(value);
    }

    private TemplateType(String code, String value, Boolean viewPage) {
        this.code = code;
        this.value = value;
        this.viewPage = viewPage;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public Boolean getViewPage(){
        return viewPage;
    }
}
