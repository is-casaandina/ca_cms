package com.casaandina.cms.util;

import java.util.HashMap;
import java.util.Map;

public enum AssistanceType {

    HOTELS(Integer.valueOf(1), "Hotel"),
    BEDROOMS(Integer.valueOf(2), "Habitaciones");

    private Integer code;
    private String value;

    private static final Map<Integer, AssistanceType> lookup = new HashMap<>();

    static {
        for(AssistanceType assistanceType : AssistanceType.values()){
            lookup.put(assistanceType.getCode(), assistanceType);
        }
    }

    private AssistanceType(Integer code, String value){
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static AssistanceType get(Integer code){
        return lookup.get(code);
    }
}
