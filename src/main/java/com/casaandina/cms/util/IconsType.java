package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum IconsType {
	
	FAVICON(Integer.valueOf(1), "FAVICON"),
	NORMAL(Integer.valueOf(2), "NORMAL");

	private Integer code;
	private String value;
	
	private static final List<IconsType> list=new ArrayList<>();
	
	private static final Map<Integer, IconsType> lookup=new HashMap<>();
	
	static{
		for(IconsType issu : IconsType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	public static IconsType get(Integer code) {
		return lookup.get(code);
	}

	private IconsType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
