package com.casaandina.cms.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum ContactType {

    MAIL_CONTACT(Integer.valueOf(1), "Mail Contact"),
    MAIL_BOOKING(Integer.valueOf(2), "Mail Booking"),
    PHONE(Integer.valueOf(3), "Telefono"),
    ADDRESS(Integer.valueOf(4), "Direccion"),
    OTHERS(Integer.valueOf(5), "Others");

    private Integer code;
    private String value;

    private static final Map<Integer, ContactType> lookup = new HashMap<>();

    static{
        Arrays.stream(ContactType.values()).forEach(x-> lookup.put(x.getCode(), x));
    }

    private ContactType(Integer code, String value){
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
