package com.casaandina.cms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ParameterType {
	
	REVINATE("revinate-opinions", "Revinate"),
	ROIBACK("roi", "Reservas Roiback"),
	SOCIAL_NETWORK("son", "Redes Sociales"),
	PRICE_ROIBACK("pro", "Precios Roiback"),
	MOTIVE("mot","Motivos"),
	GENDER("gen", "Generos"),
	STATUS_PAGE("page-states", "Estados de Pagina"),
	RATINGS("ratings", "Puntuaciones"),
	S3_DOCUMENT("s3-doc", "Documentos S3"),
	S3_JS("s3-js", "Documentos S3"),
	LIVING_ROOM("living-room", "Minimo y Maximo de Capacidad"),
	ROIBACK_HOTEL("roiback-hotel", "Url Roiback Hoteles"),
	ROIBACK_HOTEL_DAYS("roiback-hotel-days", "Dias de reserva Roiback Hotel"),
	URL_FRONT_PASSWORD("url-front-password", "Url para recuperacion de contrasenia Host y puerto"),
	ROIBACK_WEBSERVICE("roiback-webservice", "Parametros webservice roiback"),
	QUOTE_MAIL("quote-mail", "Template Mail de Cotizacion"),
	RECOVERY_PASSWORD_MAIL("recovery-password-mail","Template Mail Recuperar Contrasenia"),
	TYPE_SUBSCRIBER("", "Tipo Suscriptor"),
	CONTACT_REASON("contact-reason", "Contacto Motivo"),
	SITE_MAP("sitemap-xml", "Parametros SiteMap"),
	SEGMENT("segment", "Segmento"),
	NATURAL_PERSON_MAIL("natural-person-mail", "Template Mail Natural Person"),
	LEGAL_PERSON_MAIL("legal-person-mail", "Template Mail Legal Person"),
	RESIZE_IMAGES("resize-images","Resize Imagenes");

	private String code;
	private String value;
	
	private static final List<ParameterType> list=new ArrayList<>();
	
	private static final Map<String, ParameterType> lookup=new HashMap<>();
	
	static{
		for(ParameterType issu : ParameterType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	public static ParameterType get(String code) {
		return lookup.get(code);
	}

	private ParameterType(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
