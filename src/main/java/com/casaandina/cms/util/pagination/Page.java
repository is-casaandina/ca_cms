package com.casaandina.cms.util.pagination;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Page<T> {

	private Long totalElements;
	private Integer totalPages;
	private Boolean first;
	private Boolean last;
	private List<T> content;
}
