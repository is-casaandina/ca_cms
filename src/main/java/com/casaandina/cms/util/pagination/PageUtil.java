package com.casaandina.cms.util.pagination;

import com.casaandina.cms.model.Role;
import com.casaandina.cms.rest.response.RoleDetailResponse;
import com.casaandina.cms.util.ConstantsUtil;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public class PageUtil {
	
	private PageUtil() {
		super();
	}
	
	@SuppressWarnings({"rawtypes","unchecked"})
	public static <T> Page<T> pageToResponse(org.springframework.data.domain.Page<T> page) {
        Page pageN = new Page();

        pageN.setTotalElements(page.getTotalElements());
        pageN.setTotalPages(page.getTotalPages());
        pageN.setContent(page.getContent());
        pageN.setFirst(page.isFirst());
        pageN.setLast(page.isLast());

        return pageN;
    }
	
	@SuppressWarnings({"rawtypes","unchecked"})
    public static <T> Page<T> pageToResponse(org.springframework.data.domain.Page page, List<T> content) {
        Page<T> pageN = new Page();

        pageN.setTotalElements(page.getTotalElements());
        pageN.setTotalPages(page.getTotalPages());
        pageN.setContent(content);
        pageN.setFirst(page.isFirst());
        pageN.setLast(page.isLast());

        return pageN;
    }

	@SuppressWarnings({"rawtypes","unchecked"})
    public static <T> Page<T> pageToResponse(com.casaandina.cms.util.pagination.Page page, List<T> content) {
        Page<T> pageN = new Page();

        pageN.setTotalElements(page.getTotalElements());
        pageN.setTotalPages(page.getTotalPages());
        pageN.setContent(content);
        pageN.setFirst(page.getFirst());
        pageN.setLast(page.getLast());

        return pageN;
    }

    public static Pageable getPageable(Integer page, Integer size) {
        Integer localPage = page == null ? ConstantsUtil.DEFAULT_PAGE : page - 1;
        Integer localSize = size == null ? ConstantsUtil.DEFAULT_SIZE : size;
        return org.springframework.data.domain.PageRequest.of(localPage, localSize);
    }

    public static Pageable getPageableSort(Integer page, Integer size, Sort sort) {
        Integer localPage = page == null ? ConstantsUtil.DEFAULT_PAGE : page - 1;
        Integer localSize = size == null ? ConstantsUtil.DEFAULT_SIZE : size;
        return org.springframework.data.domain.PageRequest.of(localPage, localSize, sort);
    }
    
    @SuppressWarnings({"rawtypes","unchecked"})
    public static Page<RoleDetailResponse> roleToPageRoleDetail(org.springframework.data.domain.Page<Role> rolePage, List<RoleDetailResponse> roleDetails){
        Page page = new Page();

        page.setTotalElements(rolePage.getTotalElements());
        page.setTotalPages(rolePage.getTotalPages());
        page.setFirst(rolePage.isFirst());
        page.setLast(rolePage.isLast());
        page.setContent(roleDetails);

        return page;
    }
}
