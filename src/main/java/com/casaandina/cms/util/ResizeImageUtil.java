package com.casaandina.cms.util;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ResizeImageUtil {
	
	private ResizeImageUtil() {
		super();
	}
	
	public static BufferedImage resize(BufferedImage img, int width, int height) {
        Image  tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.SCALE_SMOOTH);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }

}
