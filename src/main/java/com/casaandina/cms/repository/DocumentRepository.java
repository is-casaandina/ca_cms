package com.casaandina.cms.repository;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Document;

@Repository
public interface DocumentRepository extends MongoRepository<Document, String> {

	@Query(value = "{'name' : {$regex : ?0, '$options' : 'i'}}", fields = "{'id': 1, 'name' : 1, 'mimeType' :1, 'size': 1, 'extension' : 1, 'directory' : 1, 'url' : 1, 'mimeType' :1, 'createdDate' :1 }")
	Page<Document> findAllPaginated(String value, Pageable pageable);
	
	@Query(value = "{'createdDate' : { $gt: ?0, $lt: ?1 }, 'name' : {$regex : ?2, '$options' : 'i'}}",
			   fields = "{'id': 1, 'name' : 1, 'mimeType' :1, 'extension' : 1, 'directory' : 1, 'url' : 1, 'mimeType' :1,'size': 1, 'createdDate' :1 }")
	Page<Document> findByCreatedAtBetweenCustomized(LocalDateTime beginDate, LocalDateTime endDate, String value, Pageable pageable);
	
	@Query(value = "{'name' : ?0 }", fields = "{'id': 1, 'name' : 1 }")
	Optional<Document> findByNameShort(String name);

	Optional<Document> findTopByOrderByCreatedDateAsc();

	Optional<Document> findTopByOrderByCreatedDateDesc();
}
