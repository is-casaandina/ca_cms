package com.casaandina.cms.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.VerificationToken;

@Repository
public interface VerificationTokenRepository extends MongoRepository<VerificationToken, String> {
	
	Optional<VerificationToken> findByToken(String token);
	
}
