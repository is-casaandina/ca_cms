package com.casaandina.cms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Script;

@Repository
public interface ScriptRepository extends MongoRepository<Script, String> {

}
