package com.casaandina.cms.repository.impl;

import com.casaandina.cms.model.Menu;
import com.casaandina.cms.repository.MenuRepositoryCustom;
import com.casaandina.cms.util.ConstantsUtil;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

public class MenuRepositoryCustomImpl implements MenuRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public MenuRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<Menu> findByLanguageCode(String languageCode) {

        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("languageCode").is(languageCode)),
                Aggregation.lookup("page_path", "pageId", "_id", "page_info"),
                Aggregation.unwind("$page_info"),
                Aggregation.match(Criteria.where("page_info.language").is(languageCode)),
                Aggregation.project()
                        .andInclude("_id")
                        .andInclude("name")
                        .andInclude("type")
                        .andInclude("parentId")
                        .andInclude("action")
                        .andInclude("order")
                        .andInclude("languageCode")
                        .andInclude("pageId").andExpression("$page_info.path").as("pagePath")
        );
        return mongoTemplate.aggregate(aggregation, ConstantsUtil.MongoDb.Collection.MENU, Menu.class).getMappedResults();
    }
}
