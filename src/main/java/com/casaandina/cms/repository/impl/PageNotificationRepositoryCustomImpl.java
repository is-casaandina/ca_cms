package com.casaandina.cms.repository.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import com.casaandina.cms.util.CaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.LanguagePageNotification;
import com.casaandina.cms.repository.PageNotificationRepositoryCustom;
import com.casaandina.cms.util.ConstantsUtil;

@Repository
public class PageNotificationRepositoryCustomImpl implements PageNotificationRepositoryCustom {
    private final MongoTemplate mongoTemplate;

    @Autowired
    public PageNotificationRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<LanguagePageNotification> findByPageIdAndLanguageCode(String pageId, String language) {
        OffsetDateTime now = OffsetDateTime.now();

        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.unwind("languages"),
                Aggregation.match(Criteria.where("pageId").is(pageId)),
                Aggregation.match(Criteria.where("status").is(ConstantsUtil.ACTIVE)),
                Aggregation.match(Criteria.where("beginDate").lte(now.toLocalDateTime())),
                Aggregation.match(Criteria.where("endDate").gte(now.toLocalDateTime())),
                Aggregation.match(Criteria.where("languages.language").is(language)),
                Aggregation.project().andInclude("languages.title")
                        .andInclude("languages.detail")
                        .andExclude("_id")
        );
        return mongoTemplate.aggregate(aggregation, ConstantsUtil.MongoDb.Collection.PAGE_NOTIFICATION, LanguagePageNotification.class).getMappedResults();
    }
}
