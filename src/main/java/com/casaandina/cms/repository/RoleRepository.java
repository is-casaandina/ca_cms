package com.casaandina.cms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Role;

@Repository
public interface RoleRepository extends MongoRepository<Role, String> {
	
	Optional<Role> findByRoleCode(String roleCode);

	Page<Role> findByStatus(Integer status, Pageable pageable);

	List<Role> findByStatus(Integer status);

	List<Role> findByStatusOrderByRoleCode(Integer status);
	
	List<Role> findByStatusAndRoleNameIgnoreCase(Integer status, String roleName);
	
	Optional<Role> findByRoleCodeAndStatus(String roleCode, Integer status);

	List<Role> findByStatusAndPermissionCodeListIn(Integer status, List<String> permissions);

}
