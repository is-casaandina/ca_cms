package com.casaandina.cms.repository;

import com.casaandina.cms.model.DestinationMap;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DestinationMapRepository extends MongoRepository<DestinationMap, String> {

    List<DestinationMap> findByCountryIdAndState(String countryId, Integer state);

    @Query(value = "{ 'id' : { $in : ?0 }, 'state' : ?1 }",
            fields = "{ 'id': 1, 'className': 1 }")
    List<DestinationMap> customFindByIdsAndState(List<String> ids, Integer state);
}
