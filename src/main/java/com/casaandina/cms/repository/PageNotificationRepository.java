package com.casaandina.cms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.PageNotification;

@Repository
public interface PageNotificationRepository extends MongoRepository<PageNotification, Integer>, PageNotificationRepositoryCustom {
    Optional<PageNotification> findById(Integer id);
    Optional<PageNotification> findByIdAndStatus(Integer id, Boolean status);
    Page<PageNotification> findByStatus(Boolean status, Pageable pageable);
    List<PageNotification> findByPageId(String pageId);
}
