package com.casaandina.cms.repository;

import com.casaandina.cms.model.Menu;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MenuRepository extends MongoRepository<Menu, String> {

	Optional<Menu> findByLanguageCodeAndTypeAndParentIdAndNameOrOrder(
			String languageCode, String type, String parentId, String name, Integer order);

	List<Menu> deleteByParentId(String parentId);

	List<Menu> findByLanguageCode(String language);

}
