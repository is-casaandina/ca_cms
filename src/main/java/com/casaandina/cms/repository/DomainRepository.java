package com.casaandina.cms.repository;

import com.casaandina.cms.model.Domain;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DomainRepository extends MongoRepository<Domain, String> {

    List<Domain> findByState(Integer state);

    Optional<Domain> findByIdAndState(String id, Integer state);

    Optional<Domain> findByPathAndState(String path, Integer state);

    @Query(value = "{ 'path': ?0, 'state' : ?1 }", fields = "{ 'id': 1, 'path' : 1, 'language': 1, 'currency' : 1, 'hideLanguage' : 1, 'tax' : 1, 'promotionAllId' : 1  }")
    Optional<Domain> customFindByPath(String path, Integer state);

    @Query(value = "{ 'path' : ?0, 'state' : ?1, 'id' : { $ne : ?2 } }")
    Optional<Domain> customFindByPathAndStateAndIdNot(String path, Integer state, String id);
}
