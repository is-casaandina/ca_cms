package com.casaandina.cms.repository;

import com.casaandina.cms.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
	
	Optional<User> findById(Integer id);
	
	Optional<User> findByIdAndStatus(Integer id, Integer status);

	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String username);

	Optional<User> findByEmailAndStatus(String username, Integer status);

	List<User> findByStatusOrderByUsername(Integer status);
	
	Optional<User> findByUsernameAndStatus(String username, Integer status);
	
	Page<User> findByStatus(Integer status, Pageable pageable);

	Page<User> findByStatusAndRoleCodeIn(Integer status, String[] roleCode, Pageable pageable);

	List<User> findByRoleCodeAndStatus(String roleCode, Integer status);

	List<User> findByRoleCodeInAndStatus(List<String> roleCode, Integer status);

}
