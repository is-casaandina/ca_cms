package com.casaandina.cms.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Icons;

@Repository
public interface IconsRepository extends MongoRepository<Icons, String> {
	
	void deleteAllByCustom(boolean custom);
	
	@Query(value = "{}", fields = "{ 'className' : 1, 'filters': 1 }")
	List<Icons> findAllCustomized();

}
