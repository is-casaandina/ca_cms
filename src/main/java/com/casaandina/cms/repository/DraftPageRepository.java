package com.casaandina.cms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.DraftPage;

import java.util.Optional;

@Repository
public interface DraftPageRepository extends MongoRepository<DraftPage, String> {
	
	void deleteAllById(String id);

	Optional<DraftPage> findByIdAndState(String id, Integer state);

	Optional<DraftPage> findByIdAndStateNot(String id, Integer state);

	@Query(value = "{ 'id': ?0 }", fields = "{ 'id': 1, 'contact' : 1 , 'descriptorId' : 1 , 'revinate' : 1,  'roiback' : 1 " +
			", 'beginDate' : 1, 'firstExpiration' : 1, 'secondExpiration' : 1, 'countdown' : 1 " +
			",'languages' : { $elemMatch : { language : ?1 } } }")
	Optional<DraftPage> customFindByIdAndLanguage(String id, String language);

}
