package com.casaandina.cms.repository;

import com.casaandina.cms.dto.SubscriberDto;
import com.casaandina.cms.model.Subscriber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SubscriberRepository extends MongoRepository<Subscriber, String> {

	@Query(value = "{ 'email': ?0, 'status': ?1 }", fields = "{ 'id' : 1, 'email' : 1 }")
	Optional<SubscriberDto> findByEmailAndStatusCustomized(String email, Integer status);
	
	List<Subscriber> findAllByStatus(Integer status);

	Optional<Subscriber> findByIdAndStatus(String id, Integer status);

	List<Subscriber> findByTypeSuscriberAndStatus(String typeSuscriber, Integer status);

	@Query(value = "{ 'typeSuscriber' : { $in : ?0 }, 'status' : ?1 , 'registerDate' : { $gt : ?2, $lt : ?3 } }",
			fields = "{ 'id' : 1, 'typeSuscriber' : 1 , 'name' : 1, 'surname': 1, 'email' : 1, 'ruc' : 1 , 'country' : 1, 'phone' : 1, 'registerDate' : 1  }")
	Page<Subscriber> customFindByTypeSubscriberAndStatusAndRegisterDateBetween(List<String> typeSubscribers, Integer status, Date startDate, Date endDate, Pageable pageable);

	@Query(value = "{ 'typeSuscriber' : { $in : ?0 }, 'status' : ?1 }",
			fields = "{ 'id' : 1, 'typeSuscriber' : 1 , 'name' : 1, 'surname': 1, 'ruc' : 1 , 'email' : 1, 'country' : 1, 'phone' : 1, 'registerDate' : 1  }")
	Page<Subscriber> customFindByTypeSubscriberAndStatus(List<String> typeSubscribers, Integer status, Pageable pageable);

	List<Subscriber> findByTypeSuscriberInAndStatusAndRegisterDateBetweenOrderByRegisterDateDesc(List<String> typeSubscribers, Integer status, Date startDate, Date endDate, Sort sort);

	List<Subscriber> findByTypeSuscriberInAndStatusOrderByRegisterDateDesc(List<String> typeSubscribers, Integer status, Sort sort);
}
