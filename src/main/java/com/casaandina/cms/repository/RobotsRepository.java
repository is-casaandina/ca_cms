package com.casaandina.cms.repository;


import com.casaandina.cms.model.Robots;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RobotsRepository extends MongoRepository<Robots, String> {
}
