package com.casaandina.cms.repository;

import com.casaandina.cms.model.Template;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TemplateRepository extends MongoRepository<Template, String> {

	Optional<Template> findByCode(String code);

	@Query(value = "{ 'categoryId' : { $ne : ?0 } }", fields = "{'id': 1, 'code': 1, 'name': 1, 'description': 1, 'imageUri': 1, 'categoryId': 1 }")
	List<Template> findTemplatesCustomized(String categoryId);

	@Query(value = "{ 'categoryId' : { $in : ?0} }", fields = "{'id': 1, 'code': 1, 'name': 1, 'description': 1, 'imageUri': 1, 'categoryId': 1 }")
	List<Template> findTemplatesByCategoriesCustomized(List<String> categories);

}
