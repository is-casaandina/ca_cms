package com.casaandina.cms.repository;

import com.casaandina.cms.model.PagePromotionView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PagePromotionViewRepository extends MongoRepository<PagePromotionView, String> {

    Page<PagePromotionView> findById(String id, Pageable pageable);

    @Query(value = "{ 'id' : { $in : ?0 }," +
            " 'item.hotel' : { $in : ?1 }," +
            " 'item.descriptor' : { $in : ?2 }," +
            " 'language' : ?3 }")
    Page<PagePromotionView> getPagePromotionByFilters(List<String> items, List<String> hotels, List<String> descriptors, String language, Pageable pageable);

    @Query(value = "{ 'item.hotel' : { $in : ?0 }," +
            " 'item.descriptor' : { $in : ?1 }," +
            " 'language' : ?2 }")
    Page<PagePromotionView> getPagePromotionByFilters(List<String> hotels, List<String> descriptors, String language, Pageable pageable);

    @Query(value = "{ 'id' : { $in : ?0 }," +
            " 'item.hotel' : { $in : ?1 }," +
            " 'item.descriptor' : { $in : ?2 }," +
            " 'language' : ?3, " +
            " 'domainId' : { $in : ?4 }, " +
            " 'clusterId' : { $in : ?5 }, " +
            " 'countryId' : { $in : ?6 }, " +
            " 'beginDate' : { $lte : ?7 }," +
            " $or : [ " +
            "   { 'firstExpiration' : { $gte : ?7 } }, " +
            "   { 'firstExpiration' : null }, " +
            "   { 'secondExpiration' : { $gte : ?7 } }, " +
            "   { 'secondExpiration' : null } " +
            "] }"
    )
    Page<PagePromotionView> getPagePromotionByFilters(List<String> items, List<String> hotels, List<String> descriptors, String language,
                                                      List<String> domainId, List<String> clusterId, List<String> countryId, LocalDateTime today, Pageable pageable);

    @Query(value = "{ 'item.hotel' : { $in : ?0 }," +
            " 'item.descriptor' : { $in : ?1 }," +
            " 'language' : ?2, " +
            " 'domainId' : { $in : ?3 }, " +
            " 'clusterId' : { $in : ?4 }, " +
            " 'countryId' : { $in : ?5 }, " +
            " 'beginDate' : { $lte : ?6 }," +
            " $or : [ " +
            "   { 'firstExpiration' : { $gte : ?6 } }, " +
            "   { 'firstExpiration' : null }, " +
            "   { 'secondExpiration' : { $gte : ?6 } }, " +
            "   { 'secondExpiration' : null } " +
            "] }"
    )
    Page<PagePromotionView> getPagePromotionByFilters(List<String> hotels, List<String> descriptors, String language,
                                                      List<String> domainId, List<String> clusterId, List<String> countryId, LocalDateTime today, Pageable pageable);

    @Query(value = " { 'beginDate' : { $lte : ?0 }, " +
            " $or : [ " +
            "   { 'firstExpiration' : { $gte : ?0 } }, " +
            "   { 'firstExpiration' : null }, " +
            "   { 'secondExpiration' : { $gte : ?0 } }, " +
            "   { 'secondExpiration' : null } " +
            "]} ", fields = "{ 'item.destination' : 1 } ")
    List<PagePromotionView> getDestinationsAll(LocalDateTime today);

    @Query(value = " { 'id' : { $in : ?1} , 'beginDate' : { $lte : ?0 }, " +
            " $or : [ " +
            "   { 'firstExpiration' : { $gte : ?0 } }, " +
            "   { 'firstExpiration' : null }, " +
            "   { 'secondExpiration' : { $gte : ?0 } }, " +
            "   { 'secondExpiration' : null } " +
            "]} ", fields = "{ 'item.destination' : 1 } ")
    List<PagePromotionView> getDestinationByPromotions(LocalDateTime today, List<String> items);

    @Query(value = " { 'beginDate' : { $lte : ?0 }, " +
            " $or : [ " +
            "   { 'firstExpiration' : { $gte : ?0 } }, " +
            "   { 'firstExpiration' : null }, " +
            "   { 'secondExpiration' : { $gte : ?0 } }, " +
            "   { 'secondExpiration' : null } " +
            "]} ", fields = "{ 'item.hotel' : 1 } ")
    List<PagePromotionView> getHotelsAll(LocalDateTime today);

    @Query(value = " { 'item.destination' : { $in : ?1} , 'beginDate' : { $lte : ?0 }, " +
            " $or : [ " +
            "   { 'firstExpiration' : { $gte : ?0 } }, " +
            "   { 'firstExpiration' : null }, " +
            "   { 'secondExpiration' : { $gte : ?0 } }, " +
            "   { 'secondExpiration' : null } " +
            "]} ", fields = "{ 'item.hotel' : 1 } ")
    List<PagePromotionView> getHotelsByDestinations(LocalDateTime today, List<String> items);

}
