package com.casaandina.cms.repository;

import com.casaandina.cms.model.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PageRepository extends MongoRepository<Page, String> {

	@Query(value = "{ 'name' : ?0 }", fields = "{ 'id' : 1, 'name' : 1}")
	Optional<Page> findByName(String name);
	
	@Query(value = "{ 'id' : ?0 }", fields = "{ 'id' : 1, 'name' : 1, 'state' : 1, 'user':1}")
	Optional<Page> findByIdCustomized(String id);

	@Query(value = "{ 'state' : { $ne : ?2 }, '$or' : [ { 'languages.path' : {$regex : ?1, '$options' : 'i'} }, { 'languages.title' : {$regex : ?1, '$options' : 'i'} } ]}",
		  fields = "{ 'id': 1, 'external' :1, 'languages' : {$elemMatch : { 'language' : ?0 }}}")
	List<Page> findPageByLanguageAndPath(String language, String value, Integer state , Pageable pageable);

	@Query(value = "{'countryId': ?0, 'categoryId': ?1, 'state' : { $ne : ?2 } }", fields = "{'id': 1, 'languages': 1 }", sort = "{'languages.title': 1}")
	List<Page> findByCountryIdAndLanguageCustomized(String countryId, String categoryId, Integer state);

	@Query(value = "{'descriptorId': ?0, 'categoryId': ?1, 'state': { $ne : ?2 } }", fields = "{'id': 1, 'languages': 1 }")
	List<Page> findHotelsByDescriptorCustomized(String descriptor, String category, Integer state);

	@Query(value = "{'categoryId': ?0, 'destinationId' : ?1, 'state': { $ne: ?2 } }", fields = "{'id': 1, 'languages': 1 }")
	List<Page> findHotelsByDestinationCustomized(String category, String destinationId, Integer state);

	@Query(value = "{'categoryId': '?0','languages.data.descriptorId' : '?1', 'languages.data.categoryId' : '?2', 'state': { $ne : ?3 } }",
			fields = "{'id': 1, 'languages': 1 }")
	List<Page> findPromotionsByDescriptorAndCategoryCustomized(String category, String descriptorData, String categoryData, Integer state);

	@Query(value = "{'categoryId': '?0', 'state': { $ne: ?1 } }", fields = "{'id': 1, 'languages': 1 }")
	List<Page> findPromotionsByCategoryCustomized(String category, Integer state);

	@Query(value = "{'id': {$ne : ?0 }, 'languages.path' : ?1 }", fields = "{ 'id' : 1, 'name' : 1, 'state' : 1, 'user':1}")
	Optional<Page> findByIdAndLanguagePathCustomized(String id, String path);

	Optional<Page> findByIdAndState(String id, Integer state);

	Optional<Page> findByIdAndStateNot(String id, Integer state);

	@Query(value = "{'descriptorId': ?0, 'categoryId': ?1, 'state': { $ne : ?2 } }", fields = "{'id': 1, 'languages': 1 }")
	List<Page> findRestaurantsByDescriptorCustomized(String descriptor, String category, Integer state);

	@Query(value = "{ 'languages.language' : ?0 , 'languages.title' : {$regex : ?1, '$options' : 'i'}, 'state' : { $ne : ?2 }, 'categoryId' : ?3}",
			fields = "{ 'id': 1, 'languages' :1, 'external' :1 }")
	List<Page> findHotelsByLanguageAndName(String language, String value, Integer state, String category , Pageable pageable);

	@Query(value =  "{ 'id' : ?0, 'categoryId' : ?2, 'state' : {$ne : ?3} }", fields = "{'languages' : {$elemMatch : {language : ?1 }} }")
	Page customFindByIdAndLanguageCode(String id, String language, String category, Integer statusNot);

	@Query(value =  "{ 'id' : ?0, 'state' : {$ne : ?2} }", fields = "{'languages' : {$elemMatch : {language : ?1 }} }")
	Page customPageFindByIdAndLanguageCode(String id, String language, Integer statusNot);

	@Query(value = "{ 'languages.path': ?0 ,'state' : { $ne : ?1 } }",
			fields = "{ 'id': 1, 'contact' : 1 , 'categoryId' : 1 , 'descriptorId' : 1 , 'revinate' : 1,  'roiback' : 1 " +
					", 'beginDate' : 1, 'firstExpiration' : 1, 'secondExpiration' : 1, 'countdown' : 1 " +
					", 'domainId' : 1 , 'countryId' : 1, 'clusterId' : 1 , 'languages' : { $elemMatch : { path : ?0 } } }")
	Optional<Page> customFindByPathAndStateNot(String path, Integer state);

	@Query(value = "{ 'id': ?0, 'state': { $ne: ?2 } }",
		   fields = "{ 'id' : 1, 'categoryId' : 1, 'languages' : { $elemMatch : { language : ?1 } } }")
	Optional<Page> customFindByIdAndLanguageAndStateNot(String id, String language, Integer state);

	@Query(value = " { 'state' : { $ne : ?2 }, 'external': ?3, '$or' :[{ 'languages.path' : {$regex : ?1, '$options' : 'i'}}, { 'languages.title' : {$regex : ?1, '$options' : 'i'}}] }",
			fields = "{ 'id': 1, 'external' :1, 'languages' : {$elemMatch : { 'language' : ?0 }}}")
	List<Page> findPageByLanguageAndPathAndExternalNot(String language, String value, Integer state, Boolean external, Pageable pageable);

	@Query(value = " { 'state' : { $ne : ?2 }, '$or' :[{ 'languages.path' : {$regex : ?1, '$options' : 'i'}}, { 'languages.title' : {$regex : ?1, '$options' : 'i'}}] }",
			fields = "{ 'id': 1, 'external' :1, 'languages' : {$elemMatch : { 'language' : ?0 }}}")
	List<Page> findPageByLanguageAndPathAndExternal(String language, String value, Integer state, Pageable pageable);

	@Query(value = "{'categoryId': ?0, 'destinationId' : ?1, 'state': { $ne: ?2 } }",
			fields = "{ 'id': 1, 'descriptorId': 1, 'roiback' : 1 , 'languages': { $elemMatch : { language: ?3 }} }")
	List<Page> findHotelsByDestinationAndLanguageCustomized(String category, String destinationId, Integer state, String language);

	@Query(value = "{'descriptorId': { $in : ?0 }, 'categoryId': { $in: ?1 } , 'state': { $ne : ?2 }, 'roiback' : { $ne: ?4 } }", fields = "{'descriptorId': 1,'languages': { $elemMatch : {language : ?3 } } }")
	List<Page> findHotelsRestaurantsByDescriptorInAndStateNotCustomized(List<String> descriptor, List<String> category, Integer state, String language, String roiback);

	@Query(value = "{ 'id': ?0 }", fields = "{'contact':1, 'languages' : { $elemMatch : { language : ?1 }}}")
	Optional<Page> customFindByIdAndLanguageNameElementMatch(String id, String language);

	@Query(value = "{'categoryId': ?0, 'destinationId' : ?1, 'descriptorId' : ?2 , 'state': { $ne: ?3 } }", fields = "{'id': 1, 'languages': 1 }")
	List<Page> findHotelsByDestinationAndDescriptorCustomized(String category, String destinationId, String descriptorId, Integer state);

	@Query(value = "{'id': { $in : ?0}, 'state': { $ne: ?1 } }",
			fields = "{ 'id': 1, 'descriptorId': 1, 'roiback' : 1 , 'languages': { $elemMatch : { language: ?2 }} }")
	List<Page> findHotelsByIdsAndLanguageCustomized(List<String> ids, Integer state, String language);

	@Query(value = "{ 'id' : ?0, 'state' : { $ne: ?1 } }",
			fields = "{ 'id': 1, 'contact' : 1 , 'categoryId' : 1 , 'descriptorId' : 1 , 'revinate' : 1,  'roiback' : 1 " +
					", 'beginDate' : 1, 'firstExpiration' : 1, 'secondExpiration' : 1, 'countdown' : 1 " +
					", 'domainId' : 1, 'countryId' : 1, 'clusterId' : 1 ," +
					" 'languages' : { $elemMatch : { language : ?2 } } }")
	Optional<Page> findByIdAndStateNotAndLanguage(String id, Integer state, String language);

	@Query(value = "{ 'id' : {$in : ?0}, 'state' : { $ne: ?1 } }", fields = "{ 'id': 1, 'contact' : 1 }")
	List<Page> getPageContactByIds(List<String> items, Integer status);

	@Query(value = "{ 'id' : { $in : ?0 }, 'state' : { $ne : ?1 } }",
			fields = "{ 'id': 1, 'descriptorId' : 1, 'roiback':1 , 'categoryId': 1 , 'languages' : { $elemMatch : { language : ?2 } }," +
					" 'languages.title':1, 'languages.path': 1, 'languages.data': 1 }")
	List<Page> customFindPageByIdsAndLanguageAndStateNot(List<String> ids, Integer state, String language);

	@Query(value = "{ 'categoryId' : ?0 , 'state' : { $ne : ?1 } }",
			fields = "{ 'id': 1, 'languages' : { $elemMatch : { language : ?2 } }, 'languages.title':1 }")
	List<Page> customFindPageByCategoryIdAndLanguageAndStateNot(String categoryId, Integer state, String language);

	@Query(value = "{ 'categoryId' : ?0, 'destinationId' : ?1, 'descriptorId' : ?2, 'roiback' : { $ne : ?4 } }",
			fields = "{ 'id': 1, 'roiback':1, 'revinate': 1 , 'languages' : { $elemMatch : { language : ?3 } }, 'languages.title' : 1 " +
					",'languages.data': 1 }")
	List<Page> customFindHotelsByDestinationIdAndDescriptorIdAndLanguage(String categoryId, String destinationId,
																		 String descriptorId, String language, String roiback);

	@Query(value = "{ 'categoryId' : ?0, 'state' : { $ne : ?1 } } ",
			fields = "{ 'id': 1, 'destinationMapId': 1, 'languages' : { $elemMatch : { language : ?2 } }, 'languages.title': 1 }")
	List<Page> customFindDestinationByStateAndLanguage(String categoryId, Integer state, String language);

	@Query(value = "{'categoryId' : ?0, 'destinationId': { $in : ?1 }, 'state' : { $ne : ?2 } }",
			fields = "{ 'id':0, 'destinationId': 1, 'descriptorId': 1 }")
	List<Page> customFindDescriptorByHotelsAndDestinationIds(String categoryId, List<String> destinations, Integer state);

	@Query(value = "{ 'id' : { $in: ?0}, 'state' : { $ne : ?1 } }",
			fields = " {'id' : 1, 'contact': 1, 'languages' : { $elemMatch : { language : ?2} }, 'languages.data': 1, 'languages.path' : 1 } ")
	List<Page> getContactByPage(List<String> id, Integer state, String language);

	@Query(value = "{ 'categoryId' : ?0, 'destinationId' : ?1, 'descriptorId': ?2, 'state' : { $ne: ?3 }, 'roiback' : { $ne : ?4 } }",
			fields = "{ 'id' : 1 }")
	List<Page> getHotelsByDestinationAndDescriptorAndStateNot(String categoryId, String destinationId,
																	String descriptorId, Integer state, String roiback);

	@Query(value = "{ 'id' : { $in: ?0},  'state' : { $ne : ?2 } }",
			fields = "{ 'languages' : { $elemMatch : { language : ?1 } }, 'languages.data' : 1, 'languages.path' : 1 }")
	List<Page> getPromotionBell(List<String> promotions, String language, Integer state);

	@Query(value = "{'categoryId': ?0, 'destinationId' : { $in: ?1 }, 'state': { $ne: ?2 } }",
			fields = "{ 'id': 1, 'languages': { $elemMatch : { language: ?3 }}, 'languages.title' : 1 }")
	List<Page> findHotelsByListDestinationsAndLanguageCustomized(String category, List<String> ids, Integer state, String language);

	@Query(value = "{'categoryId': ?0, 'state': { $ne: ?1 } }",
			fields = "{ 'id': 1, 'languages': { $elemMatch : { language: ?2 }}, 'languages.title' : 1 }")
	List<Page> findHotelsByLanguageCustomized(String category, Integer state, String language);

	@Query(value = "{ 'categoryId' : ?0, 'genericType' : ?1, 'state': { $ne: ?3 } }",
			fields = "{ 'id': 1, 'languages' : { $elemMatch : { language : ?2 } }, 'languages.title' : 1 }")
	List<Page> findPromotionByPromotionType(String categoryId, String promotionType, String language, Integer state);

	@Query(value = "{ 'categoryId' : ?0, 'genericType' : { $in : ?1 }, 'languages.data.caHotelRestPromotion.items.type' : ?2," +
			" 'languages.data.caHotelRestPromotion.items.hotel' : { $in : ?3 }, " +
			" 'languages.data.caHotelRestPromotion.items.descriptor' : { $in : ?4 } }",
			fields = "{ 'id': 1, 'languages.language' : 1, 'languages.path': 1 , 'languages.data':1 }")
	org.springframework.data.domain.Page<Page> findFeaturedPromotionByFilters(String categoryId, List<String> promotionTypes, String type, List<String> hotels,
																			  List<String> descriptors, Pageable pageable);

	@Query(value = "{ 'id' : { $in : ?0 } }", fields = "{ 'id': 1, 'descriptorId' : 1," +
			" 'categoryId' : 1, 'languages': { $elemMatch : { language: ?1 }}, 'languages.path' : 1} }")
	List<Page> getPageWithDescriptor(List<String> items, String language);

	@Query(value = "{ 'id' : ?0 }",
			fields = "{ 'id': 1, 'languages' : { $elemMatch : { language : ?1 } }," +
					" 'languages.path': 1 }")
	Optional<Page> customFindPagePathByIdAndLanguage(String id, String language);

	@Query(value = "{ 'categoryId' : { $in : ?0 } }",
			fields = "{ 'id' : 1, 'categoryId' : 1, 'modifiedDate' : 1, 'languages.path' : 1, 'languages.pagePath' : 1," +
					" 'languages.language' : 1 }")
	List<Page> customFindAllPageForSiteMap(List<String> categories);

	@Query(value = "{'categoryId': ?0, 'state': { $ne: ?1 } }",
			fields = "{ 'id': 1, 'languages': { $elemMatch : { language: ?2 }}, 'languages.title' : 1 }")
	List<Page> findPromotionsByStateAndLanguageCustomized(String category, Integer state, String language);

	@Query(value = "{ 'id' : ?0 }", fields = "{ 'id' : 1, 'languages.language' : 1, 'languages.path' : 1 }")
	Optional<Page> customFindPathsBySEO(String id);

	@Query(value = "{ 'id' : ?0 }", fields = "{ 'id' : 1, 'languages' : { $elemMatch : { language : ?1  } }, 'languages.path' : 1 }")
	Optional<Page> customFindPathsByIdAndLanguage(String id, String language);

	@Query(value = "{'categoryId': ?0, 'state': { $ne: ?1 }, 'genericType': { $ne: ?3 }," +
			" 'beginDate' : { $lte : ?4 }, " +
			" $or : [ " +
			"   {  'firstExpiration' : { $gte : ?4 } }, " +
			"   { 'firstExpiration' : null }, " +
			"   { 'secondExpiration' : { $gte : ?4 } }, " +
			"   { 'secondExpiration' : null } " +
			" ] }",
			fields = "{ 'id': 1, 'firstExpiration' : 1, 'secondExpiration' : 1, " +
					" 'languages': { $elemMatch : { language: ?2 }}, 'languages.title' : 1 }")
	List<Page> findPromotionsByStateAndLanguageCustomizedFilter(String category, Integer state, String language, String genericType, LocalDateTime today);

	@Query(value = "{'categoryId': ?0, 'destinationId' : ?1, 'state': { $ne: ?2 }, 'roiback' :  { $ne : ?4 } }",
			fields = "{ 'id': 1, 'descriptorId': 1, 'roiback' : 1 , 'languages': { $elemMatch : { language: ?3 }} }")
	List<Page> findHotelsByDestinationAndLanguageCustomizedAndRoibackNot(String category, String destinationId, Integer state, String language, String roiback);

	@Query(value = "{'categoryId' : ?0, 'destinationId': { $in : ?1 }, 'state' : { $ne : ?2 }, 'roiback' :  { $ne : ?3 } }",
			fields = "{ 'id':0, 'destinationId': 1, 'descriptorId': 1 }")
	List<Page> customFindDescriptorByHotelsAndDestinationIdsAndRoibackNot(String categoryId, List<String> destinations, Integer state, String roiback);

    @Query(value = "{ 'categoryId' : ?0, 'id' : ?1, 'state' : { $ne: ?2 } }",
            fields = "{ 'id' : 1 }")
    List<Page> getHotelsByIdAndDescriptorAndStateNot(String categoryId, String hotelId, Integer state);

    @Query(value = "{'categoryId': ?0, 'state': { $ne: ?1 }, 'roiback' :  { $ne : ?3 } }",
            fields = "{ 'id': 1, 'descriptorId': 1, 'roiback' : 1 , 'languages': { $elemMatch : { language: ?2 }}, 'languages.title' : 1 }")
    List<Page> findHotelsByLanguageCustomizedAndRoibackNot(String category, Integer state, String language, String roiback);
}
