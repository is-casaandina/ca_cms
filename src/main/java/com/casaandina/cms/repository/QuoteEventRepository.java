package com.casaandina.cms.repository;

import com.casaandina.cms.model.QuoteEvent;
import com.casaandina.cms.model.Subscriber;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuoteEventRepository extends MongoRepository<QuoteEvent, String> {

    List<QuoteEvent> findBySubscriberIn(List<Subscriber> id);

    Optional<QuoteEvent> findBySubscriber(Subscriber subscriber);

}
