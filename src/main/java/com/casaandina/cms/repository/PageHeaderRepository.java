package com.casaandina.cms.repository;

import com.casaandina.cms.model.PageHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface PageHeaderRepository extends MongoRepository<PageHeader, String> {
	
	@Query(value = "{ 'id' : ?0, 'state' : {$ne : ?1 } }", fields = "{ 'id' : 1, 'name' : 1, 'state' : 1, 'username':1, 'languages': 1, 'lastUpdate':1 }")
	Optional<PageHeader> findByIdCustomized(String id, Integer state);
	
	@Query(value = "{ 'name' : ?0 }", fields = "{ 'id' : 1, 'name' : 1}")
	Optional<PageHeader> findByName(String name);
	
	@Query(value = "{}", fields = "{ 'id' : 1, 'name' : 1, 'state' : 1, 'username':1, 'languages': 1, 'lastUpdate':1 }")
	List<PageHeader> findAllCustomized();

	List<PageHeader> findAll();

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndUsernameInAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInOrderByLastUpdateDesc(Integer[] listState,String name, List<String> users, List<String> categories, Date startDate, Date endDate, Boolean[] inEdition, Boolean[] publish, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndUsernameInAndCategoryIdInAndInEditionInAndPublishIn(Integer[] listState,String name, List<String> users, List<String> categories, Pageable pageable, Boolean[] inEdition, Boolean[] publish);

	@Query(value = "{ 'name' : ?0, 'id': {$ne : ?1} }", fields = "{ 'id' : 1, 'name' : 1}")
	Optional<PageHeader> findByNameAndIdNotCustomized(String name, String id);

	@Query(value = "{'state' : {$ne : ?0}}")
	List<PageHeader> findByActiveCustomized(Integer state);

	@Query(value = "{ 'categoryId' : ?0, 'name' : ?1, 'state' : { $ne : ?2 }, 'publish' : ?3 }",
			fields = "{ 'id': 1, 'name' : 1 }")
	Optional<PageHeader> customFindByDestinationsByNameAndStateNotAndPublish(String categoryId ,String name, Integer state, Boolean publish);

	@Query(value = "{ 'categoryId' : ?0, 'destinationId' : ?1, 'state' : { $ne : ?2}, 'publish' : ?3 }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1 }")
	List<PageHeader> customFindHotelByDestinationIdAndStateNotAndPublish(String categoryId, String destinationId, Integer state, Boolean publish);

	@Query(value = "{ 'categoryId' : ?0, 'name': {$regex : ?1, '$options' : 'i'} , 'state' : { $ne : ?2}, 'publish' : ?3 }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1 }")
	List<PageHeader> customFindHotelByNameLikeAndStateNotAndPublish(String categoryId, String name, Integer state, Boolean publish);

	@Query(value = "{ 'categoryId' : ?0, 'name': {$regex : ?1, '$options' : 'i'} , 'state' : { $ne : ?2}, 'publish' : ?3, 'destinationId' : { $ne : ?4 } }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1 }")
	List<PageHeader> customFindHotelByNameLikeAndStateNotAndPublishAndDestinationIdNot(String categoryId, String name, Integer state, Boolean publish, String destinationId);

	@Query(value = "{ 'id' : ?0 }", fields = "{ 'id' : 1, 'name' : 1}")
	Optional<PageHeader> customFindByIdShort(String id);

	@Query(value = "{ 'id' : { $in : ?0 } }", fields = "{ 'id' : 1, 'name' : 1}")
	List<PageHeader> customFindByIdsShort(List<String> id);

	@Query(value = "{ 'categoryId' : ?0, 'state' : { $ne : ?1 }, 'publish' : ?2 }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1 }")
	List<PageHeader> customFindDestinationsByStateNotPublish(String categoryId, Integer state, Boolean publish);

	Page<PageHeader> findByStateInAndCategoryIdInAndDestinationIdIn(Integer[] listState, List<String> categories, List<String> destinations, Pageable pageable);

	List<PageHeader> findByIdInAndCategoryIdAndStateNotAndPublish(List<String> ids, String category, Integer state, Boolean publish);

	@Query(value = "{ 'categoryId' : ?0, 'name' : {$regex : ?1, $options : 'i' }, 'state' : { $ne : ?2 }, 'publish' : ?3 }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1 }")
	List<PageHeader> customFindByDestinationsByNameLikeAndStateNotAndPublish(String categoryId ,String name, Integer state, Boolean publish);

	@Query(value = "{ 'categoryId' : { $in : ?0 }, 'name' : {$regex : ?1, $options : 'i' }, 'state' : { $ne : ?2 }, 'publish' : ?3 }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1, 'categoryId' :1, 'lastUpdate' :1 }")
	List<PageHeader> customFindDestinationHotelByNameLikeAndStateNotAndPublish(List<String> categoryId, String name, Integer state, Boolean publish);

	@Query(value = "{ 'categoryId' : ?0, 'state' : { $ne : ?1 }, 'publish' : ?2, 'destinationId' : { $in : ?3 } }",
			fields = "{ 'id': 1, 'name' : 1 }")
	List<PageHeader> customFindHotelsByDestinationsIds(String categoryId, Integer state, Boolean publish, List<String> destinations);

	@Query(value = "{ 'id' : {$in : ?0}, 'state' : { $ne: ?1 } }", fields = "{ 'id': 1, 'name' : 1 } ")
	List<PageHeader> getPageContactByIds(List<String> items, Integer status);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInAndGenericTypeInOrderByLastUpdateDesc(Integer[] listState,String name, List<String> categories, Date startDate, Date endDate, Boolean[] inEdition, Boolean[] publish, List<String> genericType, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishInAndGenericTypeIn(Integer[] listState,String name, List<String> categories, Boolean[] inEdition, Boolean[] publish, List<String> genericType, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInAndCountryIdInOrderByLastUpdateDesc(Integer[] listState,String name, List<String> categories, Date startDate, Date endDate, Boolean[] inEdition, Boolean[] publish, List<String> countries, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishInAndCountryIdIn(Integer[] listState,String name, List<String> categories, Boolean[] inEdition, Boolean[] publish, List<String> countries, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInOrderByLastUpdateDesc(Integer[] listState,String name, List<String> categories, Date startDate, Date endDate, Boolean[] inEdition, Boolean[] publish, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishIn(Integer[] listState,String name, List<String> categories, Boolean[] inEdition, Boolean[] publish, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndLastUpdateBetweenAndInEditionInAndPublishInAndDestinationIdInOrderByLastUpdateDesc(Integer[] listState,String name, List<String> categories, Date startDate, Date endDate, Boolean[] inEdition, Boolean[] publish, List<String> destinations, Pageable pageable);

	Page<PageHeader> findByStateInAndNameContainingIgnoreCaseAndCategoryIdInAndInEditionInAndPublishInAndAndDestinationIdIn(Integer[] listState,String name, List<String> categories, Boolean[] inEdition, Boolean[] publish, List<String> destinations, Pageable pageable);

	@Query(value = "{ 'categoryId' : { $in : ?0 }, 'name' : {$regex : ?1, $options : 'i' }, 'state' : { $ne : ?2 }, 'publish' : ?3, 'roiback' : { $ne : ?4 } }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1, 'categoryId' :1, 'lastUpdate' :1 }")
	Page<PageHeader> customFindDestinationHotelByNameLikeAndStateNotAndPublish(List<String> categoryId, String name, Integer state, Boolean publish, String roiback, Pageable pageable);

	@Query(value = "{ 'categoryId' : ?0, 'state' : { $ne : ?1 }, 'publish' : ?2 }")
	List<PageHeader> customFindByCategoryIdAndStateAndPublish(String categoryId, Integer state, Boolean publish);

	@Query(value = "{ 'id' : { $in : ?0 } , 'state' : { $ne : ?1 }, 'publish' : ?2 }",
			fields = "{ 'id' : 1, 'categoryId' : 1, 'name' : 1 }")
	List<PageHeader> customFindByIdAndStateAndPublish(List<String> id, Integer state, Boolean publish);

	@Query(value = "{ 'id' :  ?0 , 'state' : { $ne : ?1 }, 'publish' : ?2 }",
			fields = "{ 'id' : 1, 'categoryId' : 1 }")
	Optional<PageHeader> customFindByIdAndStateAndPublish(String id, Integer state, Boolean publish);

	@Query(value = "{ 'categoryId' : { $in : ?0 }, 'name' : {$regex : ?1, $options : 'i' }, 'state' : { $ne : ?2 }, 'publish' : ?3 }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1, 'categoryId' :1, 'lastUpdate' :1 }")
	Page<PageHeader> customFindDestinationHotelByNameLikeAndStateNotAndPublish(List<String> categoryId, String name, Integer state, Boolean publish, Pageable pageable);

	@Query(value = "{ 'categoryId' : { $in : ?0 }, 'name' : {$regex : ?1, $options : 'i' }, 'state' : { $ne : ?2 }, 'publish' : ?3, 'roiback' : { $ne : ?4 }, 'destinationId' : ?5 }",
			fields = "{ 'id': 1, 'name' : 1, 'roiback' : 1, 'revinate': 1, 'destinationId' : 1, 'categoryId' :1, 'lastUpdate' :1 }")
	Page<PageHeader> customFindDestinationHotelByNameLikeAndStateNotAndPublishAndDestinationId(List<String> categoryId, String name, Integer state, Boolean publish, String roiback, String destinationId, Pageable pageable);
}
