package com.casaandina.cms.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Rating;

@Repository
public interface RatingRepository extends MongoRepository<Rating, String> {
	
	List<Rating> findAllByHotelCode(String hotelCode);

}
