package com.casaandina.cms.repository;

import com.casaandina.cms.model.PageJoinView;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PageJoinViewRepository extends MongoRepository<PageJoinView, String>{

    @Query(value = "{ 'categoryId': { $in: ?0 }, 'countryId': { $in: ?1 } }")
    List<PageJoinView> customFindByCategoriesAndCountries(List<String> categories, List<String> countries, Sort sort);

    @Query(value = "{ 'categoryId': { $in: ?0 } }")
    List<PageJoinView> customFindByCategories(List<String> categories, Sort sort);
}
