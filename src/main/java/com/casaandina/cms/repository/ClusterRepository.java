package com.casaandina.cms.repository;

import com.casaandina.cms.model.Cluster;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ClusterRepository extends MongoRepository<Cluster, String> {
	
	List<Cluster> findAllByName(String name);

	@Query(value = "{}", fields = "{'id': 1, 'name': 1}")
	List<Cluster> findAllCustomized();

	@Query(value = "{ 'status' : ?0, 'countries' : { '$ref' : 'Country' , $id : ?1 } }")
	List<Cluster> customFindByStatusAndCountryId(Boolean status, String countryId);

	Optional<Cluster> findByIdAndStatus(String clusterId, Boolean status);

}
