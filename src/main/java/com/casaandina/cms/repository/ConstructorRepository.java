/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casaandina.cms.repository;

import com.casaandina.cms.model.ConstructorTemplate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 *
 * @author evasquez
 */
public interface ConstructorRepository extends MongoRepository<ConstructorTemplate, String> {
    
    Optional<ConstructorTemplate> findByCode(String code);

	@Query(value = "{ 'categoryId' : { $ne : ?0 } }", fields = "{'id': 1, 'code': 1, 'name': 1, 'description': 1, 'imageUri': 1, 'categoryId': 1 }")
	List<ConstructorTemplate> findTemplatesCustomized(String categoryId);

	@Query(value = "{ 'categoryId' : { $in : ?0} }", fields = "{'id': 1, 'code': 1, 'name': 1, 'description': 1, 'imageUri': 1, 'categoryId': 1 }")
	List<ConstructorTemplate> findTemplatesByCategoriesCustomized(List<String> categories);
    
}
