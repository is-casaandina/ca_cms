package com.casaandina.cms.repository;

import com.casaandina.cms.model.Configurations;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface ConfigurationsRepository extends MongoRepository<Configurations, String> {

    @Query(value = "{}", fields = "{ 'id': 1, 'homeId' : 1 }")
    List<Configurations> customFindAllHomeId();

    @Query(value = "{}", fields = "{ 'id': 1, 'languages' : 1, 'defaultLanguage' : 1 }")
    List<Configurations> customFindAllLanguages();

    @Query(value = "{}", fields = "{ 'id': 1, 'domain' : 1 }")
    List<Configurations> customFindAllDomain();
}
