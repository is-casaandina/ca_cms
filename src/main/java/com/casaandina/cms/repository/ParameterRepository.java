package com.casaandina.cms.repository;

import com.casaandina.cms.dto.GeneralParameters;
import com.casaandina.cms.dto.RatingParameters;
import com.casaandina.cms.model.Parameter;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParameterRepository extends MongoRepository<Parameter, String> {
	
	List<Parameter> findByCode(String code);
	
	Optional<Parameter> findByCodeAndLanguageCode(String code, String languageCode);
	
	@Query(value = "{ 'code': ?0, 'languageCode': ?1 }", fields = "{ 'id': 1, 'code': 1, 'languageCode': 1,'indicator': 1 }")
	List<Parameter> findByCodeAndLanguageCodeList(String code, String languageCode);
	
	@Query(value = "{ 'code': '?0'}", fields = "{'id': 0, 'indicator': 1}")
	RatingParameters customFindAllByCode(String code);

	@Query(value = "{ 'code': '?0'}", fields = "{'id': 1, 'indicator': 1}")
	Parameter customFindRoibackWebServiceByCode(String code);
	
	@Query(value = "{ 'code': ?0}", fields = "{'id': 0, 'indicator': {$elemMatch: {languageCode: ?1}}}")
	GeneralParameters customFindByCodeAndLanguageCode(String code, String languageCode);

}
