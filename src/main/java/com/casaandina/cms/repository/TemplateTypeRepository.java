package com.casaandina.cms.repository;

import com.casaandina.cms.model.TemplateType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TemplateTypeRepository extends MongoRepository<TemplateType,String> {

    Optional<TemplateType> findByName(String name);
    List<TemplateType> findByViewPage(boolean viewPage);
}
