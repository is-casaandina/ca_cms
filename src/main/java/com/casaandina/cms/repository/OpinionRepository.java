package com.casaandina.cms.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Opinion;

@Repository
public interface OpinionRepository extends MongoRepository<Opinion, String> {
	
	List<Opinion> findAllByHotelCode(String hotelCode);

}
