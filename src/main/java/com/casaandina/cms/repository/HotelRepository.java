package com.casaandina.cms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Hotel;

@Repository
public interface HotelRepository extends MongoRepository<Hotel, String>{
	
	Hotel findByCode(String code);

}
