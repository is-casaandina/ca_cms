package com.casaandina.cms.repository;

import com.casaandina.cms.model.Menu;

import java.util.List;

public interface MenuRepositoryCustom {
    List<Menu> findByLanguageCode(String languageCode);
}
