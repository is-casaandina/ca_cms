package com.casaandina.cms.repository;

import com.casaandina.cms.model.PageError;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PageErrorRepository extends MongoRepository<PageError, String> {

    Optional<PageError> findByErrorCodeAndState(Integer errorCode, Integer state);

    List<PageError> findByState(Integer state);

    Optional<PageError> findByIdAndState(String id, Integer state);

    Optional<PageError> findByStateAndErrorCode(Integer state, Integer errorCode);

    @Query(value = "{ 'state' : ?0, 'errorCode' : ?1, 'id' : { $ne : ?2 } }")
    Optional<PageError> customFindByStateAndErrorCodeAndIdNot(Integer state, Integer errorCode, String id);
}
