package com.casaandina.cms.repository;

import com.casaandina.cms.model.Benefits;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BenefitsRepository extends MongoRepository<Benefits, String> {
}
