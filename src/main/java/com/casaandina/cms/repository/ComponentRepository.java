package com.casaandina.cms.repository;

import com.casaandina.cms.model.Component;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ComponentRepository extends MongoRepository<Component, String> {
	
	List<Component> findByStatus(Integer status);
	
	@Query(value="{ 'status' : ?0 }", fields="{ 'id' : 1, 'name' : 1, 'code': 1, 'description': 1, 'imageUri': 1, 'template': 1, 'properties' : 1, 'status': 1, 'type' : 1 }")
    List<Component> findComponentShortByStatus(Integer status);
	
	Optional<Component> findByIdAndStatus(String id, Integer status);
	
	Optional<Component> findByCodeAndStatus(String code, Integer status);

	List<Component> findByTypeAndStatus(Integer type, Integer status);

}
