package com.casaandina.cms.repository;

import com.casaandina.cms.model.Currency;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CurrencyRepository extends MongoRepository<Currency, Integer> {
	
	Optional<Currency> findByNameAndStatus(String name, Integer status);
	
	Optional<Currency> findByCodeIsoAndStatus(String codeIso, Integer status);
	
	List<Currency> findByStatus(Integer status);
	
	@Query(value="{ 'isPrincipal' : ?0, 'status' : ?1 }", fields="{ 'codeIso': 1 }")
	Optional<Currency> findByIsPrincipalAndStatus(Integer isPrincipal, Integer status);
	
	@Query(value="{ 'codeIso' : ?0, 'status' : ?1 }", fields="{ 'value': 1 }")
	Optional<Currency> findByCodeIsoAndStatusCustomized(String codeIso, Integer status);

	@Query(value = "{ 'status' : ?0 }", fields = "{ 'codeIso': 1, 'symbol' : 1 }")
	List<Currency> findByStatusCustomized(Integer status);

}
