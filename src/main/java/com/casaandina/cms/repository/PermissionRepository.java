package com.casaandina.cms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Permission;

@Repository
public interface PermissionRepository extends MongoRepository<Permission, String> {
	
	Optional<Permission> findByPermissionCodeAndStatus(String permissionCode, Integer status);
	
	List<Permission> findByStatus(Integer status);

}
