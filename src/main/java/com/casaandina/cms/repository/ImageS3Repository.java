package com.casaandina.cms.repository;

import com.casaandina.cms.model.ImageS3;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ImageS3Repository extends MongoRepository<ImageS3, String> {
	
	@Query(value = "{'name': ?0}", fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'small' : 1, 'medium' : 1, 'large': 1, 'createDate' :1 }")
	Optional<ImageS3> findByNameCustomized(String name);
	
	@Query(value = "{'name': ?0}", fields = "{'id': 1, 'name' : 1 }")
	Optional<ImageS3> findByNameShort(String name);
	
	@Query(value = "{}", fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'mimeType' : 1 , 'small' : 1, 'medium' : 1," +
			" 'large': 1, 'createDate' :1, 'updateDate' : 1 }")
	List<ImageS3> findAllCustomized();
	
	@Query(value = "{'name' : {$regex : ?0, '$options' : 'i'}}", fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'mimeType' : 1, 'small' : 1, 'medium' : 1, 'large': 1, 'createDate' :1 }")
	Page<ImageS3> findAllPaginated(String value, Pageable pageable);
	
	Optional<ImageS3> findTopByOrderByCreateDateAsc();
	
	Optional<ImageS3> findTopByOrderByCreateDateDesc();
	
	@Query(value = "{'createDate' : { $gt: ?0, $lt: ?1 }, 'name' : {$regex : ?2, '$options' : 'i'}}", 
		   fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'mimeType' : 1, 'small' : 1, 'medium' : 1, 'large': 1, 'createDate' :1}")
	Page<ImageS3> findByCreateDateBetweenCustomized(LocalDateTime beginDate, LocalDateTime endDate, String value, Pageable pageable);

	List<ImageS3> findByCreateDateBetween(Date startDate, Date endDate);

}
