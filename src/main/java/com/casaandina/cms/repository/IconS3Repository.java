package com.casaandina.cms.repository;

import com.casaandina.cms.model.IconS3;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface IconS3Repository extends MongoRepository<IconS3, String> {
	
	Optional<IconS3> findByUrl(String imageUri);
	
	List<IconS3> findByTypeAndStatusOrderByUpdatedDate(Integer type, Integer status);

	@Query(value = "{'name': ?0}", fields = "{'id': 1, 'name' : 1, 'width': 1, 'height' : 1,'properties' : 1, 'url': 1, 'createDate' :1 }")
	Optional<IconS3> findByNameCustomized(String name);

	Optional<IconS3> findByName(String name);

	Optional<IconS3> findTopByTypeOrderByCreateDateAsc(Integer type);

	Optional<IconS3> findTopByTypeOrderByCreateDateDesc(Integer type);

	@Query(value = "{'name' : {$regex : ?0, '$options' : 'i'}, 'type': ?1 }",
			fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'type' : 1, 'url' : 1,'width': 1, 'height': 1, 'createDate' :1 }")
	Page<IconS3> findAllPaginated(String value, Integer type, Pageable pageable);

	@Query(value = "{'createDate' : { $gt: ?0, $lt: ?1 }, 'name' : {$regex : ?2, '$options' : 'i'}, 'type': ?3 }",
			fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'type' : 1, 'url' : 1,'width': 1, 'height': 1, 'createDate' :1 }")
	Page<IconS3> findByCreateDateBetweenCustomized(LocalDate beginDate, LocalDate endDate, String value, Integer type, Pageable pageable);

}
