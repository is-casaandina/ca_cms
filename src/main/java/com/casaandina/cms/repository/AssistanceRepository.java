package com.casaandina.cms.repository;

import com.casaandina.cms.model.Assistance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssistanceRepository extends MongoRepository<Assistance, String> {

    Page<Assistance> findByStatus(Integer status,  Pageable pageable);

    Page<Assistance> findByStatusAndType(Integer status, Integer type, Pageable pageable);

    @Query(value = "{ 'status' : ?0, 'type' : ?1, 'assistanceTitles.title' : {$regex : ?2, $options : 'i' } }")
    Page<Assistance> customFindByStatusAndTypeAndTitle(Integer status, Integer type, String title, Pageable pageable);

    List<Assistance> findByStatusAndType(Integer status, Integer type);

    @Query(value = "{ 'id': { $in: ?0 }, 'status' : ?1 }", fields = "{'iconClass': 1, 'assistanceTitles' : {$elemMatch : {language : ?2 }} }")
    List<Assistance> customFindByIdsAndTypeAndLanguage(List<String> ids, Integer status, String language);

}
