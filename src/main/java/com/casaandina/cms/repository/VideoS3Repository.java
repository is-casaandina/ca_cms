package com.casaandina.cms.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.VideoS3;

@Repository
public interface VideoS3Repository extends MongoRepository<VideoS3, String> {
	
	@Query(value = "{'name': ?0}", fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'uri': 1, 'small' : 1, 'medium' : 1, 'large': 1, 'createDate' :1 }")
	Optional<VideoS3> findByNameCustomized(String name);
	
	@Query(value = "{'name': ?0}", fields = "{'id': 1, 'name' : 1 }")
	Optional<VideoS3> findByNameShort(String name);
	
	Optional<VideoS3> findTopByOrderByCreateDateAsc();
	
	Optional<VideoS3> findTopByOrderByCreateDateDesc();
	
	@Query(value = "{}", fields = "{'id': 1, 'name' : 1, 'properties' : 1,'mimeType' : 1, 'url': 1, 'size': 1, 'small' : 1, 'medium' : 1, 'large': 1, 'createDate' :1 }")
	List<VideoS3> findAllCustomized();
	
	@Query(value = "{'name' : {$regex : ?0, '$options' : 'i'}}", fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'mimeType' : 1, 'size': 1, 'url': 1, 'small' : 1, 'medium' : 1, 'large': 1, 'createDate' :1 }")
	Page<VideoS3> findAllPaginated(String value, Pageable pageable);
	
	@Query(value = "{'createDate' : { $gt: ?0, $lt: ?1 }, 'name' : {$regex : ?2, '$options' : 'i'} }", 
			fields = "{'id': 1, 'name' : 1, 'properties' : 1, 'mimeType' : 1, 'size': 1, 'url': 1, 'small' : 1, 'medium' : 1, 'large': 1, 'createDate' :1}")
	Page<VideoS3> findByCreateDateBetweenCustomized(LocalDateTime beginDate, LocalDateTime endDate, String value, Pageable pageable);

}
