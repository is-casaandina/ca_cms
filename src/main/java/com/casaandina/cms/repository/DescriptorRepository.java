package com.casaandina.cms.repository;

import com.casaandina.cms.model.Descriptor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DescriptorRepository extends MongoRepository<Descriptor, String> {
	
	Optional<Descriptor> findByTitle(String title);

	List<Descriptor> findByCategory(String category);

	@Query(value = "{ 'id' : { $in : ?0 }}", fields = "{ 'id': 1, 'name' : 1, 'color' : 1, 'title': 1 }")
	List<Descriptor> customFindDescriptorByIds(List<String> ids);

	@Query(value = "{ 'category' : ?0  }", fields = "{ 'id': 1, 'name' : 1, 'color' : 1, 'title': 1 }")
	List<Descriptor> customFindDescriptorByCategory(String category);

	@Query(value = "{ }", fields = "{ 'id': 1, 'name' : 1, 'color' : 1, 'title': 1 }")
	List<Descriptor> customFindAllDescriptors();

}
