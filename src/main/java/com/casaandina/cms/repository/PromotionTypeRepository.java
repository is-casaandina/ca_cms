package com.casaandina.cms.repository;

import com.casaandina.cms.model.PromotionType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PromotionTypeRepository extends MongoRepository<PromotionType, String> {

    @Query(value = "{ 'state' : ?0}", fields = "{ 'id': 1, 'state' : 1, 'createdDate' : 1, 'languages' : { $elemMatch : { language : ?1 }} }")
    List<PromotionType> customFindByStateAndLanguage(Integer state, String language);

    List<PromotionType> findByState(Integer state);

    @Query(value = "{ 'state' : ?0 }", fields = "{ 'id' : 1 }")
    List<PromotionType> customFindByState(Integer state);
}
