package com.casaandina.cms.repository;

import com.casaandina.cms.model.FormTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FormTemplateRepository extends MongoRepository<FormTemplate, String> {

    @Query(value = "{ 'code' : ?0 }",
            fields = "{ 'code' : 1, 'name' : 1, 'languages' : { $elemMatch : { languageCode : ?1 } }}")
    Optional<FormTemplate> customFindByCodeAndLanguage(String code, String language);

}
