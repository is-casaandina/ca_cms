package com.casaandina.cms.repository;

import com.casaandina.cms.model.RoibackLowPrice;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoibackLowPriceRepository extends MongoRepository<RoibackLowPrice, String> {

    Optional<RoibackLowPrice> findTopByRoibackAndLanguageOrderByCreatedDateDesc(String roiback, String language);

}
