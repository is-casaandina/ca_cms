package com.casaandina.cms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.Option;

@Repository
public interface OptionRepository extends MongoRepository<Option, Integer> {
	
	Optional<Option> findByIdAndStatus(Integer id, Integer status);
	
	Optional<Option> findByNameAndLevelAndStatus(String name, Integer level, Integer status);
	
	List<Option> findByStatusAndIdOptionParentIsNull(Integer status);
	
	List<Option> findByIdOptionParentAndStatus(Integer idOptionParent, Integer status);
	
	List<Option> findByStatusAndNameAndIdNot(Integer status,String name, Integer id);
	
	List<Option> findByStatusAndLevelOrderByOrderDesc(Integer status, Integer level);

}
