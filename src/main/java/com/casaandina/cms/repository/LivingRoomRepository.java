package com.casaandina.cms.repository;

import com.casaandina.cms.model.LivingRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LivingRoomRepository extends MongoRepository<LivingRoom, String> {

	Page<LivingRoom> findAllByDestinationIdInAndHotelIdInAndTeatherBetween(
			List<String> destinationsId, List<String> hotelsId, Integer minTeather, Integer maxTeather, Pageable pageable);
	
	Page<LivingRoom> findAllByDestinationIdInAndHotelIdIn(List<String> destinationsId, List<String> hotelsId, Pageable pageable);
	
	Page<LivingRoom> findAllByDestinationIdInAndTeatherBetween(
			List<String> destinationsId, Integer minTeather, Integer maxTeather, Pageable pageable);
	
	Page<LivingRoom> findAllByDestinationIdIn(List<String> destinationsId, Pageable pageable);
	
	Page<LivingRoom> findAllByHotelIdInAndTeatherBetween(List<String> hotelsId, Integer minTeather, Integer maxTeather, Pageable pageable);
	
	Page<LivingRoom> findAllByHotelIdIn(List<String> hotelsId, Pageable pageable);
	
	Page<LivingRoom> findAllByTeatherBetween(Integer minTeather, Integer maxTeather, Pageable pageable);

	Page<LivingRoom> findAll(Pageable pageable);

	Page<LivingRoom> findByHotelId(String hotelId, Pageable pageable);

	Page<LivingRoom> findByCategoryIn(List<String> category, Pageable pageable);

	Page<LivingRoom> findByHotelIdAndCategoryIn(String hotelId, List<String> category, Pageable pageable);

	Optional<LivingRoom> findTopByTeatherNotNullOrderByTeatherAsc();

	Optional<LivingRoom> findTopByTeatherNotNullOrderByTeatherDesc();

	@Query(value = "{ 'destinationId' : ?0, 'hotelId' : ?1 }", fields = "{ 'name' : 1, 'sliders' : 1 }")
	List<LivingRoom> customFindLivingRoomByDestinationIdAndHotelId(String destinationId, String hotelId, Sort sort);

	@Query(value = "{ 'destinationId' : ?0, 'hotelId' : ?1, 'category' :{ $in : ?2 } }", fields = "{ 'name' : 1, 'sliders' : 1 }")
	List<LivingRoom> customFindLivingRoomByDestinationIdAndHotelIdAndCategory(String destinationId, String hotelId, List<String> categories , Sort sort);

}
