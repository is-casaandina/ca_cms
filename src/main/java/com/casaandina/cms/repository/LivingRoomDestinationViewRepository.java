package com.casaandina.cms.repository;

import com.casaandina.cms.model.LivingRoomDestinationView;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LivingRoomDestinationViewRepository extends MongoRepository<LivingRoomDestinationView, String> {

    @Query(value = "{'destinationId' : { $ne : null }, 'destination' : { $ne : null }}", fields = "{ 'destinationId' : 1, 'destination' : 1 }")
    List<LivingRoomDestinationView> customFindAllDestinationWithLivingRoomAndDestinationNot();

    @Query(value = "{'hotelId' : { $ne : null }}", fields = "{ 'hotelId' : 1 }")
    List<LivingRoomDestinationView> customFindAllLivingHotels();

    @Query(value = "{ 'destinationId' : { $in : ?0 }, 'hotelId' : { $ne : null }}",
            fields = "{ 'hotelId' : 1 }")
    List<LivingRoomDestinationView> customFindAllLivingHotelsByDestinations(List<String> destination);
}
