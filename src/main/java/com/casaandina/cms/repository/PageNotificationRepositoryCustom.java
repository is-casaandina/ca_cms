package com.casaandina.cms.repository;

import com.casaandina.cms.model.LanguagePageNotification;
import java.util.List;

public interface PageNotificationRepositoryCustom {
    List<LanguagePageNotification> findByPageIdAndLanguageCode(String pageId, String language);
}
