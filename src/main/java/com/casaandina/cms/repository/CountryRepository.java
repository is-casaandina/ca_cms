package com.casaandina.cms.repository;

import com.casaandina.cms.dto.CountryRequest;
import com.casaandina.cms.model.Country;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryRepository extends MongoRepository<Country, String> {
	
	List<CountryRequest> findAllByStatus(Boolean status);
	Optional<Country> findByCode(String code);

	@Query(value = "{ 'id': ?0, 'status' : ?1 }", fields = "{ 'id' : 1, 'name' : 1 }")
	Optional<Country> findByIdAndStatusCustomized(String countryId, Boolean status);
	
	@Query(value = "{ 'code': ?0, 'status' : ?1 }", fields = "{ 'id' : 1, 'name' : 1 }")
	Optional<Country> findByCodeAndStatusCustomized(String code, Boolean status);

	@Query(value = "{ 'status' : ?0, 'activePage' : ?1 }", fields = "{ 'id' : 1, 'name': 1, 'code': 1 }")
	List<Country> customFindAllByStatus(Boolean status, Boolean activePage);

	@Query(value = "{ 'status' : ?0 }", fields = "{ 'id' : 1, 'name': 1, 'code': 1 }")
	List<Country> customFindAllByStatus(Boolean status);

}
