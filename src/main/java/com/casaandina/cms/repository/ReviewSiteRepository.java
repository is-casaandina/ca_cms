package com.casaandina.cms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.casaandina.cms.model.ReviewSite;

@Repository
public interface ReviewSiteRepository extends MongoRepository<ReviewSite, String> {
	
	ReviewSite findBySlug(String name);

}
