package com.casaandina.cms.serviceclient;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.serviceclient.response.MethodResponse;

import java.util.List;

public interface RoibackServiceClient {

    MethodResponse getRoibackElement(final String roiback, final String language) throws CaRequiredException;

    List<PageHeader> getPageHeaderRoiback();
}
