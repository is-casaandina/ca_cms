package com.casaandina.cms.serviceclient.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "value")
@XmlAccessorType(XmlAccessType.FIELD)
public class Value {

    private Struct struct;

    private Array array;

    @XmlElement(name = "string")
    private String stringValue;

    @XmlElement(name = "boolean")
    private Boolean booleanValue;

    private Object nil;

    @XmlElement(name = "int")
    private Integer intValue;

    @XmlElement(name = "double")
    private Double doubleValue;

}
