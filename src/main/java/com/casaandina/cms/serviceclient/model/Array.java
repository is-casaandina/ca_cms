package com.casaandina.cms.serviceclient.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@lombok.Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "array")
@XmlAccessorType(XmlAccessType.FIELD)
public class Array {

    private Data data;
}
