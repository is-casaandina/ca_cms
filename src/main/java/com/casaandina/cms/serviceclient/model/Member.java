package com.casaandina.cms.serviceclient.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "member")
public class Member {

    private String name;
    private Value value;
}
