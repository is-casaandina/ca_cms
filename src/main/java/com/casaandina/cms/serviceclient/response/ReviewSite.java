package com.casaandina.cms.serviceclient.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewSite {
	
	private String name;
	
	private String slug;
	
	private String logoUrl;

}
