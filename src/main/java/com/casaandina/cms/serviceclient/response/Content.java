package com.casaandina.cms.serviceclient.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = Include.NON_NULL)
public class Content {
	
	private String title;
	
	private String body;
	
	private String author;
	
	private String authorLocation;
	
	private Integer dateReview;
	
	private Integer dateCollected;
	
	private Integer updatedAt;
	
	private Double rating;
	
	private ReviewSite reviewSite;
	
	private String name;
	
	private String logo;
	
	private String url;
	
	private String address1;
	
	private String address2;
	
	private String city;
	
	private String state;
	
	private String postalCode;
	
	private String country;
	
	private Integer tripAdvisorId;
	
	private String revinateLoginUri;
	
	private String accountType;
	
}
