package com.casaandina.cms.serviceclient.response;

import com.casaandina.cms.serviceclient.model.Params;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "methodResponse")
public class MethodResponse {

    private Params params;
}
