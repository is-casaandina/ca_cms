package com.casaandina.cms.serviceclient.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValuesByReviewSite {
	
    private Integer reviewSiteId;

    private Values values;

}
