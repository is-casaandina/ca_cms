package com.casaandina.cms.serviceclient.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RevinateRatingsResponse {
	
    private AggregateValues aggregateValues;

    private List<ValuesByReviewSite> valuesByReviewSite;

}
