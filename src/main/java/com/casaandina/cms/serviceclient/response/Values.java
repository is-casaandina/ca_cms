package com.casaandina.cms.serviceclient.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Values {

    private Double averageRating;

}
