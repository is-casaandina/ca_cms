package com.casaandina.cms.serviceclient;

import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.serviceclient.response.RevinateOpinionsResponse;
import com.casaandina.cms.serviceclient.response.RevinateRatingsResponse;

public interface RevinateServiceClient {
	
	RevinateOpinionsResponse consultOpinionsRevinate(String url) throws CaRequiredException;
	
	RevinateRatingsResponse consultRatingsRevinate(String url) throws CaRequiredException;
	
	RevinateOpinionsResponse synchronizeHotels(int page) throws CaRequiredException;

}
