package com.casaandina.cms.serviceclient.impl;

import com.casaandina.cms.component.RestTemplateCustom;
import com.casaandina.cms.dto.RoibackHotelDaysDto;
import com.casaandina.cms.dto.RoibackWebService;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.model.PageHeader;
import com.casaandina.cms.model.Parameter;
import com.casaandina.cms.model.Price;
import com.casaandina.cms.model.RoibackLowPrice;
import com.casaandina.cms.repository.PageHeaderRepository;
import com.casaandina.cms.repository.ParameterRepository;
import com.casaandina.cms.repository.RoibackLowPriceRepository;
import com.casaandina.cms.serviceclient.RoibackServiceClient;
import com.casaandina.cms.serviceclient.model.*;
import com.casaandina.cms.serviceclient.response.MethodResponse;
import com.casaandina.cms.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Log4j2
@RequiredArgsConstructor
public class RoibackServiceClientImpl implements RoibackServiceClient {

    private final RestTemplateCustom restTemplateCustom;
    private final ParameterRepository parameterRepository;
    private final RoibackLowPriceRepository roibackLowPriceRepository;
    private final PageHeaderRepository pageHeaderRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(RoibackServiceClientImpl.class);

    @Override
    public MethodResponse getRoibackElement(final String roiback, final String language) throws CaRequiredException {
        try {
            RoibackWebService roibackWebService = ObjectMapperUtils.map(parameterRepository.customFindRoibackWebServiceByCode(ParameterType.ROIBACK_WEBSERVICE.getCode()).getIndicator(), RoibackWebService.class);

            MethodResponse methodResponse = this.restTemplateCustom.exchangeResponseUtf8(roibackWebService.getUrlService(), HttpMethod.POST, getHttpEntityForRoiback(roiback, language, roibackWebService), MethodResponse.class);

            saveRoibackPricesOnMongo(methodResponse, roiback, language);

            return methodResponse;
        } catch (Exception e) {
            LOGGER.error(String.format(ConstantsUtil.AN_ERROR_OCCURRED, e.getMessage()));
            return null;
        }

    }

    private HttpEntity<?> getHttpEntityForRoiback(String roiback, String language,final RoibackWebService roibackWebService) throws CaRequiredException{

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Accept", MediaType.APPLICATION_XML_VALUE);
        MediaType mediaType = new MediaType(MediaType.APPLICATION_XML, StandardCharsets.UTF_8);
        httpHeaders.setContentType(mediaType);

        MethodCall methodCall = getMethodCallParams(roiback, language, roibackWebService);
        return new HttpEntity<>(methodCall, httpHeaders);
    }

    private MethodCall getMethodCallParams(String roiback, String language, final RoibackWebService roibackWebService) throws CaRequiredException{

        Parameter parameterRoibackHotelDays = getStateByCodeAndLanguage(ParameterType.ROIBACK_HOTEL_DAYS.getCode(),
                LanguageType.SPANISH.getCode());

        RoibackHotelDaysDto roibackHotelDaysDto = parameterRoibackHotelDays != null ? ObjectMapperUtils.map(
                parameterRoibackHotelDays.getIndicator(), RoibackHotelDaysDto.class) : null;

        List<Param> params = new ArrayList<>();

        params.add(Param.builder()
                .value(Value.builder()
                        .stringValue(roibackWebService.getUserService())
                        .build())
                .build());

        params.add(Param.builder()
                .value(Value.builder().stringValue(null).build())
                .build());

        params.add(Param.builder()
                .value(Value.builder().stringValue(language).build())
                .build());

        List<Value> valuesRoiback = new ArrayList<>();
        valuesRoiback.add(Value.builder().stringValue(roiback).build());

        params.add(Param.builder()
                .value(Value.builder()
                        .array(Array.builder()
                                .data(Data.builder()
                                        .value(valuesRoiback).build())
                                .build())
                        .build())
                .build());

        LocalDate now = LocalDate.now();
        LocalDate nowPost = LocalDate.now().plusDays(ConstantsUtil.ONE);

        if(Objects.nonNull(roibackHotelDaysDto)){
            now = now.plusDays(roibackHotelDaysDto.getFrom());
            nowPost = ObjectMapperUtils.map(now, LocalDate.class);
            nowPost = nowPost.plusDays(roibackHotelDaysDto.getUntil());
        }

        params.add(Param.builder()
                .value(Value.builder().stringValue(now.toString()).build())
                .build());

        params.add(Param.builder()
                .value(Value.builder().stringValue(nowPost.toString()).build())
                .build());

        params.add(Param.builder()
                .value(Value.builder().stringValue(null).build())
                .build());

        params.add(Param.builder()
                .value(Value.builder()
                        .struct(Struct.builder().build())
                        .build())
                .build());

        return MethodCall.builder()
                .methodName(roibackWebService.getMethodName())
                .params(Params.builder().param(params).build())
                .build();
    }

    public Parameter getStateByCodeAndLanguage(String code, String languageCode) throws CaRequiredException {

        Optional<Parameter> parameterOptional = this.parameterRepository.findByCodeAndLanguageCode(code, languageCode);

        if (!parameterOptional.isPresent()) {
            throw new CaRequiredException(ConstantsUtil.NOT_FOUND_CODE);
        } else {
            return parameterOptional.get();
        }
    }

    private void saveRoibackPricesOnMongo(MethodResponse methodResponse, String roiback, String language){
        try {
            List<Member> memberList = getMemberWithDispo(methodResponse);
            if(Objects.nonNull(memberList) && !memberList.isEmpty()){
                List<Price> prices = new ArrayList<>();
                memberList.forEach(member -> {
                        if(Objects.nonNull(member) && Objects.nonNull(member.getValue()) && Objects.nonNull(member.getValue().getStruct())
                                && Objects.nonNull(member.getValue().getStruct().getMember()) && !member.getValue().getStruct().getMember().isEmpty()){
                            member.getValue().getStruct().getMember().forEach(x -> {
                                if(Objects.nonNull(x.getValue()) && Objects.nonNull(x.getValue().getDoubleValue())){
                                    Double priceFrom = CaUtil.roundDouble(x.getValue().getDoubleValue(), ConstantsUtil.ZERO, Boolean.TRUE);
                                    LOGGER.info("priceFrom - currency: " + x.getName() + " value: " + priceFrom);
                                    prices.add(Price.builder().currency(x.getName()).from(priceFrom.intValue()).build());
                                }
                            });
                        }
                });
                persistMongoDb(prices, roiback, language);
            }
        } catch (Exception e){
            log.error(e.getMessage());
        }
    }

    private void persistMongoDb(List<Price> prices, String roiback, String language){
        if(Objects.nonNull(prices) && !prices.isEmpty()){
            List<Price> pricesForMongoDb = getListPriceLower(prices);
            if(Objects.nonNull(pricesForMongoDb) && !pricesForMongoDb.isEmpty()){
                this.roibackLowPriceRepository.save(RoibackLowPrice.builder()
                        .roiback(roiback)
                        .prices(pricesForMongoDb)
                        .language(language)
                        .createdDate(LocalDateTime.now())
                        .build());
            }
        }
    }

    private List<Member> getMemberWithDispo(MethodResponse methodResponse){
        try {
            List<Member> members = new ArrayList<>();
            Member memberWithDispo = null;
            if(methodResponse.getParams() != null && methodResponse.getParams().getParam() != null &&
                    !methodResponse.getParams().getParam().isEmpty() ){

                for(Param param : methodResponse.getParams().getParam()){
                    memberWithDispo = param.getValue().getStruct().getMember().stream()
                            .filter(x -> ConstantsUtil.RoibackWebService.CON_DISPO.equals(x.getName()))
                            .findFirst().orElse(null);
                    if(memberWithDispo != null && ConstantsUtil.RoibackWebService.CON_DISPO.equals(memberWithDispo.getName())){
                        break;
                    }
                }
            }

            if(Objects.nonNull(memberWithDispo)){
                members.addAll(getListMemberFromWithDispo(memberWithDispo));
            }
            return members;
        } catch(Exception e){
            log.error("Ocurrio un error: "+ e.getMessage());
            return Collections.emptyList();
        }
    }

    private List<Member> getListMemberFromWithDispo(Member memberWithDispo){
        List<Member> memberList = new ArrayList<>();

        Member memberBedRooms = getMemberFromKey(memberWithDispo, ConstantsUtil.RoibackWebService.BEDROOMS);
        if(Objects.nonNull(memberBedRooms)){
            List<Member> membersRates = getMembersFromKey(memberBedRooms, ConstantsUtil.RoibackWebService.RATES);
            if(CaUtil.validateListIsNotNullOrEmpty(membersRates)){
                membersRates.forEach(rate -> {
                    List<Member> membersActivities = getMembersFromKey(rate, ConstantsUtil.RoibackWebService.ACTIVITIES);
                    if(CaUtil.validateListIsNotNullOrEmpty(membersActivities)){
                        membersActivities.forEach( activity -> {
                            List<Member> membersPensions = getMembersFromKey(activity, ConstantsUtil.RoibackWebService.PENSIONS);
                            if(CaUtil.validateListIsNotNullOrEmpty(membersPensions)){
                                membersPensions.forEach(pension -> {
                                    Member memberNHalfPrice = getMemberFromKey(pension, ConstantsUtil.RoibackWebService.NHALF_PRICE);
                                    if(Objects.nonNull(memberNHalfPrice)){
                                        memberList.add(memberNHalfPrice);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
        return memberList;
    }

    private Member getMemberFromKey(Member member, String key){

        if (member.getValue() != null && member.getValue().getArray() != null
                    && member.getValue().getArray().getData() != null
                    && member.getValue().getArray().getData().getValue() != null
                    && !member.getValue().getArray().getData().getValue().isEmpty()) {
            return getMemberFromValue(member.getValue().getArray().getData().getValue(), key);
        }
        return null;
    }

    private List<Member> getMembersFromKey(Member member, String key){
        if (member.getValue() != null && member.getValue().getArray() != null
                && member.getValue().getArray().getData() != null
                && member.getValue().getArray().getData().getValue() != null
                && !member.getValue().getArray().getData().getValue().isEmpty()) {
            return getMembersFromValue(member.getValue().getArray().getData().getValue(), key);
        }
        return Collections.emptyList();
    }

    private Member getMemberFromValue(List<Value> values, String key){
        Member member = null;
        if(values!= null && !values.isEmpty()){
            for(Value value : values){
                member = value.getStruct().getMember().stream().filter(x-> key.equals(x.getName())).findFirst().orElse(null);
                if(Objects.nonNull(member)){
                    break;
                }
            }
        }
        return member;
    }

    private List<Member> getMembersFromValue(List<Value> values, String key){
        List<Member> members = new ArrayList<>();
        if(values!= null && !values.isEmpty()){
            values.stream().forEach(value -> {
                List<Member> membersPensions = value.getStruct().getMember().stream()
                        .filter(x-> key.equals(x.getName())).collect(Collectors.toList());
                members.addAll(membersPensions);
            });
        }
        return members;
    }

    @Override
    public List<PageHeader> getPageHeaderRoiback(){
        return pageHeaderRepository.customFindDestinationsByStateNotPublish(TemplateType.HOTELS_DETAIL.getCode(),
                StatusPageType.DELETED.getCode(), Boolean.TRUE);
    }

    public List<Price> getListPriceLower(List<Price> prices){
        List<Price> priceList = new ArrayList<>();
        if(Objects.nonNull(prices) && !prices.isEmpty()){
            Map<String, List<Price>> priceMap = prices.stream().collect(Collectors.groupingBy(Price::getCurrency));
            priceMap.forEach((key, value) -> {
                List<Price> priceListOrder = value.stream()
                        .sorted(Comparator.comparing(Price::getFrom))
                        .collect(Collectors.toList());
                Price price = priceListOrder.stream().findFirst().orElse(null);
                if(Objects.nonNull(price) && Objects.nonNull(price.getFrom())){
                    priceList.add(Price.builder().currency(key).from(price.getFrom()).build());
                }
            });
        }

        return priceList;
    }
}
