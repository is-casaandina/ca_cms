package com.casaandina.cms.serviceclient.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.casaandina.cms.component.RestTemplateCustom;
import com.casaandina.cms.exception.CaRequiredException;
import com.casaandina.cms.serviceclient.RevinateServiceClient;
import com.casaandina.cms.serviceclient.response.RevinateOpinionsResponse;
import com.casaandina.cms.serviceclient.response.RevinateRatingsResponse;
import com.casaandina.cms.util.CaUtil;
import com.casaandina.cms.util.ConstantsUtil;
import com.casaandina.cms.util.RevinateProperties;

@Component
public class RevinateServiceClientImpl implements RevinateServiceClient {
	
	private final RestTemplateCustom restTemplateCustom;
	
	private final RevinateProperties revinateProperties;
	
	@Autowired
	public RevinateServiceClientImpl(RestTemplateCustom restTemplateCustom,
			RevinateProperties revinateProperties) {
		
		this.restTemplateCustom = restTemplateCustom;
		this.revinateProperties = revinateProperties;
	}
	
	public RevinateOpinionsResponse consultOpinionsRevinate(final String url) throws CaRequiredException {
		
		return this.restTemplateCustom.getResponse(url, HttpMethod.GET, getHttpEntityForRevinate(), RevinateOpinionsResponse.class);
		
	}
	
	public RevinateRatingsResponse consultRatingsRevinate(final String url) throws CaRequiredException {
		
		return this.restTemplateCustom.getResponse(url, HttpMethod.GET, getHttpEntityForRevinate(), RevinateRatingsResponse.class);

	}
	
	public RevinateOpinionsResponse synchronizeHotels(int page) throws CaRequiredException {
		/**
		StringBuilder url = new StringBuilder(revinateProperties.getUrl()).append("?page=").append(page);
				
		ResponseEntity<RevinateOpinionsResponse> responseEntity = getRevinateResponse(url.toString(), RevinateOpinionsResponse.class);
		
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			return responseEntity.getBody();
		} else {
			throw new CaRequiredException(this.messageProperties.getMessage(PropertiesConstants.SERVICE_REVINATE_NOT_AVAILABLE));
		}*/
		
		return null;
		
	}
	
	private HttpEntity<?> getHttpEntityForRevinate() {
		final String timestamp = String.valueOf(System.currentTimeMillis()/1000);
		final String encoded = CaUtil.calculateHMAC(this.revinateProperties.getSecret(),
				this.revinateProperties.getUsername(), timestamp);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set(ConstantsUtil.REVINATE_USERNAME, revinateProperties.getUsername());
		httpHeaders.set(ConstantsUtil.REVINATE_TIMESTAMP, timestamp);
		httpHeaders.set(ConstantsUtil.REVINATE_KEY, revinateProperties.getKey());
		httpHeaders.set(ConstantsUtil.REVINATE_ENCODED, encoded);
		
		return new HttpEntity<>(httpHeaders);
	}

}
