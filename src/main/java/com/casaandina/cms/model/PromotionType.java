package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PromotionType {

    @Id
    private String id;
    private List<LanguageDescription> languages;
    private Integer state;
    private LocalDateTime createdDate;
}
