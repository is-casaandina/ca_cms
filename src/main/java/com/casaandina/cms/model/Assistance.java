package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "assistance")
public class Assistance {

    @Id
    private String id;
    private String iconClass;
    private List<AssistanceTitle> assistanceTitles;
    private Integer type;
    private Integer status;

    @CreatedDate
    private LocalDateTime createdDate;

    @Transient
    private String englishTitle;
    @Transient
    private String spanishTitle;
    @Transient
    private String portugueseTitle;
}
