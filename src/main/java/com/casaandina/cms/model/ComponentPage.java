package com.casaandina.cms.model;

import java.util.List;

import lombok.Data;

@Data
public class ComponentPage {

	private String code;
	private List<PropertyPage> properties;

}
