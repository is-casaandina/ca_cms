package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Domain {

    @Id
    private String id;
    private String path;
    private Integer state;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private String owner;
    private String user;
    private Double tax;
    private String promotionAllId;
    private String language;
    private String currency;
    private Boolean hideLanguage;
}
