package com.casaandina.cms.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "rating")
public class Rating {
	
	private String id;
	
	private String reviewSiteName;
	
	private Double avarageRating;
	
	private String hotelCode;
	
	private String rankingMessage;
	
	@CreatedDate
	private LocalDateTime createdDate;

}
