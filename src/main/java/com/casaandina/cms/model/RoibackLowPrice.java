package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(value = "roibackLowPrice")
public class RoibackLowPrice {

    @Id
    private String id;
    private String roiback;
    private String language;
    private List<Price> prices;
    private LocalDateTime createdDate;

    @Transient
    private String urlReservation;
}
