package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PagePromotionInfoView {

    private String descriptionLeft;
    private String descriptionRight;
    private Double price;
    private Double discount;
    private String title;
    private String description;
    private List<Object> imagePromotion;
    private Boolean isRestriction;
    private List<Object> imageRestriction;
    private String bodyRestriction;
    private String textButtonRestriction;
    private String footerRestriction;
    private String urlReservation;
}
