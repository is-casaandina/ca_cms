package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document(collection = "videoS3")
public class VideoS3 {

	@Id
	private String id;

	private String name;
	private String mimeType;
	private PropertyImage properties;
	private String url;
	private Image small;
	private Image medium;
	private Image large;
	private Double size;

	@JsonIgnore
	private LocalDateTime createDate;

	@JsonIgnore
	private LocalDateTime updateDate;

	private String userName;

	public VideoS3() {
		super();
		LocalDateTime now = LocalDateTime.now();
		this.createDate = now;
		this.updateDate = now;
	}

}
