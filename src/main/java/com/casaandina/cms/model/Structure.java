package com.casaandina.cms.model;

import java.util.List;

import lombok.Data;

@Data
public class Structure {

	private String template;
	private List<SectionPage> sections;

}
