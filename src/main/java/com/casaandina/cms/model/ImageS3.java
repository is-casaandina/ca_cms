package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document(collection = "imageS3")
public class ImageS3 {

	@Id
	private String id;

	private String name;
	private String mimeType;
	private PropertyImage properties;
	private Image small;
	private Image medium;
	private Image large;
	@JsonIgnore
	private Image extraSmall;

	private LocalDateTime createDate;

	@JsonIgnore
	private LocalDateTime updateDate;

	private String userName;

}
