package com.casaandina.cms.model;

import lombok.Data;

@Data
public class PropertyPage {

	private String code;
	private Object value;

}
