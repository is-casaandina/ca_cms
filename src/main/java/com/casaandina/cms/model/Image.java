package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Image {

	private String name;
	private Integer width;
	private Integer height;
	private String imageUri;
	private Double size;

	public Image(Integer width, Integer height) {
		this.width = width;
		this.height = height;
	}

}
