package com.casaandina.cms.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Document(collection = "pageHeader")
public class PageHeader {

	@Id
	private String id;
	private String name;
	private String username;
	private String fullname;
	private Integer state;
	private List<String> languages;
	private LocalDateTime createdDate;
	private LocalDateTime lastUpdate;
	private String parentPath;
	private Integer type;
	private Integer pageType;
	private String categoryId;
	private String destinationId;
	private String countryId;
	private Boolean publish;
	private String roiback;
	private String revinate;
	private LocalDateTime beginDate;
	private LocalDateTime firstExpiration;
	private LocalDateTime secondExpiration;
	private Boolean countdown;
	private String shortName;
	private String genericType;

	private Boolean inEdition;

	private String userEdition;

	private String userSetFree;

	@Transient
	private String destinationName;

}
