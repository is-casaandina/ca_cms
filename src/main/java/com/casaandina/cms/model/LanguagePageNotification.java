package com.casaandina.cms.model;

import lombok.Data;

@Data
public class LanguagePageNotification {
    private String language;
    private String title;
    private String detail;
}
