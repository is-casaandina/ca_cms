package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TemplateType {

    private String id;
    private String name;
    private Boolean system;
    private Boolean viewPage;
    private LocalDateTime createdDate;
}
