package com.casaandina.cms.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Document(collection = "pageNotification")
public class PageNotification {
    @Id
    private Integer id;
    private String pageId;
    private String name;
    private LocalDateTime beginDate;
    private LocalDateTime endDate;
    private List<LanguagePageNotification> languages;

    private Boolean status;
    private Integer createdBy;
    @CreatedDate
    private LocalDateTime createdDate;
    private Integer updatedBy;
    @LastModifiedDate
    private LocalDateTime updatedDate;
}
