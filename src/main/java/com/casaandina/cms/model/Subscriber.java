package com.casaandina.cms.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Document(collection = "subscriber")
@JsonInclude(value = Include.NON_NULL)
public class Subscriber {

	@Id
	private String id;
	private String name;
	private String email;
	private String phone;
	private String surname;
	private String businessName;
	private String ruc;
	private Integer segment;

	@DBRef
	private Country country;

	@DBRef
	private Page destination;

	private String city;
	
	private Integer gender;
	private Integer motive;
	private String address;
	private String message;
	private String typeSuscriber;
	private Boolean sendNotification;
	private Boolean sendNew;
	private Integer status;
	private LocalDate birthdate;
	private LocalDateTime registerDate;

}
