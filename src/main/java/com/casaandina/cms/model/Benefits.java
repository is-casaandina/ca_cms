package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "benefits")
public class Benefits {
    @Id
    private String id;
    private String textEs;
    private Link linkEs;
    private String textEn;
    private Link linkEn;
    private String textPr;
    private Link linkPr;
    private List<DetailBenefit> detail;

}

