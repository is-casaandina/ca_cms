package com.casaandina.cms.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "currency")
@JsonInclude(value = Include.NON_NULL)
public class Currency {

	@Id
	private String codeIso;

	private String name;

	private String symbol;

	private Double value;

	private LocalDate exchangeRateDate;

	private Integer isPrincipal;

	@JsonIgnore
	private Integer status;

	@JsonIgnore
	private LocalDate createdDate;

	private LocalDate updatedDate;

}
