package com.casaandina.cms.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "opinion")
public class Opinion {
	
	@Id
	private String id;
	
	private String title;
	
	private String body;
	
	private String author;
	
	private Integer dateReview;
	
	private Double rating;
	
	private String hotelCode;
	
	@DBRef
	private ReviewSite reviewSite;
	
	@CreatedDate
	private LocalDateTime createdDate;

}
