package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = Include.NON_NULL)
public class PropertyImage {

	private String title;
	private String alt;
	private String description;
	private Integer numberFrame;
	private String type;

	public PropertyImage(String title, String alt, String description) {
		super();
		this.title = title;
		this.alt = alt;
		this.description = description;
	}

}
