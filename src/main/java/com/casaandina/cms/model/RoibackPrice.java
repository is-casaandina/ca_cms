package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoibackPrice {
    private String urlPricePen;
    private String urlPriceUsd;
    private String urlPriceBrl;
}
