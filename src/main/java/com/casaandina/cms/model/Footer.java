package com.casaandina.cms.model;

import lombok.Data;

@Data
public class Footer {
	
	private String description;
	
	private String link;

}
