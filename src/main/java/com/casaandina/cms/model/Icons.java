package com.casaandina.cms.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "icons")
public class Icons {
	
	private String id;
	private String className;
	private List<String> filters;
	private boolean custom;

}
