package com.casaandina.cms.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Builder
@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "document")
public class Document {
	
	@Id
	private String id;
	
	private String name;
	
	private String mimeType;
	
	private String extension;
	
	private String directory;
	
	private String url;

	private Double size;
	
	@CreatedDate
	private LocalDateTime createdDate;

	private String userName;

}
