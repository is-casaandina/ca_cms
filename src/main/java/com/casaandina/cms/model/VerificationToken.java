package com.casaandina.cms.model;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.casaandina.cms.util.StatusTokenType;

import lombok.Data;

@Data
@Document(collection = "verificationToken")
public class VerificationToken {

	@Id
	private String id;

	private String token;

	private Integer status;

	private LocalDateTime expirationDate;

	private LocalDateTime issuedDate;

	private LocalDateTime confirmationDate;

	private Integer userId;

	public VerificationToken() {
		this.token = UUID.randomUUID().toString();
		this.issuedDate = LocalDateTime.now();
		this.expirationDate = this.issuedDate.plusDays(1);
		this.status = StatusTokenType.PENDING.getCode();
	}

}
