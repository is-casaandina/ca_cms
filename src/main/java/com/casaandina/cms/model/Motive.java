package com.casaandina.cms.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Document(collection = "motive")
public class Motive {

	@Id
	private String id;
	private String description;
	
	@JsonIgnore
	private Integer status;
	
	@JsonIgnore
	private String language;

}
