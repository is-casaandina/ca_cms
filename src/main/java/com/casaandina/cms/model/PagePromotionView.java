package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Document(collection = "pagePromotion_view")
public class PagePromotionView {

    @Id
    private String id;
    private LocalDateTime modifiedDate;
    private String genericType;
    private String language;
    private String path;
    private String domainId;
    private String clusterId;
    private String countryId;
    private String roiback;
    private PagePromotionInfoView info;
    private PagePromotionItemView item;
    private List<LanguagePage> languagesPage;
    private LocalDateTime beginDate;
    private LocalDateTime firstExpiration;
    private LocalDateTime secondExpiration;
}
