package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "draftPage")
@JsonInclude(value = Include.NON_NULL)
public class DraftPage {

	@Id
	private String id;
	private String name;
	private Integer pageType;
	private Integer type;
	private String user;
	private String owner;
	private Integer state;
	private String countryId;
	private String categoryId;
	private String descriptorId;
	private String destinationId;
	private LocalDateTime createdDate;
	private LocalDateTime publishDate;
	private LocalDateTime futurePublishDate;
	private LocalDateTime unpublishedDate;
	private LocalDateTime modifiedDate;
	private List<LanguagePage> languages;
	private Boolean external;
	private Boolean publish;
	private String roiback;
	private String revinate;
	private LocalDateTime beginDate;
	private LocalDateTime firstExpiration;
	private LocalDateTime secondExpiration;
	private Boolean countdown;
	private String shortName;
	private String genericType;
	private String domainId;
	private String clusterId;

	private Contact contact;
	private String destinationMapId;

	private String defaultHome;

	@Transient
	private Boolean inEdition;

	@Transient
	private String categoryName;

}
