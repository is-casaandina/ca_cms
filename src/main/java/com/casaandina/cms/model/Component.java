package com.casaandina.cms.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "components")
public class Component {

	@Id
	private String id;

	private String name;
	private String code;
	private String description;
	private Integer type;
	private String category;
	private String imageUri;
	private Integer status;
	private String ref;
	private String template;

	private List<Property> properties;

}
