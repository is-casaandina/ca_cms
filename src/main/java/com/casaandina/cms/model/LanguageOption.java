package com.casaandina.cms.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class LanguageOption implements Serializable {

	private static final long serialVersionUID = 8670519249948011240L;
	private String language;
	private String path;
	private String name;

}
