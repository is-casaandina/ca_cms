package com.casaandina.cms.model;

import java.util.List;

import lombok.Data;

@Data
public class SectionTemplate {

	private String id;
	private String cols;
	private String colsPage;
	private String sectionParent;
	private Integer level;
	private List<Style> styles;
	private List<ComponentTemplate> components;

}
