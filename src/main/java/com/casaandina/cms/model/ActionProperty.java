package com.casaandina.cms.model;

import lombok.Data;

import java.util.List;

@Data
public class ActionProperty {

    private String event;
    private String request;
    private List<String> dependencies;

}
