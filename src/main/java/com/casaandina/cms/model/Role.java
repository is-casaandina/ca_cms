package com.casaandina.cms.model;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "role")
public class Role {

	@Id
	private String roleCode;

	private String roleName;

	@Field("permissionCodeList")
	private List<String> permissionCodeList;

	@JsonIgnore
	private Integer status;

	@JsonIgnore
	@LastModifiedDate
	private LocalDateTime updatedDate;

	@JsonIgnore
	@CreatedDate
	private LocalDateTime createdDate;

}
