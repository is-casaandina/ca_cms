package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude( value =  JsonInclude.Include.NON_NULL)
public class PagePromotionItemView {

    private String descriptor;
    private String descriptorColor;
    private String descriptorTitle;
    private String country;
    private String destination;
    private String hotel;
    private String titleHotel;
    private String restaurant;
    private Double price;
    private Double discount;
    private Integer days;
    private Integer nights;
    private Boolean isRestriction;
    private String restriction;
    private String bodyRestriction;
    private String footerRestriction;
    private List<Object> backgroundImage;
    private List<Object> imageMain;
    private String description;
    private String type;
    private Boolean isSelected;
    private String urlReservation;
    private List<Object> imageRestriction;
}
