package com.casaandina.cms.model;

import lombok.Data;

@Data
public class Group {

	private String property;
	private String value;

}
