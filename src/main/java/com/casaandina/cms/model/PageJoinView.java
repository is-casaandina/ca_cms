package com.casaandina.cms.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "pageJoin_view")
public class PageJoinView {
    @Id
    private String id;
    private String name;
    private String countryId;
    private String genericType;
    private String categoryId;
    private Integer state;
}
