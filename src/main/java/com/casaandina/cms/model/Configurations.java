package com.casaandina.cms.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@Document(collection = "configurations")
public class Configurations {
    @Id
    private String id;
    private String sitename;
    private String company;
    private String logo;
    private String icon;
    private String copyright;
    private String css;
    private List<String> languages;
    private String defaultLanguage;
    private Metadata metadata;
    private Script script;
    private SocialNetworking socialNetworking;
    private Roiback roiback;
    private RoibackPrice roibackPrice;
    private String keywords;
    private List<String> iconsPay;
    private String homeId;
    private String fontIcons;
    private String domain;
    private ContactConfigurations contact;
}