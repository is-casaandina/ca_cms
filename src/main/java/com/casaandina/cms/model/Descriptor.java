package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document(collection = "descriptor")
@AllArgsConstructor
@NoArgsConstructor
public class Descriptor {
	
	@Id
	private String id;
	
	private String name;
	
	private String url;

	private String urlBanner;

	private String typographyStyle;
	
	private String color;
	
	private String title;
	
	private String category;
	
	@CreatedDate
	private LocalDateTime createdDate;
	
	@LastModifiedDate
	private LocalDateTime updatedDate;

}
