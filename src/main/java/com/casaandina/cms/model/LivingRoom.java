package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Document(collection = "livingRoom")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class LivingRoom {
	
	@Id
	private String id;
	
	private String name;
	
	private String squareMeters;
	
	private String lengthy;
	
	private String width;
	
	private String high;
	
	private Integer teather;
	
	private String school;
	
	private String banquet;
	
	private String directory;
	
	private String cocktail;
	
	private String hotelId;
	
	private String destinationId;

	private Integer order;

	@Transient
	private String destination;

	private List<LivingRoomSlider> sliders;

	private LocalDateTime lastUpdate;

	private String category;

}
