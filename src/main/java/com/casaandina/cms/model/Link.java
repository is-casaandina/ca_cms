package com.casaandina.cms.model;

import lombok.Data;

@Data
public class Link {
    private String url;
    private Boolean isExternal;
}
