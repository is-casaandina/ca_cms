package com.casaandina.cms.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "permission")
public class Permission {

	@Id
	private String permissionCode;

	private String permissionName;

	@JsonIgnore
	private String url;

	@JsonIgnore
	private Integer status;

	@JsonIgnore
	private LocalDateTime updatedDate;

	@JsonIgnore
	private LocalDateTime createdDate;

}
