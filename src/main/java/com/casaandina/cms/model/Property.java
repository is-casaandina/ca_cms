package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Property {

	private String code;
	private String label;
	private String description;
	private Integer cols;
	private String type;
	private String typeArray;
	private String design;
	private String labelItem;
	private String dependenceLabel;
	private Integer size;
	private String imageSize;
	private List<String> types;
	private Object definitions;
	private String typeDescription;
	private List<Property> properties;
	private String regex;
	private List<String> mimeTypes;
	private Integer maxLength;
	private Integer minLength;
	private Integer max;
	private Integer min;
	private Boolean readonly;
	private String placeholder;
	private List<String> list;
	private Boolean checked;
	private Boolean required;
	private Integer height;
	private Integer width;
	private Long weight;
	private Boolean isFloat;
	private ActionProperty action;

}
