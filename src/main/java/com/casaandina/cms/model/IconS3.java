package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Builder
@Data
@Document(collection = "iconS3")
@AllArgsConstructor
public class IconS3 {

	@Id
	private String id;
	private String name;
	private String mimeType;
	private PropertyImage properties;
	private Integer type;
	private Integer width;
	private Integer height;
	private String url;
	private LocalDate createDate;
	private LocalDate updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	private Integer status;

	private String userName;

	public IconS3() {
		super();
		LocalDate now = LocalDate.now();
		this.createDate = now;
		this.updatedDate = now;
	}

}
