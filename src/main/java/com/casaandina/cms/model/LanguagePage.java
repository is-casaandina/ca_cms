package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class LanguagePage {

	private String language;
	private String canonical;
	private String path;
	private String pagePath;
	private String parentPath;
	private Metadata metadata;
	private String social;
	private String title;
	private String category;
	private List<String> tags;
	private Content content;
	private Object data;
	private List<LanguageDetailContact> contact;

}
