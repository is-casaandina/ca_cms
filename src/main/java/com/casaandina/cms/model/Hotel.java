package com.casaandina.cms.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

@Data
@Document(collection = "hotel")
public class Hotel {
	
	@Id
	@Field(value = "_id")
	private ObjectId id;
	
	private String name;
	
	private String code;
	
	private String address;
	
	private String city;
	
	private String country;

}
