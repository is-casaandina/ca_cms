package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Contact {

    private String title1;
    private String title2;
    private List<LanguageContact> languages;
}
