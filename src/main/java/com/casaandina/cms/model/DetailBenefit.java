package com.casaandina.cms.model;

import lombok.Data;

@Data
public class DetailBenefit {
    private String icon;
    private String titleEsp;
    private String titleEng;
    private String titlePor;
    private Boolean hide;
}
