package com.casaandina.cms.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "template")
public class Template {

	@Id
	private String id;
	private String code;
	private String name;
	private String description;
	private String imageUri;
	private String html;
	private String categoryId;
	private List<String> refComponents;

}
