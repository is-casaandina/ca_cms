package com.casaandina.cms.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "option")
public class Option implements Serializable {

	private static final long serialVersionUID = 4448968696735241980L;

	@Id
	private Integer id;
	private String name;
	private String description;
	private Integer level;
	private Integer idOptionParent;
	private String idPage;
	private Integer status;
	private Integer editionStatus;
	private String lastUpdate;
	private String username;
	private String owner;
	private Integer order;
	private List<LanguageOption> languages;

}
