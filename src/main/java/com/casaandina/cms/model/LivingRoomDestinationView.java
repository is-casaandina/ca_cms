package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Document(collection = "livingRoom_destination")
public class LivingRoomDestinationView {

    @Id
    private String id;
    private String name;
    private String squareMeters;
    private String lengthy;
    private String width;
    private String high;
    private Integer teather;
    private String school;
    private String banquet;
    private String directory;
    private String cocktail;
    private String hotelId;
    private String destinationId;
    private String destination;

}
