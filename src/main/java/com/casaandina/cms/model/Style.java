package com.casaandina.cms.model;

import java.util.List;

import lombok.Data;

@Data
public class Style {

	private String property;
	private String value;
	private List<Group> groups;

}
