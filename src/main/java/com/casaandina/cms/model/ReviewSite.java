package com.casaandina.cms.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "reviewSite")
public class ReviewSite {
	
	@Id
	private String id;
	
	private String name;
	
	private String mainUrl;
	
	private String slug;
	
	private String logoUrl;
	
	private Integer reviewSiteId;

}
