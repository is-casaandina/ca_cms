package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Document(collection = "quoteEvent")
public class QuoteEvent {

	@Id
	private String id;

	@DBRef
	private Page page;

	private Integer quantityAssistants;

	private LocalDate beginDate;

	private LocalDate endDate;

	private String beginHour;

	private String endHour;

	private List<String> armedTypes;

	private List<String> foodOptions;

	private List<String> audiovisualEquipments;

	private String 	additionalComments;

	@DBRef
	private Subscriber subscriber;

	@JsonIgnore
	private LocalDateTime createdDate;

	@JsonIgnore
	private LocalDateTime updatedDate;

	public QuoteEvent() {
		super();
		LocalDateTime currentDate = LocalDateTime.now();
		this.createdDate = currentDate;
		this.updatedDate = currentDate;
	}

}
