package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "pageError")
public class PageError {

    @Id
    private String id;
    private String pageId;
    private Integer errorCode;
    private Integer state;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private String owner;
    private String user;

}
