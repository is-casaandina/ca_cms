package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Builder
@Data
@Document(collection = "country")
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class Country {
	
	@Id
	private String id;
	
	private String name;

	private String code;
	
	private Boolean status;
	
	private Integer createdBy;
	
	@CreatedDate
	private LocalDateTime createdDate;
	
	private Integer updatedBy;
	private Boolean activePage;
	
	@LastModifiedDate
	private LocalDateTime updatedDate;

}
