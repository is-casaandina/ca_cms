package com.casaandina.cms.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document(collection = "menu")
public class Menu {
	
	@Id
	private String id;
	
	private String name;
	
	private String type;
	
	private String parentId;
	
	private String action;
	
	private Integer order;
	
	private String languageCode;
	
	@CreatedDate
	private LocalDateTime createdDate;
	
	@LastModifiedDate
	private LocalDateTime updatedDate;

	private String pageId;

	private String pagePath;

	private Boolean sectionFooter;

	private String phone;

	private String email;

}
