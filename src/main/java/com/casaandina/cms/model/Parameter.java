package com.casaandina.cms.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Document(collection = "parameter")
public class Parameter {
	
	@Id
	private String id;
	
	private String description;
	
	private String code;
	
	private String languageCode;
	
	private Object indicator;

}
