package com.casaandina.cms.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Script {

	private String tracing;

	private String header;

	private String body;
	
	private String footer;

}
