package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

import static com.casaandina.cms.util.ConstantsUtil.SPACE;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "user")
public class User {

	@Id
	private Integer id;

	private String username;

	@JsonIgnore
	private String password;

	private String name;

	private String lastname;

	private String email;

	private String roleCode;

	@JsonIgnore
	private Integer status;

	@JsonIgnore
	@CreatedDate
	private LocalDateTime createdDate;

	@JsonIgnore
	@LastModifiedDate
	private LocalDateTime updatedDate;

	public String getFullname() {
		return this.name + SPACE + this.lastname;
	}

}
