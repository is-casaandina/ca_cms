package com.casaandina.cms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DestinationMap {

    @Id
    private String id;

    private String name;
    private String countryId;
    private String className;
    private Integer state;
    private LocalDateTime createdDate;
}
