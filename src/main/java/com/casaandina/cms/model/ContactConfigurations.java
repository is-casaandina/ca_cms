package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactConfigurations {

    private String mailsEs;
    private String mailsEn;
    private String mailsPt;
    private String phoneEs;
    private String phoneEn;
    private String phonePt;
    private String formPage;
}
