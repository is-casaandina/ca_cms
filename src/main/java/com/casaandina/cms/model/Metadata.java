package com.casaandina.cms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Metadata {

	private String title;
	private String description;
	private String titleSnippet;
	private String ogType;
	private String ogUrl;
	private String ogTitle;
	private String ogDescription;
	private String ogImage;
	private String ogSitename;
	private String twitterCard;
	private String twitterTitle;
	private String twitterDescription;
	private String twitterImage;
	private String canonicalLink;
	private String keywords;

}
