package com.casaandina.cms.model;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "cluster")
public class Cluster {
	
	@Id
	private String id;
	
	private String name;
	
	private Boolean status;
	
	@DBRef
	private List<Country> countries;
	
	private Integer createdBy;
	
	private LocalDateTime createdDate;
	
	private Integer updatedBy;
	
	private LocalDateTime updatedDate;

}
