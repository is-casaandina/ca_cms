package com.casaandina.cms.dto;

import com.casaandina.cms.model.ImageS3;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class InfoRestaurantDto {

    private String title;
    private String subtitle;
    private String description;
    private String textLink;
    private List<ImageS3> backgroundImage;
    private String badge1;
    private String badge2;
    private String badge3;
    private String badge4;
    private List<ImageS3> imageMain;

}
