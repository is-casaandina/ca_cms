package com.casaandina.cms.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class FeaturedPromotion {

    private List<String> items;
    private List<String> destinations;
    private List<String> hotels;
    private List<String> descriptors;
    private String language;
    private String currency;
}
