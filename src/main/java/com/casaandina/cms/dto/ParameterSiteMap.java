package com.casaandina.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParameterSiteMap {

    private String beginIndexXml;
    private String endIndexXml;
    private String beginUrlSetXml;
    private String endUrlSetXml;
    private String bodyIndexXml;
    private String bodyUrlSetXml;
}
