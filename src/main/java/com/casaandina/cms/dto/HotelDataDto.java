package com.casaandina.cms.dto;

import com.casaandina.cms.model.HotelDestination;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class HotelDataDto {

    private HotelServiceDto caServices;
    private HotelSummaryDto caHotelSummary;
    private HotelDestination caHotelDestination;
}
