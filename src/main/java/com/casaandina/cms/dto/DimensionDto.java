package com.casaandina.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DimensionDto {

	private Integer width;
	private Integer heigth;

}
