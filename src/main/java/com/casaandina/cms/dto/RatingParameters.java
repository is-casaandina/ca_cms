package com.casaandina.cms.dto;

import java.util.List;

import lombok.Data;

@Data
public class RatingParameters {
	
	private List<RatingIndicators> indicator; 

}
