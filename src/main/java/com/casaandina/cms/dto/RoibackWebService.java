package com.casaandina.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoibackWebService {

    private String userService;
    private String methodName;
    private String urlService;
}
