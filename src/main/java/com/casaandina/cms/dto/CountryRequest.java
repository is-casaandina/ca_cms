package com.casaandina.cms.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryRequest {
	
	private String id;
	
	private String name;
	
	@Size(min=2, max=2)
	@NotNull(message = "Campo obligatorio")
	private String code;
	
	private Boolean status;
	
	private Integer createdBy;
	
	private LocalDateTime createdDate;
	
	private Integer updatedBy;
	
	private LocalDateTime updatedDate;

}
