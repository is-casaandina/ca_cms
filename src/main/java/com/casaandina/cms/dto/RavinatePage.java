package com.casaandina.cms.dto;

import lombok.Data;

@Data
public class RavinatePage {
	
	private Integer id;
	
	private String name;

}
