package com.casaandina.cms.dto;

import com.casaandina.cms.model.ImageS3;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class HotelSummaryDto {

    private String description;
    private String descriptionLivingRoom;
    private Boolean isUbication;
    private String mapUrl;
    private List<ImageS3> backgroundImageMap;
    private Boolean isTraslate;
    private String descriptionTraslate;
    private Object tables;
    private String restrictions;
    private Boolean isImageTotal;
    private List<ImageS3> imageMain;
    private String imageTotalUrl;
}
