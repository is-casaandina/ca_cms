package com.casaandina.cms.dto;

import java.util.List;

public class PermisionRoleTo {

	private String roleName;
	private String roleCode;
	private List<String> permissionCodeList;

	public PermisionRoleTo() {
		super();
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<String> getPermissionCodeList() {
		return permissionCodeList;
	}

	public void setPermissionCodeList(List<String> permissionCodeList) {
		this.permissionCodeList = permissionCodeList;
	}

	@Override
	public String toString() {
		return "PermisionRoleTo [roleName=" + roleName + ", roleCode=" + roleCode + ", permissionCodeList="
				+ permissionCodeList + "]";
	}

}
