package com.casaandina.cms.dto;

public class RoleUserTo {

	private Integer page;
	private Integer size;

	public RoleUserTo() {
		super();
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

}
