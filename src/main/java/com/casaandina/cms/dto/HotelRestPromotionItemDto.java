package com.casaandina.cms.dto;

import com.casaandina.cms.model.ImageS3;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class HotelRestPromotionItemDto {

    private String descriptor;
    private String country;
    private String destination;
    private String hotel;
    private String restaurant;
    private Double price;
    private Double discount;
    private Integer days;
    private Integer nights;
    private Boolean isRestriction;
    private String restriction;
    private String bodyRestriction;
    private String footerRestriction;
    private List<ImageS3> backgroundImage;
    private String description;
    private String type;
    private Boolean isSelected;
    private String urlReservation;
    private List<ImageS3> imageRestriction;

}