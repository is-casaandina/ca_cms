package com.casaandina.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.InputStream;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InputStreamParameter {

    private long length;
    private String contentType;
    private InputStream fis;
    private String fileName;

}
