package com.casaandina.cms.dto;

import java.util.ArrayList;
import java.util.List;

public class TemplateTo {

	private String id;
	private String code;
	private String name;
	private String description;
	private String imageUri;
	private String html;
	private List<String> refComponents = new ArrayList<>();

	public TemplateTo() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public List<String> getRefComponents() {
		return refComponents;
	}

	public void setRefComponents(List<String> refComponents) {
		this.refComponents = refComponents;
	}

	@Override
	public String toString() {
		return "TemplateTo [id=" + id + ", code=" + code + ", name=" + name + ", description=" + description
				+ ", imageUri=" + imageUri + ", html=" + html + ", refComponents="
				+ refComponents + "]";
	}

}
