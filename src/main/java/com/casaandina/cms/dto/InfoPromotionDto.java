package com.casaandina.cms.dto;

import com.casaandina.cms.model.ImageS3;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class InfoPromotionDto {

    private String descriptionLeft;
    private String descriptionRight;
    private Double price;
    private Double discount;
    private String title;
    private String description;
    private List<ImageS3> imagePromotion;
    private Boolean isRestriction;
    private List<ImageS3> imageRestriction;
    private String bodyRestriction;
    private String textButtonRestriction;
    private String footerRestriction;
    private String urlReservation;

}
