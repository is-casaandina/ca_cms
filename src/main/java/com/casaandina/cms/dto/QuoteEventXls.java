package com.casaandina.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuoteEventXls {

    private String page;
    private Integer quantityAssistants;
    private LocalDate beginDate;
    private LocalDate endDate;
    private String beginHour;
    private String endHour;
    private String armedTypes;
    private String foodOptions;
    private String audiovisualEquipments;
    private String 	additionalComments;

    private String name;
    private String email;
    private String phone;
    private String registerDate;
}
