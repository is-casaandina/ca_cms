package com.casaandina.cms.dto;

import java.util.List;

import lombok.Data;

@Data
public class RevinateRatingParameter {
	
	List<RevinateRatingIndicator> indicator;

}
