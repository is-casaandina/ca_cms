package com.casaandina.cms.dto;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = Include.NON_NULL)
public class SubscriberDto {

	private String id;
	private String name;
	private String email;
	private String phone;
	private String countryId;
	private String city;
	private String pageDestinationId;
	private Integer gender;
	private Integer motive;
	private String address;
	private String message;
	private Boolean sendNotification;
	private Boolean sendNew;

	private String surname;
	private String businessName;
	private String ruc;
	private Integer segment;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate birthdate;
}
