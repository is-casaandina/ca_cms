package com.casaandina.cms.dto;

import lombok.Data;

@Data
public class RatingIndicators {
	
	private Double minRating;
	
	private Double maxRating;
	
	private String ratingType;

}
