package com.casaandina.cms.dto;

import java.io.Serializable;
import java.util.List;

import com.casaandina.cms.model.LanguageOption;

public class OptionTo implements Serializable {

	private static final long serialVersionUID = -9064070673161859137L;
	private Integer id;
	private String name;
	private String description;
	private Integer level;
	private Integer idOptionParent;
	private String idPage;
	private Integer status;
	private Integer editionStatus;
	private String lastUpdate;
	private String username;
	private Integer order;
	private List<LanguageOption> languages;

	public OptionTo() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getIdOptionParent() {
		return idOptionParent;
	}

	public void setIdOptionParent(Integer idOptionParent) {
		this.idOptionParent = idOptionParent;
	}

	public String getIdPage() {
		return idPage;
	}

	public void setIdPage(String idPage) {
		this.idPage = idPage;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getEditionStatus() {
		return editionStatus;
	}

	public void setEditionStatus(Integer editionStatus) {
		this.editionStatus = editionStatus;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public List<LanguageOption> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageOption> languages) {
		this.languages = languages;
	}

}
