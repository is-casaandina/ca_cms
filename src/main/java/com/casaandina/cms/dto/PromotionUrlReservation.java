package com.casaandina.cms.dto;

import com.casaandina.cms.model.ImageS3;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PromotionUrlReservation {

    private String idPromotion;
    private String idHotelRest;
    private String urlReservation;
    private String urlPromotion;
    private String domain;
    private Double price;
    private Boolean isRestriction;
    private ImageS3 imageRestriction;
    private String bodyRestriction;
    private String footerRestriction;
    private Double discount;
    private ImageS3 backgroundImage;
    private Integer days;
    private Integer nights;
}
