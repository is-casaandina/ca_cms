package com.casaandina.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResizeImageProperties {

    private DimensionResizeImage small;
    private DimensionResizeImage medium;
    private DimensionResizeImage extraSmall;
}
