package com.casaandina.cms.dto;

public class JwtAuthenticationTo {
	
	private String accessToken;
    private String tokenType = "Bearer";

    public JwtAuthenticationTo(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

}
