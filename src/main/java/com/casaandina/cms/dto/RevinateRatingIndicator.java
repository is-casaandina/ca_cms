package com.casaandina.cms.dto;

import lombok.Data;

@Data
public class RevinateRatingIndicator {
	
	private String url;
	
	private String endpoint;
	
	private String defaultMessage;
	
	private String customMessage;
	
	private Integer ranking;
	
	private String languageCode;

}
