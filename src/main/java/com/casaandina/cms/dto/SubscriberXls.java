package com.casaandina.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubscriberXls {

    private String name;
    private String surname;
    private String businessName;
    private String email;
    private String ruc;
    private String phone;
    private String country;
    private String city;
    private String destination;
    private String gender;
    private String motive;
    private String address;
    private String message;
    private LocalDate birthdate;
    private String registerDate;
    private String segment;
}
