package com.casaandina.cms.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class IconJsonProperties {
	
	private Integer order;
    private Integer id;
    private String name;
    private Integer prevSize;
    private Integer code;
}
