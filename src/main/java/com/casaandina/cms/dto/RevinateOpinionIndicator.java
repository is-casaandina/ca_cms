package com.casaandina.cms.dto;

import lombok.Data;

@Data
public class RevinateOpinionIndicator {
	
	private String url;
	
	private String endpoint;
	
	private Integer size;
	
	private Integer minRating;
	
	private Integer maxRating;
	
	private String sort;
	
	private String order;
	
	private String languageCode;

}
