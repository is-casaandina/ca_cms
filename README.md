Desplegar a EC2
===================
* * * * *
### Paso 1: Crear instancia EC2

Utilizar la "Amazon Linux 2 AMI (HVM), SSD Volume Type" - 64 bit.

### Tipo de instancia

t2.micro, Review and launch, launch

### KeyPair

Crear y descargar una nueva keypair (keypair.pem)

### Conectar

Dentro del dashboard de EC2, seleccionar instancias y en la nueva
instancia creada seleccionar conectar.

Copiar el ejemplo y pegar en una terminal. Ejemplo:
> ssh -i "keypair.pem" ec2-user@ec2-3-82-213-255.compute-1.amazonaws.com

Ya conectado entrar como root.

> sudo -i

* * * * *

### Paso 2: Instalar docker
 Si ya entró como root omitir sudo
> sudo yum update -y
> sudo yum install -y docker
> sudo service docker start

---> Probar docker (docker info ó docker ps -a)
> docker info

---> listar contenedores
> docker ps -a

---> listar Imagenes
> docker images

### Paso 3: Instalar git
> sudo yum install git

---> Clonar respositorio y bajarse los cambios de git

### Paso 4 Generar imagen docker y desplegar en EC2
> docker build -t (Nombre_Imagen) .

En caso haya mas de un archivo (Dockerfile, Dockerfile2)

> docker build -t (Nombre_Imagen) -f Dockerfile2 .

Si el Ec2 se queda sin espacio ir al paso 5 y luego volver a ejecutar el paso 4

Levantar contenedor
> docker run -d -it -p 8080:8093 (Nombre_Imagen)

### Paso 5
En caso el Ec2 se quede sin espacio, eliminar contenedores e imagenes
> docker ps -a (listar contenedores)
> docker rm -f idContenedor|nombreContenedor (Eliminar contenedor)
> docker images (listar imagenes)
> docker rmi -f idImagen|nombreImagen (Eliminar imagenes)
